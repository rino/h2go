/*
	Comparison functions.
*/
package h2go

import (
	"math/big"
)

type Equible interface {
	Equals(other interface{}) bool
}

// Cmp should return -1 if this < other, 1 if this > other, 0 if this == other,
// and 2 if there is an error.
type Comparable interface {
	Cmp(other Comparable) int
	String() string
}

type Comparables []Comparable

func (c Comparables) Delete(i int) {
	if i >= 0 && i < len(c) {
		copy(c[i:], c[i+1:])
		c[len(c)-1] = nil
		c = c[:len(c)-1]
	}
}

// Compare float64 values.
func CmpFloat64(this, other float64) int {
	if this == other {
		return COMPARE_EQ
	} else if other < this {
		return COMPARE_LT
	}
	return COMPARE_GT
}

// Compare int values.
func CmpInt(this, other int) int {
	if this == other {
		return COMPARE_EQ
	} else if other < this {
		return COMPARE_LT
	}
	return COMPARE_GT
}

// Compare uint values.
func CmpUint(this, other uint) int {
	if this == other {
		return COMPARE_EQ
	} else if other < this {
		return COMPARE_LT
	}
	return COMPARE_GT
}

// Compare string values.
func CmpString(this, other string) int {
	if this == other {
		return COMPARE_EQ
	} else if other < this {
		return COMPARE_LT
	}
	return COMPARE_GT
}

func Compare(i1, i2 interface{}) int {
	switch v1 := i1.(type) {
	case Comparable:
		if v2, ok := i2.(Comparable); ok {
			return v1.Cmp(v2)
		}
	case int8:
		if v2, ok := i2.(int8); ok {
			return CmpInt(int(v1), int(v2))
		}
	case int16:
		if v2, ok := i2.(int16); ok {
			return CmpInt(int(v1), int(v2))
		}
	case int32:
		if v2, ok := i2.(int32); ok {
			return CmpInt(int(v1), int(v2))
		}
	case int64:
		if v2, ok := i2.(int64); ok {
			return CmpInt(int(v1), int(v2))
		}
	case int:
		if v2, ok := i2.(int); ok {
			return CmpInt(v1, v2)
		}
	case uint8:
		if v2, ok := i2.(uint8); ok {
			return CmpUint(uint(v1), uint(v2))
		}
	case uint16:
		if v2, ok := i2.(uint16); ok {
			return CmpUint(uint(v1), uint(v2))
		}
	case uint32:
		if v2, ok := i2.(uint32); ok {
			return CmpUint(uint(v1), uint(v2))
		}
	case uint64:
		if v2, ok := i2.(uint64); ok {
			return CmpUint(uint(v1), uint(v2))
		}
	case uint:
		if v2, ok := i2.(uint); ok {
			return CmpUint(v1, v2)
		}
	case float32:
		if v2, ok := i2.(float32); ok {
			return CmpFloat64(float64(v1), float64(v2))
		}
	case float64:
		if v2, ok := i2.(float64); ok {
			return CmpFloat64(v1, v2)
		}
	case *big.Int:
		if v2, ok := i2.(*big.Int); ok {
			return v1.Cmp(v2)
		}
	case *big.Rat:
		if v2, ok := i2.(*big.Rat); ok {
			return v1.Cmp(v2)
		}
	case string:
		if v2, ok := i2.(string); ok {
			return CmpString(v1, v2)
		}
	}
	Env.Log().IsError(Env.Goofs().MakeGoof("BAD_ARGUMENTS",
		"While comparing %v (type %T) and %v (type %T), the types "+
			"must be recognised and the same",
		i1, i1, i2, i2))
	return COMPARE_ERR
}
