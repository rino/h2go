/*
	Exit gracefully.
*/
package h2go

import (
	"fmt"
	"os"
)

// Exit status = 0 for good, 1 for bad.
func Exit(status int) {
	os.Exit(status)
}

func ExitOnError(err error) {
	if IsError(err) {
		if Env != nil {
			goof, isa := err.(*Goof)
			lines := []string{}
			if isa {
				fmt.Printf("FATAL ERROR: %s\n", goof.Msg)
				lines = goof.CallerLines()
			} else {
				fmt.Printf("FATAL ERROR: %v\n", err)
				fmt.Printf("Trace (newest first):\n")
				lines = TraceToHere(
					int(Env.Config().TRACE_BACK),
					int(Env.Config().TRACE_PATHDEPTH))
			}
			for _, line := range lines {
				fmt.Printf(" %s\n", line)
			}
		}
		Exit(1)
	}
	return
}

func Fatal(msg string, a ...interface{}) {
	fmt.Printf("FATAL: "+msg+"\n", a...)
	Exit(1)
}
