/*
	This first test code run, sets globals for other tests.
*/
package h2go

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"
)

var testHome string
var testDir string

const TEST_DEBUG_LEVEL string = "basic"
const DEBUGLEVEL_DUMP = DEBUGLEVEL_FINE
const MAX_HEX_DUMP int = 1000

func init() {
	testHome, _ = os.Getwd()
	testDir = filepath.Join(testHome, "test")
	Boot("test", testDir, ScreenLogger.SetLogLevel(TEST_DEBUG_LEVEL))
	cfg := Env.Config()
	cfg.GOOF_JUMPS = 6
	Env.SetConfig(cfg)
	err := os.RemoveAll(testDir)
	ExitOnError(err)
	err = os.Mkdir(testDir, 0700)
	ExitOnError(err)
	// Dump types
	lines, goof := Env.Types().Dump()
	ExitOnError(goof)
	Env.Log().Dump(lines, "h2go User Types Dump")

	Env.SetTestingOn()
}

func TestHexDump(t *testing.T) {
	var b []byte
	for i := 0; i < 50; i++ {
		b = append(b, byte(i))
	}
	Env.Log().HexDump(b, 100, "Hex Dump Test")
}

// SUPPORT ====================================================================

type TestBox struct {
	val int
}

func (this TestBox) Cmp(other Comparable) int {
	other2, _ := other.(TestBox)
	if this.val == other2.val {
		return COMPARE_EQ
	} else if other2.val < this.val {
		return COMPARE_LT
	} else {
		return COMPARE_GT
	}
}

func (b TestBox) String() string {
	return fmt.Sprintf("%d", b.val)
}
