package h2go

import (
	"testing"
)

func TestLinkListInsert(t *testing.T) {
	input := []TestBox{
		TestBox{13},
		TestBox{5},
		TestBox{10},
		TestBox{1},
	}

	output := []TestBox{
		TestBox{1},
		TestBox{5},
		TestBox{10},
		TestBox{13},
	}

	result := NewLinkList()
	iter := result.Iterator()
	inserted := false
	for _, in := range input {
		inserted = iter.InsertSort(in)
		//iter.Insert(in)
		if inserted {
			Env.Log().Basic("InsertSorted %v", in)
		}
		iter.Inc()
		Env.Log().Basic("Result = (%v)", result)
	}

	expected := NewLinkList()
	iter2 := expected.Iterator()
	for _, out := range output {
		iter2.Insert(out)
		iter2.Inc()
	}

	Env.Log().Basic("Expected = (%v)", expected)

	if !result.Equals(expected) {
		t.Fatalf("Expected (%v), got (%v)", expected, result)
	}
}

func TestLinkListCopy(t *testing.T) {
	input := []TestBox{
		TestBox{1},
		TestBox{5},
		TestBox{10},
		TestBox{13},
	}

	list := NewLinkList()
	iter := list.Iterator()
	for _, in := range input {
		iter.Insert(in)
		iter.Inc()
	}

	list2 := list.Copy()
	if !list2.Equals(list) {
		t.Fatalf("Expected (%v), got (%v)", list, list2)
	}
}

func TestLinkListDelete(t *testing.T) {
	input := []TestBox{
		TestBox{1},
		TestBox{5},
		TestBox{10},
		TestBox{13},
	}

	result := NewLinkList()
	iter0 := result.Iterator()
	for _, in := range input {
		iter0.InsertSort(in)
		iter0.Inc()
	}
	result0 := result.Copy()

	// Delete 5, delete body link from original
	output := []TestBox{
		TestBox{1},
		TestBox{10},
		TestBox{13},
	}

	iter0.First()
	iter0.Inc()
	iter0.Delete()

	expected := NewLinkList()
	iter := expected.Iterator()
	for _, out := range output {
		iter.InsertSort(out)
		iter.Inc()
	}

	if !result.Equals(expected) {
		t.Fatalf("Expected (%v), got (%v)", expected, result)
	}

	// Delete 1, delete head link from original
	output = []TestBox{
		TestBox{5},
		TestBox{10},
		TestBox{13},
	}

	result = result0.Copy()
	iter0 = result.Iterator()
	iter0.First()
	iter0.Delete()

	expected = NewLinkList()
	iter = expected.Iterator()
	for _, out := range output {
		iter.InsertSort(out)
		iter.Inc()
	}

	if !result.Equals(expected) {
		t.Fatalf("Expected (%v), got (%v)", expected, result)
	}

	// Delete 13, delete tail link from original
	output = []TestBox{
		TestBox{1},
		TestBox{5},
		TestBox{10},
	}

	result = result0.Copy()
	iter0 = result.Iterator()
	iter0.Last()
	iter0.Delete()

	expected = NewLinkList()
	iter = expected.Iterator()
	for _, out := range output {
		iter.InsertSort(out)
		iter.Inc()
	}

	if !result.Equals(expected) {
		t.Fatalf("Expected (%v), got (%v)", expected, result)
	}
}
