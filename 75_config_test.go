package h2go

import (
	"io/ioutil"
	"path/filepath"
	"reflect"
	"testing"
)

func TestFieldBoxParamsConfig(t *testing.T) {
	cfg := DEFAULT_FIELDBOX_PARAMS_CONFIG.Copy()
	ver, goof := MakeVersion(7, 6, 5)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem constructing %T", ver)
	}
	switches, goof := MakeSwitchesFromStruct(cfg)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making %T from a %T", switches, cfg)
	}
}

type TEST_STRUCT1 struct {
	A uint16
	B string
	C bool
	D int32
	*TEST_STRUCT2
}

type TEST_STRUCT2 struct {
	E int64
	F string
	G bool
}

func (ts *TEST_STRUCT1) Validate() *Goof {
	return NO_GOOF
}

func TestConfigFile(t *testing.T) {
	cfgtxt :=
		`
TEST_STRUCT1 = {
	A: (3,uint16),
	B: "hi",
	C: true,
	D: (-45,int32),
	TEST_STRUCT2: {
		E: 34,
		F: "bye",
		G: true,
	},
};
`
	cfgpath := filepath.Join("test", "test.h2go")
	err := ioutil.WriteFile(
		cfgpath,
		[]byte(cfgtxt),
		DEFAULT_FILEMODE,
	)
	if Env.Log().IsError(err) {
		t.Fatalf("Problem writing test config to file")
	}
	config := &TEST_STRUCT1{TEST_STRUCT2: &TEST_STRUCT2{}}
	allowedRoot, err := filepath.Abs("test")
	if Env.Log().IsError(err) {
		t.Fatalf("Problem setting allowed root filepath")
	}
	cfiler, goof := MakeConfigFiler(
		allowedRoot,
		map[string]interface{}{
			"TEST_STRUCT1": config,
		},
	)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making %T", cfiler)
	}
	fileFound, _, _, goof := cfiler.Load(cfgpath)
	Env.Log().Dump(cfiler.ToStringSlice(""), "Loaded config")
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem loading config file at %q", cfgpath)
	}
	if !fileFound {
		t.Fatalf("Could not find config file at %q", cfgpath)
	}
	expected := TEST_STRUCT1{
		A: 3,
		B: "hi",
		C: true,
		D: -45,
		TEST_STRUCT2: &TEST_STRUCT2{
			E: 34,
			F: "bye",
			G: true,
		},
	}
	v1 := reflect.Indirect(reflect.ValueOf(config))
	v2 := reflect.Indirect(reflect.ValueOf(expected))
	if !reflect.DeepEqual(v1.Interface(), v2.Interface()) {
		t.Fatalf("Expected %#v (%#v), got %#v (%#v)",
			expected, expected.TEST_STRUCT2,
			config, config.TEST_STRUCT2)
	}
}
