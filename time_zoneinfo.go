// Copyright 2011 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//
// Hacked from official time package.
// TODO rewrite, this code sucks.

package h2go

import (
	"sync"
	"syscall"
)

// A Location maps time instants to the zone in use at that time.
// Typically, the Location represents the collection of time offsets
// in use in a geographical area, such as CEST and CET for central Europe.
type Location struct {
	name string
	zone []zone
	tx   []zoneTrans

	// Most lookups will be for the current time.
	// To avoid the binary search through tx, keep a
	// static one-element cache that gives the correct
	// zone for the time when the Location was created.
	// if cacheStart <= t <= cacheEnd,
	// lookup can return cacheZone.
	// The units for cacheStart and cacheEnd are seconds
	// since January 1, 1970 UTC, to match the argument
	// to lookup.
	cacheStart int64
	cacheEnd   int64
	cacheZone  *zone
	*State
}

func NewLocation() *Location {
	return &Location{
		zone:      []zone{},
		tx:        []zoneTrans{},
		cacheZone: &zone{},
		State:     NewState(),
	}
}

func MakeLocation(name string) *Location {
	loc := NewLocation()
	loc.name = name
	return loc
}

// Getters
func (loc *Location) Name() string { return loc.name }

// A zone represents a single time zone such as CEST or CET.
type zone struct {
	name   string // abbreviated name, "CET"
	offset int    // seconds east of UTC
	isDST  bool   // is this zone Daylight Savings Time?
}

// A zoneTrans represents a single time zone transition.
type zoneTrans struct {
	when         int64 // transition time, in seconds since 1970 GMT
	index        uint8 // the index of the zone that goes into effect at that time
	isstd, isutc bool  // ignored - no idea what these mean
}

// UTC represents Universal Coordinated Time (UTC).
var UTC *Location = MakeLocation("UTC")

// utcLoc is separate so that get can refer to &utcLoc
// and ensure that it never returns a nil *Location,
// even if a badly behaved client has changed UTC.
var utcLoc = *UTC

// Local represents the system's local time zone.
var Local *Location = NewLocation()

// localLoc is separate so that initLocal can initialize
// it even if a client has changed Local.
var localLoc = *Local
var localOnce sync.Once

func (l *Location) get() *Location {
	if l == nil {
		return &utcLoc
	}
	if l == &localLoc {
		localOnce.Do(initLocal)
	}
	return l
}

// String returns a descriptive name for the time zone information,
// corresponding to the argument to LoadLocation.
func (loc *Location) String() string {
	return loc.get().name
}

// FixedZone returns a Location that always uses
// the given zone name and offset (seconds east of UTC).
func FixedZone(name string, offset int) *Location {
	loc := NewLocation()
	loc.name = name
	loc.NotEmpty()
	loc.zone = []zone{{name, offset, false}}
	loc.tx = []zoneTrans{{-1 << 63, 0, false, false}}
	loc.cacheStart = -1 << 63
	loc.cacheEnd = 1<<63 - 1
	loc.cacheZone = &loc.zone[0]
	loc.NowComplete()
	return loc
}

// lookup returns information about the time zone in use at an
// instant in time expressed as seconds since January 1, 1970 00:00:00 UTC.
//
// The returned information gives the name of the zone (such as "CET"),
// the start and end times bracketing sec when that zone is in effect,
// the offset in seconds east of UTC (such as -5*60*60), and whether
// the daylight savings is being observed at that time.
func (loc *Location) lookup(sec int64) (name string, offset int, isDST bool, start, end int64) {
	loc = loc.get()

	if len(loc.tx) == 0 {
		name = "UTC"
		offset = 0
		isDST = false
		start = -1 << 63
		end = 1<<63 - 1
		return
	}

	if zone := loc.cacheZone; zone != nil &&
		loc.cacheStart <= sec &&
		sec < loc.cacheEnd {
		name = zone.name
		offset = zone.offset
		isDST = zone.isDST
		start = loc.cacheStart
		end = loc.cacheEnd
		return
	}

	// Binary search for entry with largest time <= sec.
	// Not using sort.Search to avoid dependencies.
	tx := loc.tx
	end = 1<<63 - 1
	lo := 0
	hi := len(tx)
	for hi-lo > 1 {
		m := lo + (hi-lo)/2
		lim := tx[m].when
		if sec < lim {
			end = lim
			hi = m
		} else {
			lo = m
		}
	}
	zone := &loc.zone[tx[lo].index]
	name = zone.name
	offset = zone.offset
	isDST = zone.isDST
	start = tx[lo].when
	// end = maintained during the search
	return
}

// lookupName returns information about the time zone with
// the given name (such as "EST") at the given pseudo-Unix time
// (what the given time of day would be in UTC).
func (loc *Location) lookupName(name string, unix int64) (offset int, isDST bool, ok bool) {
	loc = loc.get()

	// First try for a zone with the right name that was actually
	// in effect at the given time. (In Sydney, Australia, both standard
	// and daylight-savings time are abbreviated "EST". Using the
	// offset helps us pick the right one for the given time.
	// It's not perfect: during the backward transition we might pick
	// either one.)
	for i := range loc.zone {
		zone := &loc.zone[i]
		if zone.name == name {
			nam, offset, isDST, _, _ := loc.lookup(unix - int64(zone.offset))
			if nam == zone.name {
				return offset, isDST, true
			}
		}
	}

	// Otherwise fall back to an ordinary name match.
	for i := range loc.zone {
		zone := &loc.zone[i]
		if zone.name == name {
			return zone.offset, zone.isDST, true
		}
	}

	// Otherwise, give up.
	return
}

// lookupOffset returns information about the time zone with
// the given offset (such as -5*60*60).
func (loc *Location) lookupOffset(offset int) (name string, isDST bool, ok bool) {
	loc = loc.get()
	for i := range loc.zone {
		zone := &loc.zone[i]
		if zone.offset == offset {
			return zone.name, zone.isDST, true
		}
	}
	return
}

// NOTE(rsc): Eventually we will need to accept the POSIX TZ environment
// syntax too, but I don't feel like implementing it today.

var zoneinfo, _ = syscall.Getenv("ZONEINFO")

// LoadLocation returns the Location with the given name.
//
// If the name is "" or "UTC", LoadLocation returns UTC.
// If the name is "Local", LoadLocation returns Local.
//
// Otherwise, the name is taken to be a location name corresponding to a file
// in the IANA Time Zone database, such as "America/New_York".
//
// The time zone database needed by LoadLocation may not be
// present on all systems, especially non-Unix systems.
// LoadLocation looks in the directory or uncompressed zip file
// named by the ZONEINFO environment variable, if any, then looks in
// known installation locations on Unix systems,
// and finally looks in $GOROOT/lib/time/zoneinfo.zip.
func LoadLocation(name string) (*Location, *Goof) {
	if name == "" || name == "UTC" {
		return UTC, NO_GOOF
	}
	if name == "Local" {
		return Local, NO_GOOF
	}
	if zoneinfo != "" {
		if z, goof := loadZoneFile(zoneinfo, name); !IsError(goof) {
			z.name = name
			return z, NO_GOOF
		}
	}
	return loadLocation(name)
}
