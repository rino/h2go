/*
	Containers for byte encodable typed values.
*/
package h2go

import (
	"fmt"
	"strings"
)

// FIELDLIST ===================================================================

type FieldList struct {
	in    []*Field
	types *TypeMap
	*State
}

var _ FieldCollection = NewFieldList()
var _ Codified = NewFieldList()
var _ Stateful = NewFieldList() // embedded State

func NewFieldList() *FieldList {
	return &FieldList{
		in:    []*Field{},
		types: NewTypeMap(),
		State: NewState(),
	}
}

func (types *TypeMap) MintFieldList() *FieldList {
	flist := NewFieldList()
	flist.types = types
	// Deliberately regarded as still empty
	return flist
}

func (flist *FieldList) ToSlice() []*Field { return flist.in }
func (flist *FieldList) Len() int          { return len(flist.in) }
func (flist *FieldList) Types() *TypeMap   { return flist.types }

func (flist FieldList) Code() CODE {
	return FIELDLIST_TYPE_CODE
}

// Override State.IsEmpty
func (flist *FieldList) IsEmpty() bool { return len(flist.in) == 0 }

func (types *TypeMap) BuildFieldList(list ...interface{}) *FieldList {
	flist := types.MintFieldList()
	for _, v := range list {
		f := types.AutoField(v)
		flist = flist.Add(f)
	}
	return flist
}

func (types *TypeMap) BuildFieldListFromStrings(list []string) *FieldList {
	flist := types.MintFieldList()
	for _, v := range list {
		f := types.BuildField(v, "STRING")
		flist = flist.Add(f)
	}
	return flist
}

func (flist *FieldList) Get(i int) *Field {
	flist.RLock()
	defer flist.RUnlock()
	if i < 0 || i >= flist.Len() {
		return NewField()
	}
	return flist.in[i]
}

// Modifies FieldList in place, must be a Field else nothing is
// added.
func (flist *FieldList) Add(other interface{}) *FieldList {
	flist.Lock()
	defer flist.Unlock()
	f2 := NewField()
	switch v := other.(type) {
	case Field:
		f2 = &v
	case *Field:
		f2 = v
	default:
		return flist // do nothing
	}
	flist.in = append(flist.in, f2)
	return flist
}

func (flist *FieldList) AutoAdd(value interface{}) *FieldList {
	v := flist.Types().AutoField(value)
	return flist.Add(v)
}

func (flist *FieldList) Copy() *FieldList {
	flist.Lock()
	defer flist.Unlock()
	newlist := []*Field{}
	for _, item := range flist.in {
		newlist = append(newlist, item)
	}
	flist2 := flist.types.MintFieldList()
	flist2.in = newlist
	flist2.State = flist.State.Copy()
	return flist2
}

// Argument should be a Field or *Field.
func (flist *FieldList) Contains(other interface{}) bool {
	if flist.Len() == 0 {
		return false
	}
	for _, f := range flist.ToSlice() {
		if f.Equals(other) {
			return true
		}
	}
	return false
}

func (flist *FieldList) AddUnique(other interface{}) *FieldList {
	if !flist.Contains(other) {
		flist.Add(other)
	}
	return flist
}

func (flist *FieldList) Reverse() *FieldList {
	newList := flist.Types().MintFieldList()
	for i := flist.Len() - 1; i >= 0; i-- {
		newList = newList.Add(flist.Get(i))
	}
	return newList
}

// Order important in FieldList comparison
func (flist *FieldList) Equals(other interface{}) bool {
	flist2 := NewFieldList()
	switch v := other.(type) {
	case FieldList:
		flist2 = &v
	case *FieldList:
		flist2 = v
	default:
		return false
	}
	flist.RLock()
	flist2.RLock()
	defer flist.RUnlock()
	defer flist2.RUnlock()
	if flist.Len() != flist2.Len() {
		return false
	}
	if flist.Len() == 0 {
		return true
	}
	for i, field := range flist.ToSlice() {
		if !flist2.Get(i).Equals(field) {
			return false
		}
	}
	return true
}

// Insertion with implicit indexing and minimal copying.
// n = list length
// flist.Insert(f, 0) // inserts f at position 0
// flist.Insert(f, 1) // inserts f at position 1
// flist.Insert(f, n-1) // inserts f at position n-1
// flist.Insert(f, n) // appends f at end of list (explicit)
// flist.Insert(f, -1) // same as Insert(f, n-1)
// flist.Insert(f, -2) // same as Insert(f, n-2)
// flist.Insert(f, -n) // same as Insert(f, 0)
func (flist *FieldList) Insert(field *Field, i int) *Goof {
	flist.Lock()
	defer flist.Unlock()
	n := flist.Len()
	if i < 0 {
		i += n
	}
	if i > n {
		return Env.Goofs().MakeGoof("BAD_INDEX",
			"Attempt to index element %d of FieldList of length %d",
			i, n)
	}
	if n == 0 || i == n {
		flist.in = append(flist.in, field)
		return NO_GOOF
	}
	halfway := n / 2
	if i < halfway {
		flist.in = append([]*Field{NewField()}, flist.in...)
		if i != 0 {
			copy(flist.in[:i], flist.in[1:i+1])
		}
	} else { // i >= halfway
		flist.in = append(flist.in, nil) // keep nil for speed
		copy(flist.in[i+1:], flist.in[i:])
	}
	flist.in[i] = field
	return NO_GOOF
}

// Deletion with implicit indexing.
// flist.Delete(0) // deletes flist at position 0
// flist.Delete(1) // deletes flist at position 1
// flist.Delete(n-1) // deletes flist at position n-1
// flist.Delete(-1) // same as Delete(n)
// flist.Delete(-2) // same as Delete(n-1)
// flist.Delete(-(n+1)) // same as Delete(0)
func (flist *FieldList) Delete(i int) *Goof {
	flist.Lock()
	defer flist.Unlock()
	n := flist.Len()
	if i < 0 {
		i += n
	}
	if i > n {
		return Env.Goofs().MakeGoof("BAD_INDEX",
			"Attempt to index element %d of FieldList of length %d",
			i, n)
	}
	if n <= 1 && i == 0 {
		flist.in = []*Field{}
		return NO_GOOF
	}
	flist.in = append(flist.in[:i], flist.in[i+1:]...)
	return NO_GOOF
}

// These methods implement the BinaryEncodable interface.
// Encode a FieldList to binary, simply concatenated Fields.
func (flist FieldList) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(flist)
	flist.RLock()
	defer flist.RUnlock()
	byts, goof := bfr.Write(FIELDLIST_TYPE_CODE, box)
	out.AppendEncodedAs("flist type code", byts)
	if IsError(goof) {
		return out, goof
	}
	out2, goof := MakeUINT(flist.Len()).EncodeBinary(bfr, box)
	out2.(*BinaryRepresentation).
		RelabelLastBinaryEncoding(0, "flist size (UINT)")
	out.AppendEncoded(out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}
	for _, v := range flist.ToSlice() {
		out2, goof = v.EncodeBinary(bfr, box)
		out.AppendEncoded(out2.ChooseEncoding(0))
		if IsError(goof) {
			return out, goof
		}
	}
	return out, NO_GOOF
}

// Decode a FieldList from binary. Assumes type code already read.
// The only trick is that we have to manually snip off the code for each
// field.
func (flist *FieldList) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(flist)
	out2, goof := NewUINT().DecodeBinary(bfr, box)
	out2.(*BinaryRepresentation).
		RelabelLastBinaryEncoding(0, "flist size (UINT)")
	out.AppendEncoded(out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}
	ui, _ := out2.GetDecoded().Value().(UINT)
	i64, goof := ui.AsInt64()
	if IsError(goof) {
		return out, goof
	}
	size := int(i64)
	if size == 0 {
		return out, NO_GOOF
	}
	typ := NewProtocol()
	code := CODE(0)
	byts := []byte{}
	for i := 0; i < size; i++ {
		code, byts, goof = bfr.ReadCode(box)
		out.AppendEncodedAs("flist type code", byts)
		if IsError(goof) {
			if goof.EOF {
				break
			}
			return out, goof
		}
		// value
		typ, goof = flist.Types().ProtocolByCode(code)
		if IsError(goof) {
			return out, goof
		}
		out2, goof = typ.BinaryRx(bfr, box)
		if IsError(goof) {
			if goof.EOF {
				break
			}
			return out, goof
		}
		out.AppendEncoded(out2.ChooseEncoding(0))
		// entry
		flist.Add(out2.GetDecoded())
	}
	return out, NO_GOOF
}

// Generate FieldList binary encoder.
func FieldListBinaryEncoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		inside, goof := box.RequireOfKnownType(
			BOX_ITEM_ENCODE,
			BOX_ITEM_FIELDLIST)
		if IsError(goof) {
			return NewBinaryRepresentation(), goof
		}
		return inside.(*FieldList).EncodeBinary(bfr, box)
	}
}

// Generate FieldList binary decoder.
func FieldListBinaryDecoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		return Env.Types().MintFieldList().DecodeBinary(bfr, box)
	}
}

func (flist *FieldList) MultilineString(firstprefix, prefix string) []string {
	if flist == nil {
		return []string{"<nil>"}
	}
	result := []string{firstprefix + "["}
	line := ""
	for _, v := range flist.ToSlice() {
		line = prefix + "\t"
		switch underlying := v.Value().(type) {
		case MultilineStringable:
			lines := underlying.MultilineString(line, prefix+"\t")
			for _, line = range lines {
				result = append(result, line)
			}
			result[len(result)-1] += ","
		default:
			result = append(result, line+v.StringUsing(flist.Types())+",")
		}
	}
	result = append(result, prefix+"]")
	return result
}

func (flist FieldList) String() string {
	if &flist == nil {
		return "<nil>"
	}
	defer Env.Log().StackTrace()
	result := "["
	first := true
	for _, v := range flist.ToSlice() {
		if !first {
			result += ", "
		} else {
			first = false
		}
		result += fmt.Sprintf("%s", v.StringUsing(flist.Types()))
	}
	return result + "]"
}

func (flist *FieldList) ToStringSlice() []string {
	if flist == nil {
		return []string{"<nil>"}
	}
	defer Env.Log().StackTrace()
	result := []string{}
	for _, v := range flist.ToSlice() {
		result = append(result, fmt.Sprintf("%s", v.StringUsing(flist.Types())))
	}
	return result
}

func (flist *FieldList) FromStringSlice(slice []string) *FieldList {
	typ := flist.Types().Number["STRING"]
	for _, s := range slice {
		flist.Add(MakeField(s, typ))
	}
	return flist
}

func (flist *FieldList) EncodeString(box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AppendEncoded(flist.String())
	return out, NO_GOOF
}
func (flist *FieldList) DecodeString(txt string, box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AutoSetField(flist)
	inside, goof := box.Require(BOX_ITEM_WORKSPACE_ACTION)
	if IsError(goof) {
		return out, goof
	}
	wsa := inside.(*WorkspaceAction)
	if string(txt[len(txt)-1]) == "]" {
		txt = TrimText(txt[1 : len(txt)-1])
		if len(txt) > 0 {
			_, fliststr := ParseList(txt)
			vf := NewField()
			for _, vstr := range fliststr {
				vstr = TrimText(vstr)
				ngoofs := wsa.NumGoofs()
				vf = wsa.ProcessText(vstr, wsa.SetField, wsa.DelField)
				if wsa.NumGoofs() > ngoofs {
					return out, wsa.LastGoof()
				}
				if vf.IsEmpty() {
					vf = flist.Types().AutoField("")
				}
				flist = flist.Add(vf)
				out.AppendEncoded(vstr)
			}
		}
		if wsa.SetField.IsEmpty() {
			// Getter
			return out, NO_GOOF
		}
		return out, Env.Goofs().MakeGoof("INVALID_KEY",
			"%T is not a valid key type", flist)
	} else {
		return out, Env.Goofs().MakeGoof("SYNTAX",
			"Missing closing bracket ] while processing text %s", txt)
	}
}

// Generate FieldList string decoder for Env.Types map.
func FieldListStringDecoder() StringTask {
	return func(txt string, box *Box) (Representation, *Goof) {
		return Env.Types().MintFieldList().DecodeString(txt, box)
	}
}

// Returns true if all the field values have the given types. typnames
// are names of types (capitals) or can also be tags using the prefix
// "tag:". Each type with the given tag string will be checked.  The
// typname will be ignored if the prefix is not recognised. The type name
// "." is a wild card.
func (flist *FieldList) AreTypes(typnames ...string) bool {
	if len(typnames) != flist.Len() {
		return false
	}
	for i, name := range typnames {
		if name != "." {
			parts := strings.SplitN(name, ":", 2)
			if len(parts) == 1 {
				if flist.Get(i).Type() != flist.Types().Number[name] {
					return false
				}
			} else {
				if parts[0] == "tag" {
					names := flist.Types().WhichHaveTag(parts[1])
					found := false
					for _, name = range names {
						if flist.Get(i).Type() == flist.Types().Number[name] {
							found = true
						}
					}
					if !found {
						return false
					}
				}
			}
		}
	}
	return true
}

// Returns true if all the field values have the given types.
func (flist *FieldList) TypesAsStringSlice() []string {
	result := []string{}
	for _, field := range flist.ToSlice() {
		result = append(result, flist.Types().Name(field.Type()))
	}
	return result
}
