/*
	Value types.
*/
package h2go

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"sync"
)

type Buffer struct {
	raw   io.ReadWriter
	order ByteOrder
	sync.RWMutex
	*State
}

var _ Stateful = NewBuffer() // embedded State

// Constructors
func NewBuffer() *Buffer {
	return &Buffer{
		raw:   new(bytes.Buffer),
		State: NewState(),
	}
}

func MakeBuffer(raw io.ReadWriter, mutex sync.RWMutex, order ByteOrder) *Buffer {
	b := NewBuffer()
	b.raw = raw
	b.order = order
	b.RWMutex = mutex
	if raw != nil {
		b.NotEmpty()
	}
	return b
}

func MakeByteBuffer(order ByteOrder) *Buffer {
	return MakeBuffer(new(bytes.Buffer), sync.RWMutex{}, order)
}

func (bfr *Buffer) NewByteBuffer() *Buffer {
	return MakeByteBuffer(bfr.ByteOrder())
}

func BuildByteBufferFromBox(order ByteOrder, box *Box) (*Buffer, []byte, *Goof) {
	bfr := MakeByteBuffer(order)
	inside, goof := box.Require(BOX_ITEM_NATIVE)
	if IsError(goof) {
		return bfr, []byte{}, goof
	}
	byts, goof := bfr.Write(inside, box)
	return bfr, byts, goof
}

func (bfr *Buffer) BuildByteBufferFromBox(box *Box) (*Buffer, []byte, *Goof) {
	return BuildByteBufferFromBox(bfr.ByteOrder(), box)
}

func BuildByteBufferFromBytes(order ByteOrder, byts []byte) *Buffer {
	return MakeBuffer(bytes.NewBuffer(byts), sync.RWMutex{}, order)
}

// Copies buffer settings to a new byte Buffer. The read source is ignored,
// so the original Buffer need not be a byte Buffer.
func (bfr *Buffer) CopySettingsToNewByteBuffer() *Buffer {
	bfr2 := MakeByteBuffer(bfr.ByteOrder())
	return bfr2
}

// Copies buffer settings to a new byte Buffer. The bytes provided are loaded
// into the new buffer as quickly as possible.
func (bfr *Buffer) BuildByteBufferCopy(byts []byte) *Buffer {
	bfr2 := MakeBuffer(bytes.NewBuffer(byts), sync.RWMutex{}, bfr.ByteOrder())
	return bfr2
}

func (bfr *Buffer) CopyByteBuffer() (*Buffer, *Goof) {
	if bfr.IsByteBuffer() {
		byts, _ := bfr.Bytes()
		byts2 := []byte{}
		for _, byt := range byts {
			byts2 = append(byts2, byt)
		}
		return bfr.BuildByteBufferCopy(byts2), NO_GOOF

	} else {
		return NewBuffer(), Env.Goofs().MakeGoof("BAD_TYPE",
			"The underlying type of this h2go.Buffer is %T not bytes.Buffer",
			bfr.Raw())
	}
}

// Getters
func (bfr *Buffer) Raw() io.ReadWriter   { return bfr.raw }
func (bfr *Buffer) ByteOrder() ByteOrder { return bfr.order }

func (bfr *Buffer) IsByteBuffer() bool {
	_, isa := (bfr.Raw()).(*bytes.Buffer)
	return isa
}

func (bfr *Buffer) Bytes() ([]byte, *Goof) {
	bb, ok := (bfr.Raw()).(*bytes.Buffer)
	if !ok {
		return []byte{}, Env.Goofs().MakeGoof("BAD_TYPE",
			"The underlying type of this h2go.Buffer is %T not bytes.Buffer",
			bfr.Raw())
	}
	return bb.Bytes(), NO_GOOF
}

// Caution, can return nil.
func (bfr *Buffer) AsRawByteBuffer() (*bytes.Buffer, bool) {
	bb, isa := (bfr.Raw()).(*bytes.Buffer)
	return bb, isa
}

func (bfr *Buffer) Len() (int, *Goof) {
	bb, ok := (bfr.Raw()).(*bytes.Buffer)
	if !ok {
		return 0, Env.Goofs().MakeGoof("BAD_TYPE",
			"The underlying type of this h2go.Buffer is %T not bytes.Buffer",
			bfr.Raw())
	}
	return bb.Len(), NO_GOOF
}

// Setters
func (bfr *Buffer) SetReadWriter(raw io.ReadWriter) *Buffer {
	bfr.raw = raw
	return bfr
}

func (bfr *Buffer) StringNext(n int) string {
	byts := make([]byte, n)
	actual, err := bfr.Raw().Read(byts)
	msg := "(Next %d bytes of buffer): % x"
	if actual == n {
		msg += " ..."
	}
	if IsError(err) {
		errstr := fmt.Sprintf(ERROR_IN_STRING, err.Error())
		if actual == 0 {
			return errstr
		}
		msg = errstr + msg
	}
	return fmt.Sprintf(msg, actual, byts[:actual])
}

// Write Go native object to io.ReadWriter inside Buffer.
func (bfr *Buffer) Write(val interface{}, box *Box) ([]byte, *Goof) {
	bfr.Lock()
	defer bfr.Unlock()
	_, byts, goof := WriteBinary(bfr.Raw(), bfr.ByteOrder(), val, box)
	return byts, goof
}

// Read Go native object from io.ReadWriter inside Buffer.
func (bfr *Buffer) Read(val interface{}, box *Box) ([]byte, *Goof) {
	bfr.RLock() // other reads ok
	defer bfr.RUnlock()
	_, byts, goof := ReadBinary(bfr.Raw(), bfr.ByteOrder(), val, box)
	return byts, goof
}

// Write Go native object to io.ReadWriter inside Buffer, with compression.
func (bfr *Buffer) WriteCompressed(val interface{}, compressor string, level int, box *Box) ([]byte, *Goof) {
	byts := []byte{}
	goof := NO_GOOF
	switch compressor {
	case "gzip":
		writer, err := gzip.NewWriterLevel(bfr.Raw(), level)
		if IsError(err) {
			return byts, Env.Goofs().WrapGoof("ENCODING", err,
				"While creating new gzip writer")
		}
		bfr.Lock()
		defer bfr.Unlock()
		_, byts, goof = WriteBinary(writer, bfr.ByteOrder(), val, box)
		writer.Close() // Close required to perform the write
	default:
		if compressor != "none" {
			Env.Log().Warn("Compressor %d not recognised, using no compression",
				compressor)
		}
		bfr.Lock()
		defer bfr.Unlock()
		_, byts, goof = WriteBinary(bfr.Raw(), bfr.ByteOrder(), val, box)
	}
	return byts, goof
}

func (bfr *Buffer) ReadCompressed(val interface{}, compressor string, box *Box) ([]byte, *Goof) {
	var reader io.Reader
	var err error
	byts := []byte{}
	goof := NO_GOOF
	switch compressor {
	case "gzip":
		reader, err = gzip.NewReader(bfr.Raw())
		if IsError(err) {
			return byts, Env.Goofs().WrapGoof("DECODING", err,
				"While creating new gzip reader")
		}
	default:
		reader = bfr.Raw()
		if compressor != "none" {
			Env.Log().Warn("Compressor %d not recognised, using no compression",
				compressor)
		}
	}
	bfr.RLock() // other reads ok
	defer bfr.RUnlock()
	_, byts, goof = ReadBinary(reader, bfr.ByteOrder(), val, box)
	return byts, goof
}

func (bfr *Buffer) ReadCode(box *Box) (CODE, []byte, *Goof) {
	code := UNKNOWN_TYPE_CODE
	byts, goof := bfr.Read(&code, box)
	return code, byts, goof
}

func (bfr *Buffer) ReadExpectedCode(expected CODE, decoding interface{}, box *Box) ([]byte, *Goof) {
	if !Env.Types().ExistsByCode(expected) {
		return []byte{}, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Expected type code %d (% x) does not exist", expected)
	}
	code := UNKNOWN_TYPE_CODE
	byts, goof := bfr.Read(&code, box)
	if IsError(goof) {
		return byts, goof
	}
	if code != expected {
		goof = Env.Goofs().MakeGoof("DECODING",
			"While decoding %T, expected code for %q but got %q (%d, % x)",
			decoding, Env.Types().Name(expected), Env.Types().Name(code),
			code, code)
	}
	return byts, goof
}

/*
func (bfr *Buffer) ReadEncodeSwitches(box *Box) (EncodeSwitches, []byte, *Goof) {
	encsw := NewEncodeSwitches()
	byts, goof := bfr.Read(&encsw, box)
	return encsw, byts, goof
}
*/

func (bfr *Buffer) WriteBool(b bool, box *Box) ([]byte, *Goof) {
	if b {
		return bfr.Write(uint8(1), box)
	} else {
		return bfr.Write(uint8(0), box)
	}
}

func (bfr *Buffer) ReadBool(box *Box) (bool, []byte, *Goof) {
	b := uint8(0)
	byts, goof := bfr.Read(&b, box)
	return b == 1, byts, goof
}

func (bfr *Buffer) WriteBytes(byts []byte) ([]byte, *Goof) {
	bfr.Lock()
	defer bfr.Unlock()
	s, err := bfr.Raw().Write(byts)
	goof := NO_GOOF
	if IsError(err) {
		goof = Env.Goofs().WrapGoof("ENCODING", err,
			"While writing byte slice of length %d", len(byts))
	}
	return byts[:s], goof
}

func (bfr *Buffer) ReadBytes(byts []byte) ([]byte, *Goof) {
	bfr.RLock()
	defer bfr.RUnlock()
	s, err := bfr.Raw().Read(byts)
	goof := NO_GOOF
	if IsError(err) {
		goof = Env.Goofs().WrapGoof("ENCODING", err,
			"While reading byte slice of length %d", len(byts))
	}
	return byts[:s], goof
}
