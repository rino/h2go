/*
 */
package h2go

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"runtime"
	"strconv"
	"strings"
)

// LOGGER ======================================================================

type Logger struct {
	level     int
	out       []io.Writer
	nerrs     int // counts occurrence of errors
	pathlevel int
	checkOff  bool // global switch for Logger.Check()
	env       *Environment
	*State
}

var _ Stateful = NewLogger() // embedded State

// Does not initialise the Environment, doubly linked, do this separately.
func NewLogger() *Logger {
	return &Logger{
		out:   []io.Writer{},
		State: NewState(),
	}
}

func MakeLogger(writers []io.Writer) *Logger {
	log := NewLogger()
	log.level = DebugLevels["NONE"]
	log.NotEmpty()
	log.out = writers
	log.pathlevel = 2
	log.checkOff = false
	log.NowComplete()
	return log
}

func BuildLogger(level int, writers []io.Writer) *Logger {
	log := MakeLogger(writers)
	log.level = level
	return log
}

// Does not include Environment, which is doubly linked, use SetEnv after
// copying.
func (log *Logger) CopyPartial() *Logger {
	log2 := NewLogger()
	log2.level = log.level
	log2.out = log.out // these will likely be pointers
	log2.nerrs = log.nerrs
	log2.pathlevel = log.pathlevel
	log2.checkOff = log.checkOff
	log2.State = log.State.Copy()
	return log2
}

// Avoid a self-referential infinite loop.
func (log *Logger) Copy() *Logger {
	log2 := log.CopyPartial()
	env2 := log.Env().CopyPartial()
	env2.SetLogger(log2)
	log2.SetEnv(env2)
	return log2
}

// Getters.
func (log *Logger) Errors() int      { return log.nerrs }
func (log *Logger) Level() int       { return log.level }
func (log *Logger) ShowChecks() bool { return !log.checkOff }

func (log *Logger) LevelString() string {
	return DebugLevelName[log.Level()]
}

func (log *Logger) LevelIsNone() bool      { return log.level == DEBUGLEVEL_NONE }
func (log *Logger) LevelIsSuperFine() bool { return log.level == DEBUGLEVEL_SUPERFINE }
func (log *Logger) LevelIsFine() bool      { return log.level == DEBUGLEVEL_FINE }
func (log *Logger) LevelIsBasic() bool     { return log.level == DEBUGLEVEL_BASIC }
func (log *Logger) Env() *Environment      { return log.env }

// Setters.
// Allow the log level to be changed on the fly.
func (log *Logger) SetLogLevel(levelstr string) *Logger {
	oldname := DebugLevelName[log.Level()]
	newname := strings.ToUpper(strings.TrimSpace(levelstr))
	level, ok := DebugLevels[newname]
	if !ok {
		if log.Level() == DEBUGLEVEL_NONE {
			log.level = DEBUGLEVEL_BASIC
			log.Warn("Debug level %q not recognised, "+
				"switched on debugging to %q",
				levelstr, DebugLevelName[DEBUGLEVEL_BASIC])
		} else {
			log.Warn("Debug level %q not recognised, retained level %q",
				levelstr, oldname)
		}
	} else {
		log.level = level
		log.Advise("Debug level changed from %q to %q", oldname, newname)
	}
	return log
}

func (log *Logger) SetPathLevel(pathlevel int) *Logger {
	if pathlevel > 0 && pathlevel < 10 { // 10 is arbitrary
		log.pathlevel = pathlevel
	}
	return log
}

func (log *Logger) ResetErrCount() *Logger {
	log.nerrs = 0
	return log
}

func (log *Logger) IncErrors() int {
	log.nerrs++
	return log.nerrs
}

func (log *Logger) SetEnv(env *Environment) *Logger {
	log.env = env
	log.NotEmpty()
	log.NowComplete()
	return log
}

// Checkpoint a location in the code, with more info.
func (log *Logger) CheckTrace(back, pathdepth int, msg string, a ...interface{}) {
	if !log.LevelIsNone() && log.ShowChecks() {
		log.Dump(TraceToHere(back, pathdepth), "CHECK Trace:")
	}
	return
}

func (log *Logger) IsError(err error) bool {
	if IsError(err) {
		if goof, isa := err.(*Goof); isa {
			log.Message("part", "DUMP", "Error Source Stack")
			for _, line := range goof.CallerLines() {
				log.Message("nil", "", line)
			}
		} else {
			log.Message("full", "ERROR", err.Error())
		}
		log.IncErrors()
		return true
	}
	return false
}

func (log *Logger) WarnOnError(err error) error {
	if IsError(err) {
		log.Message("part", "WARNING", err.Error())
		log.IncErrors()
	}
	return err
}

// Permit bypassing of error checking for frequently occuring binary decoding.
func (log *Logger) DecodeError(err error, bfrlen int) error {
	if IsError(err) {
		log.Message(
			"full", "DECODE_ERROR", "Buffer[%d] %s", bfrlen, err.Error())
		log.IncErrors()
	}
	return err
}

func (log *Logger) HexDump(byts []byte, maxlen int, header string, a ...interface{}) {
	header2, lines := BytesToHexStringLines(byts, maxlen, header, a...)
	log.Message("part", "DUMP", header2)
	for _, line := range lines {
		log.Message("nil", "", line)
	}
	return
}

// Logic

func (log *Logger) IsLevel(levelstr string) bool {
	level, ok := DebugLevels[strings.ToUpper(levelstr)]
	if !ok {
		if log.Level() >= level {
			return true
		}
	}
	return false
}

// Loggers

var NilLogger *Logger = MakeNilLogger()
var ScreenLogger *Logger = MakeScreenLogger()

// Return a default Logger writing to the screen and a file.
func MakeScreenFileLogger(fname string) *Logger {
	writers := []io.Writer{
		os.Stdout,
		FileDebugWriter(fname)}
	return MakeLogger(writers)
}

// Return a Logger writing to a file.
func MakeFileLogger(fname string) *Logger {
	writers := []io.Writer{FileDebugWriter(fname)}
	return MakeLogger(writers)
}

// Return a default Logger writing to the screen only.
func MakeScreenLogger() *Logger {
	writers := []io.Writer{os.Stdout}
	return MakeLogger(writers)
}

// Return a Logger that writes to the given byte buffer.
func MakeByteBufferLogger(buf *bytes.Buffer) *Logger {
	writers := []io.Writer{os.Stdout}
	return MakeLogger(writers)
}

// Return a Logger with no writers.
func MakeNilLogger() *Logger {
	writers := []io.Writer{}
	return MakeLogger(writers)
}

// Return a file log logger writer using the given fname.
// TODO do something about protecting the user from appending to
// a very large logger file!
func FileDebugWriter(fpath string) io.Writer {
	gfile, err := os.OpenFile(
		fpath,
		os.O_CREATE|os.O_WRONLY|os.O_APPEND,
		0640)
	if IsError(err) {
		fmt.Printf("Problem opening Logger file %q: %s\n", fpath, err)
	}
	return gfile
}

// Writes the log message.  Any error encountered results in app termination.
func (log *Logger) output(msg string) {
	msg += "\n"
	for _, writer := range log.out {
		_, err := writer.Write([]byte(msg))
		if IsError(err) {
			fmt.Printf(
				"ERROR while trying to write %q to %q: %s\n",
				msg, writer, err)
		}
	}
	return
}

// Create a timestamped message for log output.
func (log *Logger) Timestamp() string {
	now, goof := NowUTC(log.Env().Calendar())
	if IsError(goof) {
		log.ErrorAlert(goof.Msg)
	}
	return now.String()
}

func (log *Logger) Logstamp(msg, prefix string) string {
	if log.Env() == nil {
		return prefix + " " + msg
	} else {
		return log.Timestamp() + " " +
			log.Env().ThreadStamps() + " " + prefix + " " + msg
	}
}

// Output log message.
func (log *Logger) Println(msg string) {
	log.output(msg)
	return
}

// Output log message as long as current level is at least SUPERFINE.
func (log *Logger) SuperFine(msg string, a ...interface{}) {
	if log.Level() >= DEBUGLEVEL_SUPERFINE {
		log.Message(
			"full", DebugLevelName[DEBUGLEVEL_SUPERFINE], msg, a...)
	}
	return
}

// Output log message as long as current level is at least FINE.
func (log *Logger) Fine(msg string, a ...interface{}) {
	if log.Level() >= DEBUGLEVEL_FINE {
		log.Message(
			"part", DebugLevelName[DEBUGLEVEL_FINE], msg, a...)
	}
	return
}

// Output log message as long as current level is at least BASIC.
func (log *Logger) Basic(msg string, a ...interface{}) {
	if log.Level() >= DEBUGLEVEL_BASIC {
		log.Message(
			"part", DebugLevelName[DEBUGLEVEL_BASIC], msg, a...)
	}
	return
}

// Output log message as long as current level is at least ADVISE.
func (log *Logger) Advise(msg string, a ...interface{}) {
	if log.Level() != DEBUGLEVEL_NONE {
		log.Message("nil", "ADVISE", msg, a...)
	}
	return
}

// A common handler for the log message methods. Use of a DebugMessageConfig
// struct offers scope to enhance message functionality in the future.
func (log *Logger) Message(callerDetail, levelstr, msg string, a ...interface{}) {
	out := ""
	sep := ": "
	if len(msg) == 0 {
		sep = ""
	}
	switch callerDetail {
	case "nil":
		out = log.Logstamp(fmt.Sprintf(msg, a...), levelstr)
	case "part":
		caller := CaptureCaller(3)
		out = log.Logstamp(fmt.Sprintf(
			TruncatePathToSlashLevel(
				log.pathlevel, caller.Filename())+"["+
				strconv.Itoa(caller.Line())+"]"+sep+
				msg, a...), levelstr)
	case "full":
		out = log.Logstamp(fmt.Sprintf(
			CaptureCaller(3).String()+sep+msg, a...), levelstr)
	}
	log.output(out)
	return
}

// Special methods.

func (log *Logger) ErrorAlert(msg string, a ...interface{}) {
	log.IncErrors()
	log.output(fmt.Sprintf("ERROR "+msg, a...))
	return
}

// Issue warning to log output.
func (log *Logger) Warn(msg string, a ...interface{}) {
	log.Message("part", "WARNING", msg, a...)
	return
}

// Checkpoint a location in the code.
func (log *Logger) Check(msg string, a ...interface{}) {
	if log.Level() != DEBUGLEVEL_NONE && log.ShowChecks() {
		log.Message("part", "CHECK", msg, a...)
	}
	return
}

// Provide a major visual break in the log output.
func (log *Logger) MajorSection(msg string, a ...interface{}) {
	if log.Level() != DEBUGLEVEL_NONE {
		log.Message("nil", "###########", "")
		log.Message("nil", "#", "")
		log.Message("nil", "#", msg, a...)
		log.Message("nil", "#", "")
		log.Message("nil", "###########", "")
	}
	return
}

// Provide a minor visual break in the log output.
func (log *Logger) MinorSection(msg string, a ...interface{}) {
	if log.Level() != DEBUGLEVEL_NONE {
		log.Message("nil", "===========", "")
		log.Message("nil", "=", msg, a...)
		log.Message("nil", "===========", "")
	}
	return
}

// Dump a slice of lines to the logger.
func (log *Logger) Dump(lines []string, msg string, a ...interface{}) {
	log.Message("part", "DUMP", msg, a...)
	for _, line := range lines {
		log.Message("nil", "", line)
	}
	return
}

// Dump a slice of lines to the logger, without a header message.
func (log *Logger) DumpQuietly(lines []string) {
	for _, line := range lines {
		log.Message("nil", "", line)
	}
	return
}

func (log *Logger) StackTrace() {
	if r := recover(); r != nil {
		trace := make([]byte, 2048)
		runtime.Stack(trace, true)
		// Don't include partial last line
		strTrace := string(trace)
		last := strings.LastIndex(strTrace, "\n")
		lines := strings.Split(strTrace, "\n")
		if len(lines) > 1 && last != len(strTrace)-1 {
			lines = lines[:len(lines)-2]
		}
		log.Dump(lines, "STACK TRACE, Recover from panic: %s", r)
	}
}
