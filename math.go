/*
	Mathematical functions.
*/
package h2go

import (
	"math"
)

type DistFuncParams struct {
	Type   string
	Mean   float64
	StdDev float64
}

func NewDistFuncParams() *DistFuncParams {
	return &DistFuncParams{}
}

func MakeNormDist(mu, sd float64) *DistFuncParams {
	dfunc := NewDistFuncParams()
	dfunc.Type = "normal"
	dfunc.Mean = mu
	dfunc.StdDev = sd
	return dfunc
}

// Breaks the given range from x0 to x1 into n equal intervals and returns the
// given probability distribution function as slices (x,y).
func GenerateDistFunc(params *DistFuncParams, x0, x1 float64, n int) (float64, []float64, []float64, *Goof) {
	dx := float64(0.0)
	x := []float64{}
	y := []float64{}
	if (x1 - x0) <= 0.0 {
		return dx, x, y, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"x0 (%f) should be <= x1 (%f)", x0, x1)
	}
	if n < 2 {
		return dx, x, y, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"n (%d) should be >= 2", n)
	}
	dx = (x1 - x0) / float64(n)
	x = make([]float64, n)
	y = make([]float64, n)
	// Some pre-calc
	k1 := float64(0.0)
	k2 := float64(0.0)
	switch params.Type {
	case "normal":
		k1 = 2.0 * math.Pow(params.StdDev, 2.0)
		k2 = math.Pow(2.0*math.Pi, -0.5) / params.StdDev
	}
	for i := 0; i < n; i++ {
		if i == 0 {
			x[i] = 0.5 * dx
		} else {
			x[i] = x[i-1] + dx
		}
		switch params.Type {
		case "normal":
			y[i] = k2 * math.Exp(-math.Pow(x[i]-params.Mean, 2.0)/k1)
		}
	}
	return dx, x, y, NO_GOOF
}

// Normalises the distribution, returning the original cumulative area under
// the curve, the new maximum value, the normalised distribution function and
// the normalised cumulative distribution function.  The interval must be
// uniform, and the length must be at least 2.
func NormaliseDistFunc(x, y []float64) (float64, float64, []float64, []float64, *Goof) {
	area := float64(0.0)
	max := float64(0.0)
	ynorm := []float64{}
	cumsum := []float64{}
	if len(x) != len(y) {
		return area, max, ynorm, cumsum, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"len(x) (%d) != len(y) (%d)", len(x), len(y))
	}
	if len(x) < 2 {
		return area, max, ynorm, cumsum, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"len(x) (%d) must be at least 2", len(x))
	}
	for i := 0; i < len(x); i++ {
		area += y[i]
	}
	dx := x[1] - x[0]
	area = area * dx
	ynorm = make([]float64, len(x))
	cumsum = make([]float64, len(x))
	for i := 0; i < len(x); i++ {
		ynorm[i] = y[i] / area
		if ynorm[i] > max {
			max = ynorm[i]
		}
		if i > 0 {
			cumsum[i] = cumsum[i-1]
		}
		cumsum[i] += ynorm[i] * dx
	}
	return area, max, ynorm, cumsum, NO_GOOF
}
