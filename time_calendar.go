package h2go

import ()

type Calendar interface {
	Name() string
	Copy() Calendar
	String() string
	NumberMonths() uint8
	MonthPhrase(m uint8, lang Language) (string, bool)
	MonthNumber(phrase string, lang Language) (uint8, bool)
	MonthModulo(m uint8) uint8
	DayPhrase(d uint8, lang Language) (string, bool)
	DaysInMonth(mth *Month) uint8
	DayNumber(phrase string, lang Language) (uint8, bool)
	DaysInWeek() uint8
	IsValidDay(mth *Month, d uint8) bool
	IsPossibleDay(d uint8) bool
	IsPossibleMonth(m uint8) bool
	IsPossibleYear(y int) bool
	IsValidDate(date *Date) bool
	DateToNum(date *Date, tn TimeNum) (TimeNum, *Goof)
	DateFromNum(tn TimeNum) (*Date, *Goof)
	CalClockToNum(cc *CalClock, tn TimeNum) (TimeNum, *Goof)
	CalClockFromNum(tn TimeNum) (*CalClock, *Goof)
	DayOfWeek(date *Date) uint8
	IsWeekend(date *Date) bool
	Stateful
}
