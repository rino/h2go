/*
	State is a great device for avoiding nil checks.  Just embed it in a struct
	and use the existing state bits (i.e. Empty, Complete), or compose a
	custom State and use the additional 6 state bits.
*/
package h2go

import (
	"sync"
)

// STATE =======================================================================

type Stateful interface {
	IsEmpty() bool
	IsComplete() bool
	NotEmpty() *State
	NowEmpty() *State
	NotComplete() *State
	NowComplete() *State
	StateEquals(other interface{}) bool
	SetToEmptyIfAllNil(objs ...interface{})
	SetToCompleteIfNoneNil(objs ...interface{})
}

type State struct {
	bits byte
	sync.RWMutex
}

func NewState() *State {
	return &State{
		bits: STATE_EMPTY,
	}
}

func (st *State) Copy() *State {
	st2 := NewState()
	st2.bits = st.bits
	return st2
}

// Getters
func (st *State) IsEmpty() bool {
	st.RLock()
	defer st.RUnlock()
	if st == nil { // unfortunately this doesn't help us if the parent is nil
		return true
	}
	return st.bits&STATE_EMPTY > 0
}

func (st *State) IsComplete() bool {
	st.RLock()
	defer st.RUnlock()
	return st.bits&STATE_COMPLETE > 0
}

func (st *State) IsRunning() bool {
	st.RLock()
	defer st.RUnlock()
	return st.bits&STATE_RUNNING > 0
}

// Setters
func (st *State) NotEmpty() *State {
	st.Lock()
	defer st.Unlock()
	st.bits = st.bits & ^STATE_EMPTY
	return st
}
func (st *State) NowEmpty() *State {
	st.Lock()
	defer st.Unlock()
	st.bits = st.bits | STATE_EMPTY
	return st
}

func (st *State) NotComplete() *State {
	st.Lock()
	defer st.Unlock()
	st.bits = st.bits & ^STATE_COMPLETE
	return st
}
func (st *State) NowComplete() *State {
	st.Lock()
	defer st.Unlock()
	st.bits = st.bits | STATE_COMPLETE
	return st
}

func (st *State) NotRunning() *State {
	st.Lock()
	defer st.Unlock()
	st.bits = st.bits & ^STATE_RUNNING
	return st
}
func (st *State) NowRunning() *State {
	st.Lock()
	defer st.Unlock()
	st.bits = st.bits | STATE_RUNNING
	return st
}

func (st State) String() string {
	result := ""
	blank := "-"
	if st.IsEmpty() {
		result += "E"
	} else {
		result += blank
	}
	if st.IsComplete() {
		result += "C"
	} else {
		result += blank
	}
	if st.IsRunning() {
		result += "R"
	} else {
		result += blank
	}
	return result
}

func (st *State) StateEquals(other interface{}) bool {
	if st2, isa := other.(*State); isa {
		if st2.bits == st.bits {
			return true
		}
	}
	return false
}

func (st *State) SetToEmptyIfAllNil(objs ...interface{}) {
	if len(objs) == 0 {
		st.NowEmpty()
		return
	}
	st.NotEmpty()
	imax := len(objs) - 1
	for i, obj := range objs {
		if obj != nil {
			break
		}
		if i == imax {
			st.NowEmpty()
		}
	}
	return
}

func (st *State) SetToCompleteIfNoneNil(objs ...interface{}) {
	st.NotComplete()
	imax := len(objs) - 1
	for i, obj := range objs {
		if obj == nil {
			break
		}
		if i == imax {
			st.NowComplete()
		}
	}
	return
}
