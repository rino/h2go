package h2go

import (
	"testing"
)

// TESTS ======================================================================
// This is a handy place to perform temporary tests that show up at the end of
// the test output.  Or which you're not sure how to categorise.

func TestBasicState(t *testing.T) {
	s0 := NewState()
	if !s0.IsEmpty() {
		t.Fatalf("A new %T should be empty", s0)
	}
	if s0.IsComplete() {
		t.Fatalf("A new %T should not be complete", s0)
	}
	s0.NotEmpty()
	if s0.IsEmpty() {
		t.Fatalf(
			"The %T was set to not empty, but it is showing as empty", s0)
	}
	s0.NowEmpty()
	if !s0.IsEmpty() {
		t.Fatalf(
			"The %T was set to empty, but it is showing as not empty", s0)
	}
	s0.NowComplete()
	if !s0.IsComplete() {
		t.Fatalf(
			"The %T was set to complete, but it is showing as not complete", s0)
	}
	s0.NotComplete()
	if s0.IsComplete() {
		t.Fatalf(
			"The %T was set to not complete, but it is showing as complete", s0)
	}
}

func TestStateNilChecks(t *testing.T) {
	s0 := NewState()
	s1 := s0.Copy()
	s0.SetToEmptyIfAllNil()
	if !s0.StateEquals(s1) {
		t.Fatalf("Should be no change in state with an empty nil check " +
			"and equals comparison")
	}
	s0.SetToEmptyIfAllNil(1, nil, "hello")
	if s0.IsEmpty() {
		t.Fatalf("State should not be empty since all objects are not nil")
	}
	s0.SetToEmptyIfAllNil(nil)
	if !s0.IsEmpty() {
		t.Fatalf("State should be empty since all objects are nil")
	}
	s0.SetToEmptyIfAllNil(nil, nil, nil)
	if !s0.IsEmpty() {
		t.Fatalf("State should be empty since all objects are nil")
	}
}
