package h2go

import (
	"reflect"
	"testing"
)

func TestConstruct(t *testing.T) {
	type C struct {
		C1 []uint
	}
	type B struct {
		*C
		B1 bool
	}
	type A struct {
		A1 string
		A2 int
		A3 *B
		A4 []string
		a5 [][]string // will be ignored, stays nil unfortunately
		A6 map[string]int
	}
	{
		var x *A
		goof := Construct(&x)
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem constructing %v (%T)", x, x)
		}
		if x == nil {
			t.Fatalf("Expecting non-nil initialisation of a %T", x)
		}
	}

	{
		var x map[string]map[int]bool
		goof := Construct(&x)
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem constructing %v (%T)", x, x)
		}
		if x == nil {
			t.Fatalf("Expecting non-nil initialisation of a %T", x)
		}
	}

	{
		var x map[string]int
		goof := Construct(&x)
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem constructing %v (%T)", x, x)
		}
		if x == nil {
			t.Fatalf("Expecting non-nil initialisation of a %T", x)
		}
	}

	{
		var x []int32
		goof := Construct(&x)
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem constructing %v (%T)", x, x)
		}
		if x == nil {
			t.Fatalf("Expecting non-nil initialisation of a %T", x)
		}
	}

	{
		var x interface{}
		goof := Construct(&x)
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem constructing %v (%T)", x, x)
		}
		if x == nil {
			t.Fatalf("Expecting non-nil initialisation of a %T", x)
		}
	}

	{
		var x chan int
		goof := Construct(&x)
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem constructing %v (%T)", x, x)
		}
		if x == nil {
			t.Fatalf("Expecting non-nil initialisation of a %T", x)
		}
	}
}

func TestStructTree(t *testing.T) {
	type StructD1 struct {
	}
	type StructB2 struct {
	}
	type StructB1 struct {
		*StructB2
		*StructD1
	}
	type StructC2 struct {
	}
	type StructC1 struct {
		*StructC2
	}
	type StructA1 struct {
		*StructB1
		*StructC1
	}

	d1 := &StructD1{}
	c2 := &StructC2{}
	b2 := &StructB2{}
	c1 := &StructC1{c2}
	b1 := &StructB1{b2, d1}
	a1 := &StructA1{b1, c1}

	stree, err := MakeStructTree(a1)
	if Env.Log().IsError(err) {
		t.Fatalf("Error while creating StructTree")
	}
	expected := []string{
		"StructA1",
		"  StructB1",
		"    StructB2",
		"    StructD1",
		"  StructC1",
		"    StructC2",
	}
	result := stree.Root().PrettyStringLines([]string{}, "  ", "")
	for i, line := range expected {
		if line != result[i] {
			Env.Log().Dump(expected, "Expected:")
			Env.Log().Dump(result, "Result:")
			t.Fatalf("Expected line %q, got %q", line, result[i])
		}
	}
}

func TestDeepCopy(t *testing.T) {
	type StructA struct {
		SA int
	}
	type Source struct {
		S1 int
		S2 string
		S3 bool
		S4 []string
		S5 *StructA
		S6 int32
	}
	type Target struct {
		S1 int
		S2 string
		S3 bool
		S4 []string
		S5 *StructA
		S6 int16
		S7 uint8
	}
	src := &Source{
		S1: 1,
		S2: "3",
		S3: true,
		S4: []string{"a", "b"},
		S5: &StructA{42},
		S6: -10,
	}
	targ := &Target{
		S1: 4,
		S2: "23",
		S3: false,
		S4: []string{"c", "d"},
		S5: &StructA{-12},
		S6: -100,
		S7: 44,
	}
	expected := &Target{
		S1: 1,
		S2: "3",
		S3: true,
		S4: []string{"a", "b"},
		S5: &StructA{42},
		S6: -100,
		S7: 44,
	}
	goof := DeepCopyStructPtrs(targ, src)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem with copy")
	}
	if !reflect.DeepEqual(targ, expected) {
		t.Fatalf("Expected target %#v, got %#v", expected, targ)
	}
}
