package h2go

import (
	"fmt"
	"strings"
	"testing"
	"unicode/utf8"
)

func CompareDecodedString(t *testing.T, in string, object Equible, typname string) {
	ws, goof := Env.Types().NewVirtualWorkspace()
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating new blank workspace")
	}
	out, goof := ws.DecodeTypedString(Env.Types().Number[typname], in)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem parsing %s", in)
	}
	if !object.Equals(out.GetDecoded().Value()) {
		t.Fatalf(
			"Problem parsing %v %s: expected %v, got %v",
			typname, in, object, out.GetDecoded())
	}
	return
}

func (ws *Workspace) CompareTabCompletions(t *testing.T, alts, expected []string) {
	if ws.HasGoofs() {
		t.Fatalf("Problem(s) with tab completion: %v", ws.FirstGoof())
	}
	if len(alts) != len(expected) {
		t.Fatalf("Number of tab alternatives %d does not match "+
			"expected number %d for alts = %q expected %q",
			len(alts), len(expected), alts, expected)
	}
	for i, alt := range alts {
		if alt != expected[i] {
			t.Fatalf("Calculated tab completion %d %q does not "+
				"match expected %q",
				i, alt, expected[i])
		}
	}
	return
}

func CompareTestingStrings(t *testing.T, original, expected, result string) {
	lines := strings.Split(original, "\n")
	Env.Log().Dump(lines, "Original:")
	linesExpected := strings.Split(expected, "\n")
	for i := 0; i < len(linesExpected); i++ {
		linesExpected[i] = fmt.Sprintf("%04d: %s", i, linesExpected[i])
	}
	Env.Log().Dump(linesExpected, "Expected %d lines:", len(linesExpected))
	linesResult := strings.Split(result, "\n")
	for i := 0; i < len(linesResult); i++ {
		linesResult[i] = fmt.Sprintf("%04d: %s", i, linesResult[i])
	}
	Env.Log().Dump(linesResult, "Got %d lines:", len(linesResult))
	line := 1
	pos := 0
	strExpected := expected
	strResult := result
	var r1 rune
	var r2 rune
	size1 := 0
	size2 := 0
	for len(strExpected) > 0 {
		r1, size1 = utf8.DecodeRuneInString(strExpected)
		r2, size2 = utf8.DecodeRuneInString(strResult)

		if r1 != r2 {
			break
		}
		strExpected = strExpected[size1:]
		strResult = strResult[size2:]
		if r1 < 128 {
			if string(r1) == "\n" {
				pos = 0
				line++
			}
		}
		pos += size1
	}
	Env.Log().Advise("Expected line %s", linesExpected[line-1])
	Env.Log().Advise("Got line      %s", linesResult[line-1])
	t.Fatalf("Unexpected conversion result at line %d, "+
		"position %d expected char %q, got %q",
		line-1, pos, string(r1), string(r2))
}
