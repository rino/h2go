/*
	Containers for byte encodable typed values.
*/
package h2go

import (
	"fmt"
	"math/big"
)

// FIELDBOX ====================================================================

type FieldBox struct {
	*Field
	params   *FieldBoxParams
	Pratio   float32 // payload size change ratio, calculated during encoding
	priority int64   // timestamp to prioritise concurrent manipulation
	state    *State  // cannot embed, due to Field.IsEmpty()
}

var _ Binaryable = NewFieldBox() // override Field
var _ Comparable = NewFieldBox() // embedded Field
var _ Equible = NewFieldBox()    // override Field
var _ Stringable = NewFieldBox() // embedded Field
var _ Codified = NewFieldBox()

// Constructors
func NewFieldBox() *FieldBox {
	return &FieldBox{
		Field:  NewField(),
		params: NewFieldBoxParams(),
		state:  NewState(),
	}
}

func MakeFieldBox(params *FieldBoxParams) *FieldBox {
	fbox := NewFieldBox()
	fbox.params = params
	fbox.state.NotEmpty()
	return fbox
}

func BuildFieldBox(f *Field, params *FieldBoxParams) *FieldBox {
	fbox := NewFieldBox()
	fbox.Field = f
	fbox.state.NotEmpty()
	fbox.params = params
	fbox.state.NowComplete()
	return fbox
}

// Getters
func (fbox *FieldBox) Params() *FieldBoxParams { return fbox.params }
func (fbox *FieldBox) State() *State           { return fbox.state }

// Setters
func (fbox *FieldBox) SetPriority(priority int64) *FieldBox {
	fbox.priority = priority
	fbox.state.NotEmpty()
	return fbox
}

func (fbox FieldBox) Equals(other interface{}) bool {
	fbox2, isa2 := other.(*FieldBox)
	if !isa2 {
		fbox3, isa3 := other.(FieldBox)
		if !isa3 {
			return false
		} else {
			return fbox.Field.Equals(fbox3.Field)

		}
	} else {
		return fbox.Field.Equals(fbox2.Field)
	}
}

func (fbox *FieldBox) String() string {
	return fmt.Sprintf("%v (params %v)", fbox.Field, fbox.Params())
}

func (fbox FieldBox) Code() CODE {
	return FIELDBOX_TYPE_CODE
}

// FIELDBOX PARAMS -------------------------------------------------------------

type FieldBoxParams struct {
	time       *big.Int // timestamp
	ver        *Version
	switches   *Switches
	compressor CompressionScheme
	encryptor  EncryptionScheme
	*State
}

var _ MultilineStringable = NewFieldBoxParams()
var _ Stateful = NewFieldBoxParams() // embedded State
var _ Validatable = NewFieldBoxParams()

// Constructors
func NewFieldBoxParams() *FieldBoxParams {
	return &FieldBoxParams{
		time:       big.NewInt(0),
		ver:        NewVersion(),
		switches:   NewSwitches(),
		compressor: NoCompression(),
		encryptor:  NoEncryption(),
		State:      NewState(),
	}
}

func MakeFieldBoxParams(ver *Version) *FieldBoxParams {
	params := NewFieldBoxParams()
	params.ver = ver
	params.SetEncodeSwitches(
		MakeEncodeSwitches(ver).SetValidator(params.SwitchesValidator))
	return params
}

func BuildFieldBoxParams(ver *Version, compressor CompressionScheme, encryptor EncryptionScheme) (*FieldBoxParams, *Goof) {
	params := MakeFieldBoxParams(ver)
	params.SetCompressionScheme(compressor)
	params.SetEncryptionScheme(encryptor)
	params.Validate()
	return params, params.Validate()
}

func BuildNonPayloadFieldBoxParams() *FieldBoxParams {
	return MakeFieldBoxParams(Env.Version())
}

func (params *FieldBoxParams) Copy() *FieldBoxParams {
	params2 := NewFieldBoxParams()
	params2.time = big.NewInt(0).Set(params.time)
	params2.ver = params.ver.Copy()
	params2.switches = params.switches.Copy()
	params2.compressor = params.compressor.Copy()
	params2.encryptor = params.encryptor.Copy()
	return params2
}

// Getters
func (params *FieldBoxParams) Timestamp() *big.Int           { return params.time }
func (params *FieldBoxParams) Version() *Version             { return params.ver }
func (params *FieldBoxParams) EncodeSwitches() *Switches     { return params.switches }
func (params *FieldBoxParams) Compressor() CompressionScheme { return params.compressor }
func (params *FieldBoxParams) Encryptor() EncryptionScheme   { return params.encryptor }

// Setters
func (params *FieldBoxParams) SetEncodeSwitches(switches *Switches) *FieldBoxParams {
	params.switches = switches
	params.NotEmpty()
	return params
}

func (params *FieldBoxParams) SetVersionEncodeSwitches() *FieldBoxParams {
	params.SetEncodeSwitches(
		MakeEncodeSwitches(params.Version()).
			SetValidator(params.SwitchesValidator))
	params.NotEmpty()
	return params
}

func (params *FieldBoxParams) SetVersion(ver *Version) *FieldBoxParams {
	params.ver = ver
	params.NotEmpty()
	return params
}

func (params *FieldBoxParams) SetTimestamp(time *big.Int) *FieldBoxParams {
	params.time = time
	params.NotEmpty()
	return params
}

func (params *FieldBoxParams) SetCompressionScheme(compressor CompressionScheme) *FieldBoxParams {
	params.compressor = compressor
	params.switches.SetAllWithKeyPrefix("COMPRESSION_", false)
	params.switches.Set(compressor.Name(), true)
	params.NotEmpty()
	return params
}

func (params *FieldBoxParams) SetEncryptionScheme(encryptor EncryptionScheme) *FieldBoxParams {
	params.encryptor = encryptor
	params.switches.SetAllWithKeyPrefix("ENCRYPTION_", false)
	params.switches.Set(encryptor.Name(), true)
	params.NotEmpty()
	return params
}

func (params *FieldBoxParams) Validate() *Goof {
	if params.Version().IsEmpty() {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"%T in %T cannot be empty",
			params.Version(), params)
	}
	goof := params.EncodeSwitches().Validate()
	if IsError(goof) {
		return goof
	}
	params.NowComplete()
	return NO_GOOF
}

func (params *FieldBoxParams) SwitchesValidator(switches *Switches) *Goof {
	compOn, goof := switches.GroupValidator("COMPRESSION_", true)
	if IsError(goof) {
		return goof
	}
	if len(compOn) == 1 && compOn[0] != "COMPRESSION_NONE" {
		if !params.Compressor().IsComplete() {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"CompressionScheme %T is not complete",
				params.Compressor())
		}
	}
	encOn, goof := switches.GroupValidator("ENCRYPTION_", true)
	if IsError(goof) {
		return goof
	}
	if len(encOn) == 1 && encOn[0] != "ENCRYPTION_NONE" {
		if !params.Encryptor().IsComplete() {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"EncryptionScheme %T is not complete",
				params.Encryptor())
		}
	}
	return NO_GOOF
}

func (params *FieldBoxParams) String() string {
	return "(Ver: " + params.Version().String() +
		", Switches: " + params.EncodeSwitches().String() +
		", Time: " + params.Timestamp().String() +
		", Comp: " + params.Compressor().Name() +
		", Enc: " + params.Encryptor().Name() + ")"
}

func (params *FieldBoxParams) MultilineString(firstprefix, prefix string) []string {
	return Env.Types().MintFieldMap().
		AutoSet("Version", params.Version().String()).
		AutoSet("Switches", params.EncodeSwitches().String()).
		AutoSet("Time", params.Timestamp().String()).
		AutoSet("Compression", params.Compressor().Name()).
		AutoSet("Encryption", params.Encryptor().Name()).
		MultilineString(firstprefix, prefix)
}

//                                                                             FIXED SIZE FIELD
//                                                                            +-------+------------------+
//                                                                            | code  | value            |
//                                                                            | CODE  | fixed size       |
//                                                                            |       |                  |
//                                                                            +-------+------------------+
//             -------------------------- metadata -------------------------
//            /                                                             \
//           /                                                               \ VARIABLE SIZE FIELD
//  +-------+---------+----------+----------+-----------+-----------+---------+-------+---------+------------------+
//  | fbox  | version | switches | other    | timestamp | timestamp | payload | code  | size    | value            |
//  | type  | UINT16  |          | settings | size      | bytes     | size    | CODE  | UINT    | bytes            |
//  | CODE  |         | (1)      |          | UINT      | variable  | UINT    |       |         | variable         |
//  +-------+---------+----------+----------+-----------+-----------+---------+-------+---------+------------------+
//                                \_______________contingent_________________/ \______________field________________/
//                                                     \___________________________________________________________/
//                                                                                   |
//  (1) ENCODE_SWITCHES_BYTE_LENGTH                                                  payload
//
// Note that the receiver must be a pointer so we can set the Pratio field.
// The associated FieldBoxParams should be validated before encode/decode.
func (fbox *FieldBox) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	env := box.EnvOrDefault(Env)
	out := MakeBinaryRepresentation(env.RecordEncoding())
	out.SetField(fbox.Field)
	fbox.State().RLock()
	defer fbox.State().RUnlock()

	if env.Log().LevelIsSuperFine() {
		env.Log().SuperFine("ENTER ENCODE for field = %v", fbox.Field)
		env.Log().Dump(TraceToHere(6, 2), "Trace (newest first):")
		if bfr.IsByteBuffer() {
			b0, goof := bfr.Bytes()
			env.Log().SuperFine("bfr (%d) % x goof=%v", len(b0), b0, goof)
		}
	}
	// FieldBox type code
	byts, goof := bfr.Write(FIELDBOX_TYPE_CODE, box)
	out.AppendEncodedAs("fbox type code", byts)
	if IsError(goof) {
		return out, goof
	}
	// Write version
	out2, goof := fbox.Params().Version().EncodeBinary(bfr, box)
	out.AppendEncodedToAs(
		[]interface{}{0, "metadata"}, "version", out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding %T version %v",
			fbox.Params(), fbox.Params().Version())
	}
	// Write switches
	out2, goof = fbox.Params().EncodeSwitches().EncodeBinary(bfr, box)
	out.AppendEncodedToAs(
		[]interface{}{0, "metadata"}, "encode switches", out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding switches %v",
			fbox.Params().EncodeSwitches())
	}

	if !fbox.Params().EncodeSwitches().IsTrue("COMPRESSION_NONE") {
		if fbox.Params().EncodeSwitches().IsTrue("COMPRESSION_GZIP") {
			// Write compression threshold
			out2, goof = fbox.Params().Compressor().Level().EncodeBinary(bfr, box)
			out.AppendEncodedToAs(
				[]interface{}{0, "metadata"}, "gzip compression level",
				out2.ChooseEncoding(0))
			if IsError(goof) {
				return out, env.Goofs().WrapGoof("ENCODING", goof,
					"While encoding gzip compression level %v for %q algorithm",
					fbox.Params().Compressor().Level(),
					fbox.Params().Compressor().Name())
			}
		}
	}

	// Put any other specialist settings here

	// If using a payload of compressed and/or encrypted data, write this to a
	// new temporary buffer before writing to the original buffer, otherwise
	// just keep writing to the original.
	bfr2 := bfr
	payloaded := !fbox.Params().EncodeSwitches().IsTrue("COMPRESSION_NONE") ||
		!fbox.Params().EncodeSwitches().IsTrue("ENCRYPTION_NONE")
	if payloaded {
		// Create blank new buffer
		bfr2 = bfr.CopySettingsToNewByteBuffer()
	}

	if fbox.Params().EncodeSwitches().IsTrue("TIMESTAMPING") {
		var err error
		byts, err = fbox.Params().Timestamp().GobEncode()
		if IsError(err) {
			return out, env.Goofs().WrapGoof("ENCODING", err,
				"While encoding big.Int timestamp %v within field",
				fbox.Params().Timestamp())
		}
		// Timestamp size
		out2, goof = MakeUINT(len(byts)).EncodeBinary(bfr2, EMPTY_BOX)
		if payloaded {
			out.AppendEncodedToAs(
				[]interface{}{"payload", "timestamp"},
				"timestamp size", out2.ChooseEncoding(0))
		} else {
			out.AppendEncodedToAs(
				[]interface{}{0, "timestamp"},
				"timestamp size", out2.ChooseEncoding(0))
		}
		if IsError(goof) {
			return out, env.Goofs().WrapGoof("ENCODING", goof,
				"While writing size %d of timestamp %v for a field",
				UINT(len(byts)), fbox.Params().Timestamp())
		}
		// Timestamp gob encoded bytes
		_, goof = bfr2.Write(byts, box)
		if payloaded {
			out.AppendEncodedToAs(
				[]interface{}{"payload", "timestamp"}, "timestamp big.Int gob",
				byts)
		} else {
			out.AppendEncodedToAs(
				[]interface{}{0, "timestamp"}, "timestamp big.Int gob",
				byts)
		}
		if IsError(goof) {
			return out, env.Goofs().WrapGoof("ENCODING", goof,
				"While writing timestamp %v bytes for a field",
				byts)
		}
	}

	// Field
	typ, goof := Env.Types().ProtocolByCode(FIELD_TYPE_CODE)
	if IsError(goof) {
		return out, goof
	}
	out2, goof = typ.BinaryTx(bfr2, box.Set(BOX_ITEM_FIELD, fbox.Field))
	if payloaded {
		out.AppendEncodedToAs(
			[]interface{}{"payload", "field"}, "field", out2.ChooseEncoding(0))
	} else {
		out.AppendEncodedToAs(
			[]interface{}{0, "field"}, "field", out2.ChooseEncoding(0))
	}
	if IsError(goof) {
		return out, goof
	}
	// If not payloaded, then we are done at this point

	if !payloaded {
		out.AppendEncoded(out2.ChooseEncoding(0))
	} else {
		// Convert payload back to bytes
		byts, _ = bfr2.Bytes()
		out.AppendEncodedToAs("payload", "raw payload", byts)
		pratio_denom := float32(len(byts))
		// Compress first, encrypt second
		if !fbox.Params().EncodeSwitches().IsTrue("COMPRESSION_NONE") {
			byts, goof = fbox.Params().Compressor().Compress(byts)
			out.AppendEncodedToAs(
				"payload",
				fbox.Params().Compressor().Name()+" compressed payload",
				byts)
			if IsError(goof) {
				return out, goof
			}
		}
		if !fbox.Params().EncodeSwitches().IsTrue("ENCRYPTION_NONE") {
			fbox.Params().Encryptor().ResetCipher()
			byts, goof = fbox.Params().Encryptor().Encrypt(byts)
			out.AppendEncodedToAs(
				"payload",
				fbox.Params().Encryptor().Name()+" encrypted payload",
				byts)
			if IsError(goof) {
				return out, goof
			}
		}

		fbox.Pratio = float32(len(byts)) / pratio_denom
		// Write size of processed payload
		out2, goof = MakeUINT(len(byts)).EncodeBinary(bfr, EMPTY_BOX)
		out.AppendEncodedToAs(0, "fbox payload size (UINT)",
			out2.ChooseEncoding(0))
		if IsError(goof) {
			return out, goof
		}
		_, goof = bfr.Write(byts, box)
		out.AppendEncodedToAs(0, "final payload", byts)
	}
	return out, goof
}

// FieldBox binary decoder assumes code has been read.  The FieldBox's
// FieldBoxParams must contain:
//   1. a Switches with the number of switches matching the encoding,
//   2. an encryption key suitable for the type required in the
//      EncodeSwitches stored in the encoding.
// If the Box provided to DecodeBinary contains a BOX_ITEM_UINT8 value of
// DECODE_BINARY_SIZE_ONLY, decoding will cease when the total byte length of
// the FieldBox, excluding the prefixed (pre-read) type code, have been
// determined and returned in the Box value BOX_ITEM_UINT64.  Otherwise the Box
// is unused, unless it provides an Environment.
func (fbox *FieldBox) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	env := box.EnvOrDefault(Env)
	out := MakeBinaryRepresentation(env.RecordEncoding())
	out.AutoSetField(fbox)
	sizeOnly := box.HasItem(BOX_ITEM_UINT8) &&
		box.Inside(BOX_ITEM_UINT8).(uint8) == DECODE_BINARY_SIZE_ONLY
	if env.Log().LevelIsSuperFine() {
		env.Log().SuperFine("ENTER DECODE")
		env.Log().Dump(TraceToHere(6, 2), "Trace:")
		if bfr.IsByteBuffer() {
			b0, goof := bfr.Bytes()
			env.Log().SuperFine("len %d buffer=% x goof=%v",
				len(b0), b0, goof)
		}
	}
	byts := []byte{}
	goof := NO_GOOF
	fbox.Params().NotComplete()
	// Read version
	out2, goof := fbox.Params().Version().DecodeBinary(bfr, box)
	out.AppendEncodedToAs(
		[]interface{}{0, "metadata"}, "version", out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, env.Goofs().WrapGoof("DECODING", goof,
			"While decoding version in a %T", fbox)
	}
	if !env.Version().BinaryEncodingIsCompatible(fbox.Params().Version()) {
		return out, GoofBinaryCompatibility(fbox.Params().Version())
	}
	// Read switches
	out2, goof = fbox.Params().EncodeSwitches().DecodeBinary(bfr, box)
	out.AppendEncodedToAs(
		[]interface{}{0, "metadata"}, "encode switches",
		out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, env.Goofs().WrapGoof("DECODING", goof,
			"While decoding switches in a %T", fbox)
	}

	goof = fbox.Params().Validate()
	if IsError(goof) {
		return out, goof
	}

	if !fbox.Params().EncodeSwitches().IsTrue("COMPRESSION_NONE") {
		compType := fbox.Params().EncodeSwitches().
			FindAllKeysWithPrefixAndValue("COMPRESSION_", true)[0]
		if compType == "COMPRESSION_GZIP" {
			// Read compression level
			level := NewUINT8()
			out2, goof = level.DecodeBinary(bfr, box)
			out.AppendEncodedToAs(
				[]interface{}{0, "metadata"}, "gzip compression level",
				out2.ChooseEncoding(0))
			if IsError(goof) {
				return out, env.Goofs().WrapGoof("DECODING", goof,
					"While decoding compression level for %q algorithm",
					compType)
			}
		}
	}

	// Other specialist settings are read here

	bfr2 := bfr
	payloaded := !fbox.Params().EncodeSwitches().IsTrue("COMPRESSION_NONE") ||
		!fbox.Params().EncodeSwitches().IsTrue("ENCRYPTION_NONE")
	if payloaded {
		// Read payload size
		out2, goof := NewUINT().DecodeBinary(bfr, EMPTY_BOX)
		out.AppendEncodedToAs(
			[]interface{}{0, "payload"}, "fbox payload size (UINT)",
			out2.ChooseEncoding(0))
		if IsError(goof) {
			return out, goof
		}
		ui, _ := out2.GetDecoded().Value().(UINT)
		i64, goof := ui.AsInt64()
		if IsError(goof) {
			return out, goof
		}
		if sizeOnly {
			box.Set(BOX_ITEM_UINT64, uint64(out.Len(0))+uint64(ui))
			return out, NO_GOOF
		}
		size := int(i64)
		// Read payload
		byts = make([]byte, size)
		_, goof = bfr.Read(&byts, box)
		out.AppendEncodedToAs([]interface{}{0, "payload"}, "raw payload", byts)
		if IsError(goof) {
			return out, goof
		}

		// Decrypt first, uncompress second
		if !fbox.Params().EncodeSwitches().IsTrue("ENCRYPTION_NONE") {
			fbox.Params().Encryptor().ResetCipher()
			byts, goof = fbox.Params().Encryptor().Decrypt(byts)
			if IsError(goof) {
				return out, goof
			}
			out.AppendEncodedToAs("payload",
				"payload after "+fbox.Params().Encryptor().Name()+" decryption",
				byts)
		}
		if !fbox.Params().EncodeSwitches().IsTrue("COMPRESSION_NONE") {
			byts, goof = fbox.Params().Compressor().Decompress(byts)
			if IsError(goof) {
				return out, goof
			}
			out.AppendEncodedToAs("payload",
				"payload after "+fbox.Params().Compressor().Name()+" decompression",
				byts)
		}

		bfr2 = bfr.BuildByteBufferCopy(byts)
	}

	// Now continue decoding bfr2
	if fbox.Params().EncodeSwitches().IsTrue("TIMESTAMPING") {
		out2, goof := NewUINT().DecodeBinary(bfr2, EMPTY_BOX)
		if payloaded {
			out.AppendEncodedToAs(
				[]interface{}{"decoded payload", "timestamp"}, "timestamp size",
				out2.ChooseEncoding(0))
		} else {
			out.AppendEncodedToAs(
				[]interface{}{0, "timestamp"}, "timestamp size",
				out2.ChooseEncoding(0))
		}
		if IsError(goof) {
			return out, goof
		}
		ui, _ := out2.GetDecoded().Value().(UINT)
		i64, goof := ui.AsInt64()
		if IsError(goof) {
			return out, goof
		}
		size := int(i64)
		byts := make([]byte, size)
		_, goof = bfr2.Read(&byts, box)
		if payloaded {
			out.AppendEncodedToAs(
				[]interface{}{"decoded payload", "timestamp"},
				"timestamp big.Int gob", byts)
		} else {
			out.AppendEncodedToAs(
				[]interface{}{0, "timestamp"},
				"timestamp big.Int gob", byts)
		}
		t := big.NewInt(0)
		err := t.GobDecode(byts)
		if IsError(err) {
			return out, env.Goofs().WrapGoof("DECODING", err,
				"While decoding %T timestamp %v",
				t, t)
		}
		fbox.Params().SetTimestamp(t)
	}
	// Finally, read Field type code
	fbox.typ, byts, goof = bfr2.ReadCode(box)
	if payloaded {
		out.AppendEncodedToAs(
			[]interface{}{"decoded payload", "field"}, "field type code",
			byts)
	} else {
		out.AppendEncodedToAs(
			[]interface{}{0, "field"}, "field type code", byts)
	}
	if IsError(goof) {
		return out, goof
	}

	typ, goof := env.Types().ProtocolByCode(fbox.typ)
	if IsError(goof) {
		return out, goof
	}
	if sizeOnly {
		if typ.BinarySizeFixed {
			box.Set(BOX_ITEM_UINT64,
				uint64(out.Len(0))+uint64(typ.BinarySize))
		} else {
			out2, goof := NewUINT().DecodeBinary(bfr2, EMPTY_BOX)
			if payloaded {
				out.AppendEncodedToAs(
					[]interface{}{"decoded payload", "field"},
					"field value length",
					out2.ChooseEncoding(0))
			} else {
				out.AppendEncodedToAs(
					[]interface{}{0, "field"},
					"field value length",
					out2.ChooseEncoding(0))
			}
			if IsError(goof) {
				return out, goof
			}
			ui, _ := out2.GetDecoded().Value().(UINT)
			box.Set(BOX_ITEM_UINT64, uint64(out.Len(0))+uint64(ui))
		}
		return out, NO_GOOF
	}

	// and the Field value
	out2, goof = typ.BinaryRx(bfr2, box)
	fbox.val = out2.GetDecoded().Value()
	if payloaded {
		out.AppendEncodedToAs(
			[]interface{}{"decoded payload", "field"},
			"field value", out2.ChooseEncoding(0))
	} else {
		out.AppendEncodedToAs(
			[]interface{}{0, "field"}, "field value",
			out2.ChooseEncoding(0))
	}
	if goof.EOF {
		goof = NO_GOOF
	}
	return out, goof
}

// Generate FieldBox binary encoder.
func FieldBoxBinaryEncoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		inside, goof := box.Require(BOX_ITEM_FIELDBOX)
		if IsError(goof) {
			return NewBinaryRepresentation(), goof
		}
		return inside.(*FieldBox).EncodeBinary(bfr, box)
	}
}

// Generate FieldBox binary decoder.
func FieldBoxBinaryDecoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		return NewFieldBox().DecodeBinary(bfr, box)
	}
}
