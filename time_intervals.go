package h2go

import (
	"fmt"
	"math/big"
	"reflect"
	"sort"
	"strings"
)

//== DURATION =================================================================

// Whole part of Duration is seconds, fractional part is sub-seconds.
// Duration is immutable, and can be negative.
type Duration struct {
	*big.Rat
	*State
}

// Constructors.

func NewDuration() *Duration {
	return &Duration{
		Rat:   big.NewRat(0, 1),
		State: NewState(),
	}
}

func MakeDuration(dt int) *Duration {
	return NewDuration().SetValue(big.NewRat(0, 1).SetInt64(int64(dt)))
}

func BuildDuration(t1, t2 Time, cal Calendar) *Duration {
	tn1, _ := t1.ToNum(new(UnixTime))
	i1 := tn1.Value().AsBigInt()
	tn2, _ := t2.ToNum(new(UnixTime))
	i2 := tn2.Value().AsBigInt()
	di := big.NewInt(0).Sub(i2, i1)
	d := big.NewRat(0, 1).SetInt(di)
	cc1, _ := t1.ToCalClock(cal)
	cc2, _ := t2.ToCalClock(cal)
	f1 := cc1.FracSec()
	f2 := cc2.FracSec()
	df := big.NewRat(0, 1).Sub(f2, f1)
	return NewDuration().SetValue(d.Add(d, df))
}

// Use seconds as the default unit
func Seconds(d int) *Duration {
	return NewDuration().SetValue(big.NewRat(0, 1).SetInt64(int64(d)))
}
func Minutes(d int) *Duration { return MakeSuperScaleDuration(d, SecInMinute) }
func Hours(d int) *Duration   { return MakeSuperScaleDuration(d, SecInHour) }
func Days(d int) *Duration    { return MakeSuperScaleDuration(d, SecInDay) }
func Weeks(d int) *Duration   { return MakeSuperScaleDuration(d, SecInWeek) }

func MilliSeconds(d int) *Duration { return MakeSubScaleDuration(d, MillisInSec) }
func MicroSeconds(d int) *Duration { return MakeSubScaleDuration(d, MicrosInSec) }
func NanoSeconds(d int) *Duration  { return MakeSubScaleDuration(d, NanosInSec) }
func PicoSeconds(d int) *Duration  { return MakeSubScaleDuration(d, PicosInSec) }

// Scale only once we are in the big space.
func MakeSuperScaleDuration(value, scale int) *Duration {
	i := big.NewInt(int64(value))
	s := big.NewInt(int64(scale))
	return NewDuration().SetValue(big.NewRat(0, 1).SetInt(s.Mul(i, s)))
}

func MakeSubScaleDuration(value, scale int) *Duration {
	return NewDuration().SetValue(big.NewRat(0, 1).SetFrac64(int64(value), int64(scale)))
}

// Getters.
func (d *Duration) AsWeeks() *Duration   { return d.DivByInt(SecInWeek) }
func (d *Duration) AsDays() *Duration    { return d.DivByInt(SecInDay) }
func (d *Duration) AsHours() *Duration   { return d.DivByInt(SecInHour) }
func (d *Duration) AsMinutes() *Duration { return d.DivByInt(SecInMinute) }

func (d *Duration) AsMilliSeconds() *Duration { return d.MultByInt(MillisInSec) }
func (d *Duration) AsMicroSeconds() *Duration { return d.MultByInt(MicrosInSec) }
func (d *Duration) AsNanoSeconds() *Duration  { return d.MultByInt(NanosInSec) }
func (d *Duration) AsPicoSeconds() *Duration  { return d.MultByInt(PicosInSec) }

func (d *Duration) AsInt64() int64 { return d.WholePart().Int64() }
func (d *Duration) AsFloat64() (float64, bool) {
	return d.Rat.Float64()
}

// Setters
func (d *Duration) SetValue(rat *big.Rat) *Duration {
	d.Rat = rat
	if rat != nil {
		d.NotEmpty()
		d.NowComplete()
	}
	return d
}

// Methods.

func (d *Duration) Negate() *Duration {
	s := big.NewRat(0, 1).Neg(d.Rat)
	return NewDuration().SetValue(s)
}

func (d *Duration) AddDuration(other *Duration) *Duration {
	s := big.NewRat(0, 1).Add(d.Rat, other.Rat)
	return NewDuration().SetValue(s)
}

func (d *Duration) DivByInt(i int) *Duration {
	s := big.NewRat(0, 1).SetInt64(int64(i))
	return NewDuration().SetValue(big.NewRat(0, 1).Quo(d.Rat, s))
}

func (d *Duration) MultByInt(i int) *Duration {
	s := big.NewRat(0, 1).SetInt64(int64(i))
	return NewDuration().SetValue(big.NewRat(0, 1).Mul(d.Rat, s))
}

func (d *Duration) WholePart() *big.Int {
	return big.NewInt(0).Div(d.Num(), d.Denom())
}

func (d *Duration) FracPart() *big.Rat {
	cmp := d.Num().Cmp(d.Denom())
	if cmp == 0 {
		return ZeroFrac()
	} else if cmp < 0 {
		return d.Rat
	}
	num := big.NewInt(0).Mod(d.Num(), d.Denom())
	return big.NewRat(0, 1).SetFrac(num, d.Denom())
}

func (d *Duration) String() string {
	// Remove trailing subsecond zeros with nano precision
	parts := strings.Split(d.FloatString(9), ".")
	if len(parts) == 1 {
		return parts[0]
	}
	subsec := strings.TrimRight(parts[1], "0")
	if len(subsec) == 0 {
		return parts[0]
	}
	return parts[0] + "." + strings.TrimRight(parts[1], "0")
}

// Supply a format string for the float and the unit, e.g. "%.2f [%s]".
// Currently only deals with seconds.
func (d *Duration) HumanString(fmtstr string) string {
	dti := d.AsInt64()
	dtf, _ := d.AsFloat64()
	if dti > 60 {
		if dti > 3600 {
			if dti > 86400 {
				return fmt.Sprintf(fmtstr, dtf/86400, "days")
			} else {
				return fmt.Sprintf(fmtstr, dtf/3600, "hrs")
			}
		} else {
			return fmt.Sprintf(fmtstr, dtf/60, "mins")
		}
	} else {
		return fmt.Sprintf(fmtstr, dtf, "s")
	}
}

func (d *Duration) Equals(other *Duration) bool {
	return d.Cmp(other.Rat) == 0
}

func (d *Duration) LessThan(other *Duration) bool {
	return d.Cmp(other.Rat) < 0
}

func (d *Duration) MoreThan(other *Duration) bool {
	return d.Cmp(other.Rat) > 0
}

//== INTERVAL =================================================================

type Interval struct {
	start Time
	end   Time
	*State
}

// Constructors.

func NewInterval() *Interval {
	return &Interval{
		start: NewCalClock(),
		end:   NewCalClock(),
		State: NewState(),
	}
}

func makeInterval(t1, t2 Time) *Interval {
	intv := NewInterval()
	intv.start = t1
	intv.end = t2
	return intv.setState()
}

func MakeIntervalUsingCalClocks(t1, t2 *CalClock) *Interval {
	return makeInterval(t1, t2)
}

func MakeIntervalUsingLocalTimes(t1, t2 *LocalTime) *Interval {
	return makeInterval(t1, t2)
}

// Validates interval.
func BuildInterval(t1, t2 Time) (*Interval, *Goof) {
	intv := NewInterval()
	goof := NO_GOOF
	if t1.IsLocalTime() || t2.IsLocalTime() {
		intv.start, goof = t1.At(t1.Location())
		if IsError(goof) {
			return intv, goof
		}
		intv.end, goof = t2.At(t1.Location())
		if IsError(goof) {
			return intv, goof
		}
	} else {
		if reflect.TypeOf(t1) != reflect.TypeOf(t2) {
			return intv, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"(%v, %v) types (%T, %T) are not allowed to differ",
				t1, t2, t1, t2)
		}
		intv.start = t1
		intv.end = t2
	}
	ok, goof := intv.start.IsBefore(intv.end)
	if IsError(goof) {
		return intv, goof
	}
	if !ok {
		return intv, Env.Goofs().MakeGoof("DATA_MISMATCH",
			"The original interval (%v, %v), when shifted to the location "+
				"of the start, (%v, %v), results in a chronological error",
			t1, t2, intv.start, intv.end)
	}
	return intv.setState(), NO_GOOF
}

// Getters.

func (intv *Interval) Start() Time        { return intv.start }
func (intv *Interval) End() Time          { return intv.end }
func (intv *Interval) Type() reflect.Type { return reflect.TypeOf(intv.start) }

// TODO if all locations are the same, return that location, otherwise
// return the location of the first interval.  Also return a boolean indicating
// uniformity.
func (intv *Interval) Location() *Location { return intv.start.Location() }

// Methods.

func (intv *Interval) setState() *Interval {
	if !intv.start.IsEmpty() || !intv.end.IsEmpty() {
		intv.NotEmpty()
	}
	if intv.start.IsComplete() && intv.end.IsComplete() {
		intv.NowComplete()
	}
	return intv
}

func (intv *Interval) Copy() *Interval {
	intv2 := NewInterval()
	intv2.start = intv.start
	intv2.end = intv.end
	intv2.State = intv.State.Copy()
	return intv2
}

func (intv *Interval) Equals(other *Interval) bool {
	compatible, _ := intv.IsCompatibleWith(other)
	sameStart, _ := intv.start.Equals(other.start)
	sameEnd, _ := intv.end.Equals(other.end)
	return compatible && sameStart && sameEnd
}

func (intv *Interval) IsCompatibleWith(other *Interval) (bool, *Goof) {
	if intv.Type() != other.Type() {
		return false, Env.Goofs().MakeGoof("BAD_TYPE",
			"Interval types %v and %v differ",
			intv.Type(), other.Type())
	}
	if intv.Location().Name() != other.Location().Name() {
		return false, Env.Goofs().MakeGoof("DATA_MISMATCH",
			"Interval locations %v and %v differ",
			intv.Location().Name(), other.Location().Name())
	}
	return true, NO_GOOF
}

func (intv *Interval) String() string {
	return fmt.Sprintf("(%v, %v)", intv.start, intv.end)
}

func (intv *Interval) UnionWith(other *Interval) (*Interval, *Goof) {
	newintv := NewInterval()
	_, goof := intv.IsCompatibleWith(other)
	if IsError(goof) {
		return newintv, goof
	}
	newintv.start, goof = intv.start.OrEarlier(other.start)
	if IsError(goof) {
		return newintv, goof
	}
	newintv.NotEmpty()
	newintv.end, goof = intv.end.OrLater(other.end)
	if IsError(goof) {
		return newintv, goof
	}
	newintv.NowComplete()
	return newintv, NO_GOOF
}

// Returns a new interval, if successful.
func (intv *Interval) IntersectWith(other *Interval) (*Interval, *Goof) {
	newintv := NewInterval()
	_, goof := intv.IsCompatibleWith(other)
	if IsError(goof) {
		return newintv, goof
	}
	if !intv.IsOverlapping(other) {
		return newintv, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Intervals %v and %v do not overlap",
			intv, other)
	}
	newintv.start, goof = intv.start.OrLater(other.start)
	if IsError(goof) {
		return newintv, goof
	}
	newintv.NotEmpty()
	newintv.end, goof = intv.end.OrEarlier(other.end)
	if IsError(goof) {
		return newintv, goof
	}
	newintv.NowComplete()
	return newintv, NO_GOOF
}

func (intv *Interval) StartsBefore(other *Interval) bool {
	before, _ := intv.start.IsBefore(other.start)
	return before
}

func (intv *Interval) IsOutside(other *Interval) bool {
	after, _ := intv.start.IsAfter(other.end)
	startsOnEnd, _ := intv.start.Equals(other.end)
	before, _ := intv.end.IsBefore(other.start)
	endsOnStart, _ := intv.end.Equals(other.start)
	return (after || startsOnEnd) || (before || endsOnStart)
}

func (intv *Interval) IsWithin(other *Interval) bool {
	after, _ := intv.start.IsAfter(other.start)
	startsOnStart, _ := intv.start.Equals(other.start)
	before, _ := intv.end.IsBefore(other.end)
	endsOnEnd, _ := intv.end.Equals(other.end)
	return (after || startsOnStart) && (before || endsOnEnd)
}

func (intv *Interval) IsOverlapping(other *Interval) bool {
	return !intv.IsOutside(other) || other.IsWithin(intv)
}

func (intv *Interval) ToDuration(cal Calendar) *Duration {
	return BuildDuration(intv.Start(), intv.End(), cal)
}

// Time zones are disregarded.
func (intv *Interval) GetDates(cal Calendar) (*Date, *Date, *Goof) {
	cc1, goof := intv.Start().ToCalClock(cal)
	if IsError(goof) {
		return cc1.Date, NewDate(), goof
	}
	cc2, goof := intv.End().ToCalClock(cal)
	if IsError(goof) {
		return cc1.Date, cc2.Date, goof
	}
	return cc1.Date, cc2.Date, NO_GOOF
}

func (intv *Interval) GetDatesAsLoc(loc *Location, cal Calendar) (*Date, *Date, *Goof) {
	lt1 := MakeLocalTime(intv.Start(), loc)
	lt2 := MakeLocalTime(intv.End(), loc)
	cc1, goof := lt1.ToCalClock(cal)
	if IsError(goof) {
		return cc1.Date, NewDate(), goof
	}
	cc2, goof := lt2.ToCalClock(cal)
	if IsError(goof) {
		return cc1.Date, cc2.Date, goof
	}
	return cc1.Date, cc2.Date, NO_GOOF
}

//== INTERVALS ================================================================

type Intervals []*Interval

// Constructors.

func NewIntervals() Intervals {
	return Intervals([]*Interval{})
}

func MakeIntervals(intvs ...*Interval) (Intervals, *Goof) {
	return BuildIntervals(intvs)
}

// Demand consistent type and location.
func BuildIntervals(intvslice []*Interval) (Intervals, *Goof) {
	intvs := Intervals(intvslice)
	if len(intvslice) == 0 {
		return intvs, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Cannot make Intervals with empty slice")
	}
	// Type and Location fixed by start of first interval added.
	typ := intvslice[0].Type()
	loc := intvslice[0].Location()
	for i, intv := range intvslice {
		if i == 0 {
			continue
		}
		if intv.Type() != typ {
			return intvs, Env.Goofs().MakeGoof("BAD_TYPE",
				"Inconsistent type %T in interval list, "+
					"differing from type %T of first interval %v",
				intv.Type(), typ, intvslice[0])
		}
		if intv.Location() != loc {
			return intvs, Env.Goofs().MakeGoof("DATA_MISMATCH",
				"Inconsistent location %v in interval list, "+
					"differing from location %v of first interval %v",
				intv.Location(), loc, intvslice[0])
		}
	}
	return intvs, NO_GOOF
}

// Getters.

func (intvs Intervals) Type() reflect.Type {
	if len(intvs) == 0 {
		return nil
	}
	return intvs[0].Type()
}
func (intvs Intervals) Location() *Location {
	if len(intvs) == 0 {
		return NewLocation()
	}
	return intvs[0].Location()
}
func (intvs Intervals) IsEmpty() bool { return len(intvs) == 0 }

// sort.Interface interface

func (intvs Intervals) Len() int { return len(intvs) }
func (intvs Intervals) Less(i, j int) bool {
	return intvs[i].StartsBefore(intvs[j])
}
func (intvs Intervals) Swap(i, j int) {
	intvs[i], intvs[j] = intvs[j], intvs[i]
	return
}

// Methods.

func (intvs Intervals) Copy() Intervals {
	newintvs := make([]*Interval, len(intvs))
	for i, intv := range intvs {
		newintvs[i] = intv
	}
	return Intervals(newintvs)
}

func (intvs Intervals) Equals(other Intervals) bool {
	if len(intvs) != len(other) {
		return false
	}
	for i, _ := range intvs {
		if !intvs[i].Equals(other[i]) {
			return false
		}
	}
	return true
}

func (intvs Intervals) Add(others ...*Interval) Intervals {
	for _, intv := range others {
		intvs = append(intvs, intv)
	}
	return intvs
}

func (intvs Intervals) String() string {
	result := "{"
	for i, intv := range intvs {
		if i > 0 {
			result += ", "
		}
		result += intv.String()
	}
	result += "}"
	return result
}

// Sorts intervals by start time, in place (modifies original).
func (intvs Intervals) Sort() Intervals {
	sort.Sort(intvs)
	return intvs
}

// Return a new Intervals list with overlapping intervals, no modification of
// original.
func (intvs Intervals) Merge() Intervals {
	newintvs := NewIntervals()
	skip := make(map[int]bool)
	for i, this := range intvs {
		if skip[i] {
			continue
		}
		newintv := this
		for j, other := range intvs[i+1:] {
			if newintv.IsOverlapping(other) {
				newintv, _ = newintv.UnionWith(other)
				skip[j+i+1] = true
			}
		}
		newintvs = append(newintvs, newintv)
	}
	return Intervals(newintvs)
}

func (intvs Intervals) IntersectWithInterval(bracket *Interval) Intervals {
	newintvs := NewIntervals()
	if bracket.IsEmpty() || intvs.IsEmpty() {
		return newintvs
	}
	intvToAdd := NewInterval()
	for _, intv := range intvs {
		if intv.IsOverlapping(bracket) {
			intvToAdd, _ = intv.IntersectWith(bracket)
			if intvToAdd.IsComplete() {
				newintvs = append(newintvs, intvToAdd)
			}
		}
	}
	return newintvs
}

func (intvs Intervals) IntersectWithIntervals(others Intervals) Intervals {
	newintvs := NewIntervals()
	if others.IsEmpty() || intvs.IsEmpty() {
		return newintvs
	}
	intvToAdd := NewInterval()
	for _, intv := range intvs {
		for _, other := range others {
			if intv.IsOverlapping(other) {
				intvToAdd, _ = intv.IntersectWith(other)
				if intvToAdd.IsComplete() {
					newintvs = append(newintvs, intvToAdd)
				}
			}
		}
	}
	return newintvs
}

// Create a list of interval periods being the given number of days (from
// midnight to midnight) encompassed by the range of the intervals list.
// If startDay is zero, start no earlier than the day on which the first
// interval starts.
//            MTWTFSSMTWTFSSMTWTFSSMTWTFSSMTWTFSSMTWTFSSMTWTFS
// original:    +------+      +--+  +--------+  +--------+
// result:    [::::::][::::::][::::::][::::::][::::::][::::::]
//            <---n-->                                   ^
//            ^                                          |
//            |                                     last interval
//        starts previous                           can finish beyond
//         Monday                                   original end
//
func (intvs Intervals) ToDayPeriods(n int, startDay uint8, loc *Location, cal Calendar) (Intervals, *Goof) {
	newintvs := NewIntervals()
	if intvs.IsEmpty() {
		return newintvs, NO_GOOF
	}

	// Earliest time in Intervals
	ccEarliest, goof := intvs[0].Start().ToCalClock(cal)
	if IsError(goof) {
		return newintvs, NO_GOOF
	}
	tEarliest, goof := ccEarliest.At(loc)
	if IsError(goof) {
		return newintvs, NO_GOOF
	}
	ltEarliest, _ := (tEarliest).(*LocalTime)

	// Latest time in Intervals
	ccLatest, goof := intvs[len(intvs)-1].End().ToCalClock(cal)
	if IsError(goof) {
		return newintvs, NO_GOOF
	}
	tLatest, goof := ccLatest.At(loc)
	if IsError(goof) {
		return newintvs, NO_GOOF
	}
	ltLatest, _ := (tLatest).(*LocalTime)

	// Ensure consistency
	ok, goof := ltLatest.IsAfter(ltEarliest)
	if IsError(goof) {
		return newintvs, NO_GOOF
	}
	if !ok {
		return newintvs, Env.Goofs().MakeGoof("DATA_MISMATCH",
			"After localising to %v, the end of the final interval %v is "+
				"not after the start of the first interval %v",
			loc, ltLatest, ltEarliest)
	}

	// Prime loop

	ccEarliest, goof = ltEarliest.ToCalClock(cal) // day may have changed
	if IsError(goof) {
		return newintvs, NO_GOOF
	}
	d := ccEarliest.Date
	if startDay > 0 {
		d, goof = d.PreviousDayOfWeek(startDay)
		if IsError(goof) {
			return newintvs, NO_GOOF
		}
	}
	lt1 := MakeLocalTime(MakeCalClock(d, Midnight), loc)
	lt2 := NewLocalTime()
	for {
		ok, goof = lt1.IsBefore(ltLatest)
		if IsError(goof) {
			return newintvs, goof
		}
		if !ok {
			break
		}
		d, goof = d.Inc(n)
		if IsError(goof) {
			return newintvs, goof
		}
		lt2 = MakeLocalTime(MakeCalClock(d, Midnight), loc)
		newintvs = newintvs.Add(MakeIntervalUsingLocalTimes(lt1, lt2))
		lt1 = lt2
	}
	return newintvs, NO_GOOF
}
