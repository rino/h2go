package h2go

import (
	"strconv"
)

// CALENDAR RULE ===============================================================

type CalendarRule struct {
	typ        int
	cal        Calendar
	duration   *Duration
	countLimit int
	startDate  *Date
	holidays   Intervals
	// By years
	skipYears int
	// By months
	dinc       *DayInc
	startMonth *Month
	monthSet   *MonthSet
	skipMonths int
	// By days
	skipDays int
	*State
}

// Constructors.

func NewCalendarRule() *CalendarRule {
	return &CalendarRule{
		cal:        NewGregorian(),
		duration:   NewDuration(),
		startDate:  NewDate(),
		holidays:   NewIntervals(),
		dinc:       NewDayInc(),
		startMonth: NewMonth(),
		monthSet:   NewMonthSet(),
		State:      NewState(),
	}
}

func MakeCalendarRule(cal Calendar) *CalendarRule {
	rule := NewCalendarRule()
	rule.cal = cal
	return rule
}

func (rule *CalendarRule) ByYears(startDate *Date, skipYears int) *CalendarRule {
	rule.typ = TIME_REPEAT_BY_YEARS
	rule.startDate = startDate
	rule.skipYears = skipYears
	return rule
}

// By-months constructor with a start date and skip months.
// Here we assume that the day of the month contained in the start date
// is repeated for subsequent months.  For example, if the start date is
// 2012-03-07, all subsequent dates will be on the 7th of the month.
func (rule *CalendarRule) ByMonthsDayConst(startDate *Date, skipMonths int) *CalendarRule {
	rule.typ = TIME_REPEAT_BY_REGULAR_MONTHS
	rule.startDate = startDate
	rule.skipMonths = skipMonths
	rule.startMonth = startDate.Month()
	rule.dinc = MakeDayIncDayOfMonth(startDate.Day())
	return rule
}

func (rule *CalendarRule) ByMonthsDayAlgo(startDate *Date, skipMonths int, dinc *DayInc) *CalendarRule {
	rule.typ = TIME_REPEAT_BY_REGULAR_MONTHS
	rule.startDate = startDate
	rule.skipMonths = skipMonths
	rule.startMonth = startDate.Month()
	rule.dinc = dinc
	return rule
}

// By-months constructor with explicit months of the year.
func (rule *CalendarRule) ByExplicitMonths(startMonth *Month, monthSet *MonthSet, dinc *DayInc) *CalendarRule {
	rule.typ = TIME_REPEAT_BY_EXPLICIT_MONTHS
	rule.startMonth = startMonth
	rule.dinc = dinc
	rule.monthSet = monthSet
	return rule
}

func (rule *CalendarRule) ByDays(startDate *Date, skipDays int) *CalendarRule {
	rule.typ = TIME_REPEAT_BY_DAYS
	rule.startDate = startDate
	rule.skipDays = skipDays
	return rule
}

// Getters.

func (rule *CalendarRule) Type() int           { return rule.typ }
func (rule *CalendarRule) Duration() *Duration { return rule.duration }
func (rule *CalendarRule) StartDate() *Date    { return rule.startDate }
func (rule *CalendarRule) SkipDays() int       { return rule.skipDays }
func (rule *CalendarRule) SkipMonths() int     { return rule.skipMonths }
func (rule *CalendarRule) SkipYears() int      { return rule.skipYears }
func (rule *CalendarRule) StartMonth() *Month  { return rule.startMonth }
func (rule *CalendarRule) MonthSet() *MonthSet { return rule.monthSet }
func (rule *CalendarRule) DayInc() *DayInc     { return rule.dinc }

// Setters.

func (rule *CalendarRule) WithCountLimit(countLimit int) *CalendarRule {
	rule.countLimit = countLimit
	return rule
}

func (rule *CalendarRule) WithDuration(duration *Duration) *CalendarRule {
	rule.duration = duration
	return rule
}

func (rule *CalendarRule) ExcludeHolidays(holidays Intervals) *CalendarRule {
	rule.holidays = holidays
	return rule
}

func (rule *CalendarRule) String() string {
	result := "{Type: " + CalendarRuleTypes[rule.Type()]
	switch rule.Type() {
	case TIME_REPEAT_BY_YEARS:
		result += ", Start: " + rule.StartDate().String()
		result += ", SkipYears: " + strconv.Itoa(rule.SkipYears())
	case TIME_REPEAT_BY_REGULAR_MONTHS:
		result += ", StartDate: " + rule.StartDate().String()
		result += ", SkipMonths: " + strconv.Itoa(rule.SkipMonths())
		result += ", StartMonth: " + rule.StartDate().Month().String()
		result += ", DayIncrementor: " +
			MakeDayIncDayOfMonth(rule.StartDate().Day()).String()
	case TIME_REPEAT_BY_EXPLICIT_MONTHS:
		result += ", StartMonth: " + rule.StartDate().Month().String()
		result += ", DayIncrementor: " +
			MakeDayIncDayOfMonth(rule.StartDate().Day()).String()
		result += ", rule.MonthSet: " + rule.MonthSet().String()
	case TIME_REPEAT_BY_DAYS:
		result += ", StartDate: " + rule.StartDate().String()
		result += ", SkipDays: " + strconv.Itoa(rule.SkipDays())
	}
	return result + "}"
}

// RULE ITERATOR ===============================================================

type RuleIterator struct {
	rule *CalendarRule
	next *Date
	*State
}

func NewRuleIterator() *RuleIterator {
	return &RuleIterator{
		rule:  NewCalendarRule(),
		next:  NewDate(),
		State: NewState(),
	}
}

func (rule *CalendarRule) MintIterator() *RuleIterator {
	iter := NewRuleIterator()
	iter.rule = rule
	iter.Reset(NewDate())
	return iter
}

func (iter *RuleIterator) Rule() *CalendarRule { return iter.rule }
func (iter *RuleIterator) Peek() *Date         { return iter.next }

func (iter *RuleIterator) Reset(start *Date) {
	if start.IsEmpty() {
		switch iter.Rule().Type() {
		case TIME_REPEAT_BY_YEARS, TIME_REPEAT_BY_DAYS:
			iter.next = iter.Rule().StartDate()
		case TIME_REPEAT_BY_REGULAR_MONTHS, TIME_REPEAT_BY_EXPLICIT_MONTHS:
			iter.next, _ =
				iter.Rule().StartMonth().FindUsingDayInc(
					iter.Rule().DayInc(), iter.Rule().holidays)
		}
	} else {
		iter.next = start
	}
	if iter.Rule().Type() == TIME_REPEAT_BY_EXPLICIT_MONTHS {
		iter.Rule().MonthSet().Reset(iter.Rule().StartMonth().Value())
	}

	return
}

// Calculate next date using rule, and return current date.
func (iter *RuleIterator) Pop() (*Date, *Goof) {
	date := NewDate()
	if goof := iter.Rule().Check(); IsError(goof) {
		return date, goof
	}
	if iter.next.IsEmpty() {
		return date, Env.Goofs().MakeGoof("NOT_INITIALISED",
			"Iterator has not been initialised")
	}
	result := *iter.next

	switch iter.Rule().Type() {
	case TIME_REPEAT_BY_YEARS:
		iter.next, _ = iter.next.Plus(iter.Rule().SkipYears(), 0, 0)
	case TIME_REPEAT_BY_DAYS:
		iter.next, _ = iter.next.Inc(iter.Rule().SkipDays())
	case TIME_REPEAT_BY_REGULAR_MONTHS:
		nextMonth, _ := iter.next.Month().Inc(iter.Rule().SkipMonths())
		iter.next, _ =
			nextMonth.FindUsingDayInc(
				iter.Rule().DayInc(), iter.Rule().holidays)
	case TIME_REPEAT_BY_EXPLICIT_MONTHS:
		nextMonth := iter.Rule().MonthSet().Next()
		nextYear := iter.next.Year()
		if iter.next.Month().Value() > nextMonth {
			nextYear.PlusInt(1)
		}

		newMonth, goof := MakeMonth(nextYear, nextMonth, iter.Rule().cal)
		if IsError(goof) {
			return date, goof
		}
		iter.next, _ =
			newMonth.FindUsingDayInc(
				iter.Rule().DayInc(), iter.Rule().holidays)
	}

	return &result, NO_GOOF
}

func (rule *CalendarRule) Check() *Goof {
	switch rule.Type() {
	case TIME_REPEAT_BY_YEARS:
		if rule.SkipYears() <= 0 {
			return Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
				"Cannot use a skipYears value of %v",
				rule.SkipYears())
		}
		break
	case TIME_REPEAT_BY_DAYS:
		if rule.SkipDays() <= 0 {
			return Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
				"Cannot use a skipDays value of %v",
				rule.SkipDays())
		}
		break
	case TIME_REPEAT_BY_REGULAR_MONTHS:
		if rule.StartMonth().IsEmpty() {
			return Env.Goofs().MakeGoof("PARAMETER_UNDEFINED",
				"Start month not set")
		}
		if rule.DayInc().IsEmpty() {
			return Env.Goofs().MakeGoof("PARAMETER_UNDEFINED",
				"Day incrementor not set")
		}
		if rule.SkipMonths() <= 0 {
			return Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
				"Cannot use a skipMonths value of %v",
				rule.SkipMonths())
		}
		break
	case TIME_REPEAT_BY_EXPLICIT_MONTHS:
		if rule.StartMonth().IsEmpty() {
			return Env.Goofs().MakeGoof("PARAMETER_UNDEFINED",
				"Start month not set")
		}
		if rule.DayInc().IsEmpty() {
			return Env.Goofs().MakeGoof("PARAMETER_UNDEFINED",
				"Day incrementor not set")
		}
		if rule.MonthSet().Len() == 0 {
			return Env.Goofs().MakeGoof("PARAMETER_UNDEFINED",
				"Cannot use an empty monthSet")
		}
		// Extra check
		if !rule.MonthSet().Exists(rule.StartMonth().Value()) {
			return Env.Goofs().MakeGoof("DATA_MISMATCH",
				"Start month %v must be present in the monthSet %v",
				rule.StartMonth(), rule.MonthSet())
		}
		break
	default:
		return Env.Goofs().MakeGoof("NOT_IMPLEMENTED",
			"Unknown CalendarRule task type %v", rule.Type())
	}

	/*
		    if rule.countLimit < 1 && rule.Duration() == nil {
				return Env.Goofs().MakeGoof("MISSING_SETTINGS",
		            "Cannot determine rule sunset, duration is nil and countLimit = %v",
					rule.countLimit)
		    }
	*/

	return NO_GOOF
}
