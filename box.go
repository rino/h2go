/*
	A Box can be used for the final argument of non-simple functions.  It
	creates a doorway allowing the addition of arguments to be prototyped
	quickly and easily without breaking an API.  It allows context, e.g.
	Environment, to be propogated without undue reliance on globals.
*/
package h2go

import (
	"fmt"
	"reflect"
	"sort"
)

// LABELLED ITEM ===============================================================

type LabelledItem struct {
	label string
	item  interface{}
}

func NewLabelledItem() *LabelledItem {
	return &LabelledItem{}
}

func MakeLabelledItem(label string, item interface{}) *LabelledItem {
	litem := NewLabelledItem()
	litem.label = label
	litem.item = item
	return litem
}

func (litem *LabelledItem) Label() string     { return litem.label }
func (litem *LabelledItem) Item() interface{} { return litem.item }

// BOX ITEM INDEX ==============================================================

type BoxItemIndex struct {
	kinds map[CODE]*LabelledItem
	max   CODE
}

func NewBoxItemIndex() *BoxItemIndex {
	return &BoxItemIndex{
		kinds: make(map[CODE]*LabelledItem),
	}
}

func BuildBoxItemIndex(kinds map[CODE]*LabelledItem) *BoxItemIndex {
	bii := NewBoxItemIndex()
	bii.kinds = kinds
	bii.UpdateMax()
	return bii
}

func (bii *BoxItemIndex) Copy() *BoxItemIndex {
	bii2 := NewBoxItemIndex()
	for k, v := range bii.kinds {
		bii2.kinds[k] = v
	}
	bii2.max = bii.max
	return bii2
}

func (bii *BoxItemIndex) GetLabel(key CODE) string {
	return bii.kinds[key].Label()
}

func (bii *BoxItemIndex) GetZero(key CODE) interface{} {
	return bii.kinds[key].Item()
}

func (bii *BoxItemIndex) GetMax() CODE  { return bii.max }
func (bii *BoxItemIndex) IsEmpty() bool { return len(bii.kinds) == 0 }

func (bii *BoxItemIndex) Set(key CODE, label string, typ interface{}) *BoxItemIndex {
	bii.kinds[key] = MakeLabelledItem(label, typ)
	if key > bii.max {
		bii.max = key
	}
	return bii
}

// This is a way for dependent packages to easily augment a
// BoxItemIndex.  Be sure to first UpdateMax().
func (bii *BoxItemIndex) Add(dkey CODE, label string, typ interface{}) CODE {
	bii.max++
	bii.kinds[bii.max] = MakeLabelledItem(label, typ)
	return bii.max
}

func (bii *BoxItemIndex) FindMax() {
	for k, _ := range bii.kinds {
		if k > bii.max {
			bii.max = k
		}
	}
	return
}
func (bii *BoxItemIndex) UpdateMax() {
	for k, _ := range bii.kinds {
		if k > bii.max {
			bii.max = k
		}
	}
	return
}

func (bii *BoxItemIndex) MergeCopy(other *BoxItemIndex) *BoxItemIndex {
	for k, v := range other.kinds {
		bii.kinds[k] = MakeLabelledItem(v.Label(), v.Item())
		if k > bii.max {
			bii.max = k
		}
	}
	return bii
}

func IsNativeUint(typ CODE) bool {
	return typ == BOX_ITEM_UINT8 ||
		typ == BOX_ITEM_UINT16 ||
		typ == BOX_ITEM_UINT32 ||
		typ == BOX_ITEM_UINT64
}

func IsNativeInt(typ CODE) bool {
	return typ == BOX_ITEM_INT8 ||
		typ == BOX_ITEM_INT16 ||
		typ == BOX_ITEM_INT32 ||
		typ == BOX_ITEM_INT64
}

func IsNativeFloat(typ CODE) bool {
	return typ == BOX_ITEM_FLOAT32 ||
		typ == BOX_ITEM_FLOAT64
}

func IsNativeNumber(typ CODE) bool {
	return IsNativeUint(typ) || IsNativeInt(typ) || IsNativeFloat(typ)
}

// BOX =========================================================================

// Boxes are used frequently, so we'll use an int rather than key string, which
// will also let the compiler help us do key checks.
type Box struct {
	in    map[CODE]interface{}
	kinds *BoxItemIndex
	env   *Environment // its own spot for better performance
}

// Constructors
func NewBox() *Box {
	return &Box{
		in:    make(map[CODE]interface{}),
		kinds: BoxItemKinds,
		env:   NewEnvironment(),
	}
}

func MakeBox(kinds *BoxItemIndex, env *Environment) *Box {
	box := NewBox()
	box.kinds = kinds
	box.env = env
	return box
}

func (box *Box) Copy() *Box {
	box2 := MakeBox(box.kinds.Copy(), box.env.Copy())
	for key, val := range box.in {
		box2.in[key] = val
	}
	return box2
}

func (box *Box) Set(key CODE, value interface{}) *Box {
	box.in[key] = value
	return box
}

func (box *Box) SetEnv(env *Environment) *Box {
	box.env = env
	return box
}

func (box *Box) SetBoxKinds(kinds *BoxItemIndex) *Box {
	box.kinds = kinds
	return box
}

func (box *Box) IsEmpty() bool     { return len(box.in) == 0 && box.env.IsEmpty() }
func (box *Box) HasEnv() bool      { return !box.env.IsEmpty() }
func (box *Box) Env() *Environment { return box.env }

func (box *Box) EnvOrDefault(env *Environment) *Environment {
	if box.HasEnv() {
		return box.Env()
	} else {
		return env
	}
}

func (box *Box) HasItem(key CODE) bool {
	_, exists := box.in[key]
	return exists
}

func (box *Box) Inside(key CODE) interface{} { return box.in[key] }

// Returns whether the key existed.
func (box *Box) Remove(key CODE) bool {
	if box.HasItem(key) {
		delete(box.in, key)
		return true
	} else {
		return false
	}
}

func (box *Box) Empty() *Box {
	box.in = make(map[CODE]interface{})
	return box
}

// Copies map entry from key1 to key2. Returns whether and existing value
// for key2 was over-written.
func (box *Box) CopyBoxValueFromTo(key1, key2 CODE) bool {
	overwritten := false
	if box.HasItem(key1) {
		val1, _ := box.in[key1]
		_, overwritten = box.in[key2]
		box.in[key2] = val1
		delete(box.in, key1)
	}
	return overwritten
}

func (box *Box) RequireEnv() (*Environment, *Goof) {
	goof := NO_GOOF
	if box.env.IsEmpty() {
		goof = Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Require an %T in the box", box.env)
	}
	return box.env, goof
}

func (box *Box) Require(key CODE) (interface{}, *Goof) {
	env := Env
	if box.HasEnv() {
		env = box.env
	}
	obj, exists := box.in[key]
	if !exists {
		return obj, env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Require an item of type %q in the box, have %q",
			box.kinds.GetLabel(key), box.ListBoxItemNames())
	}
	return obj, NO_GOOF
}

func (box *Box) RequireTyped(key CODE) (interface{}, *Goof) {
	return box.RequireOfType(key, box.kinds.GetZero(key))
}

func (box *Box) RequireOfKnownType(key, typkey CODE) (interface{}, *Goof) {
	return box.RequireOfType(key, box.kinds.GetZero(typkey))
}

func (box *Box) RequireOfType(key CODE, instance interface{}) (interface{}, *Goof) {
	env := Env
	if box.HasEnv() {
		env = box.env
	}
	obj, exists := box.in[key]
	if !exists {
		return obj, env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Require an item %q in the box, have %q",
			box.kinds.GetLabel(key), box.ListBoxItemNames())
	}
	if reflect.TypeOf(obj) != reflect.TypeOf(instance) {
		return obj, env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Argument %v is a %T but should be a %T",
			obj, obj, instance)
	}
	return obj, NO_GOOF
}

// Returns a comma separated list
func (box *Box) ListBoxItemNames() string {
	if len(box.in) == 0 {
		return "EMPTY"
	}
	names := []string{}
	for key, _ := range box.in {
		names = append(names, box.kinds.GetLabel(key))
	}
	sort.Strings(names)
	list := ""
	for i, name := range names {
		if i > 0 {
			list += ", "
		}
		list += name
	}
	return list
}

func (box *Box) ToLines() []string {
	result := []string{}
	if len(box.in) == 0 {
		result = append(result, "EMPTY")
	} else {
		labels := []string{}
		items := []*LabelledItem{}
		label := ""
		for key, val := range box.in {
			label = box.kinds.GetLabel(key)
			labels = append(labels, label)
			items = append(items, MakeLabelledItem(label, val))
		}
		sort.Strings(labels)
		for i, label := range labels {
			result = append(result,
				fmt.Sprintf("%-30s: %v", label, items[i].Item()))
		}
	}
	return result
}

/*
// Relabels an unknown item.  Does nothing if there is no such item.
func (box *Box) RelabelUnknown(key CODE) *Box {
	obj, exists := box.in[BOX_ITEM_UNKNOWN]
	if exists {
		box.in[key] = obj
		delete(box.in, BOX_ITEM_UNKNOWN)
	}
	return box
}
*/

// BOXMAP ======================================================================

/*
type BoxMap struct {
	in map[string]*Box
}

// Constructors
func NewBoxMap() *BoxMap {
	return &BoxMap{
		in: make(map[string]*Box),
	}
}

func (bmap *BoxMap) IsEmpty() bool { return len(bmap.in) == 0 }

func (bmap *BoxMap) Add(key string, thing interface{}) *BoxMap {
	bmap.in[key] = MakeBox(thing)
	return bmap
}

func (bmap *BoxMap) GetBox(key string) *Box {
	box, present := bmap.in[key]
	if present {
		return box
	} else {
		return EMPTY_BOX
	}
}
*/
