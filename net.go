/*
	TCP port lock.
*/
package h2go

import (
	"net"
	"net/url"
	"os"
	"strconv"
	"time"
)

func NewUrl() url.URL {
	return url.URL{
		User: url.User(""),
	}
}

func GetLocalAddress() (string, *Goof) {
	name, err := os.Hostname()
	if IsError(err) {
		return "", Env.Goofs().WrapGoof("HOST_NAME", err,
			"While trying to get local hostname")
	}

	addrs, err := net.LookupHost(name)
	if IsError(err) {
		return "", Env.Goofs().WrapGoof("NETWORK_ADDRESS", err,
			"While trying to look up address from local hostname")
	}
	if len(addrs) == 0 {
		return "", Env.Goofs().MakeGoof("NETWORK_ADDRESS",
			"Could not obtain local IP address")
	}

	return addrs[0], NO_GOOF
}

// PORTLOCK ====================================================================

// A port lock is more portable than a file lock.
// credit: Tamás-Gulácsi
// http://grokbase.com/t/gg/golang-nuts/1375bhjw61/go-nuts-waiting-for-file-lock

type PortLock struct {
	hostport string
	listener net.Listener // an interface
}

func NewPortLock(port int) *PortLock {
	return &PortLock{
		hostport: net.JoinHostPort("127.0.0.1", strconv.Itoa(port)),
	}
}

// Lock blocks until no one else is listening on the given port.  A loop checks
// for the given number of milliseconds, which increases geometrically.
func Lock(port, checkMillis, maxTries int) (*PortLock, *Goof) {
	plock := NewPortLock(port)
	var err error
	t := 1 * time.Millisecond
	c := 0
	for {
		plock.listener, err = net.Listen("tcp", plock.hostport)
		if IsError(err) {
			time.Sleep(t)
			t = time.Duration(float32(t) * 1.2)
			c++
			if c >= maxTries {
				return plock, Env.Goofs().MakeGoof("COULD_NOT_GET_PORT_LOCK",
					"Could not get lock for port %d after %d tries",
					port, maxTries)
			}
		} else {
			return plock, NO_GOOF
		}
	}
}

// Unlock clears the listener immediately, allowing someone else to
// create a lock.
func (plock *PortLock) Unlock() {
	if plock.listener != nil {
		plock.listener.Close()
	}
	return
}
