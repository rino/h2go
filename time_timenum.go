/*
	Immutable boxed number representing a time scale.
*/
package h2go

import (
	"fmt"
	"math/big"
	"time"
)

type TimeNum interface {
	Value() IntBox
	Time
}

// TimeNum types (boxes of boxes).

//== UNIX TIME ================================================================
// Unix time, whole secs since 1 Jan, year 1970, UTC
type UnixTime struct {
	val IntBox // secs
	*State
}

var _ TimeNum = new(UnixTime)

func NewUnixTime() *UnixTime {
	return &UnixTime{
		val:   NewInt8(),
		State: NewState(),
	}
}

func MakeUnixTime(ti IntBox) *UnixTime {
	tn := NewUnixTime()
	tn.val = ti
	if !ti.IsEmpty() {
		tn.NotEmpty()
		tn.NowComplete()
	}
	return tn
}

func MakeUnixTimeFromInt64(t int64) *UnixTime {
	return MakeUnixTime(MakeInt64(t))
}

// Satisfy TimeNum interface.
func (t *UnixTime) Value() IntBox { return t.val }

// Use existing time package conversion for testing.
func (t *UnixTime) ToCalClock2(cal Calendar) (*CalClock, *Goof) {
	ti64, _ := t.val.AsGoInt64()
	gt := time.Unix(ti64, 0)
	y, mth, d := gt.Date()
	h, min, s := gt.Clock()
	return BuildCalClock(
		y,
		uint8(mth),
		uint8(d),
		uint8(h),
		uint8(min),
		uint8(s),
		ZeroFrac(),
		cal)
}

// Satisfy Time interface.
func (t *UnixTime) String() string { return t.val.String() }
func (t *UnixTime) Copy() Time {
	t2 := NewUnixTime()
	t2.val = t.val
	t2.State = t.State.Copy()
	return t2
}
func (t *UnixTime) ToCalClock(cal Calendar) (*CalClock, *Goof) {
	return cal.CalClockFromNum(t)
}
func (t *UnixTime) ToNum(tn TimeNum) (TimeNum, *Goof) {
	switch to := tn.(type) {
	case *UnixTime:
		return MakeUnixTime(t.val), NO_GOOF
	case *JavaTime:
		return MakeJavaTime(t.val.MultByInt(int(MillisPerSec))), NO_GOOF
	case *NanoTime:
		return MakeNanoTime(t.val.MultByInt(int(NanosPerSec))), NO_GOOF
	case *GoTime:
		ti64, _ := t.val.AsGoInt64()
		tt := time.Unix(ti64, 0)
		return MakeGoTime(tt), NO_GOOF
	case *JulianDay:
		return NewJulianDay(), Env.Goofs().MakeGoof("BAD_TYPE",
			"Type %T conversion not currently implemented", to)
	}
	return NewGoTime(),
		Env.Goofs().MakeGoof("BAD_TYPE", "Type %T not recognised", tn)
}
func (t *UnixTime) PlusInt(i int) (Time, *Goof) {
	tc := MakeUnixTime(t.val)
	tc.val = tc.val.PlusInt(i)
	return tc, NO_GOOF
}
func (t *UnixTime) Equals(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().Equals(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *UnixTime) IsBefore(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().LessThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *UnixTime) IsAfter(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().MoreThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *UnixTime) OrEarlier(other Time) (Time, *Goof) {
	if before, goof := t.IsBefore(other); before {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *UnixTime) OrLater(other Time) (Time, *Goof) {
	if after, goof := t.IsAfter(other); after {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *UnixTime) At(other *Location) (Time, *Goof) {
	tnew := MakeUnixTime(t.val)
	ti := tnew.val
	ti = ti.PlusInt(other.Offset(t))
	tnew.val = ti
	return tnew, NO_GOOF
}
func (t *UnixTime) Location() *Location { return UTC }
func (t *UnixTime) IsLocalTime() bool   { return false }

func (t *UnixTime) cast(other Time) (*UnixTime, bool, *Goof) {
	to, isa := other.(*UnixTime) // Attempt faster internal cast first...
	if !isa {                    // and convert if necessary
		t2, goof := other.ToNum(new(UnixTime))
		to, _ = t2.(*UnixTime)
		if IsError(goof) {
			return to, isa, goof
		}
	}
	return to, isa, NO_GOOF
}

//== JAVA TIME ================================================================
// Java time, millisecs since 1 Jan, year 1970, UTC
type JavaTime struct {
	// millisecs, Int64 offers ~584,000,000 year range (Int32 inadequate)
	val IntBox
	*State
}

var _ TimeNum = new(JavaTime)

func NewJavaTime() *JavaTime {
	return &JavaTime{
		val:   NewInt8(),
		State: NewState(),
	}
}

func MakeJavaTime(ti IntBox) *JavaTime {
	tn := NewJavaTime()
	tn.val = ti
	if !ti.IsEmpty() {
		tn.NotEmpty()
		tn.NowComplete()
	}
	return tn
}

// Satisfy TimeNum interface.
func (t *JavaTime) Value() IntBox { return t.val }

// Use existing time package conversion for testing.
func (t *JavaTime) ToCalClock2(cal Calendar) (*CalClock, *Goof) {
	ti64, _ := t.val.AsGoInt64()
	sec := ti64 / MillisPerSec
	ms := ti64 % MillisPerSec
	gt := time.Unix(sec, 0)
	y, mth, d := gt.Date()
	h, min, s := gt.Clock()
	return BuildCalClock(
		y,
		uint8(mth),
		uint8(d),
		uint8(h),
		uint8(min),
		uint8(s),
		MakeMilliFrac(int(ms)),
		cal)
}

// Satisfy Time interface.
func (t *JavaTime) String() string { return t.val.String() }
func (t *JavaTime) Copy() Time {
	t2 := NewJavaTime()
	t2.val = t.val
	t2.State = t.State.Copy()
	return t2
}
func (t *JavaTime) ToCalClock(cal Calendar) (*CalClock, *Goof) {
	return cal.CalClockFromNum(t)
}
func (t *JavaTime) ToNum(tn TimeNum) (TimeNum, *Goof) {
	switch to := tn.(type) {
	case *UnixTime:
		return MakeUnixTime(t.val.DivByInt(int(MillisPerSec))), NO_GOOF
	case *JavaTime:
		return MakeJavaTime(t.val), NO_GOOF
	case *NanoTime:
		return MakeNanoTime(t.val.MultByInt(int(NanosPerMilli))), NO_GOOF
	case *GoTime:
		ti64, _ := t.val.AsGoInt64()
		tt := time.Unix(ti64/MillisPerSec, 0)
		return MakeGoTime(tt), NO_GOOF
	case *JulianDay:
		return NewJulianDay(), Env.Goofs().MakeGoof("BAD_TYPE",
			"Type %T conversion not currently implemented", to)
	}
	return NewGoTime(),
		Env.Goofs().MakeGoof("BAD_TYPE", "Type %T not recognised", tn)
}
func (t *JavaTime) PlusInt(i int) (Time, *Goof) {
	tc := MakeJavaTime(t.val)
	tc.val = tc.val.PlusInt(i)
	return tc, NO_GOOF
}
func (t *JavaTime) Equals(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().Equals(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *JavaTime) IsBefore(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().LessThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *JavaTime) IsAfter(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().MoreThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *JavaTime) OrEarlier(other Time) (Time, *Goof) {
	if before, goof := t.IsBefore(other); before {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *JavaTime) OrLater(other Time) (Time, *Goof) {
	if after, goof := t.IsAfter(other); after {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *JavaTime) At(other *Location) (Time, *Goof) {
	tnew := MakeJavaTime(t.val)
	ti := tnew.val
	ti = ti.PlusInt(other.Offset(t))
	tnew.val = ti
	return tnew, NO_GOOF
}
func (t *JavaTime) Location() *Location { return UTC }
func (t *JavaTime) IsLocalTime() bool   { return false }

func (t *JavaTime) cast(other Time) (*JavaTime, bool, *Goof) {
	to, isa := other.(*JavaTime) // Attempt faster internal cast first...
	if !isa {                    // and convert if necessary
		t2, goof := other.ToNum(new(JavaTime))
		to, _ = t2.(*JavaTime)
		if IsError(goof) {
			return to, isa, goof
		}
	}
	return to, isa, NO_GOOF
}

//== NANO TIME ================================================================
// Nano unix time, nanosecs since 1 Jan, year 1970, UTC
type NanoTime struct {
	val IntBox // nanosecs, Int64 offers 584 year range
	*State
}

var _ TimeNum = new(NanoTime)

func NewNanoTime() *NanoTime {
	return &NanoTime{
		val:   NewInt8(),
		State: NewState(),
	}
}

func MakeNanoTime(ti IntBox) *NanoTime {
	tn := NewNanoTime()
	tn.val = ti
	if !ti.IsEmpty() {
		tn.NotEmpty()
		tn.NowComplete()
	}
	return tn
}

// Satisfy TimeNum interface.
func (t *NanoTime) Value() IntBox { return t.val }

// Use existing time package conversion for testing.
func (t *NanoTime) ToCalClock2(cal Calendar) (*CalClock, *Goof) {
	ti64, _ := t.val.AsGoInt64()
	sec := ti64 / NanosPerSec
	ns := ti64 % NanosPerSec
	gt := time.Unix(sec, 0)
	y, mth, d := gt.Date()
	h, min, s := gt.Clock()
	return BuildCalClock(
		y,
		uint8(mth),
		uint8(d),
		uint8(h),
		uint8(min),
		uint8(s),
		MakeNanoFrac(int(ns)),
		cal)
}

// Satisfy Time interface.
func (t *NanoTime) String() string { return t.val.String() }
func (t *NanoTime) Copy() Time {
	t2 := NewNanoTime()
	t2.val = t.val
	t2.State = t.State.Copy()
	return t2
}
func (t *NanoTime) ToCalClock(cal Calendar) (*CalClock, *Goof) {
	return cal.CalClockFromNum(t)
}
func (t *NanoTime) ToNum(tn TimeNum) (TimeNum, *Goof) {
	switch to := tn.(type) {
	case *UnixTime:
		return MakeUnixTime(t.val.DivByInt(int(NanosPerSec))), NO_GOOF
	case *JavaTime:
		return MakeJavaTime(t.val.DivByInt(int(NanosPerMilli))), NO_GOOF
	case *NanoTime:
		return MakeNanoTime(t.val), NO_GOOF
	case *GoTime:
		ti64, _ := t.val.AsGoInt64()
		tt := time.Unix(ti64/NanosPerSec, 0)
		return MakeGoTime(tt), NO_GOOF
	case *JulianDay:
		return NewJulianDay(), Env.Goofs().MakeGoof("BAD_TYPE",
			"Type %T conversion not currently implemented", to)
	}
	return NewGoTime(),
		Env.Goofs().MakeGoof("BAD_TYPE", "Type %T not recognised", tn)
}
func (t *NanoTime) PlusInt(i int) (Time, *Goof) {
	tc := MakeNanoTime(t.val)
	tc.val = tc.val.PlusInt(i)
	return tc, NO_GOOF
}
func (t *NanoTime) Equals(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().Equals(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *NanoTime) IsBefore(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().LessThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *NanoTime) IsAfter(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().MoreThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *NanoTime) OrEarlier(other Time) (Time, *Goof) {
	if before, goof := t.IsBefore(other); before {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *NanoTime) OrLater(other Time) (Time, *Goof) {
	if after, goof := t.IsAfter(other); after {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *NanoTime) At(other *Location) (Time, *Goof) {
	tnew := MakeNanoTime(t.val)
	ti := tnew.val
	ti = ti.PlusInt(other.Offset(t))
	tnew.val = ti
	return tnew, NO_GOOF
}
func (t *NanoTime) Location() *Location { return UTC }
func (t *NanoTime) IsLocalTime() bool   { return false }

func (t *NanoTime) cast(other Time) (*NanoTime, bool, *Goof) {
	to, isa := other.(*NanoTime) // Attempt faster internal cast first...
	if !isa {                    // and convert if necessary
		t2, goof := other.ToNum(new(NanoTime))
		to, _ = t2.(*NanoTime)
		if IsError(goof) {
			return to, isa, goof
		}
	}
	return to, isa, NO_GOOF
}

//== GO TIME =================================================================
// Go time, nanoseconds since 1 Jan, year 1, UTC.
type GoTime struct {
	val time.Time
	*State
}

var _ TimeNum = new(GoTime)

func NewGoTime() *GoTime {
	return &GoTime{
		val:   time.Unix(0, 0),
		State: NewState(),
	}
}

func MakeGoTime(tt time.Time) *GoTime {
	tn := NewGoTime()
	tn.val = tt
	tn.NotEmpty()
	tn.NowComplete()
	return tn
}

// Satisfy TimeNum interface.
func (t *GoTime) Value() IntBox {
	// Unlike the other time numbers, Go time is stored in two numbers,
	// an int64 for the seconds and int32 for nanoseconds.  We use a BigInt
	// as a container for the resolved nanoseconds from the Go Epoch (1 Jan,
	// year 1).  This is easily transformed to other forms via the box package.
	secsInNanos := MakeBigInt(big.NewInt(t.val.Unix() - GoTimeOffset))
	// time.Time to Unix time in nanoseconds
	return secsInNanos.PlusInt(int(t.val.Nanosecond()))
}

// Use existing time package conversion for testing.
func (t *GoTime) ToCalClock2(cal Calendar) (*CalClock, *Goof) {
	gt := t.val
	y, mth, d := gt.Date()
	h, min, s := gt.Clock()
	return BuildCalClock(
		y,
		uint8(mth),
		uint8(d),
		uint8(h),
		uint8(min),
		uint8(s),
		ZeroFrac(),
		cal)
}

// Satisfy Time interface.
func (t *GoTime) String() string { return fmt.Sprintf("%v", t.val) }
func (t *GoTime) Copy() Time {
	t2 := NewGoTime()
	t2.val = t.val
	t2.State = t.State.Copy()
	return t2
}
func (t *GoTime) ToCalClock(cal Calendar) (*CalClock, *Goof) {
	y, mth, d := t.val.Date()
	h, m, s := t.val.Clock()
	f := t.val.Nanosecond()
	return BuildCalClock(
		y,
		uint8(mth),
		uint8(d),
		uint8(h),
		uint8(m),
		uint8(s),
		MakeNanoFrac(f),
		cal)
}
func (t *GoTime) ToNum(tn TimeNum) (TimeNum, *Goof) {
	switch to := tn.(type) {
	case *UnixTime:
		return MakeUnixTime(MakeInt64(t.val.Unix())), NO_GOOF
	case *JavaTime:
		secsInMillis := MakeBigInt(big.NewInt(t.val.Unix())).
			MultByInt(int(MillisPerSec))
		secsInMillis = secsInMillis.
			PlusInt(t.val.Nanosecond() / int(NanosPerMilli))
		return MakeJavaTime(secsInMillis), NO_GOOF
	case *NanoTime:
		secsInNanos := MakeBigInt(big.NewInt(t.val.Unix())).
			MultByInt(int(NanosPerSec))
		secsInNanos = secsInNanos.PlusInt(t.val.Nanosecond())
		return MakeNanoTime(secsInNanos), NO_GOOF
	case *GoTime:
		return MakeGoTime(t.val), NO_GOOF
	case *JulianDay:
		return NewJulianDay(), Env.Goofs().MakeGoof("BAD_TYPE",
			"Type %T conversion not currently implemented", to)
	}
	return NewGoTime(),
		Env.Goofs().MakeGoof("BAD_TYPE", "Type %T not recognised", tn)
}
func (t *GoTime) PlusInt(i int) (Time, *Goof) {
	tc := MakeGoTime(t.val)
	tt := tc.val.Add(time.Duration(i)) // nanos
	tc.val = tt
	return tc, NO_GOOF
}
func (t *GoTime) Equals(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().Equals(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *GoTime) IsBefore(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().LessThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *GoTime) IsAfter(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().MoreThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *GoTime) OrEarlier(other Time) (Time, *Goof) {
	if before, goof := t.IsBefore(other); before {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *GoTime) OrLater(other Time) (Time, *Goof) {
	if after, goof := t.IsAfter(other); after {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *GoTime) At(other *Location) (Time, *Goof) {
	tnew := MakeGoTime(t.val)
	tt := tnew.val
	un, _ := t.ToNum(new(UnixTime))
	newtt := tt.Add(time.Duration(other.Offset(un)))
	tnew.val = newtt
	return tnew, NO_GOOF
}
func (t *GoTime) Location() *Location { return UTC }
func (t *GoTime) IsLocalTime() bool   { return false }

func (t *GoTime) cast(other Time) (*GoTime, bool, *Goof) {
	to, isa := other.(*GoTime) // Attempt faster internal cast first...
	if !isa {                  // and convert if necessary
		t2, goof := other.ToNum(new(GoTime))
		to, _ = t2.(*GoTime)
		if IsError(goof) {
			return to, isa, goof
		}
	}
	return to, isa, NO_GOOF
}

//== JULIAN DAY ===============================================================
// Julian day number http://en.wikipedia.org/wiki/Julian_day
// Days since 24 Nov 4714 BC
type JulianDay struct {
	val IntBox // days
	*State
}

var _ TimeNum = new(JulianDay)

func NewJulianDay() *JulianDay {
	return &JulianDay{
		val:   NewInt8(),
		State: NewState(),
	}
}

func MakeJulianDay(ti IntBox) *JulianDay {
	tn := NewJulianDay()
	tn.val = ti
	if !ti.IsEmpty() {
		tn.NotEmpty()
		tn.NowComplete()
	}
	return tn
}

// Satisfy TimeNum interface.
func (t *JulianDay) Value() IntBox { return t.val }

// Satisfy Time interface.
func (t *JulianDay) String() string { return t.val.String() }
func (t *JulianDay) Copy() Time {
	t2 := NewJulianDay()
	t2.val = t.val
	t2.State = t.State.Copy()
	return t2
}
func (t *JulianDay) ToCalClock(cal Calendar) (*CalClock, *Goof) {
	date, goof := t.ToDate(cal)
	return MakeCalClock(date, Midday), goof
}
func (t *JulianDay) ToNum(tn TimeNum) (TimeNum, *Goof) {
	switch to := tn.(type) {
	case *JulianDay:
		return MakeJulianDay(t.val), NO_GOOF
	case *UnixTime, *JavaTime, *NanoTime, *GoTime:
		return NewGoTime(), Env.Goofs().MakeGoof("BAD_TYPE",
			"Type %T conversion not currently implemented", to)
	}
	return NewGoTime(),
		Env.Goofs().MakeGoof("BAD_TYPE", "Type %T not recognised", tn)
}
func (t *JulianDay) PlusInt(i int) (Time, *Goof) {
	tc := MakeJulianDay(t.val)
	tc.val = tc.val.PlusInt(i)
	return tc, NO_GOOF
}
func (t *JulianDay) Equals(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().Equals(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *JulianDay) IsBefore(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().LessThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *JulianDay) IsAfter(other Time) (bool, *Goof) {
	to, isa, goof := t.cast(other)
	if isa || !IsError(goof) {
		return t.Value().MoreThan(to.Value()), NO_GOOF
	}
	return false, goof
}
func (t *JulianDay) OrEarlier(other Time) (Time, *Goof) {
	if before, goof := t.IsBefore(other); before {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *JulianDay) OrLater(other Time) (Time, *Goof) {
	if after, goof := t.IsAfter(other); after {
		return t, goof
	} else {
		return other, goof
	}
}
func (t *JulianDay) At(other *Location) (Time, *Goof) {
	return t, NO_GOOF // TODO finish me?
}
func (t *JulianDay) Location() *Location { return UTC }
func (t *JulianDay) IsLocalTime() bool   { return false }

func (t *JulianDay) cast(other Time) (*JulianDay, bool, *Goof) {
	to, isa := other.(*JulianDay) // Attempt faster internal cast first...
	if !isa {                     // and convert if necessary
		t2, goof := other.ToNum(new(JulianDay))
		to, _ = t2.(*JulianDay)
		if IsError(goof) {
			return to, isa, goof
		}
	}
	return to, isa, NO_GOOF
}

// Special methods.
func (t *JulianDay) ToDate(cal Calendar) (*Date, *Goof) {
	return cal.DateFromNum(t)
}
