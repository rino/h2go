package h2go

import (
	"fmt"
	"math/big"
	"reflect"
	"testing"
)

func TestTypeVerification(t *testing.T) {
	Env.Log().MajorSection("Test Field type detection")
	flist := Env.Types().BuildFieldList(1, uint8(2), UINT8(3), true)
	expected := []string{"int64", "uint8", "UINT8", "TRUE"}
	if !flist.AreTypes(expected...) {
		t.Fatalf("Expected %q, got %q", expected, flist.TypesAsStringSlice())
	}
	expected = []string{"int64", "uint8", "UINT8", "tag:boolean"}
	if !flist.AreTypes(expected...) {
		t.Fatalf("Failed to verify %q as a %q", "TRUE", "tag:boolean")
	}
}

func TestAutoField(t *testing.T) {
	Env.Log().MajorSection("Test automatic Field creation")
	f1 := Env.Types().AutoField("test")
	if Env.Types().Name(f1.Type()) != "STRING" {
		t.Fatalf("Type is %s but should be STRING",
			Env.Types().Name(f1.Type()))
	}
}

func TestFieldEquality(t *testing.T) {
	Env.Log().MajorSection("Test Field equality")
	var v1 interface{} = uint8(3)
	f1 := Env.Types().AutoField(v1)
	f2 := MakeField(v1, Env.Types().Number["uint8"])
	if !f1.Equals(f2) {
		t.Fatalf("Fields %v and %v should be equal",
			f1, f2)
	}
	v1 = UINT16(3)
	f1 = Env.Types().AutoField(v1)
	f2 = MakeField(v1, Env.Types().Number["UINT16"])
	if !f1.Equals(f2) {
		t.Fatalf("Fields %v and %v should be equal",
			f1, f2)
	}
}

func TestFieldListEquality(t *testing.T) {
	Env.Log().MajorSection("Test FieldList equality")
	flist1 := Env.Types().MintFieldList()
	flist2 := Env.Types().MintFieldList()
	if !flist1.Equals(flist2) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist2)
	}
}

func TestBinaryEncodeIntField(t *testing.T) {
	Env.Log().MajorSection("Test binary encode/decode of integer Field")
	var v1 interface{} = int16(-1234)
	f1 := Env.Types().AutoField(v1)
	bfr := MakeByteBuffer(BYTEORDER)
	out, goof := f1.EncodeBinary(bfr, EMPTY_BOX)
	if Env.Log().IsError(goof) {
		t.Fatalf("Error encoding Field %v", f1)
	}
	expected := out.Len(0)

	// Field decoder assumes type code has been snipped off
	code, byts, goof := bfr.ReadCode(EMPTY_BOX)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem reading %T type code", f1)
	}
	if code != f1.Type() {
		t.Fatalf("Type code for FieldMap should be %d not %d",
			FIELDMAP_TYPE_CODE, code)
	}

	f2, goof := Env.Types().MintField(code)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem minting %T", f2)
	}
	out2, goof2 := f2.DecodeBinary(bfr, EmptyBox())
	if Env.Log().IsError(goof2) {
		b1, _ := bfr.Bytes()
		t.Fatalf("Error decoding bytes % x", b1)
	}
	l2 := len(byts) + out2.Len(0)
	if l2 != expected {
		Env.Log().Dump(out.EncodingToStringSlice(100), "Encoding")
		Env.Log().Dump(out2.EncodingToStringSlice(100), "Decoding of Encoding")
		t.Fatalf("Expected decoded size of %d, got %d", expected, l2)
	}
	if !f2.Equals(f1) {
		t.Fatalf("Expected %T %v, got decoded %v",
			f1, f1, f2)
	}
}

func TestBinaryEncodeStringField(t *testing.T) {
	Env.Log().MajorSection("Test binary encode/decode of string Field")
	var v1 interface{} = "This is a test."
	f1 := Env.Types().AutoField(v1)
	bfr := MakeByteBuffer(BYTEORDER)
	out, goof := f1.EncodeBinary(bfr, EMPTY_BOX)
	if Env.Log().IsError(goof) {
		t.Fatalf("Error encoding Field %v", f1)
	}

	// Field decoder assumes type code has been snipped off
	code, byts, goof := bfr.ReadCode(EMPTY_BOX)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem reading %T type code", f1)
	}
	if code != f1.Type() {
		t.Fatalf("Type code for FieldMap should be %d not %d",
			FIELDMAP_TYPE_CODE, code)
	}

	f2, goof := Env.Types().MintField(code)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem minting %T", f2)
	}
	out2, goof2 := f2.DecodeBinary(bfr, EmptyBox())
	out2.PrependEncoded(byts)
	if Env.Log().IsError(goof2) {
		b1, _ := bfr.Bytes()
		t.Fatalf("Error decoding bytes % x", b1)
	}
	if !out.Equals(out2) {
		Env.Log().Dump(out.EncodingToStringSlice(100), "Encoding")
		Env.Log().Dump(out2.EncodingToStringSlice(100), "Decoding of Encoding")
		t.Fatalf("Expected decoding of encoding did not match original encoding")
	}
	if !f2.Equals(f1) {
		t.Fatalf("Encoded value %v and decoded value %v should be equal",
			f1, f2)
	}
}

func TestBinaryEncodeFieldMapAndList(t *testing.T) {
	Env.Log().MajorSection("Test binary encode/decode of FieldMap and FieldList")
	fl1 := Env.Types().BuildFieldList(int(45), "whattha")

	fm1 := Env.Types().MintFieldMap()
	fm1.Set(Env.Types().AutoField("hello"), MakeField(uint8(3), Env.Types().Number["UINT8"]))
	fm1.Set(Env.Types().AutoKeyVal(uint32(4), "goodbye"))

	fm2 := Env.Types().MintFieldMap()
	fm2.Set(Env.Types().AutoKeyVal(int32(-24234), true))
	fm2.Set(Env.Types().AutoKeyVal("key", "val"))

	fl1.Add(Env.Types().AutoField(fm2))

	fm1.Set(Env.Types().AutoKeyVal("list", fl1))
	fm1.Set(Env.Types().AutoKeyVal("map", fm2))

	bfr := MakeByteBuffer(BYTEORDER)

	fm := fm2
	// Encode FieldMap
	out1, goof1 := fm.EncodeBinary(bfr, EmptyBox())
	if Env.Log().IsError(goof1) {
		t.Fatalf("Error encoding %T %v", fm1, fm1)
	}

	// Decode FieldMap
	// FieldMap decoder assumes type code has been snipped off
	typ, byts, goof2 := bfr.ReadCode(EmptyBox())
	if Env.Log().IsError(goof2) {
		t.Fatalf("Error decoding type from buffer")
	}
	if typ != Env.Types().Number["FIELDMAP"] {
		t.Fatalf("Encoded type should be %v not %v",
			Env.Types().Number["FIELDMAP"], typ)
	}

	fm3 := Env.Types().MintFieldMap()
	out3, goof3 := fm3.DecodeBinary(bfr, EmptyBox())
	out3.PrependEncoded(byts)
	if Env.Log().IsError(goof3) {
		b1, _ := bfr.Bytes()
		t.Fatalf("Error decoding bytes % x", b1)
	}
	if !out1.Equals(out3) {
		Env.Log().Dump(out1.EncodingToStringSlice(100), "Encoding")
		Env.Log().Dump(out3.EncodingToStringSlice(100), "Decoding of Encoding")
		t.Fatalf("Expected decoding of encoding did not match original encoding")
	}
	if !fm.Equals(fm3) {
		t.Fatalf("Expected %T %v, got decoded %v",
			fm, fm, fm3)
	}
}

func TestBinaryEncodeFieldBox(t *testing.T) {
	Env.Log().MajorSection(
		"Test binary encode/decode of a FieldBox with all " +
			"binary switch combinations")
	v1 := int32(478)
	// Text source http://www.maximumcompression.com/data/text.php
	v2 := `Overview: Argentina, rich in natural resources, benefits also from a highly literate population, an export-oriented agricultural sector, and a diversified industrial base. Nevertheless, following decades of mismanagement and statist policies, the economy in the late 1980s was plagued with huge external debts and recurring bouts of hyperinflation. Elected in 1989, in the depths of recession, President MENEM has implemented a comprehensive economic restructuring program that shows signs of putting Argentina on a path of stable, sustainable growth. Argentina's currency has traded at par with the US dollar since April 1991, and inflation has fallen to its lowest level in 20 years. Argentines have responded to the relative price stability by repatriating flight capital and investing in domestic industry. The economy registered an impressive 6% advance in 1994, fueled largely by inflows of foreign capital and strong domestic consumption spending.`
	//v2 := "This is a test"
	fields := Env.Types().BuildFieldList(v1, v2)
	t0 := big.NewInt(234987)
	clevel := UINT8(7)
	pass := "This is my passphrase"
	fboxparams := NewFieldBoxParams()
	var compressor CompressionScheme
	var encryptor EncryptionScheme
	// Encode/decode paramaters
	for _, cname := range BinaryAlgoCompression.SortedNames() {
		switch cname {
		case "COMPRESSION_GZIP":
			compressor = MakeGzipCompressor(clevel)
		case "COMPRESSION_NONE":
			compressor = NoCompression()
		}
		for _, ename := range BinaryAlgoEncryption.SortedNames() {
			if ename == "ENCRYPTION_RSA_OAEP" {
				// Not used for FieldBox encryption
				continue
			}
			keylen, goof := EncryptionKeyLength(ename)
			if Env.Log().IsError(goof) {
				t.Fatal("Problem getting key length for encryption algo %q",
					ename)
			}
			hash, goof := BuildDefaultPassHash(pass, keylen, EmptyBox())
			if Env.Log().IsError(goof) {
				t.Fatal("Problem creating %T for encryption algo %q",
					hash, ename)
			}

			switch ename {
			case "ENCRYPTION_SECRETBOX":
				encryptor, goof = MakeSecret(hash.Key())
			case "ENCRYPTION_AES_256":
				encryptor, goof = MakeAES_256(hash.Key())
			case "ENCRYPTION_NONE":
				encryptor = NoEncryption()
				goof = NO_GOOF
			}
			if Env.Log().IsError(goof) {
				t.Fatal("Problem creating %T", encryptor)
			}

			fboxparams, goof = BuildFieldBoxParams(
				H2GO_VERSION,
				compressor,
				encryptor)
			if Env.Log().IsError(goof) {
				t.Fatal("Problem creating %T", fboxparams)
			}
			fboxparams.SetTimestamp(t0)

			for j := 0; j < 2; j++ {
				if j == 0 {
					fboxparams.EncodeSwitches().Set("TIMESTAMPING", true)
				} else {
					fboxparams.EncodeSwitches().Set("TIMESTAMPING", false)
				}
				for i, field := range fields.ToSlice() {
					if i == 0 {
						Env.Log().MinorSection("Fixed length Field with %s",
							fboxparams.EncodeSwitches().ValueToString(true))
					} else {
						Env.Log().MinorSection("Variable length Field with %s",
							fboxparams.EncodeSwitches().ValueToString(true))
					}
					// Encode
					bfr := MakeByteBuffer(BYTEORDER)
					fbox1 := BuildFieldBox(field, fboxparams)
					out, goof := fbox1.EncodeBinary(bfr, MakeBox(BoxItemKinds, Env))
					if Env.Log().IsError(goof) {
						t.Fatalf("Problem binary encoding %T", field)
					}
					byts, _ := bfr.Bytes()
					if Env.Log().Level() >= DEBUGLEVEL_DUMP {
						Env.Log().HexDump(
							byts, MAX_HEX_DUMP, fmt.Sprintf("Encoded %T", fbox1))
						Env.Log().Dump(out.EncodingToStringSlice(10000), "Encoded FieldBox")
					}
					size := len(byts)

					// Decode
					// FieldBox decoder assumes type code has been snipped off
					code, _, goof := bfr.ReadCode(EMPTY_BOX)
					if Env.Log().IsError(goof) {
						t.Fatalf("Problem reading %T type code", fbox1)
					}
					if code != FIELDBOX_TYPE_CODE {
						t.Fatalf("Type code for %T should be %d not %d",
							fbox1, FIELDBOX_TYPE_CODE, code)
					}
					// Make a copy
					bfr2, goof := bfr.CopyByteBuffer()
					if Env.Log().IsError(goof) {
						t.Fatalf("Problem copying byte buffer")
					}
					// ..now decode FieldBox proper
					// We need to set a global EncodeSwitches in the FieldBox in case
					// the decoder needs it
					fbox2 := BuildFieldBox(NewField(), fboxparams)
					out2, goof := fbox2.DecodeBinary(bfr, MakeBox(BoxItemKinds, Env))
					if Env.Log().IsError(goof) {
						Env.Log().Dump(out2.EncodingToStringSlice(10000), "Decoded FieldBox")
						t.Fatalf("Problem binary decoding %T", fbox2)
					}
					if !fbox1.Equals(fbox2) {
						t.Fatalf("Expected %T %v, got %v", fbox1, fbox1, fbox2)
					}
					if fboxparams.EncodeSwitches().IsTrue("TIMESTAMPING") &&
						t0.Cmp(fbox2.Params().Timestamp()) != 0 {
						t.Fatalf("Expected timestamp %T %v, got %v",
							t0, t0, fbox2.Params().Timestamp())
					}
					// Test size determination only for FieldBox.
					// If the Box provided to DecodeBinary contains a
					// BOX_ITEM_UINT8 value of DECODE_BINARY_SIZE_ONLY, decoding should cease when the
					// total byte length of the FieldBox, excluding the
					// prefixed (pre-read) type code, have been determined and
					// returned in the Box value BOX_ITEM_UINT64.
					box2 := MakeBox(BoxItemKinds, Env).Set(BOX_ITEM_UINT8, DECODE_BINARY_SIZE_ONLY)
					out2, goof = fbox2.DecodeBinary(bfr2, box2)
					if Env.Log().IsError(goof) {
						t.Fatalf("Problem determining binary size of %T", fbox2)
					}
					ui64, _ := box2.Inside(BOX_ITEM_UINT64).(uint64)
					size2 := int(ui64) + CODE_BYTE_LENGTH
					if size != size2 {
						Env.Log().Dump(out.EncodingToStringSlice(100),
							"Encoding with switches %v", fboxparams.EncodeSwitches())
						Env.Log().Dump(out2.EncodingToStringSlice(100), "Decoding")
						t.Fatalf("Expected FieldBox size %d, got %d", size, size2)
					}
				}
			}
		}
	}
}

func TestPrintingFieldMap(t *testing.T) {
	Env.Log().MajorSection("Test printing of FieldMap")
	fmap1 := Env.Types().MintFieldMap()
	fmap2 := Env.Types().MintFieldMap()
	fmap1.AutoSet("MyField11", 16.3)
	fmap2.AutoSet("MyField21", uint8(3))
	fmap2.AutoSet("MyField21", fmap1)
	expected := `{"MyField21":{"MyField11":(16.3,float64)}}`
	if fmap2.String() != expected {
		t.Fatalf("Expected %s but got %s", expected, fmap2.String())
	}
}

func TestFieldListInsertionDeletion(t *testing.T) {
	Env.Log().MajorSection("Test FieldList in-place insertion and deletion")
	list0 := []interface{}{10, 20, 30, 40}
	flist0 := Env.Types().BuildFieldList(list0...)
	// append 50 to end
	f0 := Env.Types().AutoField(50)
	flist1 := flist0.Copy()
	goof := flist1.Insert(f0, flist1.Len())
	if Env.Log().IsError(goof) {
		t.Fatal("Error inserting %f", f0)
	}
	flist2 := flist0.Copy().Add(f0)
	if !flist1.Equals(flist2) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist2)
	}
	// insert 0 at start
	f0 = Env.Types().AutoField(0)
	flist1 = flist0.Copy()
	goof = flist1.Insert(f0, 0)
	if Env.Log().IsError(goof) {
		t.Fatal("Error inserting %f", f0)
	}
	list3 := []interface{}{0, 10, 20, 30, 40}
	flist3 := Env.Types().BuildFieldList(list3...)
	if !flist1.Equals(flist3) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist3)
	}
	// insert 15 btwn 10 and 20
	f0 = Env.Types().AutoField(15)
	flist1 = flist0.Copy()
	goof = flist1.Insert(f0, 1)
	if Env.Log().IsError(goof) {
		t.Fatal("Error inserting %f", f0)
	}
	list3 = []interface{}{10, 15, 20, 30, 40}
	flist3 = Env.Types().BuildFieldList(list3...)
	if !flist1.Equals(flist3) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist3)
	}
	// insert 25 btwn 20 and 30
	f0 = Env.Types().AutoField(25)
	flist1 = flist0.Copy()
	goof = flist1.Insert(f0, 2)
	if Env.Log().IsError(goof) {
		t.Fatal("Error inserting %f", f0)
	}
	list3 = []interface{}{10, 20, 25, 30, 40}
	flist3 = Env.Types().BuildFieldList(list3...)
	if !flist1.Equals(flist3) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist3)
	}
	// insert 35 btwn 30 and 40
	f0 = Env.Types().AutoField(35)
	flist1 = flist0.Copy()
	goof = flist1.Insert(f0, 3)
	if Env.Log().IsError(goof) {
		t.Fatal("Error inserting %f", f0)
	}
	list3 = []interface{}{10, 20, 30, 35, 40}
	flist3 = Env.Types().BuildFieldList(list3...)
	if !flist1.Equals(flist3) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist3)
	}
	// insert 35 btwn 30 and 40, with negative index
	f0 = Env.Types().AutoField(35)
	flist1 = flist0.Copy()
	goof = flist1.Insert(f0, -1)
	if Env.Log().IsError(goof) {
		t.Fatal("Error inserting %f", f0)
	}
	list3 = []interface{}{10, 20, 30, 35, 40}
	flist3 = Env.Types().BuildFieldList(list3...)
	if !flist1.Equals(flist3) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist3)
	}
	// delete 10
	flist1 = flist0.Copy()
	goof = flist1.Delete(0)
	if Env.Log().IsError(goof) {
		t.Fatal("Error deleting %f", f0)
	}
	list3 = []interface{}{20, 30, 40}
	flist3 = Env.Types().BuildFieldList(list3...)
	if !flist1.Equals(flist3) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist3)
	}
	// delete 20
	flist1 = flist0.Copy()
	goof = flist1.Delete(1)
	if Env.Log().IsError(goof) {
		t.Fatal("Error deleting %f", f0)
	}
	list3 = []interface{}{10, 30, 40}
	flist3 = Env.Types().BuildFieldList(list3...)
	if !flist1.Equals(flist3) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist3)
	}
	// delete 30
	flist1 = flist0.Copy()
	goof = flist1.Delete(-2)
	if Env.Log().IsError(goof) {
		t.Fatal("Error deleting %f", f0)
	}
	list3 = []interface{}{10, 20, 40}
	flist3 = Env.Types().BuildFieldList(list3...)
	if !flist1.Equals(flist3) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist3)
	}
	// delete 40
	flist1 = flist0.Copy()
	goof = flist1.Delete(-1)
	if Env.Log().IsError(goof) {
		t.Fatal("Error deleting %f", f0)
	}
	list3 = []interface{}{10, 20, 30}
	flist3 = Env.Types().BuildFieldList(list3...)
	if !flist1.Equals(flist3) {
		t.Fatalf("FieldLists %v and %v should be equal",
			flist1, flist3)
	}
}

func TestBasicFieldMapToFromStruct(t *testing.T) {
	Env.Log().MajorSection("Test converting simple FieldMap to/from struct")
	type Struct struct {
		F1 float64
		F2 string
		F3 uint8
		F4 *Field
		F5 *FieldList
		F6 bool
	}

	field := Env.Types().AutoField("bye")
	flist := Env.Types().BuildFieldList(1, 2.3, "hi")
	structExpected := &Struct{
		F1: 2.02,
		F2: "hello",
		F3: uint8(4),
		F4: field,
		F5: flist,
		F6: true,
	}

	fmapExpected := Env.Types().MintFieldMap()
	fmapExpected.AutoSet("F1", 2.02)
	fmapExpected.AutoSet("F2", "hello")
	fmapExpected.AutoSet("F3", uint8(4))
	fmapExpected.AutoSet("F4", field)
	fmapExpected.AutoSet("F5", flist)
	fmapExpected.AutoSet("F6", true)

	fmapResult := Env.Types().MintFieldMap()
	valDiff, structDiff := fmapResult.FromStruct(structExpected, true)
	if !fmapResult.Equals(fmapExpected) {
		t.Fatalf("FromStruct: Expected %v but got %v, FromStruct indicated "+
			"valDiff = %v and structDiff = %v",
			fmapExpected, fmapResult, valDiff, structDiff)
	}

	structResult := &Struct{
		F4: NewField(),
		F5: Env.Types().MintFieldList(),
	}
	goof := NO_GOOF
	valDiff, structDiff, goof = fmapExpected.ToStruct(structResult)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem converting %T %v to struct %#v",
			fmapExpected, fmapExpected, structResult)
	}
	if !reflect.DeepEqual(structExpected, structResult) {
		t.Fatalf("ToStruct: Expected %v but got %v, ToStruct indicated "+
			"valDiff = %v and structDiff = %v",
			structExpected, structResult, valDiff, structDiff)
	}
}

func TestStructFieldMapSync(t *testing.T) {
	Env.Log().MajorSection("Test value detection when converting FieldMap to struct")
	type StructD1 struct {
		fd1 float64
	}
	type StructB2 struct {
		FB2 int32
	}
	type StructB1 struct {
		FB1 string
		*StructB2
		*StructD1
	}
	type StructC2 struct {
	}
	type StructC1 struct {
		*StructC2
		FC1 uint8
	}
	type StructA1 struct {
		*StructB1
		*StructC1
	}

	d1 := &StructD1{}
	c2 := &StructC2{}
	b2 := &StructB2{int32(2)}
	c1 := &StructC1{c2, uint8(58)}
	b1 := &StructB1{"hello", b2, d1}
	a1 := &StructA1{b1, c1}

	result := Env.Types().MintFieldMap()
	_, structDiff := result.FromStruct(a1, true)
	if !structDiff {
		t.Fatalf("Expected a structural diff when copying from struct")
	}

	fmD1 := Env.Types().MintFieldMap()
	fmB2 := Env.Types().MintFieldMap()
	fmB2.AutoSet("FB2", int32(2))
	fmB1 := Env.Types().MintFieldMap()
	fmB1.AutoSet("FB1", "hello")
	fmB1.AutoSet("StructB2", fmB2)
	fmB1.AutoSet("StructD1", fmD1)
	fmC2 := Env.Types().MintFieldMap()
	fmC1 := Env.Types().MintFieldMap()
	fmC1.AutoSet("StructC2", fmC2)
	fmC1.AutoSet("FC1", uint8(58))
	expected := Env.Types().MintFieldMap()
	expected.AutoSet("StructB1", fmB1)
	expected.AutoSet("StructC1", fmC1)

	if !result.Equals(expected) {
		Env.Log().Advise("Expected:")
		Env.Log().Advise("%v", expected)
		Env.Log().Advise("Result:")
		Env.Log().Advise("%v", result)
		t.Fatalf("Result differs from expected")
	}

	// Make some changes
	fmC1.AutoSet("FC1", uint8(23))
	fmB1.AutoSet("FB1", "goodbye")

	a01 := *a1
	valDiff, _, goof := expected.ToStruct(a1)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem converting %T %v to struct %#v",
			expected, expected, a1)
	}
	if !valDiff {
		Env.Log().Advise("Before: %#+v", a01)
		Env.Log().Advise("Before: %v", a01.FC1)
		Env.Log().Advise("After : %#+v", a1)
		Env.Log().Advise("After : %v", a1.FC1)
		t.Fatalf("Expected a value diff when copying to struct")
	}
	if b2.FB2 != int32(2) {
		t.Fatalf(
			"b2.FB2 should be unchanged from %v but got %v", int32(2), b2.FB2)
	}
	if b1.FB1 != "goodbye" {
		t.Fatalf(
			"b1.FB1 should have changed to %v but got %v", "goodbye", b1.FB1)
	}
	if c1.FC1 != uint8(23) {
		t.Fatalf(
			"c1.FC1 should have changed to %v but got %v", uint8(23), c1.FC1)
	}
}

func TestNestedFieldMapGetter(t *testing.T) {
	Env.Log().MajorSection("Test accessing nested FieldMaps using NestedFieldPath")
	fmap1 := Env.Types().MintFieldMap()
	fmap2 := Env.Types().MintFieldMap()
	fmap3 := Env.Types().MintFieldMap()
	fmap1.AutoSet("field11", "hello")
	fmap1.AutoSet("field12", 3)
	fmap1.AutoSet("fmap2", fmap2)
	fmap2.AutoSet("fmap3", fmap3)
	fmap3.AutoSet("field31", uint8(2))
	expected := Env.Types().AutoField("great")
	fmap3.AutoSet("field32", expected)

	path := MakeNestedFieldPath(
		Env.Types().BuildFieldList("fmap2", "fmap3", "field32"))
	result, goof := fmap1.GetNestedField(path)

	Env.Log().Basic("Nested path = %q", path)

	if Env.Log().IsError(goof) {
		t.Fatalf("Problem getting nested field")
	}

	if !result.Equals(expected) {
		Env.Log().Advise("Fieldmap = %v", fmap1)
		t.Fatalf(
			"Expected %v for field %v but got %v", expected, "great", result)
	}
}

func TestKeysAsNestedFieldLists(t *testing.T) {
	Env.Log().MajorSection("Test producing a sorted list of key paths from nested FieldMaps")
	fmap1 := Env.Types().MintFieldMap()
	fmap2 := Env.Types().MintFieldMap()
	fmap3 := Env.Types().MintFieldMap()
	fmap1.AutoSet("X", fmap2)
	fmap2.AutoSet("G", "g")
	fmap2.AutoSet("C", fmap3)
	fmap3.AutoSet("K", "k")
	fmap3.AutoSet("E", "e")
	fmap1.AutoSet("M", "m")
	fmap1.AutoSet("P", "p")

	// {X:{
	//     G:g,
	//     C:{
	//        K:k,
	//        E:e
	//     }
	//    },
	//  M:m,
	//  P:p}

	// X G
	// X C K
	// X C E
	// M
	// P

	expected := []string{
		"\"M\"",
		"\"P\"",
		"\"X\".\"C\".\"E\"",
		"\"X\".\"C\".\"K\"",
		"\"X\".\"G\"",
	}
	result := fmap1.RecursiveSortedKeyLinkList().List().Collect()
	if len(result) != len(expected) {
		t.Fatalf("Expected %d paths, got %d", len(expected), len(result))
	}
	for i, c := range result {
		if c.String() != expected[i] {
			t.Fatalf("Expected path %s, got %s",
				expected[i], c.String())
		}
	}
}
