/*
	Useful reflection objects and functions.
*/
package h2go

import (
	"fmt"
	"reflect"
	"runtime"
)

func IsError(err error) bool {
	goof, isa := err.(*Goof)
	if isa {
		if goof.IsEmpty() || goof.Code == NO_GOOF_CODE {
			return false
		} else {
			return true
		}
	}
	//fmt.Printf("DOK err=%v type=%T\n", err, err)
	//fmt.Printf("DOK ValueOf=%v\n", reflect.ValueOf(err))
	v := reflect.ValueOf(err)
	if err != nil && v.IsValid() {
		if !v.IsNil() {
			return true
		}
	}
	return false
}

func Method(val interface{}, name string) (bool, reflect.Type) {
	found := false
	rtyp := reflect.TypeOf(val)
	for i := 0; i < rtyp.NumMethod(); i++ {
		if rtyp.Method(i).Name == name {
			found = true
		}
	}
	return found, rtyp
}

func HasMethod(val interface{}, name string) bool {
	exists, _ := Method(val, name)
	return exists
}

func MethodHasReturnTypes(val interface{}, name string, typNames []string) bool {
	exists, typ := Method(val, name)
	if !exists || typ.NumOut() != len(typNames) {
		return false
	}
	for i, outName := range typNames {
		if typ.Out(i).Name() != outName {
			return false
		}
	}
	return true
}

func IsString(val interface{}) bool {
	if val == nil {
		return false
	}
	return reflect.TypeOf(val).Kind() == reflect.String
}

// Currently no support for native complex numbers
func IsNumber(val interface{}) bool {
	kind := reflect.TypeOf(val).Kind()
	return kind >= reflect.Int && kind <= reflect.Float64
}

// Currently no support for native complex numbers
func GetUnderlyingNativeNumberInstance(val interface{}) interface{} {
	switch reflect.TypeOf(val).Kind() {
	case reflect.Int8:
		return int8(0)
	case reflect.Int16:
		return int16(0)
	case reflect.Int32:
		return int32(0)
	case reflect.Int, reflect.Int64:
		return int64(0)
	case reflect.Uint8:
		return uint8(0)
	case reflect.Uint16:
		return uint16(0)
	case reflect.Uint32:
		return uint32(0)
	case reflect.Uint, reflect.Uint64:
		return uint64(0)
	case reflect.Float32:
		return float32(0)
	case reflect.Float64:
		return float64(0)
	default:
		return nil
	}
}

// STRUCT ======================================================================

func ValueIsStruct(val reflect.Value) bool {
	return val.Kind() == reflect.Struct
}

func ValueIsStructPtr(val reflect.Value) bool {
	return val.Kind() == reflect.Ptr && val.Elem().Kind() == reflect.Struct
}

func ValueIsExportable(v reflect.Value) bool {
	return v.CanInterface()
}

func IsStructPtr(v interface{}) bool {
	return ValueIsStructPtr(reflect.ValueOf(v))
}

// Requires a pointer to a struct, and returns the struct reflect.Value,
// a pointer to the struct as a reflect.Value, and any error.
func RequireStructPtr(c interface{}) (reflect.Value, reflect.Value, string, *Goof) {
	p := reflect.ValueOf(c)
	if p.Kind() != reflect.Ptr {
		return reflect.ValueOf(nil), p, "",
			Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Require a pointer to the struct be provided")
	}
	s := p.Elem()
	if s.Kind() != reflect.Struct {
		return s, p, "", Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Require a struct be provided")
	}
	return s, p, s.Type().Name(), NO_GOOF
}

// STRUCT TREE =================================================================

// A StructTree is a Tree using StructTreeValues.
// Mapped structs must be pointers, to ensure mutability of data (non-pointer
// structs are ignored).
type StructTree struct {
	*Tree
}

func NewStructTree() *StructTree {
	return &StructTree{
		Tree: NewTree(),
	}
}

type StructTreeIterator struct {
	*TreeIterator
}

type StructTreeValue struct {
	Label  string
	Struct reflect.Value
}

var _ Comparable = NewStructTreeValue()

func NewStructTreeValue() *StructTreeValue {
	return &StructTreeValue{
		Struct: reflect.ValueOf(0),
	}
}

func MakeStructTreeValue(label string, ps reflect.Value) *StructTreeValue {
	stv := NewStructTreeValue()
	stv.Label = label
	stv.Struct = ps
	return stv
}

func (stv *StructTreeValue) Cmp(other Comparable) int {
	stv2, ok := other.(*StructTreeValue)
	if !ok {
		Env.Log().IsError(Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"other=%v is a %T, not a *Transaction", other, other))
		return COMPARE_ERR
	}
	if stv2.Label == stv.Label {
		return COMPARE_EQ
	} else if stv2.Label > stv.Label {
		return COMPARE_GT
	}
	return COMPARE_LT
}

func (stv *StructTreeValue) String() string {
	return stv.Label
}

func (stiter *StructTreeIterator) GotoLabel(label string) *StructTreeIterator {
	iter2 := stiter.This().Children().Iterator().First()
	tlink := NewTreeLink()
	stv := NewStructTreeValue()
	ok := false
	if !stiter.This().Children().IsEmpty() {
		for {
			if !iter2.This().IsEmpty() {
				tlink, ok = iter2.This().Value().(*TreeLink)
				if ok {
					stv, ok = tlink.Value().(*StructTreeValue)
					if ok && stv.Label == label {
						stiter.this = tlink
						if stiter.This().Children().IsEmpty() {
							stiter.SetAtLeaf(true)
						}
						break
					}
					iter2.Inc()
					if iter2.AtEnd() {
						break
					}
				}
			}
		}
	}
	return stiter
}

// Forms an abstract tree representation of a nested struct.
func MakeStructTree(c interface{}) (*StructTree, *Goof) {
	stree := NewStructTree()
	s, ps, _, goof := RequireStructPtr(c)
	if IsError(goof) {
		return stree, goof
	}
	stree.root = stree.MakeTreeLink(
		MakeStructTreeValue(s.Type().Name(), ps))
	stree.Root().addNestedStructs(s)
	return stree, NO_GOOF
}

// Recursively add struct TreeLinks.
func (tlink *TreeLink) addNestedStructs(s reflect.Value) *TreeLink {
	for j := 0; j < s.NumField(); j++ {
		i := s.Field(j).Interface()
		s2, ps2, _, goof := RequireStructPtr(i)
		if !IsError(goof) {
			stv := MakeStructTreeValue(s2.Type().Name(), ps2)
			tlink2 := tlink.NewTreeLink(stv)
			tlink.AddChild(tlink2)
			tlink2.addNestedStructs(s2)
		}
	}
	return tlink
}

// GOCALLER ====================================================================

// Captures a Go caller identity and location.
type GoCaller struct {
	filename string // Go code filename
	line     int    // Line number within code
	fn       string // Go function
}

func NewGoCaller() *GoCaller {
	return &GoCaller{}
}

func (caller *GoCaller) Filename() string { return caller.filename }
func (caller *GoCaller) Line() int        { return caller.line }
func (caller *GoCaller) Function() string { return caller.fn }

// Return a string representing the caller.
func (caller *GoCaller) String() string {
	return fmt.Sprintf(
		"%s in %s.%d",
		caller.fn,
		caller.filename,
		caller.line)
}

func (caller *GoCaller) IsEmpty() bool {
	if len(caller.fn) == 0 && len(caller.filename) == 0 && caller.line == 0 {
		return true
	}
	return false
}

// Captures the callers details, accounting for jumps since the call.
func CaptureCaller(jumpsSinceCall int) *GoCaller {
	pc, filename, line, _ := runtime.Caller(jumpsSinceCall)
	caller := NewGoCaller()
	caller.filename = filename
	caller.line = line
	caller.fn = runtime.FuncForPC(pc).Name()
	return caller
}

// Trace source code jumps to the callers point, newest first.
func TraceToHere(back, pathdepth int) []string {
	result := []string{}
	caller := NewGoCaller()
	const offset = 3
	start := offset + back - 1
	for i := offset; i <= start; i++ {
		caller = CaptureCaller(i)
		if !caller.IsEmpty() {
			result = append(result,
				TruncatePathToSlashLevel(pathdepth, caller.String()))
		}
	}
	return result
}

// Constructs the given variable, avoiding nil initialisations where possible.
// The exceptions are non-exportable fields of structs and funcs.  The variable
// provided must be de-referenced (i.e. prefixing with & for a pointer).
func Construct(i interface{}) *Goof {
	return construct(reflect.ValueOf(i))
}

// Constructor which attempts to initialise a variable avoiding nils.
// Acts recursively for structs.
func construct(val0 reflect.Value) *Goof {
	typ := val0.Type()
	kind := typ.Kind()
	val1 := val0
	// What we're trying to do here is strip val1 down to the underlying data
	// value.  Its a bit hacky because I find reflect non-intuitive but it works
	// for the main simple variables (see TestConstruct).
	switch kind {
	case reflect.Invalid:
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Construct requires a pointer to a value %v", val0)
	case reflect.Ptr:
		typ = typ.Elem()
		kind = typ.Kind()
		if kind == reflect.Ptr {
			val1 = reflect.Indirect(val0)
			typ = val1.Type().Elem()
			kind = typ.Kind()
		}
		switch kind {
		case reflect.Map, reflect.Slice, reflect.Interface, reflect.Chan:
			val1 = reflect.Indirect(val0)
			typ = val1.Type()
			kind = typ.Kind()
		}
	}

	switch kind {
	case reflect.Slice,
		reflect.Map,
		reflect.Interface,
		reflect.Chan,
		reflect.Struct:
		if val1.CanSet() {
			// Deal with all the nillable types
			switch kind {
			case reflect.Slice:
				val1.Set(reflect.MakeSlice(val1.Type(), 0, 0))
			case reflect.Map:
				val1.Set(reflect.MakeMap(val1.Type()))
			case reflect.Interface:
				// Make it whatever we want, in this case a bool
				val1.Set(reflect.New(reflect.TypeOf(false)))
			case reflect.Chan:
				val1.Set(reflect.MakeChan(val1.Type(), 0))
			case reflect.Struct:
				val1.Set(reflect.New(typ))
				for j := 0; j < typ.NumField(); j++ {
					val2 := reflect.Indirect(val1).Field(j)
					goof := construct(val2)
					if IsError(goof) {
						return goof
					}
				}
			}
		}
	}
	return NO_GOOF
}

// Returns error if either target, source or associated embedded
// structs are not struct pointers.
func DeepCopyStructPtrs(target, source interface{}) *Goof {
	srcValue, _, _, goof := RequireStructPtr(source)
	if IsError(goof) {
		return goof
	}
	targValue, _, _, goof := RequireStructPtr(target)
	if IsError(goof) {
		return goof
	}
struct_loop:
	for i := 0; i < srcValue.NumField(); i++ {
		sf := srcValue.Field(i)
		if sf.IsValid() && ValueIsExportable(sf) {
			// field is valid and exportable
			if ValueIsStruct(sf) {
				continue struct_loop
			}
			sint := sf.Interface()
			sFieldName := srcValue.Type().Field(i).Name
			for j := 0; j < targValue.NumField(); j++ {
				tf := targValue.Field(j)
				if tf.IsValid() && ValueIsExportable(tf) {
					// field is valid and exportable
					if ValueIsStruct(tf) {
						continue struct_loop
					}
					tint := tf.Interface()
					tFieldName := targValue.Type().Field(j).Name
					if sFieldName == tFieldName &&
						sf.Kind() == tf.Kind() {
						if sf.Kind() == reflect.Ptr &&
							sf.Elem().Kind() == reflect.Struct {
							goof := DeepCopyStructPtrs(tint, sint)
							if IsError(goof) {
								return goof
							}
						}
						tf.Set(sf)
					}
				}
			}
		}
	}
	return NO_GOOF
}
