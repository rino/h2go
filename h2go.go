/*
	Boot H2GO, and dependent packages via a RegisterBoot call.
*/
package h2go

import (
	"os"
	"sort"
)

// BOOT ========================================================================

type Booter struct {
	name   string
	order  int
	bootfn func(env *Environment) *Environment
}

func BuildBooter(name string, order int, bootfn func(*Environment) *Environment) *Booter {
	return &Booter{
		name:   name,
		order:  order,
		bootfn: bootfn,
	}
}

// All libraries should register with h2go via an init function.
func RegisterBoot(pkg string, order int, bootfn func(*Environment) *Environment) {
	if booters, exists := bootRegister[order]; exists {
		bootRegister[order] =
			append(booters, BuildBooter(pkg, order, bootfn))
	} else {
		bootRegister[order] =
			[]*Booter{BuildBooter(pkg, order, bootfn)}
	}
	return
}

func BootRegistered() {
	index := []int{}
	// Build sorted index
	for i, _ := range bootRegister {
		index = append(index, i)
	}
	sort.Ints(index)
	// Now boot all registered packages
	for _, i := range index {
		for _, booter := range bootRegister[i] {
			booter.bootfn(Env)
			Env.Log().Advise("Booted %s with order %d", booter.name, booter.order)
		}
	}
	return
}

// Called by an application (main function) or by test
// initialisation.  Do not use the Logger until Boot complete.
func Boot(threadtag, allowedRoot string, log *Logger) *Environment {
	if ENV_CREATED {
		return Env
	}
	Env = NewEnvironment()
	info, err := os.Stat(allowedRoot)
	if os.IsNotExist(err) {
		Fatal("The allowable root path %q does not exist, you need to create it",
			allowedRoot)
	}
	if !info.IsDir() {
		Fatal("The given allowable root path %q is not a recognised directory",
			allowedRoot)
	}
	ExitOnError(err)
	Env.root = allowedRoot
	Env.NotEmpty()
	Env.SetLogger(log)
	log.SetEnv(Env)

	// Order of operations important here
	// Must do this first, as type checker needs Goofs
	Env.goofs, err = MakeEnum(uint8(CODE_BYTE_LENGTH), InitGoofs)
	ExitOnError(err)
	index, exist := Env.Goofs().Number("NO_GOOF")
	if !exist {
		Fatal("A %q %T code must be defined", "NO_GOOF", Env.Goofs())
	}
	NO_GOOF_CODE = CODE(index)
	_, exist = Env.Goofs().Number("WRAPPED")
	if !exist {
		Fatal("A %q %T code must be defined", "WRAPPED", Env.Goofs())
	}
	NO_GOOF = NoGoof()

	// Version
	goof := NO_GOOF
	H2GO_VERSION, goof = MakeVersion(
		VERSION_MAJOR,
		VERSION_MINOR,
		VERSION_REVISION,
	)
	ExitOnError(goof)
	Env.SetVersion(H2GO_VERSION)
	_, ok := VERSION_PARAMS[Env.Version().AsKey()]
	if !ok {
		Fatal("The current h2go version %v does not have a VERSION_PARAMS entry",
			Env.Version)
	}

	// Box
	EMPTY_BOX = NewBox().SetBoxKinds(BoxItemKinds)

	// Types
	Env.types = MakeTypeMap(H2GO_VERSION)
	for _, maker := range InitTypes(Env) {
		goof = Env.types.Add(
			maker,
			ProtocolNilCheck,
		)
		ExitOnError(goof)
	}
	UNKNOWN_TYPE_CODE, goof = Env.Types().ProtocolCodeByName("UNKNOWN_TYPE")
	ExitOnError(goof)
	NIL_TYPE_CODE, goof = Env.Types().ProtocolCodeByName("NIL")
	ExitOnError(goof)
	FIELD_TYPE_CODE, goof = Env.Types().ProtocolCodeByName("FIELD")
	ExitOnError(goof)
	FIELDBOX_TYPE_CODE, goof = Env.Types().ProtocolCodeByName("FIELDBOX")
	ExitOnError(goof)
	FIELDMAP_TYPE_CODE, goof = Env.Types().ProtocolCodeByName("FIELDMAP")
	ExitOnError(goof)
	FIELDLIST_TYPE_CODE, goof = Env.Types().ProtocolCodeByName("FIELDLIST")
	ExitOnError(goof)

	// Config, requires Env instantiated first
	DEFAULT_ENV_CONFIG = NewEnvConfig()
	DEFAULT_ENV_CONFIG.GOOF_JUMPS = 3
	DEFAULT_ENV_CONFIG.GOOF_DEPTH = 2
	DEFAULT_ENV_CONFIG.TRACE_BACK = 3
	DEFAULT_ENV_CONFIG.TRACE_PATHDEPTH = 3
	DEFAULT_ENV_CONFIG.LANGUAGE = "English"
	DEFAULT_ENV_CONFIG.CALENDAR = "Gregorian"

	Env.config = DEFAULT_ENV_CONFIG.Copy()
	goof = Env.config.Validate()
	ExitOnError(goof)

	DEFAULT_FIELDBOX_PARAMS_CONFIG = NewFieldBoxParamsConfig()
	DEFAULT_FIELDBOX_PARAMS_CONFIG.TIMESTAMPING = true
	DEFAULT_FIELDBOX_PARAMS_CONFIG.COMPRESSION_NONE = true
	DEFAULT_FIELDBOX_PARAMS_CONFIG.COMPRESSION_GZIP = false
	DEFAULT_FIELDBOX_PARAMS_CONFIG.ENCRYPTION_NONE = true
	DEFAULT_FIELDBOX_PARAMS_CONFIG.ENCRYPTION_SECRETBOX = false
	DEFAULT_FIELDBOX_PARAMS_CONFIG.ENCRYPTION_AES_256 = false
	DEFAULT_FIELDBOX_PARAMS_CONFIG.ENCRYPTION_RSA_OAEP = false
	goof = DEFAULT_FIELDBOX_PARAMS_CONFIG.Validate()
	ExitOnError(goof)

	goof = Env.types.AttachStringBrackets(&BracketStringMap{
		bracmap: map[string]BracketStrings{
			"(": BracketStrings{
				Left:    "(",
				Right:   ")",
				Code:    Env.types.Number["FIELD"],
				Decoder: FieldStringDecoder(),
			},
			"{": BracketStrings{
				Left:    "{",
				Right:   "}",
				Code:    Env.types.Number["FIELDMAP"],
				Decoder: FieldMapStringDecoder(),
			},
			"[": BracketStrings{
				Left:    "[",
				Right:   "]",
				Code:    Env.types.Number["FIELDLIST"],
				Decoder: FieldListStringDecoder(),
			},
		},
	})
	if IsError(goof) {
		Fatal("Problem attaching bracket map: %v", goof)
	}
	// A workaround given the fact that FieldMap and FieldList must attach to a
	// TypeMap, so the Nil is unfortunately self-referential
	fmapType, goof := Env.types.ProtocolByName("FIELDMAP")
	ExitOnError(goof)
	fmapType.Nil = Env.types.MintFieldMap()
	Env.types.proto[fmapType.Code] = fmapType
	flistType, goof := Env.types.ProtocolByName("FIELDLIST")
	ExitOnError(goof)
	flistType.Nil = Env.types.MintFieldMap()
	Env.types.proto[flistType.Code] = flistType
	// Languages
	Env.langs = make(map[string]Language)
	Env.langs["English"] = MakeEnglish()
	// Calendars, depends on languages, add here
	gregorian, goof := MakeGregorian(Env.langs)
	ExitOnError(goof)
	Env.cals["Gregorian"] = gregorian
	// Enums
	BinaryAlgoCompression, err = MakeEnum(1, InitAlgoCompression)
	ExitOnError(err)
	BinaryAlgoEncryption, err = BuildEnum(1, InitAlgoEncryptionWithInfo)
	ExitOnError(err)

	ENV_CREATED = true
	BuildConstants()
	BootRegistered()
	return Env
}

func BuildConstants() {
	// Default FileMode
	out, goof := NewFilePermissions().DecodeString(DEFAULT_FILEPERMS, EMPTY_BOX)
	ExitOnError(goof)
	fperms, isa := (out.GetDecoded().Value()).(*FilePermissions)
	if !isa {
		Fatal("The %T string decoder is not emitting a %T object",
			fperms, fperms)
	}
	DEFAULT_FILEMODE = fperms.ToFileMode()
	// Time
	malformedTimeZoneInfo = Env.Goofs().MakeGoof("INVALID_DATA",
		"Malformed time zone info")
	// Clock
	TIME_SEC_IN_NANOS_AS_BIGRAT = ZeroFrac().SetInt64(1000000000)
	Midday, goof = BuildClock(12, 0, 0, ZeroFrac())
	ExitOnError(goof)
	Midnight, goof = BuildClock(0, 0, 0, ZeroFrac())
	ExitOnError(goof)
	encsw, goof := MakeSwitchesFromStruct(NewFieldBoxParamsConfig())
	ExitOnError(goof)
	ENCODE_SWITCHES_BYTE_LENGTH = encsw.NumberEncodedBytes()
	EMPTY_REPRESENTATION = NewBinaryRepresentation()
	return
}
