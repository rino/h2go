package h2go

import (
	"fmt"
	"sort"
)

//== DATE ======================================================================

type Date struct {
	month *Month
	day   uint8
	*State
}

var _ Period = new(Date)

func NewDate() *Date {
	return &Date{
		month: NewMonth(),
		State: NewState(),
	}
}

func MakeYear(y int) *IntResult {
	return MakeInt64(int64(y))
}

// Constructors.
func MakeDate(mth *Month, d uint8) (*Date, *Goof) {
	date := NewDate()
	date.month = mth
	date.NotEmpty()
	date.day = d
	if !mth.Calendar().IsValidDay(mth, d) {
		return date, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
			"The date %v is not valid in the %s calendar",
			date, mth.Calendar().Name())
	}
	date.NowComplete()
	return date, NO_GOOF
}

func BuildDate(y int, m, d uint8, cal Calendar) (*Date, *Goof) {
	month, goof := MakeMonth(MakeYear(y), m, cal)
	if IsError(goof) {
		return NewDate(), goof
	}
	return MakeDate(month, d)
}

// Getters.
func (date *Date) Year() IntBox  { return date.month.Year() }
func (date *Date) Month() *Month      { return date.month }
func (date *Date) Day() uint8         { return date.day }
func (date *Date) Calendar() Calendar { return date.month.Calendar() }

func (date *Date) Equals(other *Date) bool {
	return date.month.Equals(other.month) && date.day == other.day
}

func (date *Date) IsBefore(other *Date) bool {
	if date.Year().LessThan(other.Year()) {
		return true
	}
	if date.Year().MoreThan(other.Year()) {
		return false
	}
	if date.Month().Value() < other.Month().Value() {
		return true
	}
	if date.Month().Value() > other.Month().Value() {
		return false
	}
	if date.Day() < other.Day() {
		return true
	}
	return false
}

func (date *Date) IsAfter(other *Date) bool {
	if date.Year().MoreThan(other.Year()) {
		return true
	}
	if date.Year().LessThan(other.Year()) {
		return false
	}
	if date.Month().Value() > other.Month().Value() {
		return true
	}
	if date.Month().Value() < other.Month().Value() {
		return false
	}
	if date.Day() > other.Day() {
		return true
	}
	return false
}

func (date *Date) Copy() *Date {
	date2 := NewDate()
	date2.month = date.month
	date2.day = date.day
	date2.State = date.State.Copy()
	return date2
}

func (date *Date) String() string {
	return fmt.Sprintf(
		"%v-%02d-%02d",
		date.Year(),
		date.Month().Value(),
		date.Day())
}

func (date *Date) ToNum(tn TimeNum) (TimeNum, *Goof) {
	return date.Calendar().DateToNum(date, tn)
}

//func (date *Date) FromNum(tn TimeNum) (*Date, *Goof) {
//	return date.Calendar().DateFromNum(tn)
//}

func (date *Date) IsValid() bool {
	return date.Calendar().IsValidDate(date)
}

func (date *Date) IsLastDayOfMonth() bool {
	return date.Day() == date.Calendar().DaysInMonth(date.Month())
}

func (date *Date) IsWeekend() bool {
	return date.Calendar().IsWeekend(date)
}

func (date *Date) DayOfWeek() uint8 {
	return date.Calendar().DayOfWeek(date)
}

// Returns a date that is the first given day of the week before this date.
// If the given day of the week is the same as this date, the original
// date is returned.
func (date *Date) PreviousDayOfWeek(d uint8) (*Date, *Goof) {
	from := int(date.DayOfWeek())
	to := int(d)
	jump := to - from
	if jump > 0 {
		jump -= 7
	}
	return date.Inc(jump)
}

// Increments the date by the given abstract date units.
//
// Works left to right, that is, increments year, then month, and
// lastly day.  Maintains immutability by returning a new CalendarDate.
func (date *Date) Plus(y, m, d int) (*Date, *Goof) {
	newDate := NewDate()
	nm := int(date.Calendar().NumberMonths())
	if y == 0 && m == 0 && d == 0 {
		newDate = date.Copy()
	} else {
		q := m / nm
		rem := m % nm

		newYear := date.Year().PlusInt(y + q)
		oldMonth := date.Month()

		m1 := oldMonth.Value() + uint8(rem)
		if m1 < 1 {
			newYear = newYear.MinusInt(1)
		} else if m1 > 12 {
			newYear = newYear.PlusInt(1)
		}
		m1 = date.Calendar().MonthModulo(m1)
		newMonth, goof := oldMonth.NewMonth(newYear, m1)
		if IsError(goof) {
			return newDate, goof
		}
		oldDay := date.Day()

		// from java CalClock...
		// Normalise existing day for new month
		//newIsLeapYear =
		//    new CalendarYear
		//    (
		//        newYear,
		//        false
		//    )
		//   .isLeapYear();

		daysInNewMonth := newMonth.Days()

		excessDays := oldDay % daysInNewMonth
		oldDay = oldDay - excessDays

		date2, _ := MakeDate(newMonth, oldDay)
		oldNum, _ := date2.ToNum(new(JulianDay))

		newNum, _ := oldNum.PlusInt(int(excessDays) + d)
		jdn, _ := newNum.(*JulianDay)

		newDate, goof = jdn.ToDate(date.Calendar())
		if IsError(goof) {
			return newDate, goof
		}
	}

	return newDate, NO_GOOF
}

// Increment date by given number of days, not in-place.
func (date *Date) Inc(days int) (*Date, *Goof) {
	return date.Plus(0, 0, days)
}

// Convert date to an interval, starting midnight.
// Satisfies Period interface.
func (date *Date) ToInterval() (*Interval, *Goof) {
	nextDay, goof := date.Inc(1)
	if IsError(goof) {
		return NewInterval(), goof
	}
	return MakeIntervalUsingCalClocks(
		MakeCalClock(date, Midnight),
		MakeCalClock(nextDay, Midnight)), NO_GOOF
}

func (date *Date) PlusDuration(d *Duration) (*Date, *Goof) {
	return date.Inc(int(d.AsDays().AsInt64()))
}

// Time zones are disregarded, endpoints included in test.
func (date *Date) IsWithin(interval *Interval, cal Calendar) (bool, *Goof) {
	start, end, goof := interval.GetDates(cal)
	if IsError(goof) {
		return false, goof
	}
	if date.IsBefore(start) || date.IsAfter(end) {
		return false, NO_GOOF
	}
	return true, NO_GOOF
}

//== DATES =====================================================================

type Dates []*Date

// Constructors.

func NewDates() Dates {
	return Dates([]*Date{})
}

func MakeDates(dates ...*Date) Dates {
	return Dates(dates)
}

// Getters.

func (dates Dates) Len() int      { return len(dates) }
func (dates Dates) IsEmpty() bool { return len(dates) == 0 }

// sort.Interface interface

func (dates Dates) Less(i, j int) bool {
	return dates[i].IsBefore(dates[j])
}
func (dates Dates) Swap(i, j int) {
	dates[i], dates[j] = dates[j], dates[i]
	return
}

// Methods.

func (dates Dates) Copy() Dates {
	newdates := make([]*Date, len(dates))
	for i, date := range dates {
		newdates[i] = date
	}
	return Dates(newdates)
}

func (dates Dates) Equals(other Dates) bool {
	if len(dates) != len(other) {
		return false
	}
	for i, _ := range dates {
		if !dates[i].Equals(other[i]) {
			return false
		}
	}
	return true
}

func (dates Dates) Append(others ...*Date) Dates {
	for _, date := range others {
		dates = append(dates, date)
	}
	return dates
}

func (dates Dates) String() string {
	result := "{"
	for i, date := range dates {
		if i > 0 {
			result += ", "
		}
		result += date.String()
	}
	return result + "}"
}

// Sorts intervals by start time, in place (modifies original).
func (dates Dates) Sort() Dates {
	sort.Sort(dates)
	return dates
}
