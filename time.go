package h2go

import (
	"math/big"
	"strings"
	"time"
)

// TIME ========================================================================

// Primary interfaces.

// Time is the API for CalClock, LocalTime and TimeNums.
type Time interface {
	String() string
	Copy() Time
	// ToCalClock does not change to UTC unless from a TimeNum
	ToCalClock(cal Calendar) (*CalClock, *Goof)
	ToNum(tn TimeNum) (TimeNum, *Goof)
	// An Equals method with two return values is needed to cope with possible
	// error in Date creation.
	Equals(other Time) (bool, *Goof)
	IsBefore(other Time) (bool, *Goof)
	IsAfter(other Time) (bool, *Goof)
	OrEarlier(other Time) (Time, *Goof)
	OrLater(other Time) (Time, *Goof)
	PlusInt(i int) (Time, *Goof)
	At(other *Location) (Time, *Goof) // Performs location correction
	Location() *Location
	IsLocalTime() bool
	Stateful
}

type Period interface {
	ToInterval() (*Interval, *Goof)
}

// Now.

func Now(cal Calendar, loc *Location) (*LocalTime, *Goof) {
	t := time.Now() // UTC, using internal clock, implemented in assembly
	sec := t.Unix()
	ns := t.Nanosecond()
	tn := MakeUnixTime(MakeInt64(sec))
	cc, goof := cal.CalClockFromNum(tn)
	if IsError(goof) {
		return MakeLocalTime(cc, UTC), goof
	}
	newclk, goof := BuildClock(cc.Hour(), cc.Minute(), cc.Second(), MakeNanoFrac(ns))
	if IsError(goof) {
		return MakeLocalTime(cc, UTC), goof
	}
	utc := MakeCalClock(cc.Date, newclk)
	off := loc.Offset(tn)
	tloc, goof := utc.PlusInt(off)
	return MakeLocalTime(tloc, loc), goof
}

func NowUTC(cal Calendar) (*CalClock, *Goof) {
	lt, goof := Now(cal, UTC)
	if IsError(goof) {
		return NewCalClock(), goof
	}
	return lt.ToCalClock(cal)
}

func NowToJavaTimeBigInt() (*big.Int, *Goof) {
	now, goof := NowUTC(Env.Calendar())
	if IsError(goof) {
		return big.NewInt(0), goof
	}
	jt, goof := now.ToNum(NewJavaTime())
	if IsError(goof) {
		return big.NewInt(0), goof
	}
	return jt.Value().AsBigInt(), NO_GOOF
}

func NowFromJavaTimeBigInt(bi *big.Int) (*CalClock, *Goof) {
	jt := MakeJavaTime(MakeBigInt(bi))
	return jt.ToCalClock(Env.Calendar())
}

// CALCLOCK ====================================================================

// Time represented by a date and clock as UTC.
// CalClock is tied to a particular Calendar via the Month.
type CalClock struct {
	*Date
	*Clock
	*State
}

var _ Time = new(CalClock)

func NewCalClock() *CalClock {
	return &CalClock{
		Date:  NewDate(),
		Clock: NewClock(),
		State: NewState(),
	}
}

func MakeCalClock(date *Date, clock *Clock) *CalClock {
	cc := NewCalClock()
	cc.Clock = clock
	if !clock.IsEmpty() {
		cc.NotEmpty()
	}
	cc.Date = date
	if clock.IsComplete() && date.IsComplete() {
		cc.NowComplete()
	}
	return cc
}

func BuildCalClock(yr int, mth, d, hr, min, s uint8, f *big.Rat, cal Calendar) (*CalClock, *Goof) {
	date, goof := BuildDate(yr, mth, d, cal)
	if IsError(goof) {
		return NewCalClock(), goof
	}
	clock, goof := BuildClock(hr, min, s, f)
	if IsError(goof) {
		return NewCalClock(), goof
	}
	return MakeCalClock(date, clock), NO_GOOF
}

func (cc *CalClock) Plus(y, mth, d, h, m, s int, f *big.Rat) (*CalClock, *Goof) {
	newClock, dayCarry := cc.Clock.Plus(h, m, s, f)
	newDate, goof := cc.Date.Plus(y, mth, d+dayCarry)
	return MakeCalClock(newDate, newClock), goof
}

// Time interface.
func (cc *CalClock) String() string {
	return cc.Date.String() + " " + cc.Clock.String()
}
func (cc *CalClock) Copy() Time {
	cc2 := NewCalClock()
	cc2.Date = cc.Date.Copy()
	cc2.Clock = cc.Clock.Copy()
	cc2.State = cc2.State.Copy()
	return cc2
}
func (cc *CalClock) ToCalClock(cal Calendar) (*CalClock, *Goof) {
	return cc, NO_GOOF
}
func (cc *CalClock) ToNum(tn TimeNum) (TimeNum, *Goof) {
	return cc.Calendar().CalClockToNum(cc, tn)
}
func (cc *CalClock) Equals(other Time) (bool, *Goof) {
	cc2, goof := cc.toComparableCalClock(other)
	if !IsError(goof) {
		return cc.Date.Equals(cc2.Date) && cc.Clock.Equals(cc2.Clock), NO_GOOF
	}
	return false, goof
}
func (cc *CalClock) IsBefore(other Time) (bool, *Goof) {
	cc2, goof := cc.toComparableCalClock(other)
	if !IsError(goof) {
		if cc.Date.IsBefore(cc2.Date) {
			return true, NO_GOOF
		}
		if cc.Date.Equals(cc2.Date) {
			if cc.Clock.IsBefore(cc2.Clock) {
				return true, NO_GOOF
			}
		}
	}
	return false, goof
}
func (cc *CalClock) IsAfter(other Time) (bool, *Goof) {
	cc2, goof := cc.toComparableCalClock(other)
	if !IsError(goof) {
		if cc.Date.IsAfter(cc2.Date) {
			return true, NO_GOOF
		}
		if cc.Date.Equals(cc2.Date) {
			if cc.Clock.IsAfter(cc2.Clock) {
				return true, NO_GOOF
			}
		}
	}
	return false, goof
}
func (cc *CalClock) OrEarlier(other Time) (Time, *Goof) {
	if before, goof := cc.IsBefore(other); before {
		return cc, goof
	} else {
		return other, goof
	}
}
func (cc *CalClock) OrLater(other Time) (Time, *Goof) {
	if after, goof := cc.IsAfter(other); after {
		return cc, goof
	} else {
		return other, goof
	}
}
func (cc *CalClock) PlusInt(i int) (Time, *Goof) {
	return cc.Plus(0, 0, 0, 0, 0, i, ZeroFrac())
}
func (cc *CalClock) At(other *Location) (Time, *Goof) {
	// CalClock is always UTC, use LocalTime for localised.
	ut, _ := cc.ToNum(new(UnixTime))
	newCalClock, goof := cc.Plus(0, 0, 0, 0, 0, other.Offset(ut), ZeroFrac())
	return MakeLocalTime(newCalClock, other), goof
}
func (cc *CalClock) Location() *Location { return UTC }
func (cc *CalClock) IsLocalTime() bool   { return false }

func (cc *CalClock) toComparableCalClock(other Time) (*CalClock, *Goof) {
	to, goof := other.At(UTC)
	if lt, isa := to.(*LocalTime); isa {
		to = lt.Time()
	}
	cc2, isa := to.(*CalClock) // Attempt faster internal cast first...
	if !isa {
		cc2, goof = to.ToCalClock(cc.Calendar()) // convert if necessary
	}
	return cc2, goof
}

// LOCALTIME ===================================================================

// A concrete time, associated with a Location.
type LocalTime struct {
	time Time
	loc  *Location
	*State
}

var _ Time = new(LocalTime)

func NewLocalTime() *LocalTime {
	return &LocalTime{
		time:  NewGoTime(),
		loc:   UTC,
		State: NewState(),
	}
}

func MakeLocalTime(t Time, loc *Location) *LocalTime {
	lt := NewLocalTime()
	if lt2, isa := t.(*LocalTime); isa {
		// Use same time, new location
		lt.time = lt2.time
	} else {
		lt.time = t
	}
	lt.loc = loc
	return lt
}

func MakeUTCTime(t Time) *LocalTime {
	return MakeLocalTime(t, UTC)
}

// Getters.
func (lt *LocalTime) Time() Time          { return lt.time }
func (lt *LocalTime) Location() *Location { return lt.loc }

// Time interface.
func (lt *LocalTime) String() string {
	return lt.time.String() + " " + lt.loc.String()
}
func (lt *LocalTime) Copy() Time {
	lt2 := NewLocalTime()
	lt2.time = lt.time.Copy()
	lt2.loc = lt.loc.Copy()
	lt2.State = lt.State.Copy()
	return lt2
}
func (lt *LocalTime) ToCalClock(cal Calendar) (*CalClock, *Goof) {
	return lt.time.ToCalClock(cal)
}
func (lt *LocalTime) ToNum(tn TimeNum) (TimeNum, *Goof) {
	return lt.time.ToNum(tn)
}
func (lt *LocalTime) Equals(other Time) (bool, *Goof) {
	if t, isa := other.(*LocalTime); isa {
		tloc, goof := t.At(lt.loc)
		if IsError(goof) {
			return false, goof
		}
		to, _ := tloc.(*LocalTime)
		equals, goof := lt.time.Equals(to.time)
		return equals && lt.loc == to.loc, goof
	}
	goof := lt.compareErr()
	return false, goof
}
func (lt *LocalTime) IsBefore(other Time) (bool, *Goof) {
	if t, isa := other.(*LocalTime); isa {
		tloc, goof := t.At(lt.loc)
		if IsError(goof) {
			return false, goof
		}
		to, _ := tloc.(*LocalTime)
		return lt.time.IsBefore(to.time)
	}
	goof := lt.compareErr()
	return false, goof
}
func (lt *LocalTime) IsAfter(other Time) (bool, *Goof) {
	if t, isa := other.(*LocalTime); isa {
		tloc, goof := t.At(lt.loc)
		if IsError(goof) {
			return false, goof
		}
		to, _ := tloc.(*LocalTime)
		return lt.time.IsAfter(to.time)
	}
	goof := lt.compareErr()
	return false, goof
}
func (lt *LocalTime) OrEarlier(other Time) (Time, *Goof) {
	if before, goof := lt.IsBefore(other); before {
		return lt, goof
	} else {
		return other, goof
	}
}
func (lt *LocalTime) OrLater(other Time) (Time, *Goof) {
	if after, goof := lt.IsAfter(other); after {
		return lt, goof
	} else {
		return other, goof
	}
}
func (lt *LocalTime) PlusInt(i int) (Time, *Goof) {
	t, goof := lt.time.PlusInt(i)
	return MakeLocalTime(t, lt.loc), goof
}
func (lt *LocalTime) At(other *Location) (Time, *Goof) {
	goof := NO_GOOF
	ut, consistent := lt.ToUnixTime() // Take local time as UTC
	if !consistent {
		// TODO deal with problem rather than report it
		goof = Env.Goofs().MakeGoof("NOT_IMPLEMENTED",
			"LocalTime %v is adjacent to an offset boundary",
			lt)
	}
	off1 := lt.loc.Offset(ut)
	off2 := other.Offset(ut)
	t, _ := lt.PlusInt(off2 - off1)
	return MakeLocalTime(t, other), goof
}

//func (lt *LocalTime) Location() *Location {return lt.loc}
func (lt *LocalTime) IsLocalTime() bool { return true }

func (lt *LocalTime) compareErr() *Goof {
	return Env.Goofs().MakeGoof("BAD_TYPE",
		"LocalTime %v can only be compared to another LocalTime", lt)
}

func (lt *LocalTime) ToUnixTime() (*UnixTime, bool) {
	ut1, _ := lt.time.ToNum(new(UnixTime)) // using local time as UTC
	dt1 := lt.loc.Offset(ut1)
	ut2, _ := ut1.PlusInt(-dt1) // estimated UTC
	tn2, _ := ut2.(TimeNum)
	dt2 := lt.loc.Offset(tn2)
	return MakeUnixTime(tn2.Value()), dt1 == dt2
}

// Time zones are accounted for if necessary, endpoints included in test.
func (lt *LocalTime) IsWithin(interval *Interval) (bool, *Goof) {
	isbefore, goof := lt.IsBefore(interval.Start())
	if IsError(goof) {
		return false, goof
	}
	isafter, goof := lt.IsAfter(interval.End())
	if IsError(goof) {
		return false, goof
	}
	if isbefore || isafter {
		return false, NO_GOOF
	}
	return true, NO_GOOF
}

// LOCATION ====================================================================
// Try and keep new Location related stuff separate from hacked stuff.

func (loc *Location) Offset(tn TimeNum) int {
	uti64, _ := tn.Value().AsGoInt64()
	_, offset, _, _, _ := loc.lookup(uti64)
	return offset
}

func (loc *Location) Copy() *Location {
	loc2 := NewLocation()
	loc2.name = loc.name
	loc2.zone = loc.zone
	loc2.tx = loc.tx
	loc2.cacheStart = loc.cacheStart
	loc2.cacheEnd = loc.cacheEnd
	loc2.cacheZone = loc.cacheZone
	loc2.State = loc.State.Copy()
	return loc2
}

// TIME STAMP ==================================================================

func NewTimeStamp() (string, *big.Int, *Goof) {
	cc, goof := NowUTC(Env.Calendar())
	if IsError(goof) {
		return "", big.NewInt(0), goof
	}
	return BuildTimeStamp(cc, "")
}

func MakeTimeStamp(info string) (string, *big.Int, *Goof) {
	cc, goof := NowUTC(Env.Calendar())
	if IsError(goof) {
		return "", big.NewInt(0), goof
	}
	return BuildTimeStamp(cc, info)
}

func BuildTimeStamp(time *CalClock, info string) (string, *big.Int, *Goof) {
	jt, goof := time.ToNum(NewJavaTime())
	if IsError(goof) {
		return "", big.NewInt(0), goof
	}
	bi := jt.Value().AsBigInt()
	if len(info) == 0 {
		return bi.String(), bi, NO_GOOF
	} else {
		return "(" + bi.String() + "," + info + ")", bi, NO_GOOF
	}
}

// Be a bit lenient on additional comma separators.
func ParseTimeStamp(stamp string) (*CalClock, string, *big.Int, *Goof) {
	txt := TrimText(stamp)
	if txt[0] == '(' {
		txt = txt[1:]
	}
	if txt[len(txt)-1] == ')' {
		txt = txt[:len(txt)-1]
	}
	parts := strings.Split(txt, ",")
	if len(parts) == 0 {
		return NewCalClock(), "", big.NewInt(0),
			Env.Goofs().MakeGoof("PARSING_TEXT", "Time stamp is empty")
	}
	info := ""
	if len(parts) > 1 {
		info = strings.SplitAfterN(txt, ",", 2)[1]
	}
	bi, ok := big.NewInt(0).SetString(parts[0], 10)
	if !ok {
		return NewCalClock(), info, bi,
			Env.Goofs().MakeGoof("PARSING_TEXT",
				"Could not parse %q to a %T", parts[0])
	}
	cc, goof := NowFromJavaTimeBigInt(bi)
	if IsError(goof) {
		return cc, info, bi, goof
	}
	return cc, info, bi, NO_GOOF
}
