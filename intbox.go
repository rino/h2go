/*
	Immutable boxed native and math/big integers useful mainly for conversions.
*/
package h2go

import (
	"fmt"
	"math"
	"math/big"
	"reflect"
)

func ZeroFrac() *big.Rat { return big.NewRat(0, 1) }

type IntBox interface {
	// Comparison
	Equals(other IntBox) bool
	LessThan(other IntBox) bool
	MoreThan(other IntBox) bool
	IsZero() bool
	// Access
	Overflow() bool
	// Interchange
	String() string
	Dump() string
	AsBigInt() *big.Int
	AsGoInt64() (int64, bool)
	AsNative() interface{}
	HasRange() bool
	MaxBigInt() (*big.Int, bool) // returns whether a max is valid
	MaxGoInt64() (int64, bool)   // returns whether a max is valid
	MinBigInt() (*big.Int, bool) // returns whether a min is valid
	MinGoInt64() (int64, bool)   // returns whether a min is valid
	To(other IntBox) *IntResult
	Copy() *IntResult
	// Arithmetic
	Rem(div int64) *IntResult
	Neg() *IntResult
	Sign() int
	Plus(other IntBox) *IntResult
	Minus(other IntBox) *IntResult
	MultBy(other IntBox) *IntResult
	DivBy(other IntBox) *IntResult // shoehorn overflow still possible
	PlusInt(i int) *IntResult
	MinusInt(i int) *IntResult
	MultByInt(i int) *IntResult
	DivByInt(i int) *IntResult // shoehorn overflow still possible
	Stateful
}

// Int types (boxes of big.Int and primitives).

// IntResult.
type IntResult struct {
	val   IntBox
	oflow bool
	*State
}

var _ IntBox = NewIntResult()

func NewIntResult() *IntResult {
	return &IntResult{
		val:   NewUint8(),
		State: NewState(),
	}
}

func MakeIntResult(i IntBox, oflow bool) *IntResult {
	r := NewIntResult()
	r.val = i
	r.oflow = oflow
	r.SetToEmptyIfAllNil(i)
	return r
}

func ToResult(i IntBox) *IntResult {
	return MakeIntResult(i, false)
}

func FromResult(r *IntResult) IntBox         { return r.val }
func (r *IntResult) Value() IntBox           { return r.val }
func (r *IntResult) Explode() (IntBox, bool) { return r.val, r.oflow }
func (r *IntResult) CarryOverflow(res *IntResult) *IntResult {
	return MakeIntResult(res.Value(), r.Overflow() || res.Overflow())
}

// Comparison
func (r *IntResult) Equals(other IntBox) bool {
	if r.Overflow() == true || other.Overflow() == true {
		return false
	}
	if r.Value().Equals(other) {
		return true
	}
	return false
}
func (r *IntResult) LessThan(other IntBox) bool { return r.val.LessThan(other) }
func (r *IntResult) MoreThan(other IntBox) bool { return r.val.MoreThan(other) }
func (r *IntResult) IsZero() bool               { return r.val.IsZero() }

// Access
func (r *IntResult) Overflow() bool { return r.oflow }

// Interchange
func (r *IntResult) String() string     { return fmt.Sprintf("%s", r.val.String()) }
func (r *IntResult) Dump() string       { return fmt.Sprintf("(%s,%v)", r.val.String(), r.Overflow()) }
func (r *IntResult) AsBigInt() *big.Int { return r.val.AsBigInt() }
func (r *IntResult) AsGoInt64() (int64, bool) {
	i64, oflow := r.val.AsGoInt64()
	return i64, oflow || r.Overflow()
}
func (r *IntResult) AsNative() interface{}       { return r.val.AsNative() }
func (r *IntResult) HasRange() bool              { return r.val.HasRange() }
func (r *IntResult) MaxBigInt() (*big.Int, bool) { return r.val.MaxBigInt() }
func (r *IntResult) MaxGoInt64() (int64, bool)   { return r.val.MaxGoInt64() }
func (r *IntResult) MinBigInt() (*big.Int, bool) { return r.val.MinBigInt() }
func (r *IntResult) MinGoInt64() (int64, bool)   { return r.val.MinGoInt64() }
func (r *IntResult) To(other IntBox) *IntResult  { return r.CarryOverflow(r.val.To(other)) }
func (r *IntResult) Copy() *IntResult            { return MakeIntResult(r.Value(), r.Overflow()) }

// Arithmetic
func (r *IntResult) Rem(div int64) *IntResult       { return r.CarryOverflow(r.val.Rem(div)) }
func (r *IntResult) Neg() *IntResult                { return r.CarryOverflow(r.val.Neg()) }
func (r *IntResult) Sign() int                      { return r.val.Sign() }
func (r *IntResult) Plus(other IntBox) *IntResult   { return r.CarryOverflow(r.val.Plus(other)) }
func (r *IntResult) Minus(other IntBox) *IntResult  { return r.CarryOverflow(r.val.Minus(other)) }
func (r *IntResult) MultBy(other IntBox) *IntResult { return r.CarryOverflow(r.val.MultBy(other)) }
func (r *IntResult) DivBy(other IntBox) *IntResult  { return r.CarryOverflow(r.val.DivBy(other)) }
func (r *IntResult) PlusInt(i int) *IntResult       { return r.CarryOverflow(r.val.PlusInt(i)) }
func (r *IntResult) MinusInt(i int) *IntResult      { return r.CarryOverflow(r.val.MinusInt(i)) }
func (r *IntResult) MultByInt(i int) *IntResult     { return r.CarryOverflow(r.val.MultByInt(i)) }
func (r *IntResult) DivByInt(i int) *IntResult      { return r.CarryOverflow(r.val.DivByInt(i)) }

// BIGUINT =====================================================================
// BigUint uses the Go math/big package and allows number representation
// within the range [0:max].  Suitable for unlimited subsecond precision.
type BigUint struct {
	val *big.Int
	max *big.Int
	*State
}

var _ IntBox = NewBigUint()

func NewBigUint() *BigUint {
	return &BigUint{
		val:   big.NewInt(0),
		max:   big.NewInt(0),
		State: NewState(),
	}
}

func MakeBigUint(val *big.Int, max *big.Int) *IntResult {
	i := NewBigUint()
	i.val = val
	i.max = max
	oflow := isBigUintOverflow(val, max)
	i.SetToEmptyIfAllNil(val, max)
	i.SetToCompleteIfNoneNil(val, max)
	return MakeIntResult(i, oflow)
}

// Comparison
func (i *BigUint) Equals(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*BigUint); isa {
		return i.val.Cmp(to.val) == 0 && i.max.Cmp(to.max) == 0
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*BigUint)
	if i.val.Cmp(j.val) == 0 && i.max.Cmp(j.max) == 0 && to.oflow == false {
		return true
	}
	return false
}
func (i *BigUint) LessThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*BigUint); isa {
		return i.val.Cmp(to.val) < 0
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*BigUint)
	if i.val.Cmp(j.val) < 0 && to.oflow == false {
		return true
	}
	return false
}
func (i *BigUint) MoreThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*BigUint); isa {
		return i.val.Cmp(to.val) > 0
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*BigUint)
	if i.val.Cmp(j.val) > 0 && to.oflow == false {
		return true
	}
	return false
}
func (i *BigUint) IsZero() bool { return i.Sign() == 0 }

// Access
func (i *BigUint) Overflow() bool { return false }

// Interchange
func (i *BigUint) String() string              { return fmt.Sprintf("%v", i.val) }
func (i *BigUint) Dump() string                { return fmt.Sprintf("%v,%v", i.val, i.max) }
func (i *BigUint) AsBigInt() *big.Int          { return i.val }
func (i *BigUint) AsGoInt64() (int64, bool)    { return shoehornBigIntToInt64(i.val) }
func (i *BigUint) AsNative() interface{}       { return i.val }
func (i *BigUint) HasRange() bool              { return true }
func (i *BigUint) MaxBigInt() (*big.Int, bool) { return i.max, true }
func (i *BigUint) MaxGoInt64() (int64, bool)   { return i.max.Int64(), true }
func (i *BigUint) MinBigInt() (*big.Int, bool) { return big.NewInt(0), true }
func (i *BigUint) MinGoInt64() (int64, bool)   { return 0, true }
func (i *BigUint) To(other IntBox) *IntResult  { return convert(i, other) }
func (i *BigUint) Copy() *IntResult            { return MakeBigUint(copyBigInt(i.val), i.max) }

// Arithmetic
func (i *BigUint) Rem(div int64) *IntResult {
	rem := big.NewInt(0)
	d := big.NewInt(div)
	return MakeIntResult(MakeBigUint(rem.Mod(i.val, d), i.max), false)
}
func (i *BigUint) Neg() *IntResult { return i.Copy() } // cannot be negative
func (i *BigUint) Sign() int       { return i.val.Sign() }
func (i *BigUint) Plus(other IntBox) *IntResult {
	tnew := sumBigInt(i.val, other.AsBigInt())
	return MakeBigUint(tnew, i.max)
}
func (i *BigUint) Minus(other IntBox) *IntResult { return subtract(i, other) }
func (i *BigUint) MultBy(other IntBox) *IntResult {
	tnew := multiplyBigInt(i.val, other.AsBigInt())
	return MakeBigUint(tnew, i.max)
}
func (i *BigUint) DivBy(other IntBox) *IntResult {
	tnew := divideBigInt(i.val, other.AsBigInt())
	return MakeBigUint(tnew, i.max)
}
func (i *BigUint) PlusInt(x int) *IntResult   { return i.Plus(MakeInt64(int64(x))) }
func (i *BigUint) MinusInt(x int) *IntResult  { return i.Minus(MakeInt64(int64(x))) }
func (i *BigUint) MultByInt(x int) *IntResult { return i.MultBy(MakeInt64(int64(x))) }
func (i *BigUint) DivByInt(x int) *IntResult  { return i.DivBy(MakeInt64(int64(x))) }

// BIGINT ======================================================================
// BigInt uses the Go math/big package and allows unlimited precision.
// Suitable for cosmological time with any unit, including a zero datum.
type BigInt struct {
	val *big.Int
	*State
}

var _ IntBox = NewBigInt()

func NewBigInt() *BigInt {
	return &BigInt{
		val:   big.NewInt(0),
		State: NewState(),
	}
}

func MakeBigInt(val *big.Int) *IntResult {
	i := NewBigInt()
	i.val = val
	i.SetToEmptyIfAllNil(val)
	return ToResult(i)
}

// Comparison
func (i *BigInt) Equals(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*BigInt); isa {
		return i.val.Cmp(to.val) == 0
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*BigInt)
	if i.val.Cmp(j.val) == 0 && to.oflow == false {
		return true
	}
	return false
}
func (i *BigInt) LessThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*BigInt); isa {
		return i.val.Cmp(to.val) < 0
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*BigInt)
	if i.val.Cmp(j.val) < 0 && to.oflow == false {
		return true
	}
	return false
}
func (i *BigInt) MoreThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*BigInt); isa {
		return i.val.Cmp(to.val) > 0
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*BigInt)
	if i.val.Cmp(j.val) > 0 && to.oflow == false {
		return true
	}
	return false
}
func (i *BigInt) IsZero() bool { return i.Sign() == 0 }

// Access
func (i *BigInt) Overflow() bool { return false }

// Interchange
func (i *BigInt) String() string              { return fmt.Sprintf("%v", i.val) }
func (i *BigInt) Dump() string                { return i.String() }
func (i *BigInt) AsBigInt() *big.Int          { return i.val }
func (i *BigInt) AsGoInt64() (int64, bool)    { return shoehornBigIntToInt64(i.val) }
func (i *BigInt) AsNative() interface{}       { return i.val }
func (i *BigInt) HasRange() bool              { return false }
func (i *BigInt) MaxBigInt() (*big.Int, bool) { return big.NewInt(0), false }
func (i *BigInt) MaxGoInt64() (int64, bool)   { return 0, false }
func (i *BigInt) MinBigInt() (*big.Int, bool) { return big.NewInt(0), false }
func (i *BigInt) MinGoInt64() (int64, bool)   { return 0, false }
func (i *BigInt) To(other IntBox) *IntResult  { return convert(i, other) }
func (i *BigInt) Copy() *IntResult            { return MakeBigInt(copyBigInt(i.val)) }

// Arithmetic
func (i *BigInt) Rem(div int64) *IntResult {
	rem := big.NewInt(0)
	d := big.NewInt(div)
	return MakeBigInt(rem.Mod(i.val, d))
}
func (i *BigInt) Neg() *IntResult { return MakeBigInt(big.NewInt(0).Neg(i.val)) }
func (i *BigInt) Sign() int       { return i.val.Sign() }
func (i *BigInt) Plus(other IntBox) *IntResult {
	tnew := sumBigInt(i.val, other.AsBigInt())
	return MakeBigInt(tnew) // oflow impossible
}
func (i *BigInt) Minus(other IntBox) *IntResult { return subtract(i, other) }
func (i *BigInt) MultBy(other IntBox) *IntResult {
	tnew := multiplyBigInt(i.val, other.AsBigInt())
	return MakeBigInt(tnew)
}
func (i *BigInt) DivBy(other IntBox) *IntResult {
	tnew := divideBigInt(i.val, other.AsBigInt())
	return MakeBigInt(tnew)
}
func (i *BigInt) PlusInt(x int) *IntResult   { return i.Plus(MakeInt64(int64(x))) }
func (i *BigInt) MinusInt(x int) *IntResult  { return i.Minus(MakeInt64(int64(x))) }
func (i *BigInt) MultByInt(x int) *IntResult { return i.MultBy(MakeInt64(int64(x))) }
func (i *BigInt) DivByInt(x int) *IntResult  { return i.DivBy(MakeInt64(int64(x))) }

// INT64 =======================================================================
// Int64 uses an int64 with a range
// [-9,223,372,036,854,775,808:9,223,372,036,854,775,807].
// Suitable for representing a range of 584 years using nanosecond precision,
// including a zero datum.
type Int64 struct {
	val int64
	*State
}

var _ IntBox = NewInt64()

func NewInt64() *Int64 {
	return &Int64{
		State: NewState(),
	}
}

func MakeInt64(val int64) *IntResult {
	i := NewInt64()
	i.val = val
	i.NotEmpty()
	i.NowComplete()
	return ToResult(i)
}

// Comparison
func (i *Int64) Equals(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int64); isa {
		return i.val == to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int64)
	if i.val == j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int64) LessThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int64); isa {
		return i.val < to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int64)
	if i.val < j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int64) MoreThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int64); isa {
		return i.val > to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int64)
	if i.val > j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int64) IsZero() bool { return i.val == 0 }

// Access
func (i *Int64) Overflow() bool { return false }

// Interchange
func (i *Int64) String() string              { return fmt.Sprintf("%d", i.val) }
func (i *Int64) Dump() string                { return i.String() }
func (i *Int64) AsBigInt() *big.Int          { return big.NewInt(i.val) }
func (i *Int64) AsGoInt64() (int64, bool)    { return i.val, false }
func (i *Int64) AsNative() interface{}       { return i.val }
func (i *Int64) HasRange() bool              { return true }
func (i *Int64) MaxBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MaxInt64)), true }
func (i *Int64) MaxGoInt64() (int64, bool)   { return int64(math.MaxInt64), true }
func (i *Int64) MinBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MinInt64)), true }
func (i *Int64) MinGoInt64() (int64, bool)   { return int64(math.MinInt64), true }
func (i *Int64) To(other IntBox) *IntResult  { return convert(i, other) }
func (i *Int64) Copy() *IntResult            { return MakeInt64(i.val) }

// Arithmetic
func (i *Int64) Rem(div int64) *IntResult { return MakeInt64(i.val % div) }
func (i *Int64) Neg() *IntResult          { return MakeInt64(-i.val) }
func (i *Int64) Sign() int                { return sign(int(i.val)) }
func (i *Int64) Plus(other IntBox) *IntResult {
	tnew := NewInt64()
	a := i.val
	b, oflow := other.AsGoInt64()
	c := a + b
	tnew.val = c
	// Credit: http://teaching.idallen.com/c_programming/catchingIntegerOverflow.html
	oflow = oflow || ((a&b & ^c)|(^a & ^b & c)) < 0
	return MakeIntResult(tnew, oflow)
}
func (i *Int64) Minus(other IntBox) *IntResult { return subtract(i, other) }
func (i *Int64) MultBy(other IntBox) *IntResult {
	tnew := NewInt64()
	a := i.val
	b, oflow := other.AsGoInt64()
	c := a * b
	tnew.val = c
	oflow = oflow || (a != 0 && c/a != b) // is there a better way?
	return MakeIntResult(tnew, oflow)
}
func (i *Int64) DivBy(other IntBox) *IntResult {
	tnew := NewInt64()
	to64, oflow := other.AsGoInt64()
	tnew.val = i.val / to64
	return MakeIntResult(tnew, oflow)
}
func (i *Int64) PlusInt(x int) *IntResult   { return i.Plus(MakeInt64(int64(x))) }
func (i *Int64) MinusInt(x int) *IntResult  { return i.Minus(MakeInt64(int64(x))) }
func (i *Int64) MultByInt(x int) *IntResult { return i.MultBy(MakeInt64(int64(x))) }
func (i *Int64) DivByInt(x int) *IntResult  { return i.DivBy(MakeInt64(int64(x))) }

// INT32 =======================================================================
// Int32 uses an int32 with a range [-2,147,483,648:2,147,483,647]
// Suitable for minimising hardware resource usage in order to represent Julian
// days over a range of 11,767,033 years
type Int32 struct {
	val int32
	*State
}

var _ IntBox = NewInt32()

func NewInt32() *Int32 {
	return &Int32{
		State: NewState(),
	}
}

func MakeInt32(val int32) *IntResult {
	i := NewInt32()
	i.val = val
	i.NotEmpty()
	i.NowComplete()
	return ToResult(i)
}

// Comparison
func (i *Int32) Equals(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int32); isa {
		return i.val == to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int32)
	if i.val == j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int32) LessThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int32); isa {
		return i.val < to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int32)
	if i.val < j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int32) MoreThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int32); isa {
		return i.val > to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int32)
	if i.val > j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int32) IsZero() bool { return i.val == 0 }

// Access
func (i *Int32) Overflow() bool { return false }

// Interchange
func (i *Int32) String() string              { return fmt.Sprintf("%d", i.val) }
func (i *Int32) Dump() string                { return i.String() }
func (i *Int32) AsBigInt() *big.Int          { return big.NewInt(int64(i.val)) }
func (i *Int32) AsGoInt64() (int64, bool)    { return int64(i.val), false }
func (i *Int32) AsNative() interface{}       { return i.val }
func (i *Int32) HasRange() bool              { return true }
func (i *Int32) MaxBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MaxInt32)), true }
func (i *Int32) MaxGoInt64() (int64, bool)   { return int64(math.MaxInt32), true }
func (i *Int32) MinBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MinInt32)), true }
func (i *Int32) MinGoInt64() (int64, bool)   { return int64(math.MinInt32), true }
func (i *Int32) To(other IntBox) *IntResult  { return convert(i, other) }
func (i *Int32) Copy() *IntResult            { return MakeInt32(i.val) }

// Arithmetic
func (i *Int32) Rem(div int64) *IntResult {
	i32, oflow := shoehornInt32(MakeInt64(int64(i.val) % div))
	return MakeIntResult(MakeInt32(i32), oflow)
}
func (i *Int32) Neg() *IntResult { return MakeInt32(-i.val) }
func (i *Int32) Sign() int       { return sign(int(i.val)) }
func (i *Int32) Plus(other IntBox) *IntResult {
	tnew := NewInt32()
	a := i.val
	b, oflow := shoehornInt32(other)
	c := a + b
	tnew.val = c
	// Credit: http://teaching.idallen.com/c_programming/catchingIntegerOverflow.html
	oflow = oflow || ((a&b & ^c)|(^a & ^b & c)) < 0
	return MakeIntResult(tnew, oflow)
}
func (i *Int32) Minus(other IntBox) *IntResult { return subtract(i, other) }
func (i *Int32) MultBy(other IntBox) *IntResult {
	tnew := NewInt32()
	a := i.val
	b, oflow := shoehornInt32(other)
	c := a * b
	tnew.val = c
	oflow = oflow || (a != 0 && c/a != b) // is there a better way?
	return MakeIntResult(tnew, oflow)
}
func (i *Int32) DivBy(other IntBox) *IntResult {
	tnew := NewInt32()
	a := i.val
	b, oflow := shoehornInt32(other)
	tnew.val = a / b
	return MakeIntResult(tnew, oflow)
}
func (i *Int32) PlusInt(x int) *IntResult   { return i.Plus(MakeInt64(int64(x))) }
func (i *Int32) MinusInt(x int) *IntResult  { return i.Minus(MakeInt64(int64(x))) }
func (i *Int32) MultByInt(x int) *IntResult { return i.MultBy(MakeInt64(int64(x))) }
func (i *Int32) DivByInt(x int) *IntResult  { return i.DivBy(MakeInt64(int64(x))) }

// INT16 =======================================================================
// Int16 uses an int16 with a range [-32,768:32,767].
// Suitable for minimising hardware resource usage in order to represent a range
// of 65,535 years including a zero year.
type Int16 struct {
	val int16
	*State
}

var _ IntBox = NewInt16()

func NewInt16() *Int16 {
	return &Int16{
		State: NewState(),
	}
}

func MakeInt16(val int16) *IntResult {
	i := NewInt16()
	i.val = val
	i.NotEmpty()
	i.NowComplete()
	return ToResult(i)
}

// Comparison
func (i *Int16) Equals(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int16); isa {
		return i.val == to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int16)
	if i.val == j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int16) LessThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int16); isa {
		return i.val < to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int16)
	if i.val < j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int16) MoreThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int16); isa {
		return i.val > to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int16)
	if i.val > j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int16) IsZero() bool { return i.val == 0 }

// Access
func (i *Int16) Overflow() bool { return false }

// Interchange
func (i *Int16) String() string              { return fmt.Sprintf("%d", i.val) }
func (i *Int16) Dump() string                { return i.String() }
func (i *Int16) AsBigInt() *big.Int          { return big.NewInt(int64(i.val)) }
func (i *Int16) AsGoInt64() (int64, bool)    { return int64(i.val), false }
func (i *Int16) AsNative() interface{}       { return i.val }
func (i *Int16) HasRange() bool              { return true }
func (i *Int16) MaxBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MaxInt16)), true }
func (i *Int16) MaxGoInt64() (int64, bool)   { return int64(math.MaxInt16), true }
func (i *Int16) MinBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MinInt16)), true }
func (i *Int16) MinGoInt64() (int64, bool)   { return int64(math.MinInt16), true }
func (i *Int16) To(other IntBox) *IntResult  { return convert(i, other) }
func (i *Int16) Copy() *IntResult            { return MakeInt16(i.val) }

// Arithmetic
func (i *Int16) Rem(div int64) *IntResult {
	i16, oflow := shoehornInt16(MakeInt64(int64(i.val) % div))
	return MakeIntResult(MakeInt16(i16), oflow)
}
func (i *Int16) Neg() *IntResult { return MakeInt16(-i.val) }
func (i *Int16) Sign() int       { return sign(int(i.val)) }
func (i *Int16) Plus(other IntBox) *IntResult {
	tnew := NewInt16()
	a := i.val
	b, oflow := shoehornInt16(other)
	c := a + b
	tnew.val = c
	// Credit: http://teaching.idallen.com/c_programming/catchingIntegerOverflow.html
	oflow = oflow || ((a&b & ^c)|(^a & ^b & c)) < 0
	return MakeIntResult(tnew, oflow)
}
func (i *Int16) Minus(other IntBox) *IntResult { return subtract(i, other) }
func (i *Int16) MultBy(other IntBox) *IntResult {
	tnew := NewInt16()
	a := i.val
	b, oflow := shoehornInt16(other)
	c := a * b
	tnew.val = c
	oflow = oflow || (a != 0 && c/a != b) // is there a better way?
	return MakeIntResult(tnew, oflow)
}
func (i *Int16) DivBy(other IntBox) *IntResult {
	tnew := NewInt16()
	a := i.val
	b, oflow := shoehornInt16(other)
	tnew.val = a / b
	return MakeIntResult(tnew, oflow)
}
func (i *Int16) PlusInt(x int) *IntResult   { return i.Plus(MakeInt64(int64(x))) }
func (i *Int16) MinusInt(x int) *IntResult  { return i.Minus(MakeInt64(int64(x))) }
func (i *Int16) MultByInt(x int) *IntResult { return i.MultBy(MakeInt64(int64(x))) }
func (i *Int16) DivByInt(x int) *IntResult  { return i.DivBy(MakeInt64(int64(x))) }

// INT8 ========================================================================
// Int8 uses an int8 with a range [-128:127].
// Suitable for minimising hardware resource usage in order to represent a range
// of 255 years including a zero year.
type Int8 struct {
	val int8
	*State
}

var _ IntBox = NewInt8()

func NewInt8() *Int8 {
	return &Int8{
		State: NewState(),
	}
}

func MakeInt8(val int8) *IntResult {
	i := NewInt8()
	i.val = val
	i.NotEmpty()
	i.NowComplete()
	return ToResult(i)
}

// Comparison
func (i *Int8) Equals(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int8); isa {
		return i.val == to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int8)
	if i.val == j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int8) LessThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int8); isa {
		return i.val < to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int8)
	if i.val < j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int8) MoreThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Int8); isa {
		return i.val > to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Int8)
	if i.val > j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Int8) IsZero() bool { return i.val == 0 }

// Access
func (i *Int8) Overflow() bool { return false }

// Interchange
func (i *Int8) String() string              { return fmt.Sprintf("%d", i.val) }
func (i *Int8) Dump() string                { return i.String() }
func (i *Int8) AsBigInt() *big.Int          { return big.NewInt(int64(i.val)) }
func (i *Int8) AsGoInt64() (int64, bool)    { return int64(i.val), false }
func (i *Int8) HasRange() bool              { return true }
func (i *Int8) AsNative() interface{}       { return i.val }
func (i *Int8) MaxBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MaxInt8)), true }
func (i *Int8) MaxGoInt64() (int64, bool)   { return int64(math.MaxInt8), true }
func (i *Int8) MinBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MinInt8)), true }
func (i *Int8) MinGoInt64() (int64, bool)   { return int64(math.MinInt8), true }
func (i *Int8) To(other IntBox) *IntResult  { return convert(i, other) }
func (i *Int8) Copy() *IntResult            { return MakeInt8(i.val) }

// Arithmetic
func (i *Int8) Rem(div int64) *IntResult {
	i8, oflow := shoehornInt8(MakeInt64(int64(i.val) % div))
	return MakeIntResult(MakeInt8(i8), oflow)
}
func (i *Int8) Neg() *IntResult { return MakeInt8(-i.val) }
func (i *Int8) Sign() int       { return sign(int(i.val)) }
func (i *Int8) Plus(other IntBox) *IntResult {
	tnew := NewInt8()
	a := i.val
	b, oflow := shoehornInt8(other)
	c := a + b
	tnew.val = c
	// Credit: http://teaching.idallen.com/c_programming/catchingIntegerOverflow.html
	oflow = oflow || ((a&b & ^c)|(^a & ^b & c)) < 0
	return MakeIntResult(tnew, oflow)
}
func (i *Int8) Minus(other IntBox) *IntResult { return subtract(i, other) }
func (i *Int8) MultBy(other IntBox) *IntResult {
	tnew := NewInt8()
	a := i.val
	b, oflow := shoehornInt8(other)
	c := a * b
	tnew.val = c
	oflow = oflow || (a != 0 && c/a != b) // is there a better way?
	return MakeIntResult(tnew, oflow)
}
func (i *Int8) DivBy(other IntBox) *IntResult {
	tnew := NewInt8()
	a := i.val
	b, oflow := shoehornInt8(other)
	tnew.val = a / b
	return MakeIntResult(tnew, oflow)
}
func (i *Int8) PlusInt(x int) *IntResult   { return i.Plus(MakeInt64(int64(x))) }
func (i *Int8) MinusInt(x int) *IntResult  { return i.Minus(MakeInt64(int64(x))) }
func (i *Int8) MultByInt(x int) *IntResult { return i.MultBy(MakeInt64(int64(x))) }
func (i *Int8) DivByInt(x int) *IntResult  { return i.DivBy(MakeInt64(int64(x))) }

// UINT64 ======================================================================
// Uint64 uses an uint64 with a range [0:18,446,744,073,709,551,615]
type Uint64 struct {
	val uint64
	*State
}

var _ IntBox = NewUint64()

func NewUint64() *Uint64 {
	return &Uint64{
		State: NewState(),
	}
}

func MakeUint64(val uint64) *IntResult {
	i := NewUint64()
	i.val = val
	i.NotEmpty()
	i.NowComplete()
	return ToResult(i)
}

// Comparison
func (i *Uint64) Equals(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Uint64); isa {
		return i.val == to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Uint64)
	if i.val == j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Uint64) LessThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Uint64); isa {
		return i.val < to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Uint64)
	if i.val < j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Uint64) MoreThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Uint64); isa {
		return i.val > to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Uint64)
	if i.val > j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Uint64) IsZero() bool { return i.val == 0 }

// Access
func (i *Uint64) Overflow() bool { return false }

// Interchange
func (i *Uint64) String() string     { return fmt.Sprintf("%d", i.val) }
func (i *Uint64) Dump() string       { return i.String() }
func (i *Uint64) AsBigInt() *big.Int { return big.NewInt(0).SetUint64(i.val) }
func (i *Uint64) AsGoInt64() (int64, bool) {
	return int64(i.val), i.val > uint64(math.MaxInt64)
}
func (i *Uint64) AsNative() interface{}       { return i.val }
func (i *Uint64) HasRange() bool              { return true }
func (i *Uint64) MaxBigInt() (*big.Int, bool) { return big.NewInt(0).SetUint64(math.MaxUint64), true }
func (i *Uint64) MaxGoInt64() (int64, bool)   { return 0, false }
func (i *Uint64) MinBigInt() (*big.Int, bool) { return big.NewInt(0).SetUint64(math.MaxUint64), true }
func (i *Uint64) MinGoInt64() (int64, bool)   { return 0, false }
func (i *Uint64) To(other IntBox) *IntResult  { return convert(i, other) }
func (i *Uint64) Copy() *IntResult            { return MakeUint64(i.val) }

// Arithmetic
func (i *Uint64) Rem(div int64) *IntResult {
	i64, oflow := i.AsGoInt64()
	return MakeIntResult(MakeUint64(uint64(i64%div)), oflow)
}
func (i *Uint64) Neg() *IntResult { return i.Copy() } // cannot have negative
func (i *Uint64) Sign() int       { return sign(int(i.val)) }
func (i *Uint64) Plus(other IntBox) *IntResult {
	tnew := NewUint64()
	a := i.val
	a1, oflow := other.AsGoInt64()
	b := uint64(a1)
	c := a + b
	tnew.val = c
	// Credit: http://teaching.idallen.com/c_programming/catchingIntegerOverflow.html
	oflow = oflow || ((a&b & ^c)|(^a & ^b & c)) < 0
	return MakeIntResult(tnew, oflow)
}
func (i *Uint64) Minus(other IntBox) *IntResult { return subtract(i, other) }
func (i *Uint64) MultBy(other IntBox) *IntResult {
	tnew := NewUint64()
	a := i.val
	a1, oflow := other.AsGoInt64()
	b := uint64(a1)
	c := a * b
	tnew.val = c
	oflow = oflow || (a != 0 && c/a != b) // is there a better way?
	return MakeIntResult(tnew, oflow)
}
func (i *Uint64) DivBy(other IntBox) *IntResult {
	tnew := NewUint64()
	a := i.val
	a1, oflow := other.AsGoInt64()
	b := uint64(a1)
	tnew.val = a / b
	return MakeIntResult(tnew, oflow)
}
func (i *Uint64) PlusInt(x int) *IntResult   { return i.Plus(MakeInt64(int64(x))) }
func (i *Uint64) MinusInt(x int) *IntResult  { return i.Minus(MakeInt64(int64(x))) }
func (i *Uint64) MultByInt(x int) *IntResult { return i.MultBy(MakeInt64(int64(x))) }
func (i *Uint64) DivByInt(x int) *IntResult  { return i.DivBy(MakeInt64(int64(x))) }

// UINT32 ======================================================================
// Uint32 uses an uint32 with a range [0:4,294,967,295].
// Suitable for nanosecond precision.
type Uint32 struct {
	val uint32
	*State
}

var _ IntBox = NewUint32()

func NewUint32() *Uint32 {
	return &Uint32{
		State: NewState(),
	}
}

func MakeUint32(val uint32) *IntResult {
	i := NewUint32()
	i.val = val
	i.NotEmpty()
	i.NowComplete()
	return ToResult(i)
}

// Comparison
func (i *Uint32) Equals(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Uint32); isa {
		return i.val == to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Uint32)
	if i.val == j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Uint32) LessThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Uint32); isa {
		return i.val < to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Uint32)
	if i.val < j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Uint32) MoreThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Uint32); isa {
		return i.val > to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Uint32)
	if i.val > j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Uint32) IsZero() bool { return i.val == 0 }

// Access
func (i *Uint32) Overflow() bool { return false }

// Interchange
func (i *Uint32) String() string              { return fmt.Sprintf("%d", i.val) }
func (i *Uint32) Dump() string                { return i.String() }
func (i *Uint32) AsBigInt() *big.Int          { return big.NewInt(int64(i.val)) }
func (i *Uint32) AsGoInt64() (int64, bool)    { return int64(i.val), false }
func (i *Uint32) AsNative() interface{}       { return i.val }
func (i *Uint32) HasRange() bool              { return true }
func (i *Uint32) MaxBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MaxUint32)), true }
func (i *Uint32) MaxGoInt64() (int64, bool)   { return int64(math.MaxUint32), true }
func (i *Uint32) MinBigInt() (*big.Int, bool) { return big.NewInt(0), true }
func (i *Uint32) MinGoInt64() (int64, bool)   { return 0, true }
func (i *Uint32) To(other IntBox) *IntResult  { return convert(i, other) }
func (i *Uint32) Copy() *IntResult            { return MakeUint32(i.val) }

// Arithmetic
func (i *Uint32) Rem(div int64) *IntResult {
	ui32, oflow := shoehornUint32(MakeInt64(int64(i.val) % div))
	return MakeIntResult(MakeUint32(ui32), oflow)
}
func (i *Uint32) Neg() *IntResult { return i.Copy() } // cannot have negative
func (i *Uint32) Sign() int       { return sign(int(i.val)) }
func (i *Uint32) Plus(other IntBox) *IntResult {
	tnew := NewUint32()
	a := i.val
	b, oflow := shoehornUint32(other)
	c := a + b
	tnew.val = c
	// Credit: http://teaching.idallen.com/c_programming/catchingIntegerOverflow.html
	oflow = oflow || ((a&b & ^c)|(^a & ^b & c)) < 0
	return MakeIntResult(tnew, oflow)
}
func (i *Uint32) Minus(other IntBox) *IntResult { return subtract(i, other) }
func (i *Uint32) MultBy(other IntBox) *IntResult {
	tnew := NewUint32()
	a := i.val
	b, oflow := shoehornUint32(other)
	c := a * b
	tnew.val = c
	oflow = oflow || (a != 0 && c/a != b) // is there a better way?
	return MakeIntResult(tnew, oflow)
}
func (i *Uint32) DivBy(other IntBox) *IntResult {
	tnew := NewUint32()
	a := i.val
	b, oflow := shoehornUint32(other)
	tnew.val = a / b
	return MakeIntResult(tnew, oflow)
}
func (i *Uint32) PlusInt(x int) *IntResult   { return i.Plus(MakeInt64(int64(x))) }
func (i *Uint32) MinusInt(x int) *IntResult  { return i.Minus(MakeInt64(int64(x))) }
func (i *Uint32) MultByInt(x int) *IntResult { return i.MultBy(MakeInt64(int64(x))) }
func (i *Uint32) DivByInt(x int) *IntResult  { return i.DivBy(MakeInt64(int64(x))) }

// UINT8 =======================================================================
// Uint8 uses an uint8 with a range [0:255].
// Suitable for day or month enumeration.
type Uint8 struct {
	val uint8
	*State
}

var _ IntBox = NewUint8()

func NewUint8() *Uint8 {
	return &Uint8{
		State: NewState(),
	}
}

func MakeUint8(val uint8) *IntResult {
	i := NewUint8()
	i.val = val
	i.NotEmpty()
	i.NowComplete()
	return ToResult(i)
}

// Comparison
func (i *Uint8) Equals(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Uint8); isa {
		return i.val == to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Uint8)
	if i.val == j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Uint8) LessThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Uint8); isa {
		return i.val < to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Uint8)
	if i.val < j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Uint8) MoreThan(other IntBox) bool {
	if i.Overflow() == true || other.Overflow() == true {
		return false
	}
	// Attempt faster internal cast first...
	if to, isa := other.(*Uint8); isa {
		return i.val > to.val
	}
	// ...and convert if necessary
	to := convert(other, i)
	j, _ := to.Value().(*Uint8)
	if i.val > j.val && to.oflow == false {
		return true
	}
	return false
}
func (i *Uint8) IsZero() bool { return i.val == 0 }

// Access
func (i *Uint8) Overflow() bool { return false }

// Interchange
func (i *Uint8) String() string              { return fmt.Sprintf("%d", i.val) }
func (i *Uint8) Dump() string                { return i.String() }
func (i *Uint8) AsBigInt() *big.Int          { return big.NewInt(int64(i.val)) }
func (i *Uint8) AsGoInt64() (int64, bool)    { return int64(i.val), false }
func (i *Uint8) AsNative() interface{}       { return i.val }
func (i *Uint8) HasRange() bool              { return true }
func (i *Uint8) MaxBigInt() (*big.Int, bool) { return big.NewInt(int64(math.MaxUint8)), true }
func (i *Uint8) MaxGoInt64() (int64, bool)   { return int64(math.MaxUint8), true }
func (i *Uint8) MinBigInt() (*big.Int, bool) { return big.NewInt(0), true }
func (i *Uint8) MinGoInt64() (int64, bool)   { return 0, true }
func (i *Uint8) To(other IntBox) *IntResult  { return convert(i, other) }
func (i *Uint8) Copy() *IntResult            { return MakeUint8(i.val) }

// Arithmetic
func (i *Uint8) Rem(div int64) *IntResult {
	ui8, oflow := shoehornUint8(MakeInt64(int64(i.val) % div))
	return MakeIntResult(MakeUint8(ui8), oflow)
}
func (i *Uint8) Neg() *IntResult { return i.Copy() } // cannot have negative
func (i *Uint8) Sign() int       { return sign(int(i.val)) }
func (i *Uint8) Plus(other IntBox) *IntResult {
	tnew := NewUint8()
	a := i.val
	b, oflow := shoehornUint8(other)
	c := a + b
	tnew.val = c
	// Credit: http://teaching.idallen.com/c_programming/catchingIntegerOverflow.html
	oflow = oflow || ((a&b & ^c)|(^a & ^b & c)) < 0
	return MakeIntResult(tnew, oflow)
}
func (i *Uint8) Minus(other IntBox) *IntResult { return subtract(i, other) }
func (i *Uint8) MultBy(other IntBox) *IntResult {
	tnew := NewUint8()
	a := i.val
	b, oflow := shoehornUint8(other)
	c := a * b
	tnew.val = c
	oflow = oflow || (a != 0 && c/a != b) // is there a better way?
	return MakeIntResult(tnew, oflow)
}
func (i *Uint8) DivBy(other IntBox) *IntResult {
	tnew := NewUint8()
	a := i.val
	b, oflow := shoehornUint8(other)
	tnew.val = a / b
	return MakeIntResult(tnew, oflow)
}
func (i *Uint8) PlusInt(x int) *IntResult   { return i.Plus(MakeInt64(int64(x))) }
func (i *Uint8) MinusInt(x int) *IntResult  { return i.Minus(MakeInt64(int64(x))) }
func (i *Uint8) MultByInt(x int) *IntResult { return i.MultBy(MakeInt64(int64(x))) }
func (i *Uint8) DivByInt(x int) *IntResult  { return i.DivBy(MakeInt64(int64(x))) }

// COMMON SUPPORT FUNCTIONS **************************************************

func subtract(a, b IntBox) *IntResult {
	//negb, oflow1 := b.Neg().Explode()
	//sum, oflow2 := a.Plus(negb).Explode()
	//return MakeIntResult(sum, oflow1 || oflow2)
	return a.Plus(b.Neg())
}

func sumBigInt(a, b *big.Int) *big.Int {
	c := big.NewInt(0)
	return c.Add(a, b)
}

func multiplyBigInt(a, b *big.Int) *big.Int {
	c := big.NewInt(0)
	return c.Mul(a, b)
}

func divideBigInt(a, b *big.Int) *big.Int {
	c := big.NewInt(0) // allow big to panic about divide by zero
	return c.Div(a, b)
}

func isBigUintOverflow(bi, max *big.Int) bool {
	// Test whether c is < 0 or > max
	d := big.NewInt(0)
	d.Sub(max, bi)
	return (bi.Sign() < 0) || max.Sign() < 0 || (d.Sign() < 0)
}

func shoehornBigIntToInt64(b *big.Int) (int64, bool) {
	c := big.NewInt(math.MaxInt64)
	c.Sub(b, c)
	return b.Int64(), c.Sign() > 0
}

func shoehornInt32(i IntBox) (int32, bool) {
	b, oflow := i.AsGoInt64()
	c := int32(0)
	return int32(b), oflow || reflect.ValueOf(c).OverflowInt(b)
}

func shoehornInt16(i IntBox) (int16, bool) {
	b, oflow := i.AsGoInt64()
	c := int16(0)
	return int16(b), oflow || reflect.ValueOf(c).OverflowInt(b)
}

func shoehornInt8(i IntBox) (int8, bool) {
	b, oflow := i.AsGoInt64()
	c := int8(0)
	return int8(b), oflow || reflect.ValueOf(c).OverflowInt(b)
}

func shoehornUint32(i IntBox) (uint32, bool) {
	b, oflow := i.AsGoInt64()
	oflow = oflow || b < 0
	c := uint32(0)
	return uint32(b), oflow || reflect.ValueOf(c).OverflowUint(uint64(b))
}

func shoehornUint8(i IntBox) (uint8, bool) {
	b, oflow := i.AsGoInt64()
	oflow = oflow || b < 0
	c := uint8(0)
	return uint8(b), oflow || reflect.ValueOf(c).OverflowUint(uint64(b))
}

func convert(from, other IntBox) *IntResult {
	if other == nil {
		return MakeIntResult(from, from.Overflow())
	}
	switch to := other.Copy().Value().(type) {
	case *IntResult:
		to.val = from
		to.oflow = from.Overflow()
		return to
	case *BigUint:
		to.val = from.AsBigInt()
		return MakeIntResult(to, false)
	case *BigInt:
		to.val = from.AsBigInt()
		return MakeIntResult(to, false)
	case *Int64:
		i64, oflow := from.AsGoInt64()
		to.val = i64
		return MakeIntResult(to, oflow)
	case *Int32:
		i32, oflow := shoehornInt32(from)
		to.val = i32
		return MakeIntResult(to, oflow)
	case *Int16:
		i16, oflow := shoehornInt16(from)
		to.val = i16
		return MakeIntResult(to, oflow)
	case *Int8:
		i8, oflow := shoehornInt8(from)
		to.val = i8
		return MakeIntResult(to, oflow)
	case *Uint64:
		bi := from.AsBigInt()
		ui64max := big.NewInt(0).SetUint64(math.MaxUint64)
		oflow := false
		if bi.Cmp(ui64max) > 0 || bi.Cmp(big.NewInt(0)) < 0 {
			oflow = true
			to.NowEmpty()
		} else {
			to.NotEmpty()
		}
		to.val = bi.Uint64()
		return MakeIntResult(to, oflow)
	case *Uint32:
		ui32, oflow := shoehornUint32(from)
		to.val = ui32
		return MakeIntResult(to, oflow)
	case *Uint8:
		ui8, oflow := shoehornUint8(from)
		to.val = ui8
		return MakeIntResult(to, oflow)
	}
	return nil
}

func sign(i int) int {
	if i == 0 {
		return 0
	}
	if i < 0 {
		return -1
	} else {
		return 1
	}
}

func copyBigInt(i *big.Int) *big.Int {
	from := []*big.Int{i}
	to := make([]*big.Int, 1)
	copy(to, from)
	return to[0]
}
