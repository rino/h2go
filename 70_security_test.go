package h2go

import (
	"testing"
)

func TestSecret(t *testing.T) {
	Env.Log().MajorSection("Encrypt, decrypt using SecretBox")
	pass := "efhwe8y83h"

	msg := "This is a test"
	Env.Log().MinorSection("Encrypt, decrypt message %q", msg)
	sec, phash, goof := BuildSecret(pass, EmptyBox())
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem building %T", sec)
	}
	enc, goof := sec.Encrypt([]byte(msg))
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem encrypting with %T", sec)
	}
	dec, goof := sec.Decrypt(enc)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decrypting with %T", sec)
	}
	if string(dec) != msg {
		t.Fatalf("Expected decrypted message %q, got %q",
			msg, string(dec))
	}

	Env.Log().MinorSection("Encrypt, decrypt double hash of passphrase")
	enc, goof = sec.Encrypt(phash.DblHash())
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem encrypting with %T", sec)
	}
	dec, goof = sec.Decrypt(enc)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decrypting with %T", sec)
	}
	if len(dec) != len(phash.DblHash()) {
		t.Fatalf("Expected decrypted message length %d, got %d",
			len(phash.DblHash()), len(dec))
	}
	for i, byt := range phash.DblHash() {
		if byt != dec[i] {
			t.Fatalf("Expected decrypted message %X differs "+
				"from actual %X at index %d",
				phash.DblHash(), dec, i)
		}
	}
}

func TestAES_256(t *testing.T) {
	Env.Log().MajorSection("Encrypt, decrypt using AES-256")
	pass := "efhwe8y83h"

	msg := "This is a test"
	Env.Log().MinorSection("Encrypt, decrypt message %q", msg)
	aes256, phash, goof := ConstructAES_256(pass, EmptyBox())
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem building %T", aes256)
	}
	enc, goof := aes256.Encrypt([]byte(msg))
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem encrypting with %T", aes256)
	}
	dec, goof := aes256.Decrypt(enc)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decrypting with %T", aes256)
	}
	if string(dec) != msg {
		t.Fatalf("Expected decrypted message %q, got %q",
			msg, string(dec))
	}

	Env.Log().MinorSection("Encrypt, decrypt double hash of passphrase")
	enc, goof = aes256.Encrypt(phash.DblHash())
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem encrypting with %T", aes256)
	}
	dec, goof = aes256.Decrypt(enc)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decrypting with %T", aes256)
	}
	if len(dec) != len(phash.DblHash()) {
		t.Fatalf("Expected decrypted message length %d, got %d",
			len(phash.DblHash()), len(dec))
	}
	for i, byt := range phash.DblHash() {
		if byt != dec[i] {
			t.Fatalf("Expected decrypted message %X differs "+
				"from actual %X at index %d",
				phash.DblHash(), dec, i)
		}
	}
}

func TestRSA_OAEP(t *testing.T) {
	Env.Log().MajorSection("Encrypt, decrypt using RSA-OAEP")
	msg := "This is a test"
	roaep := NewRSA_OAEP()
	Env.Log().MinorSection("Generate random RSA key")
	_, goof := roaep.SetRandomKeys()
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem generating key for %T", roaep)
	}
	Env.Log().MinorSection("Encrypt, decrypt")
	enc, goof := roaep.Encrypt([]byte(msg))
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem encrypting with %T", roaep)
	}
	dec, goof := roaep.Decrypt(enc)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decrypting with %T", roaep)
	}
	if string(dec) != msg {
		t.Fatalf("Expected decrypted message %q, got %q",
			msg, string(dec))
	}
}

func TestKeysEncodeAndEquals(t *testing.T) {
	Env.Log().MajorSection("Test factory, encode, decode, equals for EncryptionSchemes")
	for _, name := range []string{
		"ENCRYPTION_NONE",
		"ENCRYPTION_SECRETBOX",
		"ENCRYPTION_AES_256",
		"ENCRYPTION_RSA_OAEP",
	} {
		Env.Log().MinorSection(name)
		enc, goof := NewEncryptionScheme(name)
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem creating new %T %q", enc, name)
		}
		enc2, _ := NewEncryptionScheme(name)
		_, goof = enc.SetRandomKeys()
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem creating random key for %T", enc)
		}
		txt, goof := enc.KeysToString()
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem encoding keys to string for %T", enc)
		}
		Env.Log().Basic("Encoded = %q", txt)
		goof = enc2.KeysFromString(txt)
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem decoding keys from string for %T", enc2)
		}
		txt2, goof := enc2.KeysToString()
		Env.Log().IsError(goof)
		if !enc.Equals(enc2) {
			t.Fatalf("Expected decoded keys %q to equal encoded %q for %T",
				txt2, txt, enc2)
		}
		Env.Log().Basic("Decoded = %q", txt2)
	}
}
