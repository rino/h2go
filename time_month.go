// Calendrical time revolves around the division of the year, otherwise known as the month.
package h2go

import (
	"fmt"
	"regexp"
	"strings"
)

//== MONTH =====================================================================

type Month struct {
	val  uint8
	year IntBox
	cal  Calendar
	*State
}

func NewMonth() *Month {
	return &Month{
		year:  NewInt64(),
		cal:   NewGregorian(),
		State: NewState(),
	}
}

func MakeMonth(y IntBox, m uint8, cal Calendar) (*Month, *Goof) {
	month := NewMonth()
	if y.IsEmpty() {
		return month, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Cannot create month, year %v is empty", y)
	}
	if cal.IsEmpty() {
		return month, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Cannot create month, calendar %v is empty", cal)
	}
	month.val = m
	month.NotEmpty()
	month.year = y
	month.cal = cal
	month.NowComplete()
	return month, NO_GOOF
}

// New month using same calendar.
func (month *Month) NewMonth(y IntBox, m uint8) (*Month, *Goof) {
	return MakeMonth(y, m, month.Calendar())
}

// Getters.

func (month *Month) Value() uint8       { return month.val }
func (month *Month) Year() IntBox       { return month.year }
func (month *Month) Calendar() Calendar { return month.cal }

func (month *Month) Equals(other *Month) bool {
	return month.val == other.val &&
		month.year.Equals(other.year) &&
		month.cal.Name() == other.cal.Name()
}

func (month *Month) FirstDay() *Date {
	date, _ := MakeDate(month, 1)
	return date
}

func (month *Month) LastDay() *Date {
	date, _ := MakeDate(month, month.Calendar().DaysInMonth(month))
	return date
}

func (month *Month) Inc(incMonth int) (*Month, *Goof) {
	date, _ := MakeDate(month, 1)
	newdate, goof := date.Plus(0, incMonth, 0)
	return newdate.Month(), goof
}

// A special parser for identifying calendar days.
//
// The standard parse method takes a string and a CalClockZone in order
// to come up with a CalendarDate.  This parser takes the additional
// arguments MonthPeriod and Holidays in order to find a valid
// CalendarDate, interpreting English expressions like:
//
// - "the 3rd Sunday of the month"
// - "second business day after the 1st Monday"
// - "the 2nd weekday prior to the 25th"
// - "the 2nd weekday prior to the end of the month"
//
// The pattern is:
//
// - [incrementor] [qualifier] [anchor]
//
// where [qualifier] is optional, with the pattern
//
// - "of the|before|prior to|after|following the"
//
// and where [incrementor] is optional, with the pattern
//
// - "the|" [numeric] [dayType]
//
// where [numeric] is either a numeral or a valid OrdinalEnglish value and
// [dayType] is either "day|weekday|business day" or a valid DayOfWeek, and
// [anchor] is
//
// - "the|" [numeric]|[numeric|dayType]
//
// Currently the day can be incremented in two steps, which seems to be of
// most practical use, although DayIncrementor is quite general and
// additional steps could be added.
//
func (month *Month) FindUsingDayInc(dinc *DayInc, holidays Intervals) (*Date, *Goof) {
	holidaysWithinMonth := NewIntervals()
	if !holidays.IsEmpty() {
		//holidaysWithinMonth = holidays.AreWithin(month.ToInterval())
	}

	dinc.CheckComplete()
	dinc.next.CheckComplete()
	if dinc.IsComplete() && !dinc.next.IsComplete() {
		// We only have one valid incrementor
		return dinc.FromDayOfMonth(
			month.FirstDay(), holidaysWithinMonth)
	} else {
		// We have two valid incrementors.
		// This could be readily generalised to start at the last incrementor and
		// loop back to the first, if necessary, but two levels should cover most
		// requirements for now.

		if dinc.next.dtype == TIME_DAY_OF_MONTH_FROM_END {
			return dinc.FromDayOfMonth(
				month.LastDay(), holidaysWithinMonth)
		} else {
			date2, goof :=
				dinc.next.FromDayOfMonth(
					month.FirstDay(), holidaysWithinMonth)
			if IsError(goof) {
				return NewDate(), goof
			}
			return dinc.FromDayOfMonth(date2, holidaysWithinMonth)
		}
	}
}

func (month *Month) Find(input string, lang Language, holidays Intervals) (*Date, *Goof) {
	dinc, goof := MakeDayIncFromString(input, lang, month.Calendar())
	if IsError(goof) {
		return NewDate(), goof
	}
	return month.FindUsingDayInc(dinc, holidays)
}

//== DAY INCREMENTOR ===========================================================

type DayInc struct {
	val   uint8
	sign  int   // Relative to child (next)
	dow   uint8 // Day of week
	dtype int   // Day type
	prev  *DayInc
	next  *DayInc
	*State
}

func NewDayInc() *DayInc {
	return &DayInc{
		State: NewState(),
	}
}

func (dinc *DayInc) SetNextInc(other *DayInc) *DayInc {
	dinc.next = other
	return dinc
}

func (dinc *DayInc) SetDayOfWeek(dow uint8) *DayInc {
	dinc.dow = dow
	dinc.dtype = TIME_DAY_OF_WEEK
	return dinc
}

// Sets the incrementor to the nth occurrence of the given day of the week.
//
// These "concrete" constructors must implement a pattern expected by
// consumers of DayIncrementor elsewhere in the code, that is, using
// a two-step incrementor, even if the second step is blank.  In other
// words, we need to initialise nextInc.
func MakeDayIncDayOfWeek(nth, dow uint8) *DayInc {
	dinc := NewDayInc()
	dinc.next = NewDayInc()
	dinc.val = nth
	dinc.NotEmpty()
	dinc.SetDayOfWeek(dow)
	dinc.CheckComplete()
	return dinc
}

// Sets the incrementor to a day of the month.  There is no way for us to
// check whether this will result in a valid date.
//
// These "concrete" constructors must implement a pattern expected by
// consumers of DayIncrementor elsewhere in the code, that is, using
// a two-step incrementor, even if the second step is blank.  In other
// words, we need to initialise nextInc.
func MakeDayIncDayOfMonth(dom uint8) *DayInc {
	dinc := NewDayInc()
	dinc.next = NewDayInc()
	dinc.val = dom
	dinc.NotEmpty()
	dinc.dtype = TIME_DAY_OF_MONTH_FROM_START
	dinc.CheckComplete()
	return dinc
}

// Interpret incrementor from a string.
func MakeDayIncFromString(input string, lang Language, cal Calendar) (*DayInc, *Goof) {
	orig := input
	dinc := NewDayInc()
	dinc.next = NewDayInc()
	processed := dinc.preprocess(input, lang)
	words := strings.Split(processed, " ")
	witer := MakeWordIterator(words)

	// Currently a two-step incrementor
	for {
		dinc.CheckComplete()
		if dinc.IsComplete() {
			if dinc.isSignDefined() {
				dinc.next.lex(witer, lang, cal)
			} else {
				dinc.lexQualifier(witer, lang)
			}
		} else {
			dinc.lex(witer, lang, cal)
		}
		if witer.Next() == NILWORD {
			break
		}
	}

	// parse

	dinc.CheckComplete()
	dinc.next.CheckComplete()
	// This inc is something like "the 25th"
	if !dinc.IsComplete() &&
		!dinc.next.IsComplete() &&
		!dinc.next.isValueDefined() {
		dinc.dtype = TIME_DAY_OF_MONTH_FROM_START
		dinc.NotEmpty()
	}

	dinc.CheckComplete()
	// nextInc is something like "the 25th"
	if dinc.IsComplete() &&
		!dinc.next.IsComplete() &&
		dinc.next.isValueDefined() {
		dinc.next.dtype = TIME_DAY_OF_MONTH_FROM_START
		dinc.next.NotEmpty()
	}

	dinc.next.CheckComplete()
	if !dinc.IsComplete() {
		return dinc, Env.Goofs().MakeGoof("INVALID_DATA",
			"Input %#q was parsed into a day incrementor %v "+
				"which is incomplete",
			orig, dinc)
	}

	return dinc, NO_GOOF
}

func (dinc *DayInc) preprocess(input string, lang Language) string {
	Env.Log().Fine("Preprocessing input %q", input)
	switch lang.(type) {
	case *English:
		// Remove leading/trailing and duplicate spaces
		input = strings.Trim(input, " ")
		input = strings.ToLower(input)

		// Get rid of any leading "the"
		input = strings.TrimPrefix(input, "the ")

		// Remove any dashes
		input = strings.Replace(input, "-", "", -1)

		// Reduce all internal multiple spaces to single spaces
		old := input
		for {
			input = strings.Replace(input, "  ", " ", -1)
			if input == old {
				break
			}
			old = input
		}

		// Convert last/final colloquialisms into our formalism, so for example
		// "last Sunday" -> "1st Sunday before the end of the month"
		// check if first word is last/first and if only two words
		if strings.Count(input, " ") <= 2 {
			re := regexp.MustCompile("(last|final) ([a-z]+)")
			input = re.ReplaceAllString(input, "1st $2 before the end of the month")
		}

		// Replace multiple word expressions with recognised
		// single word tokens, to make processing a bit simpler
		// End of month token...
		re := regexp.MustCompile(
			"(end of month" +
				"|end of the month" +
				"|last day of month" +
				"|last day of the month" +
				"|final day of month" +
				"|final day of the month" +
				")")
		input = re.ReplaceAllString(input, TIME_END_OF_MONTH_TOKEN)
	}
	Env.Log().Fine("... to %q", input)
	return input
}

func (dinc *DayInc) lex(witer *WordIterator, lang Language, cal Calendar) {
	word := witer.Current()
	switch lang.(type) {
	case *English:
		ord, goof := GetOrdinal(word, lang)
		if !IsError(goof) {
			dinc.val = ord
			dinc.NotEmpty()
		} else if dow, exists := cal.DayNumber(word, lang); exists {
			dinc.dow = dow
			dinc.dtype = TIME_DAY_OF_WEEK
			dinc.NotEmpty()
		} else if word == "business" ||
			word == "working" ||
			word == "work" {
			dinc.dtype = TIME_WORKDAY
			dinc.NotEmpty()
		} else if word == "weekday" {
			dinc.dtype = TIME_WEEKDAY
			dinc.NotEmpty()
		} else if word == "day" {
			if dinc.dtype == 0 {
				dinc.dtype = TIME_ORDINARY_DAY
				dinc.NotEmpty()
			}
		} else if word == TIME_END_OF_MONTH_TOKEN {
			dinc.dtype = TIME_DAY_OF_MONTH_FROM_END
			dinc.NotEmpty()
		}
		break
	}
	return
}

func (dinc *DayInc) lexQualifier(witer *WordIterator, lang Language) {
	word := witer.Current()
	switch lang.(type) {
	case *English:
		if word == "before" || word == "prior" {
			dinc.sign = -1
			dinc.NotEmpty()
		} else if word == "after" || word == "following" {
			dinc.sign = 1
			dinc.NotEmpty()
		}
	}
	return
}

//public int getValue()           {return value;}
//public int getSign()            {return sign;}
//public DayOfWeek getDayOfWeek() {return dayOfWeek;}
//public DayType getDayType()     {return dayType;}
//public DayIncrementor getPrevInc() {return prevInc;}
//public DayIncrementor getNextInc() {return nextInc;}

func (dinc *DayInc) isValueDefined() bool   { return dinc.val != 0 }
func (dinc *DayInc) isDayTypeDefined() bool { return dinc.dtype != 0 }
func (dinc *DayInc) isSignDefined() bool    { return dinc.sign != 0 }

func (dinc *DayInc) CheckComplete() {
	if (dinc.isValueDefined() && dinc.isDayTypeDefined()) ||
		dinc.dtype == TIME_DAY_OF_MONTH_FROM_END {
		dinc.NowComplete()
	}
	return
}

func (dinc *DayInc) String() string {
	return fmt.Sprintf(
		"(val=%v, sign=%v, dow=%v, dtype=%v, prev=%v, next=%v)",
		dinc.val,
		dinc.sign,
		dinc.dow,
		dinc.dtype,
		dinc.prev,
		dinc.next)
}

// Increment from the given day of the month.
//
// Uses a purely iterative algorithm, which may be slower but is
// simpler and more flexible.
func (dinc *DayInc) FromDayOfMonth(date *Date, holidays Intervals) (*Date, *Goof) {

	Env.Log().Fine("%v", dinc)
	found := false
	safety := 1

	newDate := NewDate()
	goof := NO_GOOF
	if dinc.dtype == TIME_DAY_OF_MONTH_FROM_START {
		newDate, goof = MakeDate(date.Month(), dinc.val)
		if !IsError(goof) {
			found = true
		} else {
			goof = Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
				"Day increment TIME_DAY_OF_MONTH_FROM_START value %d "+
					"produces an invalid date for the month %q in the "+
					"%s calendar",
				dinc.val,
				date.Month().String(),
				date.Calendar().Name())
		}
	} else {
		count := uint8(0)
		safetyLim := 31
		direction := 1
		if dinc.sign < 0 {
			direction = -1
		}
		newDate = date.Copy()

		isDayOfWeek := (dinc.dtype == TIME_DAY_OF_WEEK)
		isFirst := true

		for {
			// We must deal with the special case of TIME_DAY_OF_WEEK
			// missing the first day of the month
			if !isDayOfWeek || (isDayOfWeek && !isFirst) {
				newDate, goof = newDate.Inc(direction)
				Env.Log().IsError(goof)
			}

			Env.Log().Fine("newDate = %v", newDate)

			switch dinc.dtype {
			case TIME_ORDINARY_DAY:
				count++
				Env.Log().Fine("TIME_ORDINARY_DAY")
				break
			case TIME_WEEKDAY:
				if !newDate.IsWeekend() {
					count++
					Env.Log().Fine("TIME_WEEKDAY")
				}
				break
			case TIME_WORKDAY:
				if true { //newDate.isWorkday(holidays)
					count++
					// TODO
					Env.Log().Warn("FINISH ME")
					Env.Log().Fine("TIME_WORKDAY")
				}
				break
			case TIME_DAY_OF_WEEK:
				if newDate.DayOfWeek() == dinc.dow {
					count++
					Env.Log().Fine("TIME_DAY_OF_WEEK")
				}
				break
			}

			if dinc.val == count {
				found = true
			} else if !isFirst && newDate.IsLastDayOfMonth() {
				Env.Log().Fine("Last day of month, breaking")
				break
			}

			safety++
			isFirst = false
			if found || safety > safetyLim {
				break
			}
		}
	}

	if found {
		Env.Log().Fine("Result = %v", newDate)
		return newDate, goof
	}

	return NewDate(), Env.Goofs().MakeGoof("INVALID_DATA",
		"Could not find a valid date starting at %v "+
			" after %v increment(s) using %v with the holidays %v.",
		date, safety, dinc, holidays)

}

func (month *Month) String() string {
	phrase, _ := month.Calendar().MonthPhrase(month.val, Env.Language())
	return CapitaliseFirstLetter(phrase) + " " + month.Year().String()
}

func (month *Month) Days() uint8 {
	return month.Calendar().DaysInMonth(month)
}

//== MONTH SET =================================================================

type MonthSet struct {
	cal    Calendar
	lang   Language
	months map[uint8]bool
	ptr    uint8
	*State
}

func NewMonthSet() *MonthSet {
	return &MonthSet{
		cal:    Env.Calendars()["Gregorian"],
		lang:   Env.Languages()["English"],
		months: make(map[uint8]bool),
		State:  NewState(),
	}
}

func MakeMonthSet(cal Calendar, lang Language) *MonthSet {
	ms := NewMonthSet()
	ms.cal = cal
	ms.lang = lang
	return ms
}

func (ms *MonthSet) Add(month string, lang Language) bool {
	i, exists := ms.cal.MonthNumber(strings.ToLower(month), ms.lang)
	if exists {
		ms.months[i] = true
		ms.NotEmpty()
		return true
	}
	return false
}

func (ms *MonthSet) Exists(i uint8) bool {
	_, exists := ms.months[i]
	return exists
}

func (ms *MonthSet) Reset(i uint8) bool {
	ms.ptr = i
	_, exists := ms.months[i]
	return exists
}

func (ms *MonthSet) Len() int { return len(ms.months) }

func (ms *MonthSet) Next() uint8 {
	if ms.Len() == 0 {
		return 0
	}
	for {
		ms.ptr++
		if ms.ptr > ms.cal.NumberMonths() {
			ms.ptr = 1
		}
		_, exists := ms.months[ms.ptr]
		if exists {
			return ms.ptr
		}
	}
}

func (ms *MonthSet) String() string {
	result := "{"
	for k, _ := range ms.months {
		monthName, _ := ms.cal.MonthPhrase(k, ms.lang)
		result += fmt.Sprintf(
			"(%v:%q)",
			k, monthName)
	}
	result += "}"
	return result
}
