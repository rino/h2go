/*
	Something of a customisable type and encoding layer on top of the native Go
	types.

	ProtocolMap.binaryEncoder/Decoder differ from the BinaryEncodable interface
	methods for a user defined type, e.g.

	    ProtocolMap.EncodeBinaryr(ProtocolMap.Number["FIELDMAP"])(bfr) "type encoder"
	    versus
	    FieldMap.EncodeBinary(bfr) "method encoder"

	in that the former encodes the type to the front and wraps variable length
	data in a version container.  ProtocolMap.DecodeBinaryr then assumes the type
	has been removed.  Typically the type encoder will be generated from a
	method encoder, which should therefore not prepend the type.
*/
package h2go

import (
	"fmt"
	"sort"
	"strings"
)

type Codified interface {
	Code() CODE
}

type ProtocolMaker func(CODE) Protocol
type ProtocolChecker func(Protocol) *Goof

type GenericTask func(box *Box) (Representation, *Goof)

func NilGenericTask() GenericTask {
	return func(box *Box) (Representation, *Goof) {
		return NewBinaryRepresentation(), NO_GOOF
	}
}

// PROTOCOL ====================================================================
type Protocol struct {
	Name string
	Code CODE
	Nil  interface{}
	Tags []string
	// Generic
	GenericTx      GenericTask
	genericTxState *State
	GenericRx      GenericTask
	genericRxState *State
	// Binary
	BinarySizeFixed bool
	BinarySize      int // for fixed size cases
	BinaryTx        BinaryTask
	binaryTxState   *State
	BinaryRx        BinaryTask
	binaryRxState   *State
	// String
	StringTx      StringTask
	stringTxState *State
	StringRx      StringTask
	stringRxState *State
	Args          []string
	Box           *Box
}

// Allowing Nil to retain nil value is useful in this case.
func NewProtocol() Protocol {
	return Protocol{
		Tags:           []string{},
		genericTxState: NewState(),
		genericRxState: NewState(),
		binaryTxState:  NewState(),
		binaryRxState:  NewState(),
		stringTxState:  NewState(),
		stringRxState:  NewState(),
		Args:           []string{},
		Box:            MakeBox(BoxItemKinds, Env),
	}
}

// Generic
func (p *Protocol) GenericTxIsEmpty() bool {
	return p.genericTxState.IsEmpty()
}
func (p *Protocol) GenericRxIsEmpty() bool {
	return p.genericRxState.IsEmpty()
}

// Binary
func (p *Protocol) BinaryTxIsEmpty() bool {
	return p.binaryTxState.IsEmpty()
}
func (p *Protocol) BinaryRxIsEmpty() bool {
	return p.binaryRxState.IsEmpty()
}

// String
func (p *Protocol) StringTxIsEmpty() bool {
	return p.stringTxState.IsEmpty()
}
func (p *Protocol) StringRxIsEmpty() bool {
	return p.stringRxState.IsEmpty()
}

func ProtocolNilCheck(p Protocol) *Goof {
	if p.Nil == nil && p.Name != "NIL" {
		return Env.Goofs().MakeGoof("BAD_TYPE",
			"Cannot use a <nil> for Nil type for protocol %q, "+
				"only %q can do that",
			p.Name, "NIL")
	}
	return NO_GOOF
}

// PROTOCOL MAP ================================================================
type ProtocolMap struct {
	proto          map[CODE]Protocol
	Number         map[string]CODE
	Version        *Version
	StringBrackets *BracketStringMap
	nextcode       CODE
	*State
}

var _ Stateful = NewProtocolMap() // embedded State

// Constructors
func NewProtocolMap() *ProtocolMap {
	return &ProtocolMap{
		proto:          make(map[CODE]Protocol),
		Number:         make(map[string]CODE),
		Version:        NewVersion(),
		StringBrackets: NewBracketStringMap(),
		State:          NewState(),
	}
}

func MakeProtocolMap(ver *Version) *ProtocolMap {
	pm := NewProtocolMap()
	pm.Version = ver
	pm.NotEmpty()
	return pm
}

func (pmap *ProtocolMap) Copy() *ProtocolMap {
	pmap2 := NewProtocolMap()
	pmap2.Version = pmap.Version.Copy()
	for k, v := range pmap.proto {
		pmap2.proto[k] = v
	}
	for k, v := range pmap.Number {
		pmap2.Number[k] = v
	}
	pmap2.nextcode = pmap.nextcode
	pmap2.State = pmap.State.Copy()
	return pmap2
}

func (pmap *ProtocolMap) Protocols() map[CODE]Protocol {
	return pmap.proto
}

func (pmap *ProtocolMap) ProtocolByName(name string) (Protocol, *Goof) {
	code, exist := pmap.Number[name]
	if !exist {
		return NewProtocol(), Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Protocol %q does not exist", name)
	}
	return pmap.ProtocolByCode(code)
}

func (pmap *ProtocolMap) ProtocolCodeByName(name string) (CODE, *Goof) {
	protocol, goof := pmap.ProtocolByName(name)
	if IsError(goof) {
		return 0, goof
	}
	return protocol.Code, NO_GOOF
}

func (pmap *ProtocolMap) ProtocolByCode(code CODE) (Protocol, *Goof) {
	protocol, exist := pmap.proto[code]
	if !exist {
		return NewProtocol(), Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Protocol %d (% x) does not exist", code, code)
	}
	return protocol, NO_GOOF
}

func (pmap *ProtocolMap) ExistsByCode(code CODE) bool {
	_, result := pmap.proto[code]
	return result
}

func (pmap *ProtocolMap) ExistsByName(name string) bool {
	_, result := pmap.Number[name]
	return result
}

func (pmap *ProtocolMap) Name(code CODE) string {
	protocol, goof := pmap.ProtocolByCode(code)
	if IsError(goof) {
		return "ERROR: UNKNOWN PROTOCOL"
	}
	return protocol.Name
}

func (pmap *ProtocolMap) NilByCode(code CODE) (interface{}, *Goof) {
	protocol, goof := pmap.ProtocolByCode(code)
	if IsError(goof) {
		return 0, goof
	}
	return protocol.Nil, NO_GOOF
}

func (pmap *ProtocolMap) Nil(name string) (interface{}, *Goof) {
	protocol, goof := pmap.ProtocolByName(name)
	if IsError(goof) {
		return 0, goof
	}
	return protocol.Nil, NO_GOOF
}

func (pmap *ProtocolMap) IsString(code CODE) (bool, *Goof) {
	protocol, goof := pmap.ProtocolByCode(code)
	if IsError(goof) {
		return false, goof
	}
	return IsString(protocol.Nil), NO_GOOF
}

func (pmap *ProtocolMap) Tags(code CODE) ([]string, *Goof) {
	protocol, goof := pmap.ProtocolByCode(code)
	if IsError(goof) {
		return []string{}, goof
	}
	return protocol.Tags, NO_GOOF
}

func (pmap *ProtocolMap) Args(code CODE) ([]string, *Goof) {
	protocol, goof := pmap.ProtocolByCode(code)
	if IsError(goof) {
		return []string{}, goof
	}
	return protocol.Args, NO_GOOF
}

func (pmap *ProtocolMap) HexByName(name string) (string, *Goof) {
	code, exist := pmap.Number[name]
	if !exist {
		return "", Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Protocol %q does not exist", name)
	}
	return pmap.HexByCode(code)
}

func (pmap *ProtocolMap) HexByCode(code CODE) (string, *Goof) {
	_, exist := pmap.proto[code]
	if !exist {
		return "", Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Protocol %d (% x) does not exist", code)
	}
	bfr, _, _ := BuildByteBufferFromBox(
		BYTEORDER, NewBox().Set(BOX_ITEM_NATIVE, code))
	byts, _ := bfr.Bytes()
	return fmt.Sprintf("% x", byts), NO_GOOF
}

// This is designed to be the only way to add custom types.
func (pmap *ProtocolMap) Add(maker ProtocolMaker, checkers ...ProtocolChecker) *Goof {
	if pmap.nextcode == CODE_MAX {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Protocol map has reached maximum protocols, %d",
			CODE_MAX)
	}
	code := pmap.nextcode
	protocol := maker(code)
	if len(protocol.Name) == 0 {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Protocol %#v must specify a Name", protocol)
	}
	protocol.Code = code

	if protocol.Box == nil {
		protocol.Box = EmptyBox()
	}
	// Pre-calculate binary size in bytes
	if protocol.BinarySizeFixed && protocol.BinarySize <= 0 {
		if protocol.BinaryTx == nil {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Since BinarySizeFixed is true and BinarySize is <= 0 (%d), "+
					"require a non-nil BinaryTx for protocol %q",
				protocol.BinarySize, protocol.Name)
		}
		bfr := MakeByteBuffer(BYTEORDER)
		_, goof := protocol.BinaryTx(bfr,
			protocol.Box.Set(BOX_ITEM_ENCODE, protocol.Nil))
		if IsError(goof) {
			return goof
		}
		protocol.BinarySize, _ = bfr.Len()
		protocol.BinarySize -= CODE_BYTE_LENGTH
		if protocol.BinarySize <= 0 {
			return Env.Goofs().MakeGoof("ENCODING",
				"While binary encoding a %T, %v (%q), got size of %d bytes",
				protocol.Nil, protocol.Nil, protocol.Name, protocol.BinarySize)
		}
	}
	if len(protocol.Args) > 0 && Env != nil && Env.Types != nil {
		if len(protocol.Args) > 0 && !Env.Types().Contains(protocol.Args...) {
			return Env.Goofs().MakeGoof("BAD_TYPE",
				"One or more global types %q was not found in the %T",
				protocol.Args, pmap)
		}
	}
	// Generic
	if protocol.GenericTx == nil {
		protocol.GenericTx = NilGenericTask()
		protocol.genericTxState = NewState().NowEmpty()
	} else {
		protocol.genericTxState = NewState().NotEmpty()
	}
	if protocol.GenericRx == nil {
		protocol.GenericRx = NilGenericTask()
		protocol.genericRxState = NewState().NowEmpty()
	} else {
		protocol.genericRxState = NewState().NotEmpty()
	}
	// Binary
	if protocol.BinaryTx == nil {
		protocol.BinaryTx = NilBinaryTask()
		protocol.binaryTxState = NewState().NowEmpty()
	} else {
		protocol.binaryTxState = NewState().NotEmpty()
	}
	if protocol.BinaryRx == nil {
		protocol.BinaryRx = NilBinaryTask()
		protocol.binaryRxState = NewState().NowEmpty()
	} else {
		protocol.binaryRxState = NewState().NotEmpty()
	}
	// String
	if protocol.StringTx == nil {
		protocol.StringTx = NilStringTask()
		protocol.stringTxState = NewState().NowEmpty()
	} else {
		protocol.stringTxState = NewState().NotEmpty()
	}
	if protocol.StringRx == nil {
		protocol.StringRx = NilStringTask()
		protocol.stringRxState = NewState().NowEmpty()
	} else {
		protocol.stringRxState = NewState().NotEmpty()
	}
	goof := NO_GOOF
	for _, checker := range checkers {
		goof = checker(protocol)
		if IsError(goof) {
			return goof
		}
	}
	pmap.proto[code] = protocol
	pmap.Number[protocol.Name] = code
	pmap.nextcode++
	pmap.NotEmpty()
	return NO_GOOF
}

func (pmap *ProtocolMap) String() string {
	i := 0
	result := ""
	for c, proto := range pmap.Protocols() {
		if i > 0 {
			result += " "
		}
		result += fmt.Sprintf("%d:%s", c, proto.Name)
		i++
	}
	return result
}

func (pmap *ProtocolMap) Get(name string) Protocol {
	return pmap.proto[pmap.Number[name]]
}

/*
func (pmap *ProtocolMap) IsUnknown(name string) bool {
	return pmap.Number[name] == UNKNOWN_TYPE_CODE
}
*/

func (proto Protocol) IsUnknown() bool {
	return proto.Code == UNKNOWN_TYPE_CODE
}

func (proto Protocol) IsBoolean() bool {
	_, result := proto.Nil.(bool)
	return result
}

func (proto Protocol) IsComparable() bool {
	if IsString(proto.Nil) ||
		IsNumber(proto.Nil) ||
		proto.IsBoolean() {
		return true
	}
	return false
}

func (code CODE) IsUnknown() bool {
	return code == UNKNOWN_TYPE_CODE
}

func (code CODE) IsNil() bool {
	return code == NIL_TYPE_CODE
}

func (code CODE) IsNilOrUnknown() bool {
	return code.IsNil() || code.IsUnknown()
}

func (proto Protocol) HasTag(name string) bool {
	for _, tag := range proto.Tags {
		if tag == name {
			return true
		}
	}
	return false
}

func (pmap *ProtocolMap) WhichHaveTag(name string) []string {
	result := []string{}
	for _, proto := range pmap.Protocols() {
		for _, tag := range proto.Tags {
			if tag == name {
				result = append(result, proto.Name)
			}
		}
	}
	return result
}

func (pmap *ProtocolMap) Dump() ([]string, *Goof) {
	result := []string{}
	goof := NO_GOOF
	bfr := NewBuffer()
	byts := []byte{}
	line := ""
	for code, proto := range pmap.Protocols() {
		bfr, _, goof = BuildByteBufferFromBox(
			BYTEORDER, NewBox().Set(BOX_ITEM_NATIVE, code))
		if IsError(goof) {
			return result, goof
		}
		byts, _ = bfr.Bytes()
		line = fmt.Sprintf("% x: %d,%s", byts, code, proto.Name)
		if proto.BinarySizeFixed {
			line += fmt.Sprintf(" (%d bytes)", proto.BinarySize)
		}
		result = append(result, line)
	}
	sort.Strings(result)
	return result, NO_GOOF
}

// Returns true if all the given names or tags can be associated with
// protocol entries. Tag labels are prefixed with "tag:". Prefixes that
// are not recognised will be ignored. The type name "." is a wild card.
func (pmap *ProtocolMap) Contains(names ...string) bool {
	for _, name := range names {
		if name != "." {
			parts := strings.SplitN(name, ":", 2)
			if len(parts) == 1 {
				if !pmap.ExistsByName(name) {
					return false
				}
			} else {
				if parts[0] == "tag" {
					taggedNames := pmap.WhichHaveTag(parts[1])
					found := false
					for _, tagged := range taggedNames {
						if pmap.ExistsByName(tagged) {
							found = true
						}
					}
					if !found {
						return false
					}
				}
			}
		}
	}
	return true
}

func (types *TypeMap) AttachStringBrackets(bracmap *BracketStringMap) *Goof {
	for lb, bstrs := range bracmap.Raw() {
		if len(bstrs.Left) == 0 {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Fix the StringBrackets specification for %s, left bracket "+
					"missing, should it be %q?", bstrs, lb)
		}
		if len(lb) != 1 || len(bstrs.Left) != 1 {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Fix the StringBrackets specification for %s, left bracket "+
					"length must be 1", bstrs)
		}
		if lb != bstrs.Left {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Fix the StringBrackets specification for %s, left bracket "+
					"does not match key %q", bstrs, lb)
		}
		if len(bstrs.Right) == 0 {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Fix the StringBrackets specification for %s, right "+
					"bracket missing", bstrs)
		}
		if len(bstrs.Right) != 1 {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Fix the StringBrackets specification for %s, right bracket "+
					"length must be 1", bstrs)
		}
		if bstrs.Code == 0 {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Fix the StringBrackets specification for %s, code "+
					"missing", bstrs)
		}
		if bstrs.Decoder == nil {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Fix the StringBrackets specification for %s, decoder "+
					"function cannot be nil", bstrs)
		}
		for lbExisting, bstrsExisting := range Env.Types().StringBrackets.Raw() {
			if lb == lbExisting {
				return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
					"Failed to add %T %#v with key %q as there is already a "+
						"Env.Type.StringBrackets entry %#v with that key",
					bstrs, bstrs, lb, bstrsExisting)
			}
		}
		Env.Types().StringBrackets.bracmap[lb] = bstrs
	}
	Env.Types().StringBrackets.leftset, Env.Types().StringBrackets.rightset =
		Env.Types().StringBrackets.BuildSets()
	return NO_GOOF
}
