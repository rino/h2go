/*
	Containers for byte encodable typed values.
*/
package h2go

import (
	"fmt"
	"math/big"
	"reflect"
)

// FIELDMAP ====================================================================

type FieldMap struct {
	in    map[Field]*Field
	types *TypeMap
	*State
}

var _ FieldCollection = NewFieldMap()
var _ Codified = NewFieldMap()
var _ Stateful = NewFieldMap() // embedded State

func NewFieldMap() *FieldMap {
	return &FieldMap{
		in:    make(map[Field]*Field),
		types: NewTypeMap(),
		State: NewState(),
	}
}

func (types *TypeMap) MintFieldMap() *FieldMap {
	fmap := NewFieldMap()
	fmap.types = types
	// Deliberately regarded as still empty
	return fmap
}

func (fmap *FieldMap) ToMap() map[Field]*Field { return fmap.in }
func (fmap *FieldMap) Len() int                { return len(fmap.in) }
func (fmap *FieldMap) Types() *TypeMap         { return fmap.types }

func (fmap FieldMap) Code() CODE {
	return FIELDMAP_TYPE_CODE
}

// Override State.IsEmpty
func (fmap *FieldMap) IsEmpty() bool { return len(fmap.in) == 0 }

// Provide more fluent interface by removing boolean. Automatically
// wraps non-Field objects to generate valid key.
func (fmap *FieldMap) Get(key interface{}) *Field {
	defer Env.Log().StackTrace()
	fmap.RLock()
	defer fmap.RUnlock()
	var field *Field
	exists := false
	switch k := key.(type) {
	case Field:
		field, exists = fmap.in[k]
	case *Field:
		field, exists = fmap.in[*k]
	default:
		kf := fmap.Types().AutoKey(key)
		field, exists = fmap.in[kf]
	}
	if !exists {
		return NewField()
	}
	return field
}

func (fmap *FieldMap) KeyExists(key interface{}) bool {
	fmap.RLock()
	defer fmap.RUnlock()
	exists := false
	switch k := key.(type) {
	case Field:
		_, exists = fmap.in[k]
	case *Field:
		_, exists = fmap.in[*k]
	default:
		kf := fmap.Types().AutoKey(key)
		_, exists = fmap.in[kf]
	}
	return exists
}

func (fmap *FieldMap) KeyExistsOfType(key interface{}, typ string) (bool, *Goof) {
	fmap.RLock()
	defer fmap.RUnlock()
	_, exists := fmap.Types().Number[typ]
	if !exists {
		return false, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"While trying to verify if a %T key exists, "+
				"the given type %q was not recognised",
			fmap, typ)
	}
	exists = false
	f := NewField()
	switch k := key.(type) {
	case Field:
		f, exists = fmap.in[k]
	case *Field:
		f, exists = fmap.in[*k]
	default:
		kf := fmap.Types().AutoKey(key)
		f, exists = fmap.in[kf]
	}
	if exists {
		if f.Type() == fmap.Types().Number[typ] {
			return true, NO_GOOF
		} else {
			return false, Env.Goofs().MakeGoof("BAD_TYPE",
				"Field value for key %v has type %q, expected %q",
				key, fmap.Types().Name(f.Type()), typ)
		}
	} else {
		return false, Env.Goofs().MakeGoof("MISSING_KEY",
			"Key %v does not exist", key)
	}
}

func (fmap *FieldMap) Set(key interface{}, value *Field) *FieldMap {
	fmap.Lock()
	defer fmap.Unlock()
	switch k := key.(type) {
	case Field:
		fmap.in[k] = value
	case *Field:
		fmap.in[*k] = value
	default:
		kf := fmap.Types().AutoKey(key)
		fmap.in[kf] = value
	}
	return fmap
}

func (fmap *FieldMap) AutoSet(key, value interface{}) *FieldMap {
	k := fmap.Types().AutoKey(key)
	v := fmap.Types().AutoField(value)
	return fmap.Set(k, v)
}

// Differs from FieldList.DeleteField in that deletion is done in-place.
func (fmap *FieldMap) Delete(key interface{}) *FieldMap {
	fmap.Lock()
	defer fmap.Unlock()
	switch k := key.(type) {
	case Field:
		delete(fmap.in, k)
	case *Field:
		delete(fmap.in, *k)
	default:
		kf := fmap.Types().AutoKey(key)
		delete(fmap.in, kf)
	}
	return fmap
}

func (fmap *FieldMap) Equals(other interface{}) bool {
	fmap2 := NewFieldMap()
	switch v := other.(type) {
	case FieldMap:
		fmap2 = &v
	case *FieldMap:
		fmap2 = v
	default:
		return false
	}
	fmap.RLock()
	fmap2.RLock()
	defer fmap.RUnlock()
	defer fmap2.RUnlock()
	if fmap.Len() != fmap2.Len() {
		return false
	}
	if fmap.Len() == 0 {
		return true
	}
	for key, val := range fmap.ToMap() {
		val2 := fmap2.Get(key)
		if val2.IsEmpty() || !val.Equals(val2) {
			return false
		}
	}
	return true
}

// Assertion convenience methods.  Use functions rather than methods, because
// derivative packages cannot embed FieldMap.
func (fmap *FieldMap) GetBool(key Field) (bool, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return false, GoofMissingField(key)
	}
	result, ok := (f.Value()).(bool)
	if !ok {
		return false, GoofBadFieldType(key, "bool", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetUint8(key Field) (uint8, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(uint8)
	if !isa {
		return 0, GoofBadFieldType(key, "uint8", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetUint16(key Field) (uint16, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(uint16)
	if !isa {
		return 0, GoofBadFieldType(key, "uint16", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetUint32(key Field) (uint32, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(uint32)
	if !isa {
		return 0, GoofBadFieldType(key, "uint32", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetUint64(key Field) (uint64, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(uint64)
	if !isa {
		return 0, GoofBadFieldType(key, "uint64", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetInt8(key Field) (int8, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(int8)
	if !isa {
		return 0, GoofBadFieldType(key, "int8", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetInt16(key Field) (int16, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(int16)
	if !isa {
		return 0, GoofBadFieldType(key, "int16", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetInt32(key Field) (int32, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(int32)
	if !isa {
		return 0, GoofBadFieldType(key, "int32", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetInt64(key Field) (int64, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(int64)
	if !isa {
		return 0, GoofBadFieldType(key, "int64", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetBigInt(key Field) (*big.Int, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return (&big.Int{}).SetInt64(0.0), GoofMissingField(key)
	}
	result, isa := (f.Value()).(*big.Int)
	if !isa {
		return (&big.Int{}).SetInt64(0.0),
			GoofBadFieldType(key, "*big.Int", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetBigRat(key Field) (*big.Rat, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return (&big.Rat{}).SetFloat64(0.0), GoofMissingField(key)
	}
	result, isa := (f.Value()).(*big.Rat)
	if !isa {
		return (&big.Rat{}).SetFloat64(0.0),
			GoofBadFieldType(key, "*big.Rat", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetFloat32(key Field) (float32, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0.0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(float32)
	if !isa {
		return 0.0, GoofBadFieldType(key, "float32", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetFloat64(key Field) (float64, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return 0.0, GoofMissingField(key)
	}
	result, isa := (f.Value()).(float64)
	if !isa {
		return 0.0, GoofBadFieldType(key, "float64", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetString(key Field) (string, *Goof) {
	f := fmap.Get(key)
	if f.IsEmpty() {
		return "", GoofMissingField(key)
	}
	result, ok := (f.Value()).(string)
	if !ok {
		return "", GoofBadFieldType(key, "string", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetFieldMap(key Field) (*FieldMap, *Goof) {
	fmap0 := NewFieldMap()
	f := fmap.Get(key)
	if f.IsEmpty() {
		return fmap0, GoofMissingField(key)
	}
	result := f.AsFieldMap()
	if result.IsEmpty() {
		return fmap0, GoofBadFieldType(key, "FieldMap", f)
	}
	return result, NO_GOOF
}

func (fmap *FieldMap) GetFieldList(key Field) (*FieldList, *Goof) {
	flist0 := NewFieldList()
	f := fmap.Get(key)
	if f.IsEmpty() {
		return flist0, GoofMissingField(key)
	}
	result := f.AsFieldList()
	if result.IsEmpty() {
		return flist0, GoofBadFieldType(key, "FieldList", f)
	}
	return result, NO_GOOF
}

// These methods implement the BinaryEncodable interface.
// Encode a FieldMap to binary, simply concatenated Fields.
func (fmap FieldMap) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(fmap)
	fmap.RLock()
	defer fmap.RUnlock()
	Env.Log().SuperFine("ENTER FIELDMAP ENCODE for %v", fmap)
	byts, goof := bfr.Write(FIELDMAP_TYPE_CODE, box)
	out.AppendEncodedAs("fmap type code", byts)
	if IsError(goof) {
		return out, goof
	}
	out2, goof := MakeUINT(fmap.Len()).EncodeBinary(bfr, box)
	out2.(*BinaryRepresentation).
		RelabelLastBinaryEncoding(0, "fmap size (UINT)")
	out.AppendEncoded(out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}
	for k, v := range fmap.ToMap() {
		// key
		if !fmap.Types().proto[k.Type()].IsComparable() {
			return out, Env.Goofs().MakeGoof("INVALID_KEY",
				"Cannot use %v of type %v as key in a FieldMap",
				k, k.Type())
		}
		out2, goof = k.EncodeBinary(bfr, box)
		out.AppendEncoded(out2.ChooseEncoding(0))
		if IsError(goof) {
			return out, goof
		}
		// value
		out2, goof = v.EncodeBinary(bfr, box)
		out.AppendEncoded(out2.ChooseEncoding(0))
		if IsError(goof) {
			return out, goof
		}
	}
	return out, NO_GOOF
}

// Decode a FieldMap from binary. Assumes type code already read.
// The only trick is that we have to manually snip off the type code for each
// field.
func (fmap *FieldMap) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(fmap)
	out2, goof := NewUINT().DecodeBinary(bfr, box)
	out2.(*BinaryRepresentation).
		RelabelLastBinaryEncoding(0, "fmap size (UINT)")
	out.AppendEncoded(out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}
	ui, _ := out2.GetDecoded().Value().(UINT)
	i64, goof := ui.AsInt64()
	if IsError(goof) {
		return out, goof
	}
	size := int(i64)
	if size == 0 {
		return out, NO_GOOF
	}
	k := NewField()
	v := NewField()
	byts := []byte{}
	for i := 0; i < size; i++ {
		k = NewField()
		v = NewField()
		// key
		k.typ, byts, goof = bfr.ReadCode(box)
		out.AppendEncodedAs("key type code", byts)
		if IsError(goof) {
			if goof.EOF {
				break
			}
			return out, goof
		}
		out2, goof = k.DecodeBinary(bfr, box)
		out.AppendEncodedAs("key field", out2.ChooseEncoding(0))
		if IsError(goof) {
			if goof.EOF {
				break
			}
			return out, goof
		}
		// value
		v.typ, byts, goof = bfr.ReadCode(box)
		out.AppendEncodedAs("value type code", byts)
		if IsError(goof) {
			if goof.EOF {
				break
			}
			return out, goof
		}
		out2, goof = v.DecodeBinary(bfr, box)
		out.AppendEncodedAs("value field", out2.ChooseEncoding(0))
		if IsError(goof) {
			if goof.EOF {
				break
			}
			return out, goof
		}
		// entry
		fmap.Set(k, v)
	}
	return out, NO_GOOF
}

// Generate FieldMap binary encoder.
func FieldMapBinaryEncoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		inside, goof := box.RequireOfKnownType(
			BOX_ITEM_ENCODE,
			BOX_ITEM_FIELDMAP)
		if IsError(goof) {
			return NewBinaryRepresentation(), goof
		}
		return inside.(*FieldMap).EncodeBinary(bfr, box)
	}
}

// Generate FieldMap binary decoder for bfr.Types() map.
func FieldMapBinaryDecoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		return Env.Types().MintFieldMap().DecodeBinary(bfr, box)
	}
}

// The ordering of Go maps is randomised.
// FieldMap keys must be sorted alphanumerically to give us a consistent basis
// for comparing representations and calculating checksums.
func (fmap *FieldMap) MultilineString(firstprefix, prefix string) []string {
	if fmap == nil {
		return []string{"<nil>"}
	}
	result := []string{}
	result = append(result, firstprefix+"{")
	errstr := ""
	line := ""
	iter := fmap.TopLevelSortedKeyLinkList()

	if !iter.List().IsEmpty() {
		for {
			if iter.atEnd {
				break
			}
			if !iter.This().IsEmpty() {
				ks, _ := (iter.This().Value()).(KeyString)
				k := ks.Key
				v := fmap.Get(k)
				errstr = ""
				if !fmap.Types().proto[k.Type()].IsComparable() {
					errstr = fmt.Sprintf("//"+ERROR_IN_STRING, "INVALID_KEY")
				}
				line = fmt.Sprintf(
					prefix+"\t"+errstr+"%s: ",
					k.StringUsing(fmap.Types()))
				switch underlying := v.Value().(type) {
				case MultilineStringable:
					lines := underlying.MultilineString(line, prefix+"\t"+errstr)
					for _, line = range lines {
						result = append(result, line)
					}
					result[len(result)-1] += ","
				default:
					result = append(result, line+v.StringUsing(fmap.Types())+",")
				}
			}
			iter.Inc()
		}
	}

	result = append(result, prefix+"}")
	return result
}

// The ordering of Go maps is randomised.
// FieldMap keys must be sorted alphanumerically to give us a consistent basis
// for comparing representations and calculating checksums.
func (fmap FieldMap) String() string {
	if &fmap == nil {
		return "<nil>"
	}
	defer Env.Log().StackTrace()
	result := "{"
	first := true
	errstr := ""
	iter := fmap.TopLevelSortedKeyLinkList()

	if !iter.List().IsEmpty() {
		for {
			if iter.AtEnd() {
				break
			}
			if !iter.This().IsEmpty() {
				ks, _ := (iter.This().Value()).(KeyString)
				k := ks.Key
				v := fmap.Get(k)
				errstr = ""
				if !fmap.Types().proto[k.Type()].IsComparable() {
					errstr = fmt.Sprintf(ERROR_IN_STRING, "INVALID_KEY")
				}
				if !first {
					result += ", "
				} else {
					first = false
				}
				result += fmt.Sprintf(
					errstr+"%s:%s",
					k.StringUsing(fmap.Types()),
					v.StringUsing(fmap.Types()))
			}
			iter.Inc()
		}
	}

	return result + "}"
}

func (fmap *FieldMap) EncodeString(box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AppendEncoded(fmap.String())
	return out, NO_GOOF
}

func (fmap *FieldMap) DecodeString(txt string, box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AutoSetField(fmap)
	inside, goof := box.Require(BOX_ITEM_WORKSPACE_ACTION)
	if IsError(goof) {
		return out, goof
	}
	wsa := inside.(*WorkspaceAction)
	if string(txt[len(txt)-1]) == "}" {
		txt = TrimText(txt[1 : len(txt)-1])
		if len(txt) > 0 {
			_, fmapstrs := ParseMap(txt)
			kf := NewField()
			vf := NewField()
			for kstr, vstr := range fmapstrs {
				ngoofs := wsa.NumGoofs()
				kf = wsa.ProcessText(kstr, wsa.SetField, wsa.DelField)
				if wsa.NumGoofs() > ngoofs {
					return out, wsa.LastGoof()
				}
				vf = wsa.ProcessText(vstr, wsa.SetField, wsa.DelField)
				if wsa.NumGoofs() > ngoofs {
					return out, wsa.LastGoof()
				}
				if kf.IsEmpty() {
					kf = fmap.Types().AutoField("")
				}
				if vf.IsEmpty() {
					vf = fmap.Types().AutoField("")
				}
				fmap.Set(kf, vf)
				out.AppendEncoded(kstr)
				out.AppendEncoded(vstr)
			}
		}
		if wsa.SetField.IsEmpty() {
			// Getter
			return out, NO_GOOF
		}
		return out, Env.Goofs().MakeGoof("INVALID_KEY",
			"%T is not a valid key type", fmap)
	} else {
		return out, Env.Goofs().MakeGoof("SYNTAX",
			"Missing closing bracket } while processing text %q", txt)
	}
}

// Generate FieldMap binary decoder for Env.Types map.
func FieldMapStringDecoder() StringTask {
	return func(txt string, box *Box) (Representation, *Goof) {
		return Env.Types().MintFieldMap().DecodeString(txt, box)
	}
}

// FIELDMAP <-> STRUCT =========================================================

// Need a mechanism to associate FieldMap with a struct
type NestedFieldPath struct {
	*FieldList
	pathsep      string
	typedStrings bool
}

var _ Comparable = NewNestedFieldPath()
var _ FieldCollection = NewNestedFieldPath() // embedded FieldList

func NewNestedFieldPath() *NestedFieldPath {
	return &NestedFieldPath{
		FieldList: NewFieldList(),
	}
}

func (path *NestedFieldPath) PathSeparator() string { return path.pathsep }
func (path *NestedFieldPath) TypedStrings() bool    { return path.typedStrings }

func MakeNestedFieldPath(flist *FieldList) *NestedFieldPath {
	nfp := NewNestedFieldPath()
	nfp.FieldList = flist
	nfp.pathsep = "."
	return nfp
}

func BuildNestedFieldPath(keys ...interface{}) *NestedFieldPath {
	return MakeNestedFieldPath(Env.Types().BuildFieldList(keys...))
}

func (path *NestedFieldPath) SetPathSeparator(pathsep string) *NestedFieldPath {
	path.pathsep = pathsep
	return path
}

func (path *NestedFieldPath) SetTypedStrings(typedStrings bool) *NestedFieldPath {
	path.typedStrings = typedStrings
	return path
}

func (path *NestedFieldPath) String() string {
	result := ""
	valstr := ""
	if path.Len() > 0 {
		for c := 0; c < path.Len()-1; c++ {
			if path.TypedStrings() {
				valstr = path.Get(c).TypedString(path.FieldList.Types())
			} else {
				valstr = path.Get(c).String()
			}
			result = fmt.Sprintf("%s%s%s",
				result, valstr, path.pathsep)
		}
		if path.TypedStrings() {
			valstr = path.Get(path.Len() - 1).TypedString(path.FieldList.Types())
		} else {
			valstr = path.Get(path.Len() - 1).String()
		}
		result = fmt.Sprintf("%s%s", result, valstr)
	}
	return result
}

// Alphanumeric comparison of path elements.
func (path *NestedFieldPath) Cmp(other Comparable) int {
	path2, ok := other.(*NestedFieldPath)
	result := 0
	if !ok {
		Env.Log().IsError(Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"When comparing %T %v to other object %v of type %T",
			path2, path2, other, other))
		result = COMPARE_ERR
	} else {
		for i, v := range path.ToSlice() {
			if i < path2.Len() {
				result = CmpString(v.String(), path2.Get(i).String())
				if result == COMPARE_EQ {
					continue
				}
			}
		}
	}
	return result
}

func (fmap *FieldMap) getNestedFieldMap(path *NestedFieldPath) (*FieldMap, *Goof) {
	fmap0 := NewFieldMap()
	if path.Len() == 0 {
		return fmap0, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"No path elements provided")
	} else if path.Len() == 1 {
		return fmap, NO_GOOF
	}
	fmap2 := path.Types().MintFieldMap()
	for k, v := range fmap.ToMap() {
		fmap2.Set(&k, v)
	}
	fmap3 := NewFieldMap()
	for c := 0; c < path.Len()-1; c++ {
		fmap3, _ = fmap2.GetFieldMap(*path.Get(c))
		if !fmap3.IsEmpty() {
			fmap2 = fmap3
		} else {
			if c != path.Len()-2 {
				return fmap0, Env.Goofs().MakeGoof("KEY_MISMATCH",
					"Cannot find FieldMap %q in path %v",
					path.Get(c), path)
			}
		}
	}
	return fmap3, NO_GOOF
}

// Specify field nested in successive FieldMaps by specifying a slice
// of field labels, in heirarchical order.
func (fmap *FieldMap) GetNestedField(path *NestedFieldPath) (*Field, *Goof) {
	fmap2, goof := fmap.getNestedFieldMap(path)
	if !fmap2.IsEmpty() {
		return fmap2.Get(path.Get(path.Len() - 1)), goof
	} else {
		return NewField(), goof
	}
}

func (fmap *FieldMap) KeysAsNestedFieldLists() []*FieldList {
	lists := []*FieldList{}
	lists2 := []*FieldList{}
	for k, v := range fmap.ToMap() {
		if v.Type() == fmap.Types().Number["FIELDMAP"] {
			lists2 = v.AsFieldMap().KeysAsNestedFieldLists()
			for _, list := range lists2 {
				_ = list.Insert(k.Copy(), 0)
				lists = append(lists, list)
			}
		} else {
			lists = append(lists, fmap.Types().MintFieldList().Add(k))
		}
	}
	return lists
}

func (fmap *FieldMap) KeysAsNestedFieldPaths() []*NestedFieldPath {
	result := []*NestedFieldPath{}
	keylists := fmap.KeysAsNestedFieldLists()
	for _, flist := range keylists {
		result = append(result, MakeNestedFieldPath(flist))
	}
	return result
}

// Copies FieldMap data to the given pointer to a struct, resolving nested
// FieldMaps/structs.  If there are structural or type mismatches for
// existing data, nothing is done.  Flags are returned indicating whether
// there was a difference in (1) values for common fields or (2) the structure.
// For a complete copy, all associated fields and nested structs must exist and
// be of matching type.  A pointer must be given to the struct, assuring
// mutability.
func (fmap *FieldMap) ToStruct(i interface{}) (bool, bool, *Goof) {
	s, _, _, goof := RequireStructPtr(i)
	if IsError(goof) {
		return false, false, goof
	}

	fFieldName := ""
	tag := ""
	valDiff := false
	valDiff2 := false
	structDiff := false
	structDiff2 := false
	found := false
	var sf reflect.Value
	var structField reflect.StructField
	sftype := UNKNOWN_TYPE_CODE
	fmapType := fmap.Types().Number["FIELDMAP"]
	flistType := fmap.Types().Number["FIELDLIST"]
	fieldType := fmap.Types().Number["FIELD"]
	isNonFieldStructPtr := false
	prefix := fmt.Sprintf("While converting %T to %T, ", fmap, i)

fmap_loop:
	for k, v := range fmap.ToMap() {
		fFieldName = k.AsString()
		if len(fFieldName) == 0 {
			Env.Log().Warn(
				prefix+"key %v was found but should be a non-empty string",
				fmap, i, k)
			continue
		}
		fmap2 := v.AsFieldMap()
		sf = s.FieldByName(fFieldName)
		structField, found = s.Type().FieldByName(fFieldName)
		if found {
			// Ok try using struct tag
			tag = string(structField.Tag)
			sftype = 0
			if len(tag) > 0 {
				sftype = fmap.Types().Number[tag] // ignored if unrecognised
			}
			if sftype.IsNilOrUnknown() {
				sftype = fmap.Types().GetType(sf.Interface())
			}
			isNonFieldStructPtr = ValueIsStructPtr(sf) &&
				!(sftype == fieldType || sftype == fmapType || sftype == flistType)
			if sf.IsValid() && sf.CanSet() {
				if !fmap2.IsEmpty() {
					if isNonFieldStructPtr {
						// order of || important next
						valDiff2, structDiff2, goof =
							fmap2.ToStruct(sf.Interface())
						if IsError(goof) {
							return valDiff, structDiff, NO_GOOF
						}
						valDiff = valDiff || valDiff2
						structDiff = structDiff || structDiff2
					}
				} else {
					if isNonFieldStructPtr {
						continue fmap_loop
					}

					switch f := (sf.Interface()).(type) {
					case *Field:
						if f != nil && !f.Equals(v) {
							sf.Set(reflect.ValueOf(v))
							valDiff = true
						}
						continue fmap_loop
					case *FieldList:
						if f != nil && !f.Equals(v.Value()) {
							sf.Set(reflect.ValueOf(v.Value()))
							valDiff = true
						}
						continue fmap_loop
					}

					if sf.Kind() == reflect.Int || sf.Kind() == reflect.Uint {
						Env.Log().Warn(
							prefix+"field %s of struct %#v of type %v is too generic, "+
								"must use a specific byte size int, ignoring field",
							fFieldName, i, sf.Kind())
						continue fmap_loop
					}

					if sftype == v.Type() ||
						(fmap.Types().IsBoolean(sftype) && fmap.Types().IsBoolean(v.Type())) {
						if sf.Interface() != v.Value() {
							sf.Set(reflect.ValueOf(v.Value()))
							valDiff = true
						}
					} else {
						Env.Log().Warn(
							prefix+"value %v type does not match required "+
								"struct type %q, specify explicitly with \"(%v,%v)\"",
							v, fmap.Types().Name(sftype),
							v.Value(), fmap.Types().Name(sftype))
					}

				}
			} else {
				structDiff = true
			}
		}
	}
	return valDiff, structDiff, NO_GOOF
}

// Copies struct data to the given FieldMap, resolving nested FieldMaps/structs.
// If there are structural or type mismatches for existing data, nothing is
// done.  If the force flag is true, FieldMap fields that do not exist will be
// created.  Flags are returned indicating whether there was a difference in
// (1) values for the common fields or (2) the structure.
// Note that struct fields must be exportable (capitalised).
func (fmap *FieldMap) FromStruct(i interface{}, force bool) (bool, bool) {
	s, _, _, goof := RequireStructPtr(i)
	if IsError(goof) {
		return false, false
	}
	st := s.Type()

	sFieldName := ""
	tag := ""
	valDiff := false
	valDiff2 := false
	structDiff := false
	structDiff2 := false
	fmap2 := NewFieldMap()
	k := NewField()
	atLeastOne := false
	fmapType := fmap.Types().Number["FIELDMAP"]
	flistType := fmap.Types().Number["FIELDLIST"]
	fieldType := fmap.Types().Number["FIELD"]
	isNonFieldStructPtr := false
	wasFieldType := false

struct_loop:
	for j := 0; j < s.NumField(); j++ {
		sf := s.Field(j)
		if sf.IsValid() && ValueIsExportable(sf) {
			// field is valid and exportable
			if ValueIsStruct(sf) {
				continue struct_loop
			}
			si := sf.Interface()
			sFieldName = st.Field(j).Name
			// Tagging allows manual override of ambiguous types, but is not checked
			sftype := UNKNOWN_TYPE_CODE
			tag = string(st.Field(j).Tag)
			if len(tag) > 0 {
				sftype, _ = fmap.Types().Number[tag]
			}
			if sftype.IsNilOrUnknown() {
				sftype = fmap.Types().GetType(si)
				// Don't double-wrap FIELD types
				wasFieldType = false
				if sftype == fieldType {
					f, isa := si.(*Field)
					if !isa {
						continue struct_loop
					}
					sftype = f.Type()
					si = f.Value()
					wasFieldType = true
				}
			}
			k = fmap.Types().AutoField(sFieldName)
			isNonFieldStructPtr = ValueIsStructPtr(sf) &&
				!(wasFieldType || sftype == fmapType || sftype == flistType)
			if fmap.KeyExists(k) {
				if isNonFieldStructPtr {
					fmap2 = fmap.Get(k).AsFieldMap()
					if !fmap2.IsEmpty() {
						valDiff2, structDiff2 =
							fmap2.FromStruct(si, force)
						valDiff = valDiff || valDiff2
						structDiff = structDiff || structDiff2
					}
					si = fmap2
					sftype = fmap.Types().Number["FIELDMAP"]
				}
			} else {
				structDiff = true
				if force {
					if isNonFieldStructPtr {
						fmap2 = fmap.Types().MintFieldMap()
						structDiff = true
						fmap2.FromStruct(si, force)
						si = fmap2
						sftype = fmap.Types().Number["FIELDMAP"]
					}
				} else {
					continue struct_loop
				}
			}
			fnew := MakeField(si, sftype)
			if fmap.KeyExists(k) && !isNonFieldStructPtr {
				fold := fmap.Get(k)
				valDiff = (fnew != fold)
			}
			fmap.Set(k, fnew)
			atLeastOne = true
		}
	}
	if !atLeastOne {
		Env.Log().Warn("No struct fields were mapped to the given FieldMap, " +
			"the struct may have no exportable (capitalised) fields.")
	}
	return valDiff, structDiff
}

// Return an iterator over a sorted LinkList of the keys.  One level only, not
// recursive
func (fmap *FieldMap) TopLevelSortedKeyLinkList() *LinkListIterator {
	klist := NewLinkList()
	iter := klist.Iterator()
	for k, _ := range fmap.ToMap() {
		iter.InsertSort(MakeKeyString(k))
		iter.Inc()
	}

	if !klist.IsEmpty() {
		iter.First()
	}

	return iter
}

// Return an iterator over a sorted LinkList of the NestedFieldPaths.  Collects
// all FieldMaps recursively.
func (fmap *FieldMap) RecursiveSortedKeyLinkList() *LinkListIterator {
	nfps := fmap.KeysAsNestedFieldPaths()
	nfplist := NewLinkList()
	iter := nfplist.Iterator()
	for _, nfp := range nfps {
		iter.InsertSort(nfp)
		iter.Inc()
	}

	if !nfplist.IsEmpty() {
		iter.First()
	}

	return iter
}
