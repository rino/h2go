/*
	This first test code run, sets globals for other tests.
*/
package h2go

import (
	"testing"
	"time"
)

// TESTS ======================================================================

func TestHashTokenStringDecode(t *testing.T) {
	Env.Log().MajorSection(
		"Test string decoding of a qualifying token")

	ver := H2GO_VERSION.Copy()
	qt0, goof := MakeHashToken(
		"localhost:9003",
		ver,
		hashTokenRequirements(6),
	)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating %T", qt0)
	}

	qt1 := NewHashToken()
	_, goof = qt1.DecodeString(qt0.String(), EmptyBox())
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decoding %T string %q", qt0, qt0.String())
	}

	if qt0.String() != qt1.String() {
		t.Fatalf("Expected %q, got %q", qt0.String(), qt1.String())
	}
}

func TestMakeHashToken(t *testing.T) {
	Env.Log().MajorSection(
		"Test creation and hashing of a Qualifying Token")
	speczbits := 4
	ver := H2GO_VERSION.Copy()
	qt, goof := BuildHashToken(
		"localhost:9003",
		speczbits,
		ver,
		hashTokenRequirements(speczbits),
	)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating %T", qt)
	}
	hash := make([]byte, qt.TokenByteLength())
	zerobyts := int(qt.Zeromask()[0])
	for i := 0; i < qt.TokenByteLength(); i++ {
		if i < zerobyts {
			hash[i] = byte(0)
		} else {
			hash[i] = byte(255)
		}
	}
	hash[zerobyts] = hash[zerobyts] >> uint(qt.Rembits(speczbits))
	Env.Log().HexDump(hash, 1000,
		"Artificial, correct hash with %d zero bits", speczbits)
	correct, goof := qt.WouldBeCorrect(hash)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem checking correctness of given hash")
	}
	if !correct {
		t.Fatalf("Expected this hash to be correct")
	}
	hash[0] = hash[0] | byte(128)
	Env.Log().HexDump(hash, 1000, "Artificial, almost correct hash")
	correct, goof = qt.WouldBeCorrect(hash)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem checking correctness of given hash")
	}
	if correct {
		t.Fatalf("Expected this hash to be incorrect")
	}
}

func TestSingleSearchForValidHashToken(t *testing.T) {
	Env.Log().MajorSection(
		"Test control of a single process to search for a correct hash")
	speczbits := 8
	ver := H2GO_VERSION.Copy()
	box := EmptyBox()

	qt, goof := BuildHashToken(
		"localhost:9003",
		speczbits,
		ver,
		hashTokenRequirements(speczbits),
	)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating %T", qt)
	}

	Env.Log().MinorSection("Start a search, then stop it manually")

	go qt.Search(10000, speczbits, "test1", box)
	time.Sleep(5 * time.Second)
	qt.Stop()
	goof = qt.Wait()

	if IsError(goof) && !goof.Is("VALUE_NOT_FOUND") {
		Env.Log().IsError(goof)
		t.Fatalf("Could not find correct %T", qt)
	}

	Env.Log().Basic("Expected goof: %v", goof.Msg)
	Env.Log().Basic("%d hashes performed to find token %q", qt.Counter(), qt.String())
	Env.Log().HexDump(qt.Hash(), 1000, "Hash bytes")

	Env.Log().MinorSection("Start a search, let it run to completion")
	speczbits = 6
	qt, goof = BuildHashToken(
		"localhost:9003",
		speczbits,
		ver.Copy(),
		hashTokenRequirements(speczbits),
	)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating %T", qt)
	}

	go qt.Search(10000, speczbits, "test2", box)
	// Pause a moment to ensure the State running flag has been activated
	time.Sleep(5 * time.Millisecond)
	goof = qt.Wait()

	if Env.Log().IsError(goof) {
		t.Fatalf("Could not find correct %T", qt)
	}

	Env.Log().Basic("%d hashes performed to find token %q", qt.Counter(), qt.String())
	Env.Log().HexDump(qt.Hash(), 1000, "Hash bytes")
	if !qt.IsCorrect() {
		t.Fatalf("Expected this hash to be incorrect")
	}
}

func TestConcurrentSearchForValidHashToken(t *testing.T) {
	speczbits := 5
	ver := H2GO_VERSION.Copy()
	Env.Log().MajorSection(
		"Test use of concurrent search for a correct hash with %d zero bits",
		speczbits)
	box := EmptyBox()

	qt0, goof := BuildHashToken(
		"localhost:9003",
		speczbits,
		ver,
		hashTokenRequirements(speczbits),
	)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating %T", qt0)
	}

	finder := MakeHashTokenFinder(qt0)
	goof = finder.Spawn(4, 100000, speczbits, box)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem spawning %T search", finder)
	}

	found, qt := finder.Wait()

	Env.Log().Basic("Found: %v", found)
	Env.Log().Basic("Counters: %d", finder.SumCounters())
	if found {
		Env.Log().Basic("Token: %q", qt)
		Env.Log().HexDump(qt.Hash(), 1000, "Hash bytes")
	}
}

// SUPPORT ====================================================================

func hashTokenRequirements(reqdzbits int) HashTokenRequirements {
	return func(ver *Version) (int, *ScryptParams, [2]int) {
		return reqdzbits,
			MakeDefaultScryptParams(int64(HASHTOKEN_HASH_BYTE_LENGTH)),
			[2]int{-3600, 600}
	}
}
