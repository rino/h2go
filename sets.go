/*
	Slices with unique elements.
*/
package h2go

import ()

type StringSet []string

var _ Equible = NewStringSet()

func NewStringSet() StringSet {
	return StringSet([]string{})
}

func BuildStringSet(v ...string) StringSet {
	set := StringSet([]string{})
	for _, elem := range v {
		set, _ = set.Add(elem)
	}
	return set
}

// Not in-place
func (set StringSet) Add(v string) (StringSet, bool) {
	for _, elem := range set {
		if elem == v {
			return set, false
		}
	}
	set = append(set, v)
	return set, true
}

func (set StringSet) Contains(v string) bool {
	for _, elem := range set {
		if elem == v {
			return true
		}
	}
	return false
}

// Same elements, order not important
func (set StringSet) Equals(other interface{}) bool {
	if set2, isa := other.(StringSet); isa {
		if len(set) != len(set2) {
			return false
		}
		if len(set) == 0 {
			return true
		}
		// Use of a map to avoid redundant comparisons is probably
		// slower for small sets, faster for larger sets.
		map2 := make(map[int]string, len(set2))
		for i, elem := range set2 {
			map2[i] = elem
		}
		found := false
		for _, elem := range set {
			found = false
			for j, elem2 := range map2 {
				if elem == elem2 {
					if len(map2) == 1 {
						return true
					}
					delete(map2, j)
					found = true
				}
			}
			if !found {
				break
			}
		}
	}
	return false
}

func (set StringSet) Copy() StringSet {
	set2 := make(StringSet, len(set))
	copy(set2, set)
	return set2
}
