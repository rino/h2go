/*
	Miscellaneous file stuff.
*/
package h2go

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// Caution, can return nil *os.File.
func OpenFile(abspath string, flags int, filemode os.FileMode) (*os.File, *Goof) {
	file, err := os.OpenFile(abspath, flags, filemode)
	if IsError(err) {
		return nil, Env.Goofs().WrapGoof("OPENING_FILE", err,
			"While opening file %s", abspath)
	}
	return file, NO_GOOF
}

// Test whether the given file exists.
func FileExists(abspath string) bool {
	finfo, err := os.Stat(abspath)
	if !os.IsNotExist(err) && !finfo.IsDir() {
		return true
	}
	return false
}

// Test whether the given directory exists.
func DirExists(abspath string) bool {
	finfo, err := os.Stat(abspath)
	if !os.IsNotExist(err) && finfo.IsDir() {
		return true
	}
	return false
}

func Touch(path string) *Goof {
	file, goof := OpenFile(path, FILEFLAG_CREATE, DEFAULT_FILEMODE)
	if !IsError(goof) {
		err := file.Close()
		if !IsError(err) {
			return NO_GOOF
		} else {
			return Env.Goofs().WrapGoof("CLOSING_FILE", err,
				"While closing file %v", file)
		}
	} else {
		return goof
	}
}

func FilterFilesByRegex(dirpath, expr string) ([]string, *Goof) {
	result := []string{}
	regx, err := regexp.Compile(expr)
	if IsError(err) {
		return result, Env.Goofs().WrapGoof("BAD_ARGUMENTS", err,
			"While compiling regular expression from \"%s\"", expr)
	}
	files, err := ioutil.ReadDir(dirpath)
	if IsError(err) {
		return result, Env.Goofs().WrapGoof("READING_DIR", err,
			"While reading file names in directory %s", dirpath)
	}
	for _, file := range files {
		if !file.IsDir() && regx.MatchString(file.Name()) {
			result = append(result, file.Name())
		}
	}
	return result, NO_GOOF
}

// Currently makes a linux kill file.
func MakeKillFile(killpath string, host string, pid int) *Goof {
	err := os.RemoveAll(killpath)
	if IsError(err) {
		return Env.Goofs().WrapGoof("DELETING_FILE_OR_DIR", err,
			"While trying to remove kill file %s", killpath)
	}
	file, err2 :=
		os.OpenFile(killpath, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0744)
	defer file.Close()
	if IsError(err2) {
		return Env.Goofs().WrapGoof("OPENING_FILE", err2,
			"While trying to open kill file %s", killpath)
	}
	filetext := "#!/bin/bash\n# host %s\n" +
		"pkill -SIGKILL -P %d # Terminate subprocesses\n" +
		"kill -SIGKILL %d # Terminate parent\n"
	_, err = fmt.Fprintf(file, filetext, host, pid, pid)
	if IsError(err) {
		return Env.Goofs().WrapGoof("WRITING_FILE", err,
			"While trying to write text to kill file %s", killpath)
	}
	return NO_GOOF
}

// Credit https://groups.google.com/forum/#!topic/golang-nuts/aPWnfWoTVec
func DirList(path string) ([][]string, *Goof) {
	result := [][]string{}
	if _, err := os.Stat(path); IsError(err) {
		return result, Env.Goofs().WrapGoof("READING_DIR", err,
			"While reading directory %q", path)
	}
	dir, err := os.Open(path)
	defer dir.Close()
	fileinfos, err := dir.Readdir(-1)
	if IsError(err) {
		return result, Env.Goofs().WrapGoof("READING_DIR", err,
			"Could not get files in %q", path)
	}
	result = make([][]string, len(fileinfos))
	for y, fileinfo := range fileinfos {
		result[y] = []string{
			fmt.Sprintf("%v", fileinfo.Mode()),
			fmt.Sprintf("%v", fileinfo.ModTime()),
			fmt.Sprintf("%v", fileinfo.Size()),
			fmt.Sprintf("%v", fileinfo.Name()),
		}
	}
	return result, NO_GOOF
}

func DirLines(path, prefix string) ([]string, *Goof) {
	fileinfo, goof := DirList(path)
	if IsError(goof) {
		return []string{}, goof
	}
	return TabulateText(
		fileinfo,
		[]string{},
		[]int{-1, -1, -1, 0},
		[]int{1, 1, 1, -1},
		false,
		prefix)
}

func CheckPath(path, allowedRoot string) *Goof {
	path = filepath.Clean(path)
	if !strings.HasPrefix(path, allowedRoot) {
		return Env.Goofs().MakeGoof("AUTHORISATION",
			"Path %q is not below allowable root path %q",
			path, allowedRoot)
	}
	return NO_GOOF
}

// Creates a test directory (first input) and multiple subdirs in the current directory.
func CreateDirs(dirs ...string) ([]string, *Goof) {
	paths := []string{}
	if len(dirs) == 0 {
		return paths, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"You need to provide some directory names")
	}
	cwd, err := os.Getwd()
	if IsError(err) {
		return paths, Env.Goofs().WrapGoof("GETTING_CURRENT_DIRECTORY", err, "")
	}
	path := ""
	for _, dir := range dirs {
		path = filepath.Join(cwd, dir)
		err = os.Mkdir(path, 0777)
		if IsError(err) {
			return paths, Env.Goofs().WrapGoof("CREATING_FILE_OR_DIR", err,
				"While making one of %d directories, %s", len(dirs), path)
		}
		paths = append(paths, path)
	}
	return paths, NO_GOOF
}

// Returns all files that match the given filter, starting from the
// given root directory.
func FindPaths(path string, filter func(fpath string, info os.FileInfo, level int) bool, maxlevel int) ([]string, *Goof) {
	result := []string{}
	var err error
	path, err = filepath.Abs(path)
	if IsError(err) {
		return result, Env.Goofs().WrapGoof("BAD_ARGUMENTS", err,
			"While making given path %q absolute", path)
	}
	info, err := os.Stat(path)
	if IsError(err) {
		return result, Env.Goofs().WrapGoof("GETTING_DIR_INFO", err,
			"While getting info on %q", path)
	}
	if !info.IsDir() {
		return result, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Given root path %q must be a directory", path)
	}
	nscan := 0
	level := 0
	findPaths := func(fpath string, info os.FileInfo, inerr error) error {
		if IsError(inerr) {
			return inerr
		}

		level = strings.Count(
			strings.TrimPrefix(fpath, path),
			string(filepath.Separator))

		if level > maxlevel {
			return filepath.SkipDir
		}

		if filter(fpath, info, level) {
			result = append(result, fpath)
		}
		nscan++
		return nil
	}

	err = filepath.Walk(path, findPaths)
	if IsError(err) {
		return result, Env.Goofs().WrapGoof("WALKING_FILEPATH", err,
			"While walking file path from root %q", path)
	} else {
		return result, NO_GOOF
	}
}

func CopyFile(srcpath, targpath string) *Goof {
	src, err := os.Open(srcpath)
	if IsError(err) {
		return Env.Goofs().WrapGoof("OPENING_FILE", err,
			"While opening source %q to copy to target %q",
			srcpath, targpath)
	}
	defer src.Close()

	targ, err := os.Create(targpath) // create if file does not exist
	if IsError(err) {
		return Env.Goofs().WrapGoof("OPENING_FILE", err,
			"While creating target %q to copy from source %q",
			targpath, srcpath)
	}
	defer targ.Close()

	_, err = io.Copy(targ, src)
	if IsError(err) {
		return Env.Goofs().WrapGoof("IO_COPYING", err,
			"While copying source file %q to target %q",
			srcpath, targpath)
	}

	err = targ.Sync()
	if IsError(err) {
		return Env.Goofs().WrapGoof("SYNCING_FILE", err,
			"While syncing target %q",
			targpath)
	}

	return NO_GOOF
}

// FILE PERMISSIONS ============================================================

// FileAction ------------------------------------------------------------------
type FileAction struct {
	Read  bool
	Write bool
	Exec  bool
}

var _ Equible = NewFileAction()
var _ Stringable = NewFileAction()

func NewFileAction() *FileAction {
	return &FileAction{}
}

func (faction *FileAction) CanRead() *FileAction {
	faction.Read = true
	return faction
}

func (faction *FileAction) CanWrite() *FileAction {
	faction.Write = true
	return faction
}

func (faction *FileAction) CanExec() *FileAction {
	faction.Exec = true
	return faction
}

func (faction FileAction) String() string {
	result := ""
	if faction.Read {
		result += "r"
	} else {
		result += "-"
	}
	if faction.Write {
		result += "w"
	} else {
		result += "-"
	}
	if faction.Exec {
		result += "x"
	} else {
		result += "-"
	}
	return result
}

func (faction FileAction) ToBinary() uint16 {
	result := uint16(0)
	if faction.Read {
		result = result | 4
	}
	if faction.Write {
		result = result | 2
	}
	if faction.Exec {
		result = result | 1
	}
	return result
}

func (faction FileAction) EncodeString(box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AppendEncoded(faction.String())
	return out, NO_GOOF
}

func (faction *FileAction) DecodeString(txt string, box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AutoSetField(faction)
	if len(txt) < 3 {
		return out, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Given txt %q must have 3 characters to be a %T", txt, faction)
	}
	in := strings.ToLower(txt[:3])
	if in[0] == 'r' {
		faction.Read = true
	} else if in[0] == '-' {
		faction.Read = false
	} else {
		return out, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Character number %d, %q in %q not recognised by %T",
			0, in[0], in, faction)
	}
	if in[1] == 'w' {
		faction.Write = true
	} else if in[1] == '-' {
		faction.Write = false
	} else {
		return out, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Character number %d, %q in %q not recognised by %T",
			1, in[1], in, faction)
	}
	if in[2] == 'x' {
		faction.Exec = true
	} else if in[2] == '-' {
		faction.Exec = false
	} else {
		return out, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Character number %d, %q in %q not recognised by %T",
			2, in[2], in, faction)
	}
	out.AppendEncoded(in[:3])
	return out, NO_GOOF
}

func (faction FileAction) Equals(other interface{}) bool {
	faction2, isa := other.(FileAction)
	if !isa {
		_, isa = other.(*FileAction)
		if isa {
			faction2 = *(other.(*FileAction))
		} else {
			return false
		}
	}
	return faction == faction2
}

// FilePermissions -------------------------------------------------------------
type FilePermissions struct {
	User  *FileAction
	Group *FileAction
	Other *FileAction
	*State
}

var _ Stateful = NewFilePermissions() // embedded State

func NewFilePermissions() *FilePermissions {
	return &FilePermissions{
		User:  NewFileAction(),
		Group: NewFileAction(),
		Other: NewFileAction(),
		State: NewState(),
	}
}

func MakeFilePermissions(txt string) (*FilePermissions, *Goof) {
	out, goof := NewFilePermissions().DecodeString(txt, EMPTY_BOX)
	fperms, _ := out.GetDecoded().Value().(*FilePermissions)
	return fperms, goof
}

func (fperms FilePermissions) String() string {
	return fperms.User.String() + fperms.Group.String() + fperms.Other.String()
}

func (fperms FilePermissions) EncodeString(box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AppendEncoded(fperms.String())
	return out, NO_GOOF
}

func (fperms *FilePermissions) DecodeString(txt string, box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AutoSetField(fperms)
	if len(txt) < 9 {
		return out, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Given txt %q must have 10 characters to be a %T", txt, fperms)
	}
	in := strings.ToLower(txt[:9])
	out2, goof := fperms.User.DecodeString(in[0:3], EMPTY_BOX)
	out.AppendEncoded(out2)
	if IsError(goof) {
		return out, goof
	}
	out2, goof = fperms.Group.DecodeString(in[3:6], EMPTY_BOX)
	out.AppendEncoded(out2)
	if IsError(goof) {
		return out, goof
	}
	out2, goof = fperms.Other.DecodeString(in[6:], EMPTY_BOX)
	out.AppendEncoded(out2)
	if IsError(goof) {
		return out, goof
	}
	fperms.NotEmpty()
	return out, NO_GOOF
}

func (fperms FilePermissions) Equals(other interface{}) bool {
	fperms2, isa := other.(FilePermissions)
	if !isa {
		_, isa = other.(*FilePermissions)
		if isa {
			fperms2 = *(other.(*FilePermissions))
		} else {
			return false
		}
	}
	return *fperms.User == *fperms2.User &&
		*fperms.Group == *fperms2.Group &&
		*fperms.Other == *fperms2.Other
}

func (fperms FilePermissions) ToFileMode() os.FileMode {
	return os.FileMode(fperms.User.ToBinary())<<6 |
		os.FileMode(fperms.Group.ToBinary())<<3 |
		os.FileMode(fperms.Other.ToBinary())
}

func FileModeFromString(txt string) (os.FileMode, *Goof) {
	fmode := os.FileMode(0)
	out, goof := NewFilePermissions().DecodeString(txt, EMPTY_BOX)
	if IsError(goof) {
		return fmode, goof
	}
	fperms, _ := (out.GetDecoded().Value()).(*FilePermissions)
	return fperms.ToFileMode(), NO_GOOF
}
