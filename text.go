/*
	Text processing functions.
*/
package h2go

import (
	"fmt"
	"path/filepath"
	"strings"
)

// WORDITER ====================================================================

type WordIterator struct {
	words []string
	i     int // pointer
}

func MakeWordIterator(words []string) *WordIterator {
	return &WordIterator{
		words: words,
		i:     0,
	}
}

// Reset word counter.
func (witer *WordIterator) Reset() *WordIterator {
	witer.i = 0
	return witer
}

func (witer *WordIterator) Current() string {
	return witer.words[witer.i]
}

// Return next word and increment counter.  Return end of words token
// if no more left.
func (witer *WordIterator) Next() string {
	if witer.i < len(witer.words)-1 {
		witer.i++
		return witer.words[witer.i]
	}
	return NILWORD
}

// Return the next word, without incrementing the pointer.
func (witer *WordIterator) PeekNext() string {
	if witer.i < len(witer.words)-1 {
		return witer.words[witer.i+1]
	}
	return NILWORD
}

// Return the previous word, without decrementing the pointer.
func (witer *WordIterator) PeekPrev() string {
	if witer.i > 0 {
		return witer.words[witer.i-1]
	}
	return NILWORD
}

// Utilities.

func CapitaliseFirstLetter(s string) string {
	return strings.ToUpper(s[0:1]) + s[1:]
}

func StringIsIn(other string, words []string) bool {
	for _, word := range words {
		if other == word {
			return true
		}
	}
	return false
}

func HasSuffix(other string, words []string) (string, bool) {
	for _, word := range words {
		if strings.HasSuffix(other, word) {
			return word, true
		}
	}
	return "", false
}

// "a/b/c/d/e" to level 2 -> "d/e"
// "a/b/c/d/e" to level 3 -> "c/d/e" etc
func TruncatePathToSlashLevel(level int, path string) string {
	result := ""
	parts := strings.Split(filepath.ToSlash(path), "/")
	for d := level; d > 0; d-- {
		if len(parts) > d-1 {
			result += parts[len(parts)-d]
			if d > 1 {
				result += "/"
			}
		}
	}
	return result
}

// Text processing

func TrimText(txt string) string {
	return strings.TrimSpace(txt)
}

// In the given string "into", delete "del" characters at position "at", and
// insert the string "insert" at that position.
func InsertString(str, insert string, at, del, offset int) (string, int) {
	result := ""
	at += offset
	if at > (len(str)-1)-del {
		result = str[:at] + insert
	} else if at >= 0 {
		result = str[:at] + insert + str[at+del:]
	}
	return result, offset + len(insert) - del
}

func RemovePunctuationMarks(txt string) string {
	result := ""
	for _, c := range txt {
		if strings.IndexRune(PUNCTUATION_MARKS, c) == -1 {
			result += string(c)
		}
	}
	return result
}

func CollapseMultipleWhiteSpaces(txt string) string {
	result := ""
	spaceon := false
	firstnonspace := false
	for _, c := range txt {
		if c == ' ' || c == '\t' {
			if !spaceon {
				spaceon = true
			}
		} else {
			if spaceon {
				if firstnonspace {
					result += " " // trim spaces at front
				}
				spaceon = false
			}
			result += string(c)
			if !firstnonspace {
				firstnonspace = true
			}
		}
	}
	// trim spaces at end by not adding explicitely
	return result
}

// Replace tabs with spaces, collapse multiple spaces
func Normalise(txt string) string {
	txt = CollapseMultipleWhiteSpaces(txt)
	return txt
}

func ParseBool(txt string) (bool, *Goof) {
	switch strings.ToUpper(txt) {
	case "TRUE", "YES":
		return true, NO_GOOF
	case "FALSE", "NO":
		return false, NO_GOOF
	default:
		return false, Env.Goofs().MakeGoof("PARSING_TEXT",
			"%q not recognised as a boolean", txt)
	}
}

// Generic parser using supplied left and right characters (which should
// differ).  Returns slice of captured strings, as well as associated pair
// [start, end] including the left/right characters.  Double quotes provide
// protection from being captured, if the protect argument is set to true.
// The user can limit the number of words returned (-1 returns all).
func ParseDifferingBrackets(txt, left, right string, lim int, protect bool) ([]string, [][]int, string) {
	words := []string{}
	endpoints := [][]int{}
	rem := "" // remainder left since limit may invoke early return
	if len(txt) == 0 || len(left) == 0 || len(right) == 0 {
		return words, endpoints, rem
	}
	if !strings.ContainsAny(txt, left+right) {
		return words, endpoints, rem
	}
	cl := left[0]
	cr := right[0]
	dblquote := false
	cnt := 0
	word := ""
	laststart := 0
	limcnt := 0
	limit := lim > 0
	for i := 0; i < len(txt); i++ {
		switch txt[i] {
		case '"':
			if protect {
				dblquote = !dblquote
			}
		case cl:
			if !dblquote {
				cnt++
			}
			if cnt == 1 {
				laststart = i
			}
		case cr:
			if !dblquote {
				cnt--
				if cnt < 0 {
					cnt = 0 // ignore additional right chars
					break
				}
				if cnt == 0 {
					word = txt[laststart+1 : i]
					words = append(words, word)
					endpoints = append(endpoints, []int{laststart, i})
					laststart = i + 1
					rem = txt[i+1:]
					limcnt++
					if limit && lim == limcnt {
						break
					}
				}
			}
		}
	}
	return words, endpoints, rem
}

// Extracts text between brackets (incl brackets).  The bracket characters are
// the same to the left and right.  This function cannot protect text in double
// quotes, since the bracket may itself be double quotes.  Returns slice of
// captured strings, as well as associated pair [start, end] including the
// brackets.
func ParseSameBrackets(txt, bracket string, lim int) ([]string, [][]int, string) {
	words := []string{}
	endpoints := [][]int{}
	rem := "" // remainder left since limit may invoke early return
	if len(txt) == 0 || len(bracket) == 0 {
		return words, endpoints, rem
	}
	bracketed := false
	b := bracket[0]
	word := ""
	start := 0
	limcnt := 0
	limit := lim > 0
	for i := 0; i < len(txt); i++ {
		if txt[i] == b {
			bracketed = !bracketed
			if bracketed {
				start = i
			}
			if i > 0 && !bracketed {
				word = txt[start+1 : i]
				words = append(words, word)
				endpoints = append(endpoints, []int{start, i})
				rem = txt[i+1:]
				limcnt++
				if limit && lim == limcnt {
					break
				}
			}
		}
	}
	return words, endpoints, rem
}

// Extracts text labels prefixed with the given character, for subsequent
// cross referencing to a Workspace.  See tests for examples.
func ParsePrefixedLabels(txt, prefix string, lang Language, lim int) ([]string, [][]int, string) {
	labels := []string{}
	endpoints := [][]int{}
	rem := "" // remainder left since limit may invoke early return
	if len(txt) == 0 || len(prefix) == 0 {
		return labels, endpoints, rem
	}
	extracting := false
	p := prefix[0]
	label := ""
	endpoint := []int{}
	start := 0
	limcnt := 0
	limit := lim > 0
	roundBracketsIncluded := false
	collect := false
	otherAllowedChars := ".:"
	closingBracket := false
	for i := 0; i < len(txt); i++ {
		if i > 0 {
			if txt[i-1] == p {
				if txt[i] == '(' {
					otherAllowedChars += "()"
					roundBracketsIncluded = true
				}
			}
			if extracting && txt[i] == '[' {
				otherAllowedChars += "[]"
			}
		}
		if txt[i] == p {
			if i > 0 && txt[i-1] == p {
				// Allow prefix to be "escaped" by itself, e.g. $$
				extracting = false
			} else if !extracting && !lang.IsDigit(string(txt[i+1])) {
				extracting = true
				start = i
			}
		}
		if extracting {
			closingBracket = roundBracketsIncluded && txt[i] == ')'
			collect = (i-start) > 0 && // haven't just started and..
				(i == len(txt)-1 || // at end or..
					// non-label char or..
					!(lang.IsPrintable(string(txt[i])) ||
						strings.ContainsAny(
							string(txt[i]), otherAllowedChars)) ||
					closingBracket) // is closing bracket
			if collect {
				extracting = false
				if i == len(txt)-1 {
					label = txt[start+1:]
					rem = ""
					endpoint = []int{start, i}
				} else {
					label = txt[start+1 : i]
					if closingBracket {
						endpoint = []int{start, i}
						rem = txt[i+1:]
					} else {
						endpoint = []int{start, i - 1}
						rem = txt[i:]
					}
				}
				label = strings.Trim(label, "()")
				labels = append(labels, label)
				endpoints = append(endpoints, endpoint)
				limcnt++
				if limit && lim == limcnt {
					break
				}
			}
		}
	}
	return labels, endpoints, rem
}

// Splits given text by given separator, ignoring separators within
// double quotes.  If the separator is not found, the slice will be
// empty and the remainder will be the initial input.  Differs from
// strings.Split in that a separate remainder is returned.
func SplitOutsideDoubleQuotes(txt, sep string, lim int) ([]string, int, string, bool) {
	result := []string{}
	lastend := 0
	rem := "" // remainder left since limit may invoke early return
	foundSep := false
	if len(txt) == 0 || len(sep) == 0 {
		return result, lastend, rem, foundSep
	}
	ls := len(sep)
	dblquote := false
	laststart := 0
	limcnt := 0
	limit := lim > 0
	rem = txt
	for i := 0; i <= len(txt)-ls; i++ {
		if txt[i] == '"' {
			dblquote = !dblquote
		} else if txt[i:i+ls] == sep {
			if !dblquote && i > laststart {
				result = append(result, txt[laststart:i])
				laststart = i + 1
				lastend = i
				foundSep = true
				rem = txt[i+ls:]
				limcnt++
				if limit && lim == limcnt {
					break
				}
			}
		}
	}
	if !foundSep || (!limit && lastend < len(txt)-ls) {
		result = append(result, rem)
		rem = ""
	}
	return result, lastend, rem, foundSep
}

func CountSplitProtected(txt, sep string) int {
	p, _, _, _ := SplitOutsideDoubleQuotes(txt, sep, -1)
	return len(p)
}

// Removes double-quoted strings from given string in a single pass.
func RemoveDoubleQuoted(txt string, lim int) string {
	if len(txt) == 0 {
		return ""
	}
	dblquote := false
	result := ""
	limcnt := 0
	limit := lim > 0
	for i := 0; i < len(txt); i++ {
		if txt[i] == '"' {
			dblquote = !dblquote
			if !dblquote {
				limcnt++
				if limit && lim == limcnt {
					result += txt[i+1:]
					break
				}
			}
		} else {
			if !dblquote {
				result += string(txt[i])
			}
		}
	}
	return result
}

// Removes comment text sequences initiated by "//" up to the next \n.
func RemoveComments(txt string) string {
	if len(txt) == 0 {
		return ""
	}
	slash := false
	comment := false
	result := ""
	for i := 0; i < len(txt); i++ {
		if txt[i] == '/' {
			if slash { // we have a double slash
				if !comment { // start a comment, but only if not started yet
					comment = true
					slash = false
				}
			} else {
				slash = true // prime flag for potential double slash
				continue     // only include in result if it is really a single
			}
		} else {
			if !comment && slash { // ah, that slash was a single, include it
				result += "/"
			}
			slash = false
			if txt[i] == '\n' {
				if comment {
					comment = false
					continue // don't include in result
				}
			}
		}
		if !comment {
			result += string(txt[i])
		}
	}
	if !comment && slash { // ah, that final slash was a single, include it
		result += "/"
	}
	return result
}

// String brackets

// This implementation supports single character brackets only
type BracketStrings struct {
	Left    string
	Right   string
	Code    CODE
	Decoder StringTask
}

func (bstrs BracketStrings) String() string {
	return fmt.Sprintf(
		"%s:L%qR%q", Env.Types().Name(bstrs.Code), bstrs.Left, bstrs.Right)
}

type BracketStringMap struct {
	bracmap  map[string]BracketStrings
	leftset  string
	rightset string
}

func NewBracketStringMap() *BracketStringMap {
	return &BracketStringMap{
		bracmap: make(map[string]BracketStrings),
	}
}

func (bracmap *BracketStringMap) Raw() map[string]BracketStrings {
	return bracmap.bracmap
}
func (bracmap *BracketStringMap) Len() int {
	return len(bracmap.bracmap)
}
func (bracmap *BracketStringMap) Get(brac string) (BracketStrings, bool) {
	bstrs, ok := bracmap.bracmap[brac]
	return bstrs, ok
}
func (bracmap *BracketStringMap) Add(brac string, bstrs BracketStrings) *BracketStringMap {
	bracmap.bracmap[brac] = bstrs
	return bracmap
}

func (bracmap *BracketStringMap) Left() string  { return bracmap.leftset }
func (bracmap *BracketStringMap) Right() string { return bracmap.rightset }

// Returns two strings, the first containing all known left brackets and
// the second all known right brackets.
func (bracmap *BracketStringMap) BuildSets() (string, string) {
	l := ""
	r := ""
	for _, bstrs := range bracmap.Raw() {
		l += bstrs.Left
		r += bstrs.Right
	}
	return l, r
}

// Type specific parsing

// Assume that external spaces have been trimmed.
// First return parameter is the string actually consumed.
func ParseMap(txt string) (string, map[string]string) {
	bcount := 0
	laststart := 0
	lastend := 0
	gotkey := false
	result := make(map[string]string)
	dblquote := false
	valid := false
	k := ""
	for i := 0; i < len(txt); i++ {
		if strings.Contains(Env.Types().StringBrackets.Left(), string(txt[i])) {
			bcount++
		} else if strings.Contains(Env.Types().StringBrackets.Right(), string(txt[i])) {
			bcount--
		} else {
			switch txt[i] {
			case '"':
				dblquote = !dblquote
			case ':':
				valid = (!dblquote && bcount == 0)
				if valid {
					if !gotkey {
						k = txt[laststart:i]
						laststart = i + 1
						gotkey = true
					} else {
						laststart++ // do not count this is in next value
					}
				}
			case ',':
				valid = (!dblquote && bcount == 0)
				if valid {
					if gotkey {
						result[k] = txt[laststart:i]
						laststart = i + 1
						gotkey = false
						lastend = i
					} else {
						laststart++ // do not count this is in next key
					}
				}
			}
		}
	}
	if laststart < len(txt) {
		valid = (!dblquote && bcount == 0)
		if gotkey && valid {
			result[k] = txt[laststart:]
			lastend = len(txt)
		}
		// Ignore dregs if not complete
	}
	return txt[:lastend], result
}

// Assume that external spaces have been trimmed.
// First return parameter is the string actually consumed.
func ParseList(txt string) (string, []string) {
	bcount := 0
	result := []string{}
	laststart := 0
	lastend := 0
	dblquote := false
	for i := 0; i < len(txt); i++ {
		if strings.Contains(Env.Types().StringBrackets.Left(), string(txt[i])) {
			bcount++
		} else if strings.Contains(Env.Types().StringBrackets.Right(), string(txt[i])) {
			bcount--
		} else {
			switch txt[i] {
			case '"':
				dblquote = !dblquote
			case ',':
				if !dblquote && bcount == 0 {
					if (i - laststart) > 0 {
						result = append(result, txt[laststart:i])
						laststart = i + 1
						lastend = i
					} else {
						laststart++ // do not count this is in next value
					}
				}
			}
		}
	}
	if laststart < len(txt) {
		result = append(result, txt[laststart:])
		lastend = len(txt)
	}
	return txt[:lastend], result
}

// Recursive function returning lines of text wrapped to given width.
func Wrap(input []string, w int) []string {
	L := len(input)
	if L == 0 {
		return []string{""}
	}
	last := L - 1
	input[last] = strings.TrimPrefix(input[last], " ")
	if len(input[last]) <= w {
		return input
	}
	chop := input[last][:w]
	rem := ""
	i := strings.LastIndex(chop, " ")
	rem = input[last][w:]
	if i != -1 {
		if len(rem) > 0 && rem[0] == ' ' {
			// Don't shorten chop if already nicely split
			i = w - 1
		}
		if strings.ContainsAny(string(chop[i]), "[(<") {
			// Prefer to push opening brackets to next line
			i--
		}
		chop = chop[:i+1]
		rem = input[last][i+1:]
		chop = strings.TrimSpace(chop)
		if len(rem) > 0 && strings.ContainsAny(string(rem[0]), ",;") {
			// Prefer to keep dividers on first line
			if len(chop) < w {
				//Env.Log().Basic("start of rem = %q, lifting back to first line", rem[0])
				chop = chop + string(rem[0])
				rem = rem[1:]
			}
		}
	}
	input[last] = chop
	input = append(input, rem)
	return Wrap(input, w)
}

// Receive a string slice args and searches for the given sequence of keywords.
// Returns a result consisting of the next nreq arguments, and a string slice
// of the remaining arguments.
// e.g.
// args = ["smile", "for", "the", "camera,", "say", "cheese"]
// keywords = ["for", "the"]
// nreq = 2
// result = ["camera,", "say"]
// rem = ["smile", "cheese"]
func ExtractKeywordArgs(keywords, args []string, nreq int) ([]string, []string, *Goof) {
	result := []string{}
	rem := []string{}
	if len(keywords) == 0 {
		return result, rem, Env.Goofs().MakeGoof("BAD_ARGUMENTS", "No keywords provided")
	}
	capturing := false
	grabRemainder := false
	ncap := 0 // number captured
	ikey := 0 // key index
	for _, arg := range args {
		//arg = strings.ToLower(TrimText(arg))
		arg = TrimText(arg)
		if !grabRemainder && !capturing && strings.ToLower(arg) == keywords[ikey] {
			ikey++
			if ikey == len(keywords) {
				capturing = true // start capturing args
			}
		} else if capturing {
			result = append(result, arg)
			ncap++
			if ncap == nreq {
				capturing = false
				grabRemainder = true
			}
		} else {
			rem = append(rem, arg)
		}
	}
	if capturing {
		return result, rem, Env.Goofs().MakeGoof("PARSING_TEXT",
			"Required to capture %d arguments, could only capture %d",
			nreq, len(result))
	}
	return result, rem, NO_GOOF
}

// Headers, column width and alignment are optional.  Align columns using -1
// for left, 0 for centre, 1 for right, right is default.  If width is empty,
// maximum width of each column will be used.  For any column width is <= 0,
// maximum widths will be used for all, plus constant padding on left and
// right.  For example, if any width is -2, column width will be the maximum
// calculated from the strings, plus padding of two spaces on each side.
//
//                 x
//   +--------------------------->
//   |
// y |      rows = [y][x]string
//   |
//   v
//
func TabulateText(rows [][]string, headers []string, width []int, align []int, useLines bool, prefix string) ([]string, *Goof) {
	result := []string{}
	ncols := 0
	// Validate
	for y, row := range rows {
		if y == 0 {
			ncols = len(row)
		} else {
			if len(row) != ncols {
				return result, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
					"Number of columns in row %d (%q), %d, differs "+
						"from number in first row, %d",
					y, row, len(row), ncols)
			}
		}
	}
	if len(headers) > 0 && len(headers) != ncols {
		return result, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Number of header columns, %d, differs "+
				"from number of data columns, %d",
			len(headers), ncols)
	}
	if len(width) > 0 && len(width) != ncols {
		return result, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Number of column widths, %d, differs "+
				"from number of data columns, %d",
			len(width), ncols)
	}
	if len(align) > 0 && len(align) != ncols {
		return result, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Number of column alignment indicators, %d, differs "+
				"from number of data columns, %d",
			len(align), ncols)
	}

	// Defaults
	allrows := rows
	margin := make([]int, ncols)
	if len(width) == 0 {
		// Find max width of each column
		width = make([]int, ncols)
		for _, row := range allrows {
			for x, word := range row {
				if len(word) > width[x] {
					width[x] = len(word)
				}
			}
		}
	}

	// Prepend header row
	if len(headers) > 0 {
		allrows = append(allrows, []string{})
		copy(allrows[1:], allrows[0:])
		allrows[0] = headers
	}

	textboxlines := [][][]string{}
	maxwidth := []int{}
	maxdepth := []int{}
	for y, row := range allrows {
		rowlines := [][]string{}
		for x := 0; x < ncols; x++ {
			if y == 0 {
				maxwidth = append(maxwidth, 0)
			}
			if x == 0 {
				maxdepth = append(maxdepth, 0)
			}
			lines := []string{}
			if width[x] <= 0 {
				lines2 := strings.Split(row[x], "\n")
				for k, line := range lines2 {
					//lines = append(lines, zbase.TrimText(line))
					lines = append(lines, line)
					if len(lines[k]) > maxwidth[x] {
						maxwidth[x] = len(lines[k])
					}
				}
				margin[x] = -width[x]
			} else {
				margin[x] = 0
				row[x] = strings.Replace(row[x], "\n", " ", -1)
				row[x] = strings.Replace(row[x], "%", "%%", -1)
				lines = Wrap([]string{row[x]}, width[x])
			}
			rowlines = append(rowlines, lines)
			if len(lines) > maxdepth[y] {
				maxdepth[y] = len(lines)
			}
		}
		textboxlines = append(textboxlines, rowlines)
	}

	// Set previously determined column widths, if necessary
	xpad := make([]int, ncols)
	for x := 0; x < ncols; x++ {
		if width[x] <= 0 {
			xpad[x] = -width[x]
			width[x] = maxwidth[x]
		}
	}

	// Flatten textboxlines to individual rows
	newrows := [][]string{}
	rowmarkers := []int{}
	entry := ""
	r := 0
	for y, _ := range allrows {
		for k := 0; k < maxdepth[y]; k++ {
			newrow := []string{}
			for x := 0; x < ncols; x++ {
				entry = ""
				if k < len(textboxlines[y][x]) {
					entry = textboxlines[y][x][k]
				}
				newrow = append(newrow, entry)
			}
			newrows = append(newrows, newrow)
			r++
		}
		rowmarkers = append(rowmarkers, r-1)
	}

	rows = newrows

	if len(align) == 0 {
		align = make([]int, ncols)
		for x := 0; x < ncols; x++ {
			align[x] = 1 // right alignment default
		}
	}

	// Table lines
	hline := ""
	line := ""
	vline := ""
	if useLines {
		vline = "|"
	}
	if useLines {
		for x, w := range width {
			hline += "+" + strings.Repeat("-", w+2*xpad[x])
		}
		hline += "+"
	}

	spaces := 0
	// Finally, the data
	if useLines {
		result = append(result, prefix+hline)
	}
	r = 0
	a := 0
	for y, row := range rows {
		line = ""
		for x, text := range row {
			a = align[x]
			if len(headers) > 0 && y == 0 {
				a = 0
			}
			switch a {
			case -1:
				spaces = 0
			case 0:
				spaces = (width[x] - len(text)) / 2
			case 1:
				spaces = width[x] - len(text)
			}
			line +=
				vline +
					strings.Repeat(" ", spaces+margin[x]) +
					text +
					strings.Repeat(" ", width[x]-len(text)-spaces+margin[x]+strings.Count(text, "%%"))
		}
		line += vline
		result = append(result, prefix+line)
		if useLines && rowmarkers[r] == y {
			result = append(result, prefix+hline)
			r++
		}
	}
	return result, NO_GOOF
}

func HasPrefixSuffix(s, pre, suff string) bool {
	return strings.HasPrefix(s, pre) && strings.HasSuffix(s, suff)
}

func StringIsDecimalInteger(s string) bool {
	if len(s) == 0 {
		return false
	}
	for _, c := range s {
		if !CharIsDecimalDigit(c) {
			return false
		}
	}
	return true
}

func CharIsDecimalDigit(c int32) bool {
	if c >= '0' && c <= '9' {
		return true
	}
	return false
}

func RuneIsIn(c rune, runes []rune) bool {
	for _, r := range runes {
		if c == r {
			return true
		}
	}
	return false
}

// File path processing

// If the given path is a/b/c/.d/.e returns c/.d/.e
func LowestNotDotBranch(path string) string {
	dir := ""
	name := ""
	result := ""
	c := 0
	safetyLim := 10000
	for {
		dir = filepath.Dir(path)
		name = filepath.Base(path)
		if dir == "." {
			break
		}
		result = filepath.Join(name, result)
		if !strings.HasPrefix(result, ".") {
			break
		}
		c++
		if c > safetyLim {
			break
		}
		path = dir
	}
	return result
}
