package h2go

import (
	"fmt"
	"math/big"
)

type Clock struct {
	hour    uint8
	minute  uint8
	second  uint8
	fracsec *big.Rat
	*State
}

func NewClock() *Clock {
	return &Clock{
		fracsec: big.NewRat(1, 1),
		State:   NewState(),
	}
}

func BuildClock(h, m, s uint8, f *big.Rat) (*Clock, *Goof) {
	clk := NewClock()
	if f == nil {
		return clk, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Cannot build a new clock with a nil fracsec")
	}
	clk.hour = h
	clk.NotEmpty()
	clk.minute = m
	clk.second = s
	clk.fracsec = f
	clk.NowComplete()
	return clk, NO_GOOF
}

func MakeNanoFrac(ns int) *big.Rat  { return big.NewRat(int64(ns), 1000000000) }
func MakeMicroFrac(us int) *big.Rat { return big.NewRat(int64(us), 1000000) }
func MakeMilliFrac(ms int) *big.Rat { return big.NewRat(int64(ms), 1000) }

// Getters.

func (clk *Clock) Hour() uint8       { return clk.hour }
func (clk *Clock) Minute() uint8     { return clk.minute }
func (clk *Clock) Second() uint8     { return clk.second }
func (clk *Clock) FracSec() *big.Rat { return clk.fracsec }

func (clk *Clock) FracIsZero() bool { return clk.FracSec().Sign() == 0 }

func (clk *Clock) FracAsNano() uint32 {
	ns, _ := (&big.Rat{}).Mul(clk.FracSec(), TIME_SEC_IN_NANOS_AS_BIGRAT).Float64()
	return uint32(ns)
}

func (clk *Clock) String() string {
	str := fmt.Sprintf(
		"%02d:%02d:%02d",
		clk.Hour(),
		clk.Minute(),
		clk.Second())
	if clk.fracsec.Sign() != 0 {
		f64, _ := clk.fracsec.Float64()
		frac := fmt.Sprintf("%.9f", f64)
		frac = frac[1:]
		str += frac
	}
	return str
}

func (clk *Clock) Equals(other *Clock) bool {
	return clk.hour == other.hour &&
		clk.minute == other.minute &&
		clk.second == other.second &&
		clk.fracsec.Cmp(other.fracsec) == 0
}

func (clk *Clock) IsBefore(other *Clock) bool {
	if clk.Hour() < other.Hour() {
		return true
	}
	if clk.Hour() > other.Hour() {
		return false
	}
	if clk.Minute() < other.Minute() {
		return true
	}
	if clk.Minute() > other.Minute() {
		return false
	}
	if clk.Second() < other.Second() {
		return true
	}
	if clk.Second() > other.Second() {
		return false
	}
	if clk.FracSec().Cmp(clk.FracSec()) < 0 {
		return true
	}
	return false
}

func (clk *Clock) IsAfter(other *Clock) bool {
	if clk.Hour() > other.Hour() {
		return true
	}
	if clk.Hour() < other.Hour() {
		return false
	}
	if clk.Minute() > other.Minute() {
		return true
	}
	if clk.Minute() < other.Minute() {
		return false
	}
	if clk.Second() > other.Second() {
		return true
	}
	if clk.Second() < other.Second() {
		return false
	}
	if clk.FracSec().Cmp(clk.FracSec()) > 0 {
		return true
	}
	return false
}

func (clk *Clock) Copy() *Clock {
	clk2 := NewClock()
	clk2.hour = clk.hour
	clk2.minute = clk.minute
	clk2.second = clk.second
	clk2.fracsec = clk.fracsec
	clk2.State = clk.State.Copy()
	return clk2
}

func (clk *Clock) Plus(h, m, s int, f *big.Rat) (*Clock, int) {
	f2 := new(big.Rat).Add(clk.fracsec, f)
	csInt := new(big.Int).Div(f2.Num(), f2.Denom())
	csRat := new(big.Rat).SetInt(csInt)
	newFrac := new(big.Rat).Sub(f2, csRat)
	cs := int(csInt.Int64()) // carry seconds
	s2 := int(clk.second) + s + cs
	newSecond := s2 % SecondsInMinute
	cm := s2 / SecondsInMinute // carry minutes
	if newSecond < 0 {
		newSecond += SecondsInMinute
		cm -= 1
	}
	m2 := int(clk.minute) + m + cm
	newMinute := m2 % MinutesInHour
	ch := m2 / MinutesInHour // carry hours
	if newMinute < 0 {
		newMinute += MinutesInHour
		ch -= 1
	}
	h2 := int(clk.hour) + h + ch
	newHour := h2 % HoursInDay
	cd := h2 / HoursInDay // carry days
	if newHour < 0 {
		newHour += HoursInDay
		cd -= 1
	}
	newclk, _ := BuildClock(
		uint8(newHour), uint8(newMinute), uint8(newSecond), newFrac)
	return newclk, cd
}
