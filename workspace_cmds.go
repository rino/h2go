/*
	Base commands available to a Workspace.
*/
package h2go

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

// SYSTEM ======================================================================

// run =========================================================================

type CmdRun struct {
	*ExecutableCommand
	RunScriptLevel int
}

func (cmd CmdRun) Args() string {
	return "<(file)>"
}

func (cmd CmdRun) ShortDesc() string {
	return "execute commands from script file"
}

func (cmd CmdRun) LongDesc() string {
	return `Commands in a script execute just as if they were
entered interactively, but use a ; to delimit commands.  Scripts can be
run in a nested fashion.`
}

func (cmd CmdRun) Examples() string {
	return `run myScript // execute commands in myScript as if from the command line`
}

func (cmd CmdRun) Execute(input *Input) {
	goof := input.HasOneArg()
	if cmd.Wspace.IsError(goof) {
		return
	}
	cmd.RunScriptLevel++
	cmd.Wspace.ClearGoofs()
	path := TrimText(input.Args[0])
	_, path, _, goof = cmd.Wspace.IsSafePath(path)
	if IsError(goof) {
		return
	}
	if _, err := os.Stat(path); IsError(err) {
		cmd.Wspace.WrapGoof("READING_FILE", err,
			"While trying to read workspace file %q", path)
		return
	}
	file, err := os.Open(path)
	defer file.Close()
	if IsError(err) {
		cmd.Wspace.WrapGoof("OPENING_FILE", err,
			"While trying to read workspace file %q", path)
		return
	}
	r := bufio.NewReader(file)
	byts := []byte{}
	line := ""
	cumcmd := "" // cumulative command
	scmd := ""   // split command
	scmds := []string{}
	isPrefix := false
	lastgood := 0
	linenum := 1
	rem := ""
	nprocess := 0
	foundSep := false
	for {
		byts, isPrefix, err = r.ReadLine()
		line = string(byts)
		if isPrefix {
			cmd.Wspace.MakeGoof("PARSING_TEXT",
				"Line %d is too long (length > %d)",
				linenum, len(string(byts)))
			return
		}
		if IsError(err) {
			if err != io.EOF {
				cmd.Wspace.WrapGoof("READING_FILE", err,
					"Error reading line %d of %s",
					linenum, path)
				return
			}
			cmd.RunScriptLevel--
			break
		}
		line = RemoveComments(line)
		line = strings.Replace(line, "\n", " ", -1)
		cumcmd += rem + line
		scmds, _, _, foundSep = SplitOutsideDoubleQuotes(cumcmd, ";", -1)
		if strings.Contains(line, ";") && foundSep && len(scmds) > 0 {
			nprocess = 1
			if len(scmds) > 1 {
				nprocess = len(scmds) - 1
				rem = TrimText(scmds[len(scmds)-1])
			}
			for i := 0; i < nprocess; i++ {
				scmd = TrimText(scmds[i])
				if len(scmd) > 0 {
					cmd.Wspace.Process(
						scmd,
						fmt.Sprintf("file %s line %d text %q",
							path, linenum, scmd))
					if cmd.Wspace.HasGoofs() {
						return
					}
					cumcmd = ""
					lastgood = linenum
				}
			}
		} else {
			rem = ""
		}
		linenum++
	}
	if len(cumcmd) > 0 {
		cmd.Wspace.MakeGoof("PARSING_TEXT",
			"Missing ; from line %d in %q", lastgood+1, path)
	} else {
		cmd.Wspace.Advise("Processed %d lines of file %s", linenum-1, path)
	}
	return
}

func (cmd CmdRun) TabCompleter(line string, args []string) []string {
	result := cmd.RedirectCompleter(args)
	if len(result) == 0 {
		result = cmd.FileCompleter(args)
	}
	return result
}

// cd ==========================================================================

type CmdCd struct {
	*ExecutableCommand
}

func (cmd CmdCd) Args() string {
	return "<(path)>"
}

func (cmd CmdCd) ShortDesc() string {
	return "change to the given directory"
}

func (cmd CmdCd) LongDesc() string {
	return `Change the current working directory.  Unlike the linux cd command,
a directory path must be specified.`
}

func (cmd CmdCd) Examples() string {
	return `cd a/b/c // changes to a/b/c`
}

func (cmd CmdCd) Execute(input *Input) {
	goof := input.HasOneArg()
	if cmd.Wspace.IsError(goof) {
		return
	}
	path := TrimText(input.Args[0])
	_, abspath, _, goof := cmd.Wspace.IsSafePath(path)
	if IsError(goof) {
		return
	}
	err := os.Chdir(abspath)
	if IsError(err) {
		cmd.Wspace.WrapGoof("CHANGING_DIR", err,
			"While changing to directory %q (%q)", path, abspath)
	}
	return
}

func (cmd CmdCd) TabCompleter(line string, args []string) []string {
	result := cmd.RedirectCompleter(args)
	if len(result) == 0 {
		result = cmd.DirCompleter(args)
	}
	return result
}

// ls ==========================================================================

type CmdLs struct {
	*ExecutableCommand
}

func (cmd CmdLs) Args() string {
	return "{(path)}"
}

func (cmd CmdLs) ShortDesc() string {
	return "list directory contents"
}

func (cmd CmdLs) LongDesc() string {
	return `Similar to the unix command ls -alh, lists details of directory
contents.  If no directory is supplied, the current working directory is
assumed.`
}

func (cmd CmdLs) Examples() string {
	return `ls a/b/c // lists the contents of directory ./a/b/c
ls // lists contents of the current working directory`
}

func (cmd CmdLs) Execute(input *Input) {
	path := "."
	if len(input.Args) != 0 {
		path = TrimText(input.Args[0])
	}
	table, goof := DirLines(path,
		cmd.Wspace.Prefixes["OK"]+cmd.Wspace.Prefixes["PROMPT"])
	cmd.Wspace.IsError(goof)
	cmd.Wspace.LiteralLines(table)
	return
}

func (cmd CmdLs) TabCompleter(line string, args []string) []string {
	result := cmd.RedirectCompleter(args)
	if len(result) == 0 {
		result = cmd.DirCompleter(args)
	}
	return result
}

// pwd =========================================================================

type CmdPwd struct {
	*ExecutableCommand
}

func (cmd CmdPwd) Args() string {
	return ""
}

func (cmd CmdPwd) ShortDesc() string {
	return "print path for the current working directory"
}

func (cmd CmdPwd) LongDesc() string {
	return `Print the path for the current working directory.`
}

func (cmd CmdPwd) Examples() string {
	return `pwd // displays current directory path`
}

func (cmd CmdPwd) Execute(input *Input) {
	wd, err := os.Getwd()
	if IsError(err) {
		cmd.Wspace.WrapGoof("GETTING_DIR_INFO", err,
			"While getting current directory info")
		return
	} else {
		cmd.Wspace.Out(wd)
	}
	return
}

func (cmd CmdPwd) TabCompleter(line string, args []string) []string {
	return cmd.RedirectCompleter(args)
}

// INTROSPECTIVE ===============================================================

// echo ========================================================================

type CmdEcho struct {
	*ExecutableCommand
}

func (cmd CmdEcho) Args() string {
	return "{(msg)}"
}

func (cmd CmdEcho) ShortDesc() string {
	return "print literal"
}

func (cmd CmdEcho) LongDesc() string {
	return `Prints the given text to the output, or a blank line if no
message is given.  The text is printed verbatim.  This is mostly useful in
scripts.`
}

func (cmd CmdEcho) Examples() string {
	return `echo // prints a blank line
echo My message // prints "My message"`
}

func (cmd CmdEcho) Execute(input *Input) {
	cmd.Wspace.Out(input.Text)
	return
}

func (cmd CmdEcho) TabCompleter(line string, args []string) []string {
	return cmd.RedirectCompleter(args)
}

// help ========================================================================

type CmdHelp struct {
	*ExecutableCommand
}

func (cmd CmdHelp) Args() string {
	return "{(cmd)}"
}

func (cmd CmdHelp) ShortDesc() string {
	return "display command information"
}

func (cmd CmdHelp) LongDesc() string {
	return `Without an argument, lists all commands with short descriptions.
Include a command to obtain a fuller description, with examples.`
}

func (cmd CmdHelp) Examples() string {
	return `help // lists all commands
help echo // shows more detailed information for the echo command`
}

func (cmd CmdHelp) Execute(input *Input) {
	prefix := " "
	table := []string{}
	goof := NO_GOOF
	if len(input.Args) == 0 {
		table, goof = TabulateText(
			cmd.Wspace.HelpTable,
			[]string{},
			[]int{0, -1},
			[]int{-1, -1},
			false, "")
		cmd.Wspace.LiteralLines(table)
	} else {
		cmdLabel := input.Args[0]
		if len(input.Args) > 1 {
			cmdLabel += " " + input.Args[1]
		}
		c, exists := cmd.Wspace.Commands[cmdLabel]
		if !exists {
			cmd.Wspace.KeywordNotRecognised(cmdLabel)
			return
		}
		cmd.Wspace.Literal("Command from %s group:", cmd.Wspace.CommandGroup[cmdLabel])
		rows := [][]string{}
		tmprows := strings.Split(c.Args(), "\n")
		for _, row := range tmprows {
			rows = append(rows, []string{"", row})
		}
		rows[0][0] = fmt.Sprintf("%s", strings.ToUpper(cmdLabel))
		table, goof = TabulateText(
			rows,
			[]string{},
			[]int{12,
				cmd.Wspace.HELP_WIDTH - 12},
			[]int{-1, -1},
			false, prefix)
		cmd.Wspace.IsError(goof)
		cmd.Wspace.LiteralLines(table)
		rows = [][]string{
			[]string{strings.Replace(c.LongDesc(), "\n", " ", -1)},
		}
		table, goof = TabulateText(
			rows,
			[]string{},
			[]int{cmd.Wspace.HELP_WIDTH},
			[]int{-1},
			false, prefix)
		cmd.Wspace.IsError(goof)
		cmd.Wspace.LiteralLines(table)
		cmd.Wspace.Literal(cmd.Wspace.HELP_KEY)
		cmd.Wspace.Literal("Examples:")
		rows = [][]string{}
		for _, line := range strings.Split(c.Examples(), "\n") {
			rows = append(rows, []string{line})
		}
		table, goof = TabulateText(
			rows,
			[]string{},
			[]int{cmd.Wspace.HELP_WIDTH},
			[]int{-1},
			false, prefix)
		cmd.Wspace.LiteralLines(table)
	}
	cmd.Wspace.IsError(goof)
	return
}

func (cmd CmdHelp) TabCompleter(line string, args []string) []string {
	result := cmd.RedirectCompleter(args)
	if len(result) == 0 {
		for name, _ := range cmd.Wspace.Commands {
			if strings.HasPrefix(name, args[0]) {
				result = append(result, cmd.Label+" "+name)
			}
		}
	}
	return result
}

// whos ========================================================================

type CmdWhos struct {
	*ExecutableCommand
}

func (cmd CmdWhos) Args() string {
	return "{[type]}"
}

func (cmd CmdWhos) ShortDesc() string {
	return "display workspace objects"
}

func (cmd CmdWhos) LongDesc() string {
	return `Tabulates all user-defined variables ("workspace objects").
Including the "type" keyword displays the object names and types only.`
}

func (cmd CmdWhos) Examples() string {
	return `whos // displays the entire workspace, including values
whos type // displays the workspace, just types`
}

func (cmd CmdWhos) Execute(input *Input) {
	if cmd.Wspace.FieldMap.Len() == 0 {
		return
	}
	typeOnly := false
	if len(input.Args) == 1 {
		if strings.ToLower(strings.TrimSpace(input.Args[0])) == "type" {
			typeOnly = true
		}
	}
	rows := make([][]string, cmd.Wspace.FieldMap.Len())
	headers := []string{}
	align := []int{}
	width := []int{}
	if typeOnly {
		headers = []string{"Name", "Bytes", "Type"}
		align = []int{0, -1, -1}
		width = []int{0, 50, 0}
	} else {
		headers = []string{"Name", "Value", "Type"}
		align = []int{0, -1, -1}
		width = []int{0, 50, 0}
	}
	// Sort by keys
	keys := []interface{}{}
	for k, _ := range cmd.Wspace.FieldMap.ToMap() {
		keys = append(keys, k)
	}
	//sort.Strings(keys)

	y := 0
	typstr := ""
	name := ""
	for _, k := range keys {
		wsf := cmd.Wspace.FieldMap.Get(k) // wsf = workspace field
		name = fmt.Sprintf("%v", k)
		switch ut := (wsf.Value()).(type) {
		case *FieldMap:
			typstr = fmt.Sprintf("%v{%d}", wsf.Type(), ut.Len())
		case *FieldList:
			typstr = fmt.Sprintf("%v[%d]", wsf.Type(), ut.Len())
		default:
			typstr = fmt.Sprintf("%v", wsf.Type())
		}
		if typeOnly {
			sizestr := ""
			b, isa := (wsf.Value()).(Binaryable)
			if isa {
				out, goof := CalculateBinarySize(b, BYTEORDER)
				cmd.Wspace.IsError(goof)
				sizestr = fmt.Sprintf("%d", out.Len)
			}
			rows[y] = []string{
				name,
				sizestr,
				typstr,
			}
		} else {
			rows[y] = []string{
				name,
				fmt.Sprintf("%v", wsf.Value()),
				typstr,
			}
		}
		y++
	}
	table, goof := TabulateText(
		rows, headers, width, align, true,
		cmd.Wspace.Prefixes["OK"]+cmd.Wspace.Prefixes["PROMPT"])
	cmd.Wspace.IsError(goof)
	cmd.Wspace.LiteralLines(table)
	return
}

func (cmd CmdWhos) TabCompleter(line string, args []string) []string {
	result := cmd.RedirectCompleter(args)
	if len(result) == 0 && len(args) > 0 {
		if strings.HasPrefix("type", args[0]) {
			result = []string{cmd.Label + " " + "type"}
		}
	}
	return result
}

// clear =======================================================================

type CmdClear struct {
	*ExecutableCommand
}

func (cmd CmdClear) Args() string {
	return ""
}

func (cmd CmdClear) ShortDesc() string {
	return "clear workspace of all values"
}

func (cmd CmdClear) LongDesc() string {
	return `Deletes all workspace objects.`
}

func (cmd CmdClear) Examples() string {
	return `clear // wipes the workspace`
}

func (cmd CmdClear) Execute(input *Input) {
	cmd.Wspace.ClearData()
	return
}

func (cmd CmdClear) TabCompleter(line string, args []string) []string {
	return cmd.RedirectCompleter(args)
}
