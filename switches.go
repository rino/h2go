/*
	General purpose binary switches.
*/
package h2go

import (
	"reflect"
	"sort"
	"strings"
)

type Switches struct {
	bools     map[string]bool
	mask      map[string]byte
	keys      []string // sorted key list
	n         int      // number of encoded bytes
	validator func(*Switches) *Goof
	*State
}

var _ Binaryable = NewSwitches()
var _ Validatable = NewSwitches()
var _ Stateful = NewSwitches()

func NewSwitches() *Switches {
	result := &Switches{
		bools:     make(map[string]bool),
		mask:      make(map[string]byte),
		keys:      []string{},
		validator: func(*Switches) *Goof { return NO_GOOF },
		State:     NewState(),
	}
	return result
}

// The intention is that the keys are created only once.
func MakeSwitches(bools map[string]bool) (*Switches, *Goof) {
	sw := NewSwitches()
	sw.bools = bools
	sw.NotEmpty()
	return sw.preCalc()
}

func MakeSwitchesFromStruct(i interface{}) (*Switches, *Goof) {
	sw := NewSwitches()
	s, _, _, goof := RequireStructPtr(i)
	if IsError(goof) {
		return sw, goof
	}
	st := s.Type()
	var sf reflect.Value

	for j := 0; j < s.NumField(); j++ {
		sf = s.Field(j)
		if sf.IsValid() && ValueIsExportable(sf) {
			// field is valid and exportable
			if sf.Type().Kind() == reflect.Bool {
				sw.bools[st.Field(j).Name] = sf.Interface().(bool)
				sw.NotEmpty()
			}
		}
	}
	return sw.preCalc()
}

// Getters
func (sw *Switches) Names() []string         { return sw.keys }
func (sw *Switches) NumberEncodedBytes() int { return sw.n }
func (sw *Switches) Exists(key string) bool {
	_, exists := sw.bools[key]
	if exists {
		return true
	}
	return false
}

func (sw *Switches) Validate() *Goof {
	return sw.validator(sw)
}

func (sw *Switches) IsTrue(key string) bool {
	b, exists := sw.bools[key]
	if exists {
		return b
	}
	return false
}

func (sw *Switches) OrThoseWithKeyPrefix(prefix string) bool {
	result := false
	c := 0
	for _, k := range sw.keys {
		if strings.HasPrefix(k, prefix) {
			if c == 0 {
				result = sw.bools[k]
				c++
			} else {
				result = result || sw.bools[k]
			}
		}
	}
	return result
}

func (sw *Switches) AndThoseWithKeyPrefix(prefix string) bool {
	result := false
	c := 0
	for _, k := range sw.keys {
		if strings.HasPrefix(k, prefix) {
			if c == 0 {
				result = sw.bools[k]
				c++
			} else {
				result = result && sw.bools[k]
			}
		}
	}
	return result
}

func (sw *Switches) RequireNumberWithKeyPrefixBeThis(n int, prefix string, b bool) *Goof {
	subset := []string{}
	c := 0
	for _, k := range sw.keys {
		if strings.HasPrefix(k, prefix) {
			subset = append(subset, k)
			if sw.bools[k] == b {
				c++
			}
		}
	}
	if c != n {
		return Env.Goofs().MakeGoof("CONFLICTING_SETTINGS",
			"Require that %d of %s be %s, found %d",
			n, sw.SubsetToString(subset), b, c)
	}
	return NO_GOOF
}

func (sw *Switches) FindAllKeysWithPrefix(prefix string) []string {
	subset := []string{}
	for _, k := range sw.keys {
		if strings.HasPrefix(k, prefix) {
			subset = append(subset, k)
		}
	}
	return subset
}

func (sw *Switches) FindAllKeysWithPrefixAndValue(prefix string, b bool) []string {
	subset := []string{}
	for _, k := range sw.keys {
		if strings.HasPrefix(k, prefix) {
			if sw.bools[k] == b {
				subset = append(subset, k)
			}
		}
	}
	return subset
}

func (sw *Switches) FindAllKeysWithValue(b bool) []string {
	subset := []string{}
	for _, k := range sw.keys {
		if sw.bools[k] == b {
			subset = append(subset, k)
		}
	}
	return subset
}

// Setters
func (sw *Switches) SetValidator(validator func(*Switches) *Goof) *Switches {
	sw.validator = validator
	return sw
}

func (sw *Switches) Set(key string, b bool) (*Goof, bool) {
	b0, exists := sw.bools[key]
	if exists {
		sw.bools[key] = b
		return NO_GOOF, b0
	}
	return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
		"The switch %q does not exist", key), false
}

func (sw *Switches) SetByNumber(i int, b bool) bool {
	if i >= 0 && i < len(sw.keys) {
		b0 := sw.bools[sw.keys[i]]
		sw.bools[sw.keys[i]] = b
		return b0
	}
	return false
}

func (sw *Switches) SetAllWithKeyPrefix(prefix string, b bool) {
	for _, k := range sw.keys {
		if strings.HasPrefix(k, prefix) {
			sw.bools[k] = b
		}
	}
	return
}

func (sw *Switches) preCalc() (*Switches, *Goof) {
	if len(sw.bools) > 255 {
		return sw, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Number of switches limited to 255, got %d",
			len(sw.bools))
	}
	for k, _ := range sw.bools {
		sw.keys = append(sw.keys, k)
	}
	sort.Strings(sw.keys)
	sw.n = sw.nBytes(len(sw.keys)) + 1
	rem := uint8(0)
	for i, k := range sw.keys {
		_, rem = sw.locateBit(i)
		sw.mask[k] = 1 << (7 - rem)
	}
	sw.NowComplete()
	return sw, NO_GOOF
}

func (sw *Switches) locateBit(i int) (int, uint8) {
	return i / 8, uint8(i - (i/8)*8)
}

func (sw *Switches) nBytes(nbits int) int {
	n := nbits / 8
	if nbits-n*8 > 0 {
		n++
	}
	return n
}

func (sw *Switches) Copy() *Switches {
	sw2 := NewSwitches()
	for k, b := range sw.bools {
		sw2.bools[k] = b
	}
	for _, k := range sw.keys {
		sw2.keys = append(sw2.keys, k)
	}
	for k, b := range sw.mask {
		sw2.mask[k] = b
	}
	sw2.n = sw.n
	sw2.validator = sw.validator
	sw2.State = sw.State.Copy()
	return sw2
}

func (sw *Switches) String() string {
	return sw.SubsetToString(sw.keys)
}

func (sw *Switches) SubsetToString(keys []string) string {
	result := "("
	boolstr := ""
	bitcount := 0
	for i, k := range keys {
		if sw.bools[k] {
			boolstr = "1"
		} else {
			boolstr = "0"
		}
		if i != len(keys)-1 {
			if bitcount == 7 {
				boolstr += ";"
				bitcount = 0
			} else {
				boolstr += ","
			}
		}
		result += k + ":" + boolstr
		bitcount++
	}
	return result + ")"
}

func (sw *Switches) ToStringSlice() []string {
	return sw.SubsetToStringSlice(sw.keys)
}

func (sw *Switches) SubsetToStringSlice(keys []string) []string {
	result := []string{}
	boolstr := ""
	for _, k := range keys {
		if sw.bools[k] {
			boolstr = "1"
		} else {
			boolstr = "0"
		}
		result = append(result, k+":"+boolstr)
	}
	return result
}

func (sw *Switches) ValueToString(b bool) string {
	return sw.SubsetToString(sw.FindAllKeysWithValue(b))
}

func (sw *Switches) Equals(other interface{}) bool {
	sw2 := NewSwitches()
	switch v := other.(type) {
	case Switches:
		sw2 = &v
	case *Switches:
		sw2 = v
	default:
		return false
	}
	if len(sw.keys) != len(sw2.keys) {
		return false
	}
	exist := false
	b2 := false
	for k, b := range sw.bools {
		b2, exist = sw2.bools[k]
		if !exist || b != b2 {
			return false
		}
	}
	return true
}

func (sw *Switches) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	byts := make([]byte, sw.n-1)
	for i, k := range sw.keys {
		if sw.bools[k] {
			byts[i/8] |= sw.mask[k]
		}
	}
	byts2, goof := bfr.WriteBytes([]byte{byte(len(sw.keys))})
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AppendEncodedAs("number of switches", byts2)
	byts, goof = bfr.WriteBytes(byts)
	out.AppendEncodedAs("switches "+sw.String(), byts)
	return out, goof
}

// The Switches must contain a valid map.
func (sw *Switches) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	nbits := byte(0)
	byts, goof := bfr.Read(&nbits, box)
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.SetField(MakeField(sw, Env.Types().Number["UNKNOWN_CODE"]))
	out.AppendEncodedAs("number of switches", byts)
	if nbits != byte(len(sw.keys)) {
		return out, Env.Goofs().MakeGoof("COUNT_MISMATCH",
			"While decoding a %T, the encoded number of switches, "+
				"%d, does not equal the map length %d",
			sw, nbits, len(sw.keys))
	}
	byts = make([]byte, sw.nBytes(int(nbits)))
	byts, goof = bfr.ReadBytes(byts)
	if IsError(goof) {
		out.AppendEncodedAs("switches", byts)
		return out, goof
	}

	for i, k := range sw.keys {
		if byts[i/8]&sw.mask[k] > 0 {
			sw.bools[k] = true
		} else {
			sw.bools[k] = false
		}
	}
	out.AppendEncodedAs("switches "+sw.String(), byts)
	return out, goof
}

// Return all possible combinations of switches (for testing).
func (sw *Switches) Enumerate() []*Switches {
	return sw.enumerate(sw.Copy(), 0, []*Switches{})
}

func (sw *Switches) enumerate(in *Switches, i int, out []*Switches) []*Switches {
	branch1 := in.Copy()
	branch2 := in.Copy()
	branch1.SetByNumber(i, true)
	branch2.SetByNumber(i, false)
	if i < len(sw.keys)-1 {
		out = sw.enumerate(branch1, i+1, out)
		out = sw.enumerate(branch2, i+1, out)
	} else {
		out = append(out, branch1)
		out = append(out, branch2)
	}
	return out
}

func (sw *Switches) GroupValidator(prefix string, b bool) ([]string, *Goof) {
	subset := sw.FindAllKeysWithPrefixAndValue(prefix, true)
	if len(subset) == 0 {
		return subset, Env.Goofs().MakeGoof("MISSING_SETTINGS",
			"At least one %q switch should be %v", prefix, b)
	}
	if len(subset) > 1 {
		return subset, Env.Goofs().MakeGoof("CONFLICTING_SETTINGS",
			"More than one %q switch is %v, %s",
			prefix, b, sw.SubsetToString(subset))
	}
	return subset, NO_GOOF
}
