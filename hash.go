/*
 */
package h2go

import (
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// HASH TOKEN REQUIREMENTS =====================================================

// Returns the required number of zerobits, the Scrypt parameters, and the
// start and end of the allowable window (in seconds) from the present time.
// If before now, the start/end offset should be -ve, if after, +ve.
type HashTokenRequirements func(ver *Version) (int, *ScryptParams, [2]int)

// HASH TOKEN ==================================================================

// A Scrypt proof of work hash.  Inspired by Hashcash, based on the Golang
// Scrypt implementation at https://godoc.org/golang.org/x/crypto/scrypt
//
// "1.2.3,10,16384,8,10,http://123.6.7.87:9003,201607081310,9fc0987fac0c179dd4bf91f9"
//
// "1.2.3" web2go version number
// "10" number of leading zeros in hash
// "16384,8,10" scrypt N, p, r in the Golang implementation
// "http://123.6.7.87:9003" origin of sender
// "201607081310" datetime YYYYMMDDhhmm, UTC, rounded to 10 min
// "9fc0987fac0c179dd4bf91f9" random hex string
type HashToken struct {
	ver       *Version
	speczbits int // Specified, may differ from Required per version
	reqs      HashTokenRequirements
	zeromask  [2]byte
	params    *ScryptParams
	origin    string
	time      *CalClock
	rand      []byte
	hash      []byte
	counter   int
	goof      chan *Goof
	stop      bool
	*State
}

var _ Stringable = NewHashToken()
var _ Stateful = NewHashToken() // embedded State

func NewHashToken() *HashToken {
	return &HashToken{
		ver:      NewVersion(),
		zeromask: [2]byte{0, 0},
		reqs: func(ver *Version) (int, *ScryptParams, [2]int) {
			return 0,
				MakeDefaultScryptParams(
					int64(HASHTOKEN_HASH_BYTE_LENGTH)),
				[2]int{-3600, 600}
		},
		params: NewScryptParams(),
		time:   NewCalClock(),
		rand:   make([]byte, HASHTOKEN_RAND_BYTE_LENGTH),
		hash:   make([]byte, HASHTOKEN_HASH_BYTE_LENGTH),
		goof:   make(chan *Goof),
		State:  NewState(),
	}
}

func MakeHashToken(origin string, ver *Version, reqs HashTokenRequirements) (*HashToken, *Goof) {
	if strings.Contains(origin, ",") {
		return NewHashToken(), Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Origin %q cannot contain commas", origin)
	}
	reqdzbits, _, _ := reqs(ver)
	return BuildHashToken(
		origin,
		reqdzbits,
		ver,
		reqs)
}

// Make the required speczbits an input parameter to make testing easier.
func BuildHashToken(origin string, speczbits int, ver *Version, reqs HashTokenRequirements) (*HashToken, *Goof) {
	htoken := NewHashToken()
	goof := NO_GOOF
	htoken.ver = ver
	htoken.NotEmpty()
	if speczbits < 0 || speczbits > htoken.TokenBitLength() {
		return htoken, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"The required speczbits must be between 0 and %d, got %d",
			htoken.TokenBitLength(), speczbits)
	}
	htoken.speczbits = speczbits
	htoken.reqs = reqs
	htoken.MakeZeroMask()
	_, params, _ := htoken.reqs(ver)
	htoken.params = params
	htoken.origin = origin
	htoken.time, goof = NowUTC(Env.Calendars()["Gregorian"])
	if IsError(goof) {
		return htoken, goof
	}
	htoken.rand = RandomBytes(HASHTOKEN_RAND_BYTE_LENGTH)
	htoken.NowComplete()
	htoken.NotRunning()
	return htoken, goof
}

func (htoken *HashToken) Copy() *HashToken {
	htoken2 := NewHashToken()
	htoken2.ver = htoken.ver.Copy()
	htoken2.speczbits = htoken.speczbits
	htoken2.reqs = htoken.reqs
	htoken2.zeromask[0] = htoken.zeromask[0]
	htoken2.zeromask[1] = htoken.zeromask[1]
	htoken2.params = htoken.params.Copy()
	htoken2.origin = htoken.origin
	htoken2.time, _ = (htoken.time.Copy()).(*CalClock)
	for i, byt := range htoken.rand {
		htoken2.rand[i] = byt
	}
	htoken2.counter = htoken.counter
	htoken2.State = htoken.State.Copy()
	return htoken2
}

// Getters
func (htoken *HashToken) Version() *Version                   { return htoken.ver }
func (htoken *HashToken) SpecifiedZerobits() int              { return htoken.speczbits }
func (htoken *HashToken) Requirements() HashTokenRequirements { return htoken.reqs }
func (htoken *HashToken) Zeromask() [2]byte                   { return htoken.zeromask }
func (htoken *HashToken) ScryptParams() *ScryptParams         { return htoken.params }
func (htoken *HashToken) Origin() string                      { return htoken.origin }
func (htoken *HashToken) Time() *CalClock                     { return htoken.time }
func (htoken *HashToken) Rand() []byte                        { return htoken.rand }
func (htoken *HashToken) Hash() []byte                        { return htoken.hash }
func (htoken *HashToken) Goof() chan *Goof                    { return htoken.goof }
func (htoken *HashToken) Counter() int                        { return htoken.counter }
func (htoken *HashToken) TokenByteLength() int                { return HASHTOKEN_HASH_BYTE_LENGTH }
func (htoken *HashToken) TokenBitLength() int                 { return HASHTOKEN_HASH_BYTE_LENGTH * 8 }

func (htoken *HashToken) Rembits(zbits int) int {
	return zbits - (zbits/8)*8
}

func (htoken *HashToken) SetRequirements(reqs HashTokenRequirements) *HashToken {
	htoken.reqs = reqs
	return htoken
}

func (htoken *HashToken) MakeZeroMask() {
	htoken.zeromask[0] = byte(htoken.speczbits / 8)
	htoken.zeromask[1] =
		(byte(255) >> byte(htoken.Rembits(htoken.speczbits))) ^ 255
	return
}

// Checks only that the specified number of speczbits are present.
func (htoken *HashToken) IsCorrect() bool {
	for i := 0; i < int(htoken.zeromask[0]); i++ {
		if htoken.hash[i] != 0 {
			return false
		}
	}
	if htoken.Rembits(htoken.speczbits) > 0 {
		if (htoken.hash[int(htoken.zeromask[0])] & htoken.zeromask[1]) != 0 {
			return false
		}
	}
	return true
}

func (htoken *HashToken) WouldBeCorrect(byts []byte) (bool, *Goof) {
	if len(byts) != htoken.TokenByteLength() {
		return false, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Given byte slice of length %d cannot be a valid %T of length %d",
			len(byts), htoken, htoken.TokenByteLength())
	}
	for i := 0; i < int(htoken.zeromask[0]); i++ {
		if byts[i] != 0 {
			return false, NO_GOOF
		}
	}
	if htoken.Rembits(htoken.speczbits) > 0 {
		if (byts[int(htoken.zeromask[0])] & htoken.zeromask[1]) != 0 {
			return false, NO_GOOF
		}
	}
	return true, NO_GOOF
}

func CheckHashToken(tokstr, origin string, reqdVersion *Version, reqs HashTokenRequirements, box *Box) *Goof {
	env := EnvOrDefault(box)
	if len(tokstr) == 0 {
		return env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Empty hash token")
	}
	ht := NewHashToken().SetRequirements(reqs)
	_, goof := ht.DecodeString(tokstr, box)
	if IsError(goof) {
		return goof
	}

	// Version, customise this part as required
	if ht.Version().Major() != reqdVersion.Major() {
		return env.Goofs().MakeGoof("INVALID_DATA",
			"Qualifying token %q major version %d does not match current %d",
			tokstr, ht.Version().Major(), reqdVersion.Major())
	}
	// Zerobits
	reqdzbits, reqdparams, secOffsets := ht.Requirements()(ht.Version())
	if ht.SpecifiedZerobits() != reqdzbits {
		return env.Goofs().MakeGoof("INVALID_DATA",
			"Qualifying token %q specifies %d leading zero bits, require %d",
			tokstr, ht.SpecifiedZerobits(),
			reqdzbits)
	}
	// Scrypt params
	if !ht.ScryptParams().Equals(reqdparams) {
		return env.Goofs().MakeGoof("INVALID_DATA",
			"Qualifying token %q Scrypt params are %v, should be %v",
			tokstr, ht.ScryptParams(), reqdparams)
	}
	// Origin
	if ht.Origin() != origin {
		return env.Goofs().MakeGoof("INVALID_DATA",
			"Qualifying token %q origin %q does not match header origin %q",
			tokstr, ht.Origin(), origin)
	}
	// Datetime, must be within specified window
	now, goof := Now(env.Calendars()["Gregorian"], UTC)
	if IsError(goof) {
		return env.Goofs().WrapGoof("DATE_AND_TIME", goof,
			"While getting current time to check %T %q",
			ht, tokstr)
	}
	t1, goof := now.PlusInt(secOffsets[0])
	if IsError(goof) {
		return env.Goofs().WrapGoof("DATE_AND_TIME", goof,
			"While getting calculating start of valid time window for %T %q",
			ht, tokstr)
	}
	t2, goof := now.PlusInt(secOffsets[1])
	if IsError(goof) {
		return env.Goofs().WrapGoof("DATE_AND_TIME", goof,
			"While getting calculating end of valid time window for %T %q",
			ht, tokstr)
	}
	interval, goof := BuildInterval(t1, t2)
	if IsError(goof) {
		return env.Goofs().WrapGoof("DATE_AND_TIME", goof,
			"While building valid time interval window for %T %q",
			ht, tokstr)
	}
	ok, goof := now.IsWithin(interval)
	if IsError(goof) {
		return env.Goofs().WrapGoof("DATE_AND_TIME", goof,
			"While testing if time is within valid window for %T %q ",
			ht, tokstr)
	}
	if !ok {
		return env.Goofs().MakeGoof("INVALID_DATA",
			"Qualifying token %q timestamp is not within required "+
				"interval %v, (%d, %d)[s] offset from current "+
				"time %v",
			tokstr, interval, secOffsets[0], secOffsets[1], now)
	}
	// Hash correctness
	goof = ht.HashNow(box)
	if IsError(goof) {
		return goof
	}
	if !ht.IsCorrect() {
		return env.Goofs().MakeGoof("INVALID_DATA",
			"Qualifying token %q hash does not have the required %d "+
				"leading zero bits",
			tokstr, reqdzbits)
	}
	return NO_GOOF
}

func (htoken *HashToken) Prefix() string {
	return htoken.Version().String() + "," +
		strconv.Itoa(htoken.SpecifiedZerobits()) + "," +
		htoken.params.String() + "," +
		htoken.origin
}

func (htoken *HashToken) TimeString(yr int64, mth, d, h, m uint8) string {
	return fmt.Sprintf("%04d%02d%02d%02d%02d", yr, mth, d, h, m)
}

func (htoken *HashToken) String() string {
	yr, _ := htoken.time.Year().AsGoInt64()
	return htoken.Prefix() + "," +
		htoken.TimeString(
			yr,
			htoken.time.Month().Value(),
			htoken.time.Day(),
			htoken.time.Hour(),
			htoken.time.Minute()) + "," +
		hex.EncodeToString(htoken.rand)
}

func (htoken *HashToken) EncodeString(box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AppendEncoded(htoken.String())
	return out, NO_GOOF
}

func (htoken *HashToken) DecodeString(txt string, box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AutoSetField(htoken)
	parts := strings.Split(txt, ",")
	if len(parts) != 8 {
		return out, Env.Goofs().MakeGoof("PARSING_TEXT",
			"While parsing a %T stamp %q, expected %d parts, got %d",
			htoken, txt, 8, len(parts))
	}

	// Version
	txt2 := parts[0]
	out2, goof := htoken.Version().DecodeString(txt2, box)
	out.AppendEncodedTo([]interface{}{0, "version"}, out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}

	// Zerobits
	txt2 = TrimText(parts[1])
	var err error
	htoken.speczbits, err = strconv.Atoi(txt2)
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing speczbits field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "speczbits"}, txt2)

	// Scrypt params
	// N (cost)
	txt2 = TrimText(parts[2])
	cost, err := strconv.Atoi(txt2)
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing Scrypt params N (cost) field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "Scrypt params cost"}, txt2)
	// r
	txt2 = TrimText(parts[3])
	r, err := strconv.Atoi(txt2)
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing Scrypt params r field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "Scrypt params r"}, txt2)
	// p
	txt2 = TrimText(parts[4])
	p, err := strconv.Atoi(txt2)
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing Scrypt params p field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "Scrypt params p"}, txt2)
	htoken.params = MakeScryptParams(
		int64(cost), int64(r), int64(p), int64(htoken.TokenByteLength()))

	// Origin
	htoken.origin = TrimText(parts[5])
	out.AppendEncodedTo([]interface{}{0, "Origin"}, htoken.origin)

	// Datetime
	txt2 = TrimText(parts[6])
	if len(txt2) != 12 {
		return out, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Expected datetime field %q of length %d in %T, got %d",
			txt2, 12, htoken, len(txt2))
	}
	// year
	year, err := strconv.Atoi(txt2[:4])
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing year in datetime field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "datetime year"}, txt2[:4])
	// month
	month, err := strconv.Atoi(txt2[4:6])
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing month in datetime field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "datetime month"}, txt2[4:6])
	// day
	day, err := strconv.Atoi(txt2[6:8])
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing day in datetime field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "datetime day"}, txt2[6:8])
	// hour
	hour, err := strconv.Atoi(txt2[8:10])
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing hour in datetime field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "datetime hour"}, txt2[8:10])
	// minute
	minute, err := strconv.Atoi(txt2[10:])
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing minute in datetime field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "datetime minute"}, txt2[10:])
	htoken.time, goof = BuildCalClock(
		year,
		uint8(month),
		uint8(day),
		uint8(hour),
		uint8(minute),
		0,
		ZeroFrac(),
		Env.Calendars()["Gregorian"])
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", goof,
			"While building %T from datetime field %q in %T %q",
			htoken.time, txt2, htoken, txt)
	}

	// Random
	txt2 = TrimText(parts[7])
	if len(txt2) != HASHTOKEN_RAND_BYTE_LENGTH*2 {
		return out, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Expected random hex string field %q of length %d in %T, got %d",
			txt2, HASHTOKEN_RAND_BYTE_LENGTH*2, htoken, len(txt2))
	}
	htoken.rand, err = hex.DecodeString(txt2)
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While parsing random hex string field %q in %T %q",
			txt2, htoken, txt)
	}
	out.AppendEncodedTo([]interface{}{0, "random hex string"}, txt2)
	return out, NO_GOOF
}

func (htoken *HashToken) HashNow(box *Box) *Goof {
	goof := NO_GOOF
	htoken.hash, goof = htoken.params.Hash([]byte(htoken.String()), []byte{}, box)
	return goof
}

func (htoken *HashToken) Stop() {
	htoken.stop = true
	return
}

// Wait until search has ended through either valid completion or error.
func (htoken *HashToken) Wait() *Goof {
	goof := NO_GOOF
	if htoken.IsRunning() {
		goof = <-htoken.goof
	}
	return goof
}

// Run as a goroutine, accepts a limit to the number of minutes it should run.
// Time is discretised into 10 min intervals.
func (htoken *HashToken) Search(limit, reqdzbits int, tag string, box *Box) {
	htoken.NowRunning()
	defer htoken.NotRunning()
	env := EnvOrDefault(box).CopyAndStamp("(HashToken Search " + tag + ")")
	box = box.Copy().SetEnv(env)

	if reqdzbits < 0 || reqdzbits > htoken.TokenBitLength() {
		htoken.goof <- env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"The required speczbits must be between 0 and %d, got %d",
			htoken.TokenBitLength(), reqdzbits)
	}
	htoken.UpdateTime(env)
	goof := NO_GOOF
	htoken.counter = 0
	t0 := time.Now().Unix()
	dt := int64(60) // interval between updating time
search_loop:
	for {
		htoken.counter++
		htoken.rand = RandomBytes(HASHTOKEN_RAND_BYTE_LENGTH)
		goof = htoken.HashNow(box)
		if IsError(goof) {
			htoken.goof <- goof
			break search_loop
		}
		if htoken.IsCorrect() {
			htoken.goof <- NO_GOOF
			break search_loop
		}
		if htoken.counter >= limit {
			htoken.goof <- env.Goofs().MakeGoof("VALUE_NOT_FOUND",
				"Could not find valid %T after reaching given limit of %d attempts",
				htoken, limit)
			break search_loop
		}
		if htoken.stop {
			htoken.goof <- env.Goofs().MakeGoof("VALUE_NOT_FOUND",
				"Search for valid %T terminated prematurely after %d attempts",
				htoken, htoken.counter)
			break search_loop
		}
		if time.Now().Unix()-t0 > dt {
			t0 = time.Now().Unix()
			htoken.UpdateTime(env)
		}
	}
	return
}

// Round time to start of most recent ten minute interval.
func (htoken *HashToken) UpdateTime(env *Environment) {
	goof := NO_GOOF
	htoken.time, goof = NowUTC(env.Calendars()["Gregorian"])
	if IsError(goof) {
		htoken.goof <- goof
		return
	}
	clock, _ := BuildClock(
		htoken.time.Hour(),
		(htoken.time.Minute()/10)*10,
		0,
		ZeroFrac())
	htoken.time = MakeCalClock(htoken.time.Date, clock)
	return
}

// QUALIFYING TOKEN FINDER =====================================================

type HashTokenFinder struct {
	parent   *HashToken
	children []*HashToken
	goofs    []*Goof
	running  int // Keep a tally of running search workers
	*State
}

func NewHashTokenFinder() *HashTokenFinder {
	return &HashTokenFinder{
		parent:   NewHashToken(),
		children: []*HashToken{},
		goofs:    []*Goof{},
		State:    NewState(),
	}
}

func MakeHashTokenFinder(htoken *HashToken) *HashTokenFinder {
	htfinder := NewHashTokenFinder()
	htfinder.parent = htoken
	htoken.NotEmpty()
	htoken.NowComplete()
	return htfinder
}

func (htfinder *HashTokenFinder) Parent() *HashToken     { return htfinder.parent }
func (htfinder *HashTokenFinder) Children() []*HashToken { return htfinder.children }
func (htfinder *HashTokenFinder) Goofs() []*Goof         { return htfinder.goofs }
func (htfinder *HashTokenFinder) WorkersRunning() int    { return htfinder.running }

func (htfinder *HashTokenFinder) Spawn(n, limit, reqdzbits int, box *Box) *Goof {
	htfinder.NowRunning()
	if n < 0 {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Number of search processes requested, %d, cannot be < 0", n)
	}
	htfinder.children = make([]*HashToken, n)
	for i := 0; i < n; i++ {
		htfinder.children[i] = htfinder.parent.Copy()
		go htfinder.children[i].Search(limit, reqdzbits, strconv.Itoa(i), box)
		htfinder.running++
	}
	return NO_GOOF
}

func (htfinder *HashTokenFinder) Wait() (bool, *HashToken) {
	notFound := NewHashToken()
	children := htfinder.Children()
	if len(children) == 0 {
		return false, notFound
	}
	htfinder.goofs = make([]*Goof, len(children))
	for {
		time.Sleep(100 * time.Millisecond)
		for i := 0; i < len(children); i++ {
			select {
			case goof := <-children[i].Goof():
				Env.Log().Fine("Search %d ended: %v", i, goof)
				htfinder.goofs[i] = goof
				htfinder.running--
				if !IsError(goof) {
					htfinder.ShutAllDown()
					htfinder.NotRunning()
					return true, children[i]
				}
			default:
			}
		}
		if htfinder.WorkersRunning() == 0 {
			htfinder.NotRunning()
			return false, notFound
		}
	}
}

func (htfinder *HashTokenFinder) ShutAllDown() {
	children := htfinder.Children()
	for i := 0; i < len(children); i++ {
		children[i].Stop()
	}
	return
}

func (htfinder *HashTokenFinder) SumCounters() int {
	children := htfinder.Children()
	result := 0
	for i := 0; i < len(children); i++ {
		result += children[i].Counter()
	}
	return result
}

func WaitToGenerateHashToken(origin string, n, limit int, ver *Version, reqs HashTokenRequirements, box *Box) (bool, *HashToken, int, *Goof) {
	ht0, goof := MakeHashToken(origin, ver, reqs)
	if IsError(goof) {
		return false, ht0, 0, goof
	}

	finder := MakeHashTokenFinder(ht0)
	goof = finder.Spawn(4, 100000, ht0.SpecifiedZerobits(), box)
	if IsError(goof) {
		return false, ht0, 0, goof
	}

	found, ht := finder.Wait()

	count := finder.SumCounters()
	Env.Log().Fine("Search completed after %d hashes", count)
	if found {
		Env.Log().Fine("Found %q", ht)
	} else {
		Env.Log().Fine("Correct %T not found", ht)
	}
	return found, ht, count, NO_GOOF
}

func GoGenerateHashToken(origin string, n, limit int, ver *Version, reqs HashTokenRequirements, box *Box) (*HashTokenFinder, *Goof) {
	ht0, goof := MakeHashToken(origin, ver, reqs)
	if IsError(goof) {
		return NewHashTokenFinder(), goof
	}

	finder := MakeHashTokenFinder(ht0)
	goof = finder.Spawn(4, 100000, ht0.SpecifiedZerobits(), box)
	if IsError(goof) {
		return finder, goof
	}
	return finder, NO_GOOF
}
