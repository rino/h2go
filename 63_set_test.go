package h2go

import (
	"testing"
)

func TestStringSet(t *testing.T) {
	set0 := BuildStringSet("a", "b", "c")

	// Add new element
	elem := "e"
	set, added := set0.Add(elem)
	expected := StringSet([]string{"a", "b", "c", elem})
	if !added {
		t.Fatalf("Element %q should have been added to %q", elem, set)
	}
	if !set.Equals(expected) {
		t.Fatalf("Expected %q, got %q", expected, set)
	}

	// Add element that is already present
	elem = "a"
	set, added = set0.Add(elem)
	expected = StringSet([]string{"a", "b", "c"})
	if added {
		t.Fatalf("Element %q should not have been added to %q", elem, set)
	}
	if !set.Equals(expected) {
		t.Fatalf("Expected %q, got %q", expected, set)
	}

	// Copy StringSet
	set1 := set0.Copy()
	if !set1.Equals(set0) {
		t.Fatalf("Expected %q, got %q", set1, set0)
	}
}

func TestStringSetEquals(t *testing.T) {
	s1 := BuildStringSet("a", "b", "c", "d", "e")
	s2 := s1.Copy()
	if !s1.Equals(s2) {
		t.Fatalf("The set %v should equal itself", s1)
	}
	s2 = BuildStringSet("c", "d", "a", "b", "e")
	if !s1.Equals(s2) {
		t.Fatalf("The sets %v and %v should equal each other", s1, s2)
	}
}
