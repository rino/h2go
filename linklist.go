/*
	Provides sortable, doubly linked list.

     Head
	+----+    +----+    +----+    +----+
	|    |<---|    |<---|    |<---|    |
	|    |--->|    |--->|    |--->|    |
	+----+    +----+    +----+    +----+
       ^         |         |         |     ^  Rightmost position
       |         |         |         |     |  of iterator (atEnd = true)
	   +---------+---------+---------+
*/
package h2go

import ()

// LINK ========================================================================
type Link struct {
	list  *LinkList
	value Comparable
	prev  *Link
	next  *Link
	*State
}

var _ Stateful = NewLink() // embedded State

// Construct without circular references
func NewLink() *Link {
	return &Link{
		list: &LinkList{
			head: &Link{
				prev:  &Link{State: NewState()},
				next:  &Link{State: NewState()},
				State: NewState(),
			},
		},
		prev:  &Link{State: NewState()},
		next:  &Link{State: NewState()},
		State: NewState(),
	}
}

func (link *Link) Value() Comparable { return link.value }

func (link *Link) SetValue(value Comparable) *Link {
	link.value = value
	if value != nil {
		link.NotEmpty()
	}
	return link
}

func (link *Link) String() string {
	return link.value.String()
}

// LINKLIST ====================================================================
type LinkList struct {
	head *Link
	*State
}

var _ Stateful = NewLinkList() // embedded State

func NewLinkList() *LinkList {
	return &LinkList{
		head:  NewLink(),
		State: NewState(),
	}
}

func (list *LinkList) MakeLink(value Comparable) *Link {
	link := NewLink()
	link.list = list
	link.SetValue(value)
	return link
}

// Override State.IsEmpty()
func (list *LinkList) IsEmpty() bool {
	return list.head.IsEmpty()
}

func (list *LinkList) String() string {
	if list.IsEmpty() {
		return ""
	}
	iter := list.Iterator()
	iter.First()
	result := ""
	first := true
	for {
		if iter.atEnd {
			break
		}
		if !first {
			result += ", "
		} else {
			first = false
		}
		result += iter.this.String()
		iter.Inc()
	}
	return result
}

func (list *LinkList) Collect() Comparables {
	list.RLock()
	defer list.RUnlock()
	result := Comparables([]Comparable{})
	if !list.IsEmpty() {
		iter := list.Iterator()
		iter.First()
		for {
			if iter.atEnd {
				break
			}
			if !iter.this.IsEmpty() {
				result = append(result, iter.this.value)
			}
			iter.Inc()
		}
	}
	return result
}

func (link *Link) EmptyCmp(other *Link) int {
	if link.IsEmpty() {
		if other.IsEmpty() {
			return COMPARE_EQ
		} else {
			return COMPARE_NOT_EQ
		}
	}
	return COMPARE_UNKNOWN
}

func (list *LinkList) Equals(other *LinkList) bool {
	list.RLock()
	other.RLock()
	defer list.RUnlock()
	defer other.RUnlock()
	if list.IsEmpty() && other.IsEmpty() {
		return true
	}
	iter1 := list.Iterator()
	iter2 := other.Iterator()
	cmp := 0
	for {
		cmp = iter1.this.EmptyCmp(iter2.this)
		if cmp == COMPARE_NOT_EQ {
			return false
		}
		cmp = iter1.this.value.Cmp(iter2.this.value)
		if cmp != COMPARE_EQ {
			return false
		}
		iter1.Inc()
		iter2.Inc()
		if iter1.atEnd || iter2.atEnd {
			if iter1.atEnd && iter2.atEnd {
				return true
			}
			return false
		}
	}
}

func (list *LinkList) Copy() *LinkList {
	list.RLock()
	defer list.RUnlock()
	list2 := NewLinkList()
	if list.head.IsEmpty() {
		return list2
	}
	iter := list.Iterator()
	newlink := NewLink()
	prev := NewLink()
	for {
		if iter.AtEnd() {
			break
		}
		newlink = list2.MakeLink(iter.this.value)
		if iter.AtFirst() {
			list2.head = newlink
		} else {
			prev.next = newlink
		}
		newlink.prev = prev
		prev = newlink
		iter.Inc()
	}
	list2.State = list.State.Copy()
	return list2
}

// LINKLIST ITERATOR ===========================================================

type LinkListIterator struct {
	list  *LinkList
	this  *Link
	atEnd bool
}

func NewLinkListIterator() *LinkListIterator {
	return &LinkListIterator{
		list: NewLinkList(),
		this: NewLink(),
	}
}

func (list *LinkList) Iterator() *LinkListIterator {
	iter := NewLinkListIterator()
	iter.list = list
	iter.this = list.head
	iter.atEnd = list.IsEmpty()
	return iter
}

func (iter *LinkListIterator) This() *Link     { return iter.this }
func (iter *LinkListIterator) List() *LinkList { return iter.list }
func (iter *LinkListIterator) AtEnd() bool     { return iter.atEnd }

func (iter *LinkListIterator) SetAtEnd(atEnd bool) *LinkListIterator {
	iter.atEnd = atEnd
	return iter
}

func (iter *LinkListIterator) First() *LinkListIterator {
	iter.this = iter.list.head
	iter.SetAtEnd(false)
	return iter
}

func (iter *LinkListIterator) Last() *LinkListIterator {
	iter.List().RLock()
	defer iter.List().RUnlock()
	for {
		if iter.this.next.IsEmpty() {
			break
		}
		iter.this = iter.this.next
	}
	iter.SetAtEnd(false)
	return iter
}

func (iter *LinkListIterator) AtFirst() bool {
	return iter.this.prev.IsEmpty() && !iter.AtEnd()
}

func (iter *LinkListIterator) AtLast() bool {
	return iter.this.next.IsEmpty() && !iter.AtEnd()
}

func (iter *LinkListIterator) Inc() *LinkListIterator {
	iter.List().RLock()
	defer iter.List().RUnlock()
	if iter.List().IsEmpty() {
		iter.SetAtEnd(true)
	} else {
		if !iter.atEnd {
			if iter.this.next.IsEmpty() {
				iter.SetAtEnd(true)
			} else {
				iter.this = iter.this.next
				iter.SetAtEnd(false)
			}
		}
	}
	return iter
}

func (iter *LinkListIterator) Dec() *LinkListIterator {
	iter.List().RLock()
	defer iter.List().RUnlock()
	if iter.List().IsEmpty() {
		iter.SetAtEnd(true)
	} else {
		if iter.atEnd {
			iter.SetAtEnd(false)
		}
		if !iter.AtFirst() {
			iter.this = iter.this.prev
		}
	}
	return iter
}

// Remove the link from the list, but do not destroy the link.
func (iter *LinkListIterator) Delete() *LinkListIterator {
	iter.List().Lock()
	defer iter.List().Unlock()
	if iter.this.next.IsEmpty() {
		if iter.this.prev.IsEmpty() {
			// singleton
			iter.list.head.NowEmpty()
			iter.this.NowEmpty()
			iter.atEnd = false
		} else {
			// tail
			iter.this = iter.this.prev
			iter.this.next.NowEmpty()
			iter.atEnd = true
		}
	} else {
		if iter.this.prev.IsEmpty() {
			// head
			iter.this = iter.this.next
			iter.this.prev.NowEmpty()
			iter.list.head = iter.this
		} else {
			// body
			prev := iter.this.prev
			next := iter.this.next
			iter.this = iter.this.next
			prev.next = iter.this
			next.prev = iter.this
		}
	}
	return iter
}

// Insert value at current iterator position, which does not change.
func (iter *LinkListIterator) Insert(other Comparable) *LinkListIterator {
	iter.List().RLock()
	defer iter.List().RUnlock()
	link := iter.List().MakeLink(other)
	if iter.List().IsEmpty() {
		iter.list.head = link
		iter.SetAtEnd(false)
	} else {
		if iter.atEnd {
			iter.this.next = link
			link.prev = iter.this
			iter.SetAtEnd(false)
		} else {
			if !iter.AtFirst() {
				link.prev = iter.this.prev
				iter.this.prev.next = link
			} else {
				iter.list.head = link
			}
			link.next = iter.this
			iter.this.prev = link
		}
	}
	iter.this = link
	return iter
}

// Move the iterator to insert the value in sorted order as defined by its
// comparator function.  Advises in return whether insertion was accomplished.
// If other equals a link already present, nothing is inserted.
func (iter *LinkListIterator) InsertSort(other Comparable) bool {
	first := true // first loop?
	if iter.List().IsEmpty() {
		iter.Insert(other)
		return true
	}
	left := false
	cmp := 0
	if iter.atEnd {
		//cmp = iter.this.value.Cmp(other)
		cmp = other.Cmp(iter.this.value)
		if cmp == COMPARE_EQ || cmp == COMPARE_ERR {
			return false
		} else if cmp != COMPARE_GT {
			iter.Insert(other)
			return true
		} else {
			iter.Dec()
			first = false
			left = true
		}
	}
	for {
		//cmp = iter.this.value.Cmp(other)
		cmp = other.Cmp(iter.this.value)
		if cmp == COMPARE_EQ || cmp == COMPARE_ERR {
			return false
		} else if cmp == COMPARE_GT {
			if iter.AtFirst() {
				break
			}
			if first {
				first = false
				left = true
				continue
			} else {
				if left {
					iter.Dec()
					continue
				} else {
					break
				}
			}
		} else {
			iter.Inc()
			if first {
				first = false
				left = false
				continue
			} else {
				if left {
					break
				} else {
					if iter.atEnd {
						break
					} else {
						continue
					}
				}
			}
		}
	}
	iter.Insert(other)
	return true
}
