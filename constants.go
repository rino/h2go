/*
	Environment values and constants.

	Defines three important singletons: Log, Types and Goofs.
*/
package h2go

import (
	"encoding/binary"
	"math/big"
	"os"
)

// VERSION =====================================================================
// Update version here, used to check for compatibility with binary
// encoding etc.
// Use Semantic Versioning http://semver.org
const (
	VERSION_MAJOR    uint = 1
	VERSION_MINOR    uint = 0
	VERSION_REVISION uint = 0
)

var H2GO_VERSION *Version
var VERSION_SIZE int

// Version history
var VERSION_PARAMS map[int]VersionParams = map[int]VersionParams{
	VersionKey(1, 0, 0): VersionParams{
		Compressor:       "none",
		CompressionLevel: 7, // 1 lowest (fastest) 9 highest (slowest)
	},
}

// CONFIGURATION ===============================================================

var DEFAULT_ENV_CONFIG *EnvConfig
var DEFAULT_FIELDBOX_PARAMS_CONFIG *FieldBoxParamsConfig

// DEBUG =======================================================================
const (
	TIMESTAMP_FORMAT string = "2006-01-02 15:04:00.000000 MST"
)

const ( // order important
	DEBUGLEVEL_NONE = iota
	DEBUGLEVEL_BASIC
	DEBUGLEVEL_FINE
	DEBUGLEVEL_SUPERFINE
)

var DebugLevels = map[string]int{
	"NONE":      DEBUGLEVEL_NONE,
	"BASIC":     DEBUGLEVEL_BASIC,
	"FINE":      DEBUGLEVEL_FINE,
	"SUPERFINE": DEBUGLEVEL_SUPERFINE,
}

// The map is small enough to reverse manually for speed/simplicity
var DebugLevelName = map[int]string{
	DEBUGLEVEL_NONE:      "NONE",
	DEBUGLEVEL_BASIC:     "BASIC",
	DEBUGLEVEL_FINE:      "FINE",
	DEBUGLEVEL_SUPERFINE: "SUPERFINE",
}

// ENVIRONMENT =================================================================
var Env *Environment
var ENV_CREATED bool = false

var bootRegister map[int][]*Booter = make(map[int][]*Booter)

// TEST ========================================================================
const (
	TEST_MAILBOX_USER string = "test_user"
	TEST_MAILBOX_PASS string = "test_pass"
)

// CODES =======================================================================
type CODE UINT16

var CODE_BYTE_LENGTH int = 2

const (
	CODE_MAX     CODE = 65535
	CODE_HALF    CODE = 32767
	CODE_QUARTER CODE = 16383
)

// TYPES =======================================================================
var (
	UNKNOWN_TYPE_CODE   CODE
	NIL_TYPE_CODE       CODE
	FIELD_TYPE_CODE     CODE
	FIELDBOX_TYPE_CODE  CODE
	FIELDMAP_TYPE_CODE  CODE
	FIELDLIST_TYPE_CODE CODE
)

var EMPTY_REPRESENTATION Representation

// Note that the Rx functions should always use Representation.AutoAdd to return the
// target decoded object to allow ProtocolMap.<Binary/String>RxBy<Code/Name> to
// function correctly.
func InitTypes(env *Environment) []ProtocolMaker {
	return []ProtocolMaker{
		func(code CODE) Protocol {
			return Protocol{
				Name:            "UNKNOWN_TYPE",
				Nil:             interface{}(0),
				BinarySizeFixed: true,
				BinarySize:      CODE_BYTE_LENGTH,
				BinaryTx:        CodeOnlyBinaryEncoder(code),
				BinaryRx:        ConstBinaryDecoder(nil),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "NIL",
				Nil:             nil,
				BinarySizeFixed: true,
				BinarySize:      CODE_BYTE_LENGTH,
				BinaryTx:        CodeOnlyBinaryEncoder(code),
				BinaryRx:        ConstBinaryDecoder(nil),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "TRUE",
				Nil:             true,
				BinarySizeFixed: true,
				BinarySize:      CODE_BYTE_LENGTH,
				BinaryTx:        CodeOnlyBinaryEncoder(code),
				BinaryRx:        ConstBinaryDecoder(true),
				StringRx:        BooleanStringDecoder(),
				Tags:            []string{"boolean"},
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "FALSE",
				Nil:             false,
				BinarySizeFixed: true,
				BinarySize:      CODE_BYTE_LENGTH,
				BinaryTx:        CodeOnlyBinaryEncoder(code),
				BinaryRx:        ConstBinaryDecoder(false),
				StringRx:        BooleanStringDecoder(),
				Tags:            []string{"boolean"},
			}
		},
		//func(code CODE) Protocol {
		//		return Protocol{
		//	Name:            "UINT", // not the same as type UINT
		//	Nil:             uint(0),
		//	BinarySizeFixed: false,
		//	BinaryTx:        NativeNumberFieldBinaryEncoder(10),
		//	BinaryRx:        NativeNumberFieldBinaryDecoder(uint(0)),
		//	StringRx:        NativeNumberFieldStringDecoder(uint(0)),
		//}
		//},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "UINT8",
				Nil:             UINT8(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(UINT8(0)),
				StringRx:        NativeNumberFieldStringDecoder(UINT8(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "UINT16",
				Nil:             UINT16(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(UINT16(0)),
				StringRx:        NativeNumberFieldStringDecoder(UINT16(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "UINT32",
				Nil:             UINT32(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(UINT32(0)),
				StringRx:        NativeNumberFieldStringDecoder(UINT32(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "uint8",
				Nil:             uint8(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(uint8(0)),
				StringRx:        NativeNumberFieldStringDecoder(uint8(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "uint16",
				Nil:             uint16(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(uint16(0)),
				StringRx:        NativeNumberFieldStringDecoder(uint16(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "uint32",
				Nil:             uint32(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(uint32(0)),
				StringRx:        NativeNumberFieldStringDecoder(uint32(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "uint64",
				Nil:             uint64(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(uint64(0)),
				StringRx:        NativeNumberFieldStringDecoder(uint64(0)),
			}
		},
		//func(code CODE) Protocol {
		//		return Protocol{
		//	Name:            "INT",
		//	Nil:             int(0),
		//	BinarySizeFixed: false,
		//	BinaryTx:        NativeNumberFieldBinaryEncoder(20),
		//	BinaryRx:        NativeNumberFieldBinaryDecoder(int(0)),
		//	StringRx:        NativeNumberFieldStringDecoder(int(0)),
		//}
		//},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "int8",
				Nil:             int8(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(int8(0)),
				StringRx:        NativeNumberFieldStringDecoder(int8(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "int16",
				Nil:             int16(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(int16(0)),
				StringRx:        NativeNumberFieldStringDecoder(int16(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "int32",
				Nil:             int32(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(int32(0)),
				StringRx:        NativeNumberFieldStringDecoder(int32(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "int64",
				Nil:             int64(0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(int64(0)),
				StringRx:        NativeNumberFieldStringDecoder(int64(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "float32",
				Nil:             float32(0.0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(float32(0)),
				StringRx:        NativeNumberFieldStringDecoder(float32(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "float64",
				Nil:             float64(0.0),
				BinarySizeFixed: true,
				BinaryTx:        NativeNumberFieldBinaryEncoder(code),
				BinaryRx:        NativeNumberFieldBinaryDecoder(float64(0)),
				StringRx:        NativeNumberFieldStringDecoder(float64(0)),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "BIGINT",
				Nil:             (&big.Int{}).SetInt64(0),
				BinarySizeFixed: false,
				BinaryTx:        BigNumFieldBinaryEncoder(code),
				BinaryRx:        BigIntFieldBinaryDecoder(),
				StringRx:        BigIntFieldStringDecoder(),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "BIGRAT",
				Nil:             (&big.Rat{}).SetFloat64(0.0),
				BinarySizeFixed: false,
				BinaryTx:        BigNumFieldBinaryEncoder(code),
				BinaryRx:        BigRatFieldBinaryDecoder(),
				StringRx:        BigRatFieldStringDecoder(),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "BYTES",
				Nil:             []byte{},
				BinarySizeFixed: false,
				BinaryTx:        ByteSliceFieldBinaryEncoder(code),
				BinaryRx:        ByteSliceFieldBinaryDecoder(),
				StringRx:        ByteFieldStringDecoder(),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "STRING",
				Nil:             "",
				BinarySizeFixed: false,
				BinaryTx:        StringFieldBinaryEncoder(code),
				BinaryRx:        StringFieldBinaryDecoder(),
				StringRx:        IdentityStringDecoder(),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "WORKSPACE_FN",
				Nil:             "",
				BinarySizeFixed: false,
				BinaryTx:        StringFieldBinaryEncoder(code),
				BinaryRx:        StringFieldBinaryDecoder(),
				StringRx:        IdentityStringDecoder(),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "FIELD",
				Nil:             NewField(),
				BinarySizeFixed: false,
				BinaryTx:        FieldBinaryEncoder(),
				BinaryRx:        FieldBinaryDecoder(),
				StringRx:        FieldStringDecoder(),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "FIELDBOX",
				Nil:             NewFieldBox(),
				BinarySizeFixed: false,
				BinaryTx:        FieldBoxBinaryEncoder(),
				BinaryRx:        FieldBoxBinaryDecoder(),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "FIELDMAP",
				Nil:             NewFieldMap(), // update later
				BinarySizeFixed: false,
				BinaryTx:        FieldMapBinaryEncoder(),
				BinaryRx:        FieldMapBinaryDecoder(),
				StringRx:        FieldMapStringDecoder(),
			}
		},
		func(code CODE) Protocol {
			return Protocol{
				Name:            "FIELDLIST",
				Nil:             NewFieldList(), // update later
				BinarySizeFixed: false,
				BinaryTx:        FieldListBinaryEncoder(),
				BinaryRx:        FieldListBinaryDecoder(),
				StringRx:        FieldListStringDecoder(),
			}
		},
	}
}

// GOOFS =======================================================================
// Arbitrary, used to identifying goofs between server and client
var NO_GOOF *Goof

const GOOF_ID_LENGTH uint64 = 6

var NO_GOOF_CODE CODE

var InitGoofs []string = []string{
	"UNKNOWN_CODE",
	"NO_GOOF",
	"GENERIC",
	"WRAPPED",
	"INVALID_NIL",
	// Mismatches
	"OUTSIDE_ALLOWED_RANGE",
	"INT_MISMATCH",
	"DATA_MISMATCH",
	"COUNT_MISMATCH",
	"MAX_SMALLER_THAN_MIN",
	"CHECKSUM_MISMATCH",
	// Key problems
	"KEY_NOT_FOUND",
	"INVALID_KEY",
	"MISSING_KEY",
	"KEY_ALREADY_EXISTS",
	"KEY_MISMATCH",
	// Value problems
	"VALUE_NOT_FOUND",
	"MISSING_VALUE",
	"READING_VALUE",
	"MISSING_FIELD",
	"INVALID_DATA",
	// Operating System
	"GETTING_SYSTEM_INFO",
	"EXTERNAL_CODE",
	"USER_INPUT",
	// Internal
	"INITIALISATION",
	// File system
	"FILE_NOT_FOUND",
	"DIR_NOT_FOUND",
	"CREATING_FILE_OR_DIR",
	"DELETING_FILE_OR_DIR",
	"PATH_NOT_ALLOWED",
	"WALKING_FILEPATH",
	"FILE_NOT_DEFINED",
	"READING_FILE",
	"WRITING_FILE",
	"TRUNCATING_FILE",
	"SETTING_FILE_POINTER",
	"MISSING_FILE_POINTER",
	"READING_DIR",
	"CHANGING_DIR",
	"GETTING_DIR_INFO",
	"GETTING_FILE_INFO",
	"OPENING_FILE",
	"CLOSING_FILE",
	"RENAMING_FILE",
	"SYNCING_FILE",
	"FILE_ALREADY_EXISTS",
	"GETTING_CURRENT_DIRECTORY",
	// Bad parameters
	"BAD_ARGUMENTS",
	"BAD_TYPE",
	"BAD_TYPE_IN_SOURCE_CODE",
	"BAD_INDEX",
	"BAD_NAME",
	"BAD_COMMAND",
	"BAD_CONFIG",
	"NOT_IMPLEMENTED",
	"CONFLICTING_SETTINGS",
	"MISSING_SETTINGS",
	"PARAMETER_UNDEFINED",
	"UNRECOGNISED_SYMBOLS",
	"SYNTAX",
	"PARSING_TEXT",
	"PARSING_URL",
	"PARSING_IP_ADDRESS",
	"TAB_COMPLETION",
	"NOT_INITIALISED",
	"LIMIT_EXCEEDED",
	// Unexpected data size
	"ITEM_COUNT",
	"STRING_LENGTH",
	// Encode/decode
	"DECODING",
	"ENCODING",
	"UNDEFINED_DECODER",
	"UNDEFINED_ENCODER",
	"COMPRESSING",
	"UNCOMPRESSING",
	// IO
	"BUFFER_WRITE",
	"BUFFER_READ",
	"IO_COPYING",
	"IO_WRITE",
	"IO_READ",
	"END_OF_FILE",
	// Channel
	"NO_SUCH_CHANNEL",
	// Workspace
	"WORKSPACE",
	// User problems
	"NO_SUCH_USER",
	"USER_ALREADY_EXISTS",
	"INVALID_USER_INFO",
	"INCORRECT_PASSPHRASE",
	"AUTHORISATION",
	"COULD_NOT_SEND_EMAIL",
	// Crypto
	"CRYPTOGRAPHY",
	"ENCRYPTING",
	"DECRYPTING",
	"GENERATING_HASH",
	"GENERATING_CRYPTO_KEYS",
	"GENERATING_BLOCK_CIPHER",
	// Date and Time
	"COULD_NOT_FIND_CALENDAR",
	"DATE_AND_TIME",
	// Mathematical
	"INT_OVERFLOW",
	// Concurrency
	"COULD_NOT_GET_PORT_LOCK",
}

// SECURITY ====================================================================
const (
	SALT_LENGTH               int    = 16    // stored in hash
	SCRYPT_COST               int64  = 16384 // Must be power of two > 1
	SCRYPT_R                  int64  = 8
	SCRYPT_P                  int64  = 10    // r * p < 2^30
	SCRYPT_PARAMS_SIZE        int    = 4 * 8 // bytes for packed parameters
	NONCE_LENGTH              int    = 24
	GENERATED_PASS_MIN_LENGTH uint64 = 10 // characters
	GENERATED_PASS_MAX_LENGTH uint64 = 16 // characters
	// This is the hex encoding of a randomly generated 128-byte sequence
	H2GO_CIPHERTEXT string = `e748b61e0bd598bd15c89a1dc99e013f23eed86fa34325cf1c49a5bcc77bed7dddae3eab31355ae695cf5b37b985aee90eefd9fd4deb5c735ea94305beceea524a437d9eb497b4b60b2c127cf894c8bfa468502a0c2d29cd1f980090950f64eb1d10ccb889102d51bf61f8316f94fd39b554145af1e65fd4db5c18dcfc406688`
)

const (
	SECRETBOX_KEY_LENGTH int64 = 32  // encryption key, bytes
	AES_256_KEY_LENGTH   int64 = 32  // encryption key, bytes
	RSA_OAEP_KEY_LENGTH  int64 = 512 // bytes, 4096 bits
	COMMON_KEY_LENGTH    int64 = 32
)

var RSA_KEY_BITSIZE int = int(RSA_OAEP_KEY_LENGTH) * 8

var RSA_OAEP_MAX_MSG_LENGTH int = NewRSA_OAEP().MaxMessageLength()

var ZERO_BYTE_SLICE [1000]byte

const (
	PREFIX_SECRET string = "secret:"
)

// HASH ========================================================================
const (
	HASHTOKEN_RAND_BYTE_LENGTH int = 12
	HASHTOKEN_HASH_BYTE_LENGTH int = 20
)

// BINARY =======================================================================

var BYTEORDER binary.ByteOrder = binary.BigEndian

const (
	DECODE_BINARY_SIZE_ONLY        uint8 = 1
	DECODE_BINARY_VALUE_SIZE_ONLY  uint8 = 2
	DECODE_BINARY_RECORD_SIZE_ONLY uint8 = 3
)

// Switches
var ENCODE_SWITCHES_BYTE_LENGTH int

// Compression
var BinaryAlgoCompression *Enum
var InitAlgoCompression []string = []string{
	"COMPRESSION_NONE",
	"COMPRESSION_GZIP",
}

// Encryption
var BinaryAlgoEncryption *Enum
var InitAlgoEncryptionWithInfo map[string]map[string]interface{} = map[string]map[string]interface{}{
	"ENCRYPTION_NONE": map[string]interface{}{
		"key_length": int64(0),
	},
	"ENCRYPTION_SECRETBOX": map[string]interface{}{
		"key_length": SECRETBOX_KEY_LENGTH,
	},
	"ENCRYPTION_AES_256": map[string]interface{}{
		"key_length": AES_256_KEY_LENGTH,
	},
	"ENCRYPTION_RSA_OAEP": map[string]interface{}{
		"key_length": RSA_OAEP_KEY_LENGTH,
	},
}

// STRING ======================================================================
const ERROR_IN_STRING string = "<! ERROR:%s !>"
const HEXDUMP_FORMAT string = "%-51s %s"

// FILE ========================================================================
const (
	FILEFLAG_APPEND     int    = os.O_APPEND
	FILEFLAG_READ_ONLY  int    = os.O_RDONLY
	FILEFLAG_WRITE_ONLY int    = os.O_WRONLY
	FILEFLAG_READ_WRITE int    = os.O_RDWR
	FILEFLAG_CREATE     int    = os.O_CREATE
	DEFAULT_FILEFLAG    int    = FILEFLAG_READ_WRITE | FILEFLAG_CREATE | FILEFLAG_APPEND
	DEFAULT_FILEPERMS   string = "rw-r-----"
	PREFIX_TMPFILE      string = ".tmp_"
)

var DEFAULT_FILEMODE os.FileMode

// LINKLIST ====================================================================

// Comparisons
const (
	COMPARE_UNKNOWN  int = 0
	COMPARE_ONE_LT   int = -2 // belongs one element less than
	COMPARE_LT       int = -1
	COMPARE_GT       int = 1
	COMPARE_ONE_GT   int = 2 // belongs one element greater than
	COMPARE_EQ       int = 20
	COMPARE_NOT_EQ   int = 21
	COMPARE_NEW      int = 30
	COMPARE_REPLACE  int = 50
	COMPARE_CONTINUE int = 100
	COMPARE_ERR      int = 200
)

// TREE ========================================================================

// Pretty print
const (
	SPACE_CHAR int = 32
)

// WORD ITERATOR ===============================================================
const NILWORD string = "!#NILWORD"

// TEXT ========================================================================
const (
	PUNCTUATION_MARKS string = `!"#$%&\'()*+,-./:;<=>?@[\\]^_{|}~`
)

// WORKSPACE ===================================================================

const WORKSPACE_HELP_KEY string = `Bracket key: [keyword] (arg) <required> {optional}`

const WORKSPACE_HELP_WIDTH int = 79

var WORKSPACE_PREFIXES = map[string]string{
	"PROMPT":  "> ",
	"OK":      " ",
	"UNKNOWN": "?",
	"WARN":    "!",
	"ERROR":   "X",
	"ADVISE":  ">",
	"DUMP":    " ",
	"CHECK":   "#",
}

// STATE =======================================================================

const (
	STATE_EMPTY    byte = 1
	STATE_COMPLETE byte = 1 << 1
	STATE_RUNNING  byte = 1 << 2
)

// BOX =========================================================================

var EMPTY_BOX *Box

func EmptyBox() *Box {
	return MakeBox(BoxItemKinds, Env)
}

const (
	BOX_ITEM_UNKNOWN CODE = iota
	BOX_ITEM_NATIVE
	BOX_ITEM_BYTE
	BOX_ITEM_UINT8
	BOX_ITEM_UINT16
	BOX_ITEM_UINT32
	BOX_ITEM_UINT64
	BOX_ITEM_INT8
	BOX_ITEM_INT16
	BOX_ITEM_INT32
	BOX_ITEM_INT64
	BOX_ITEM_FLOAT32
	BOX_ITEM_FLOAT64
	BOX_ITEM_NUMBER
	BOX_ITEM_STRING
	BOX_ITEM_BYTE_SLICE
	BOX_ITEM_UINT8_SLICE
	BOX_ITEM_UINT16_SLICE
	BOX_ITEM_UINT32_SLICE
	BOX_ITEM_UINT64_SLICE
	BOX_ITEM_INT8_SLICE
	BOX_ITEM_INT16_SLICE
	BOX_ITEM_INT32_SLICE
	BOX_ITEM_INT64_SLICE
	BOX_ITEM_FLOAT32_SLICE
	BOX_ITEM_FLOAT64_SLICE
	BOX_ITEM_NUMBER_SLICE
	BOX_ITEM_STRING_SLICE
	BOX_ITEM_BIGINT
	BOX_ITEM_BIGRAT
	BOX_ITEM_BIGNUM
	BOX_ITEM_BINARYABLE
	BOX_ITEM_BINARY_ENCODABLE
	BOX_ITEM_BINARY_DECODABLE
	BOX_ITEM_ENCODE
	BOX_ITEM_FIELD
	BOX_ITEM_FIELD_VALUE
	BOX_ITEM_FIELDLIST
	BOX_ITEM_FIELDMAP
	BOX_ITEM_FIELDBOX
	BOX_ITEM_FIELDBOX_PARAMS
	BOX_ITEM_WORKSPACE
	BOX_ITEM_WORKSPACE_ACTION
	BOX_ITEM_ENTITY_ACTIVITY
	//
	BOX_ITEM_RESULT
)

var BoxItemKinds *BoxItemIndex = BuildBoxItemIndex(map[CODE]*LabelledItem{
	BOX_ITEM_UNKNOWN:          MakeLabelledItem("h2go.BOX_ITEM_UNKNOWN", interface{}(nil)),
	BOX_ITEM_NATIVE:           MakeLabelledItem("h2go.BOX_ITEM_NATIVE", interface{}(nil)),
	BOX_ITEM_BYTE:             MakeLabelledItem("h2go.BOX_ITEM_BYTE", byte(0)),
	BOX_ITEM_UINT8:            MakeLabelledItem("h2go.BOX_ITEM_UINT8", uint8(0)),
	BOX_ITEM_UINT16:           MakeLabelledItem("h2go.BOX_ITEM_UINT16", uint16(0)),
	BOX_ITEM_UINT32:           MakeLabelledItem("h2go.BOX_ITEM_UINT32", uint32(0)),
	BOX_ITEM_UINT64:           MakeLabelledItem("h2go.BOX_ITEM_UINT64", uint64(0)),
	BOX_ITEM_INT8:             MakeLabelledItem("h2go.BOX_ITEM_INT8", int8(0)),
	BOX_ITEM_INT16:            MakeLabelledItem("h2go.BOX_ITEM_INT16", int16(0)),
	BOX_ITEM_INT32:            MakeLabelledItem("h2go.BOX_ITEM_INT32", int32(0)),
	BOX_ITEM_INT64:            MakeLabelledItem("h2go.BOX_ITEM_INT64", int64(0)),
	BOX_ITEM_FLOAT32:          MakeLabelledItem("h2go.BOX_ITEM_FLOAT32", float32(0.0)),
	BOX_ITEM_FLOAT64:          MakeLabelledItem("h2go.BOX_ITEM_FLOAT64", float64(0.0)),
	BOX_ITEM_NUMBER:           MakeLabelledItem("h2go.BOX_ITEM_NUMBER", interface{}(nil)),
	BOX_ITEM_STRING:           MakeLabelledItem("h2go.BOX_ITEM_STRING", ""),
	BOX_ITEM_BYTE_SLICE:       MakeLabelledItem("h2go.BOX_ITEM_BYTES", []byte{}),
	BOX_ITEM_UINT8_SLICE:      MakeLabelledItem("h2go.BOX_ITEM_UINT8_SLICE", []uint8{}),
	BOX_ITEM_UINT16_SLICE:     MakeLabelledItem("h2go.BOX_ITEM_UINT16_SLICE", []uint16{}),
	BOX_ITEM_UINT32_SLICE:     MakeLabelledItem("h2go.BOX_ITEM_UINT32_SLICE", []uint32{}),
	BOX_ITEM_UINT64_SLICE:     MakeLabelledItem("h2go.BOX_ITEM_UINT64_SLICE", []uint64{}),
	BOX_ITEM_INT8_SLICE:       MakeLabelledItem("h2go.BOX_ITEM_INT8_SLICE", []int8{}),
	BOX_ITEM_INT16_SLICE:      MakeLabelledItem("h2go.BOX_ITEM_INT16_SLICE", []int16{}),
	BOX_ITEM_INT32_SLICE:      MakeLabelledItem("h2go.BOX_ITEM_INT32_SLICE", []int32{}),
	BOX_ITEM_INT64_SLICE:      MakeLabelledItem("h2go.BOX_ITEM_INT64_SLICE", []int64{}),
	BOX_ITEM_FLOAT32_SLICE:    MakeLabelledItem("h2go.BOX_ITEM_FLOAT32_SLICE", []float32{}),
	BOX_ITEM_FLOAT64_SLICE:    MakeLabelledItem("h2go.BOX_ITEM_FLOAT64_SLICE", []float64{}),
	BOX_ITEM_NUMBER_SLICE:     MakeLabelledItem("h2go.BOX_ITEM_NUMBER_SLICE", interface{}(nil)),
	BOX_ITEM_STRING_SLICE:     MakeLabelledItem("h2go.BOX_ITEM_STRING_SLICE", []string{}),
	BOX_ITEM_BIGINT:           MakeLabelledItem("h2go.BOX_ITEM_BIGINT", big.NewInt(0)),
	BOX_ITEM_BIGRAT:           MakeLabelledItem("h2go.BOX_ITEM_BIGRAT", ZeroFrac()),
	BOX_ITEM_BIGNUM:           MakeLabelledItem("h2go.BOX_ITEM_BIGNUM", interface{}(nil)),
	BOX_ITEM_BINARYABLE:       MakeLabelledItem("h2go.BOX_ITEM_BINARYABLE", interface{}(nil)),
	BOX_ITEM_BINARY_ENCODABLE: MakeLabelledItem("h2go.BOX_ITEM_BINARY_ENCODABLE", interface{}(nil)),
	BOX_ITEM_BINARY_DECODABLE: MakeLabelledItem("h2go.BOX_ITEM_BINARY_DECODABLE", interface{}(nil)),
	BOX_ITEM_ENCODE:           MakeLabelledItem("h2go.BOX_ITEM_ENCODE", interface{}(nil)),
	BOX_ITEM_FIELD:            MakeLabelledItem("h2go.BOX_ITEM_FIELD", NewField()),
	BOX_ITEM_FIELD_VALUE:      MakeLabelledItem("h2go.BOX_ITEM_FIELD_VALUE", interface{}(nil)),
	BOX_ITEM_FIELDLIST:        MakeLabelledItem("h2go.BOX_ITEM_FIELDLIST", NewFieldList()),
	BOX_ITEM_FIELDMAP:         MakeLabelledItem("h2go.BOX_ITEM_FIELDMAP", NewFieldMap()),
	BOX_ITEM_FIELDBOX:         MakeLabelledItem("h2go.BOX_ITEM_FIELDBOX", NewFieldBox()),
	BOX_ITEM_FIELDBOX_PARAMS:  MakeLabelledItem("h2go.BOX_ITEM_FIELDBOX_PARAMS", NewFieldBoxParams()),
	BOX_ITEM_WORKSPACE:        MakeLabelledItem("h2go.BOX_ITEM_WORKSPACE", NewWorkspace()),
	BOX_ITEM_WORKSPACE_ACTION: MakeLabelledItem("h2go.BOX_ITEM_WORKSPACE_ACTION", NewWorkspaceAction()),
	BOX_ITEM_ENTITY_ACTIVITY:  MakeLabelledItem("h2go.BOX_ITEM_ENTITY_ACTIVITY", NewEntityActivity()),
	//
	BOX_ITEM_RESULT: MakeLabelledItem("h2go.BOX_ITEM_RESULT", interface{}(nil)),
})

// TIME ========================================================================
const (
	// Clock
	SecondsInMinute int = 60
	MinutesInHour   int = 60
	HoursInDay      int = 24
	// Intervals
	SecInMinute int = 60
	SecInHour   int = 60 * SecInMinute
	SecInDay    int = 24 * SecInHour
	SecInWeek   int = 7 * SecInDay
	MillisInSec int = 1000
	MicrosInSec int = 10 * MillisInSec
	NanosInSec  int = 10 * MicrosInSec
	PicosInSec  int = 10 * NanosInSec
	// TimeNum
	SecondsPerDay int64 = 60 * 60 * 60 * 24
	NanosPerSec   int64 = 1000000000
	MillisPerSec  int64 = 1000
	NanosPerMilli int64 = 1000000
	GoTimeOffset  int64 = (1969*365 + 1969/4 - 1969/100 + 1969/400) * SecondsPerDay
)

// Clock
var TIME_SEC_IN_NANOS_AS_BIGRAT *big.Rat

var Midday *Clock
var Midnight *Clock

// Month
const (
	TIME_END_OF_MONTH_TOKEN string = "!#EOM"
)

// Day types for DayIncrementor
const (
	_ = iota // reserve 0 for nil equivalent
	TIME_WEEKDAY
	TIME_WORKDAY
	TIME_DAY_OF_WEEK
	TIME_ORDINARY_DAY
	TIME_DAY_OF_MONTH_FROM_START
	TIME_DAY_OF_MONTH_FROM_END
)

// Parser
const TIMEFIELD_UNDEFINED int = -1

// ParserTokenType
const (
	TIME_PARSER_TOKEN_TYPE_NONE    int = iota // 0
	TIME_PARSER_TOKEN_TYPE_WORD               // 1
	TIME_PARSER_TOKEN_TYPE_NUMBER             // 2
	TIME_PARSER_TOKEN_TYPE_GAP                // 3
	TIME_PARSER_TOKEN_TYPE_DIVIDER            // 4
)

// LexerCharType
const (
	TIME_LEXER_CHAR_TYPE_NONE    int = iota // 0
	TIME_LEXER_CHAR_TYPE_LETTER             // 1
	TIME_LEXER_CHAR_TYPE_DIGIT              // 2
	TIME_LEXER_CHAR_TYPE_SPACE              // 3
	TIME_LEXER_CHAR_TYPE_DIVIDER            // 4
)

// Schedule
const (
	TIME_NIL_REPEAT = iota
	TIME_REPEAT_BY_YEARS
	TIME_REPEAT_BY_EXPLICIT_MONTHS
	TIME_REPEAT_BY_REGULAR_MONTHS
	TIME_REPEAT_BY_DAYS
)

var CalendarRuleTypes map[int]string = map[int]string{
	0: "TIME_NIL_REPEAT",
	1: "TIME_REPEAT_BY_YEARS",
	2: "TIME_REPEAT_BY_EXPLICIT_MONTHS",
	3: "TIME_REPEAT_BY_REGULAR_MONTHS",
	4: "TIME_REPEAT_BY_DAYS",
}

// Zoneinfo

var malformedTimeZoneInfo *Goof

// ENTITY ======================================================================
const (
	PREFIX_LOG          string = "log:"
	STATE_BLOCKED       string = "blocked"
	STATE_LOGGED_IN     string = "logged_in"
	STATE_QUALIFIED     string = "qualified"
	STATE_LAST_ACTIVITY string = "last_activity"
	USER_EMAIL          string = "email"
	USER_PASSHASH       string = "passhash"
	USER_QUALTOKEN      string = "qualtoken"
	USER_REGTOKEN       string = "regtoken"
	USER_UPDATE_PASS    string = "update_pass"
	USER_PASS_TIME      string = "passtime"
	USER_PERMISSIONS    string = "perms"
	USER_GROUPS         string = "groups"
)

const MAX_LENGTH_ACTIVITY_LOG int = 1000
