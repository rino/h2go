package h2go

import (
	"fmt"
	"testing"
)

func TestDistributionFunctions(t *testing.T) {
	Env.Log().MinorSection("Generate and normalise a normal distribution")
	normalDist := MakeNormDist(10.0, 2.0)
	n := 50
	x0 := float64(0.0)
	x1 := float64(20.0)
	dx, x, y, goof := GenerateDistFunc(normalDist, x0, x1, n)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem generating normal distribution")
	}
	Env.Log().Basic("n = %d x0 = %f x1 = %f dx = %f", n, x0, x1, dx)
	Env.Log().Basic("x[0]-0.5*dx = %f x[n-1]+0.5*dx = %f", x[0]-0.5*dx, x[n-1]+0.5*dx)
	lines := []string{}
	for i, xc := range x {
		lines = append(lines, fmt.Sprintf("%03d (%8.4f,%8.4f)", i, xc, y[i]))
	}
	Env.Log().DumpQuietly(lines)
	area, max, ynorm, cumsum, goof := NormaliseDistFunc(x, y)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem normalising normal distribution")
	}
	Env.Log().Basic("old area = %f new max = %f", area, max)
	lines = []string{}
	for i, xc := range x {
		lines = append(lines,
			fmt.Sprintf("%03d (%8.4f,%8.4f,%8.4f)", i, xc, ynorm[i], cumsum[i]))
	}
	Env.Log().DumpQuietly(lines)
}
