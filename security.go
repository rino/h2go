/*
	Cryptographic hashing and encryption.
	Secretbox (with a fixed 32 byte key length) is used for encryption.
	ScryptParams is used for passphrase hashing and proof of work.
*/
package h2go

import (
	"bufio"
	"bytes"
	"code.google.com/p/gopass" // Get passphrase from command line
	"crypto/aes"
	"crypto/cipher"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"golang.org/x/crypto/nacl/secretbox"
	"golang.org/x/crypto/scrypt"
	"hash/crc32"
	"math/big"
	"net/url"
	"os"
	"strconv"
	"strings"
)

// SCRYPT PARAMETERS ===========================================================

// ScryptParams satisfies the BinaryEncodable interface.
type ScryptParams struct {
	cost   int64
	r      int64
	p      int64
	length int64
	*State
}

var _ Binaryable = NewScryptParams()
var _ Equible = NewScryptParams()
var _ Stringable = NewScryptParams()
var _ Stateful = NewScryptParams() // embedded State

func NewScryptParams() *ScryptParams {
	return &ScryptParams{
		State: NewState(),
	}
}

func MakeScryptParams(cost, r, p, length int64) *ScryptParams {
	params := NewScryptParams()
	params.cost = cost
	params.r = r
	params.p = p
	params.length = length
	params.NotEmpty()
	return params
}

func MakeDefaultScryptParams(length int64) *ScryptParams {
	return MakeScryptParams(
		SCRYPT_COST,
		SCRYPT_R,
		SCRYPT_P,
		length)
}

func (params *ScryptParams) Length() int64 { return params.length }

func (params *ScryptParams) Copy() *ScryptParams {
	params2 := MakeScryptParams(
		params.cost,
		params.r,
		params.p,
		params.length,
	)
	params2.State = params.State.Copy()
	return params2
}

func (params *ScryptParams) Equals(other interface{}) bool {
	params2, isa := other.(*ScryptParams)
	if !isa {
		params3, isa := other.(ScryptParams)
		if isa {
			params2 = &params3
		} else {
			return false
		}
	}
	return params.cost == params2.cost &&
		params.r == params2.r &&
		params.p == params2.p &&
		params.length == params2.length
}

func (params *ScryptParams) String() string {
	return strconv.FormatInt(params.cost, 10) + "," +
		strconv.FormatInt(params.r, 10) + "," +
		strconv.FormatInt(params.p, 10)
}

//  +-------+-------+-------+-------+
//  |       |       |       |       |
//  | cost  |   r   |   p   | length|
//  | int64 | int64 | int64 | int64 |
//  +-------+-------+-------+-------+
//  \______________________________/
//                  |
//         SCRYPT_PARAMS_SIZE
//
func (params *ScryptParams) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(params)
	byts, goof := bfr.Write(int64(params.cost), box)
	out.AppendEncodedAs("scrypt cost (int64)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding ScryptParams params.cost %#v", params)
	}
	byts, goof = bfr.Write(int64(params.r), box)
	out.AppendEncodedAs("scrypt r (int64)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding ScryptParams params.r %#v", params)
	}
	byts, goof = bfr.Write(int64(params.p), box)
	out.AppendEncodedAs("scrypt p (int64)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding ScryptParams params.p %#v", params)
	}
	byts, goof = bfr.Write(int64(params.length), box)
	out.AppendEncodedAs("scrypt length (int64)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding ScryptParams params.length %#v", params)
	}
	return out, NO_GOOF
}

func (params *ScryptParams) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	byts, goof := bfr.Read(&params.cost, box)
	out.AppendEncoded(byts)
	out.AppendEncodedAs("scrypt cost (int64)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding ScryptParams params.cost")
	}
	byts, goof = bfr.Read(&params.r, box)
	out.AppendEncodedAs("scrypt r (int64)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding ScryptParams params.r")
	}
	byts, goof = bfr.Read(&params.p, box)
	out.AppendEncodedAs("scrypt p (int64)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding ScryptParams params.p")
	}
	byts, goof = bfr.Read(&params.length, box)
	out.AppendEncodedAs("scrypt length (int64)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding ScryptParams params.length")
	}
	return out, NO_GOOF
}

func (params *ScryptParams) EncodeString(box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AppendEncoded(params.String())
	return out, NO_GOOF
}

func (params *ScryptParams) DecodeString(txt string, box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	return out, NO_GOOF
}

func (params *ScryptParams) Hash(in, salt []byte, box *Box) ([]byte, *Goof) {
	env := box.EnvOrDefault(Env)
	out, err := scrypt.Key(
		in,
		salt,
		int(params.cost),
		int(params.r),
		int(params.p),
		int(params.length))
	goof := NO_GOOF
	if IsError(err) {
		goof = env.Goofs().WrapGoof("GENERATING_HASH", err,
			"While generating scrypt hash from given %d bytes % x",
			len(in), in)
	}
	return out, goof
}

// PASSPHRASE HASH =============================================================

// Store the salt with the hash.
type PassHash struct {
	params  *ScryptParams
	salt    []byte
	key     []byte // the hashed result
	dblhash []byte // stored
	*State
}

var _ Binaryable = NewPassHash()
var _ Stateful = NewPassHash() // embedded State

func NewPassHash() *PassHash {
	return &PassHash{
		params:  NewScryptParams(),
		salt:    []byte{},
		key:     []byte{},
		dblhash: []byte{},
		State:   NewState(),
	}
}

func MakePassHash(params *ScryptParams, salt []byte) *PassHash {
	phash := NewPassHash()
	phash.params = params
	phash.NotEmpty()
	phash.salt = salt
	return phash
}

func BuildDefaultPassHash(pass string, length int64, box *Box) (*PassHash, *Goof) {
	return BuildPassHash(
		pass,
		MakeDefaultScryptParams(length),
		RandomBytes(SALT_LENGTH),
		box)
}

func BuildPassHash(pass string, params *ScryptParams, salt []byte, box *Box) (*PassHash, *Goof) {
	phash := MakePassHash(params, salt)
	goof := phash.Generate(pass, box)
	if !IsError(goof) {
		phash.NowComplete()
	}
	return phash, goof
}

// Getters
func (phash *PassHash) Params() *ScryptParams { return phash.params }
func (phash *PassHash) Salt() []byte          { return phash.salt }
func (phash *PassHash) Key() []byte           { return phash.key }
func (phash *PassHash) DblHash() []byte       { return phash.dblhash }

func (phash *PassHash) KeyAsHexString() string {
	return hex.EncodeToString(phash.Key())
}

func (phash *PassHash) DblHashAsHexString() string {
	return hex.EncodeToString(phash.DblHash())
}

func (phash *PassHash) Copy() *PassHash {
	phash2 := NewPassHash()
	phash2.params = phash.params.Copy()
	phash2.salt = make([]byte, len(phash.salt))
	copy(phash2.salt, phash.salt)
	phash2.key = make([]byte, len(phash.key))
	copy(phash2.key, phash.key)
	phash2.dblhash = make([]byte, len(phash.dblhash))
	copy(phash2.dblhash, phash.dblhash)
	phash2.State = phash.State.Copy()
	return phash2
}

func (phash *PassHash) Generate(pass string, box *Box) *Goof {
	goof := NO_GOOF
	phash.key, goof = phash.Params().Hash([]byte(pass), phash.salt, box)
	if IsError(goof) {
		return goof
	}
	phash.dblhash, goof = phash.Params().Hash(phash.key, phash.salt, box)
	return goof
}

//  +--------+-------------------+
//  |dblhash |  dblhash (hash)   |
//  |  size  |                   |
//  | UINT32 |                   |
//  +--------+-------------------+
func (phash *PassHash) EncodeDblHashBinary(bfr *Buffer, box *Box) *Goof {
	_, goof := bfr.Write(AsUINT32(len(phash.DblHash())), box)
	if IsError(goof) {
		return Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding PassHash double hash key size %v", len(phash.Key()))
	}
	_, goof = bfr.Write(phash.DblHash(), box)
	if IsError(goof) {
		return Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding PassHash double hash %v", phash.DblHash())
	}
	return NO_GOOF
}

func (phash *PassHash) DecodeDblHashBinary(bfr *Buffer, box *Box) ([]byte, *Goof) {
	sz := UINT32(0)
	byts, goof := bfr.Read(&sz, box)
	if IsError(goof) {
		return byts, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding PassHash double hash size")
	}
	phash.dblhash = make([]byte, int(sz))
	byts, goof = bfr.Read(&phash.dblhash, box)
	if IsError(goof) {
		return byts, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding PassHash double hash")
	}
	return byts, NO_GOOF
}

//  +------------------+--------+-----------------+--------+-------------------+
//  |      scrypt      |  salt  |      salt       |dblhash |  dblhash (hash)   |
//  |      params      |  size  |                 |  size  |                   |
//  |SCRYPT_PARAMS_SIZE| UINT32 |   SALT_LENGTH   | UINT32 |                   |
//  +------------------+--------+-----------------+--------+-------------------+
func (phash *PassHash) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out0, goof := phash.Params().EncodeBinary(bfr, box)
	if IsError(goof) {
		return out0, goof
	}
	out, _ := out0.(*BinaryRepresentation)
	byts, goof := bfr.Write(AsUINT32(len(phash.Salt())), box)
	out.AppendEncodedAs("phash salt size (UINT32)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding PassHash salt size %v", len(phash.Salt()))
	}
	byts, goof = bfr.Write(phash.Salt(), box)
	out.AppendEncodedAs("phash salt", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding PassHash salt %v", phash.Salt())
	}
	byts, goof = bfr.Write(AsUINT32(len(phash.DblHash())), box)
	out.AppendEncodedAs("phash dblhash size (UINT32)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding PassHash double hash key size %v", len(phash.Key()))
	}
	byts, goof = bfr.Write(phash.DblHash(), box)
	out.AppendEncodedAs("phash dblhash", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("ENCODING", goof,
			"While encoding PassHash double hash %v", phash.DblHash())
	}
	return out, NO_GOOF
}

func (phash *PassHash) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out0, goof := phash.Params().DecodeBinary(bfr, box)
	if IsError(goof) {
		return out0, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding PassHash params")
	}
	out, _ := out0.(*BinaryRepresentation)
	// salt
	sz := UINT32(0)
	byts, goof := bfr.Read(&sz, box)
	out.AppendEncodedAs("phash salt size (UINT32)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding PassHash salt size")
	}
	phash.salt = make([]byte, int(sz))
	byts, goof = bfr.Read(&phash.salt, box)
	out.AppendEncodedAs("phash salt", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding PassHash salt")
	}
	// dblhash (hash)
	byts, goof = bfr.Read(&sz, box)
	out.AppendEncodedAs("phash dblhash size (UINT32)", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding PassHash double hash size")
	}
	phash.dblhash = make([]byte, int(sz))
	byts, goof = bfr.Read(&phash.dblhash, box)
	out.AppendEncodedAs("phash dblhash", byts)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding PassHash double hash")
	}
	return out, NO_GOOF
}

func (phash *PassHash) HexStringEncode(order ByteOrder) string {
	return HexStringEncode(phash, order)
}

func (phash *PassHash) HexStringDecode(str string, order ByteOrder) (Representation, *Goof) {
	return HexStringDecode(str, phash, order)
}

func (phash *PassHash) MatchesString(pass string, box *Box) (bool, *Goof) {
	other, goof := BuildPassHash(pass, phash.Params(), phash.Salt(), box)
	if IsError(goof) {
		return false, goof
	}
	return bytes.Equal(other.DblHash(), phash.DblHash()), NO_GOOF
}

func (phash *PassHash) MatchesBytes(other []byte, order ByteOrder) (bool, *Goof) {
	bfr := MakeByteBuffer(order)
	_, goof := phash.EncodeBinary(bfr, EMPTY_BOX)
	if IsError(goof) {
		return false, goof
	}
	byts, _ := bfr.Bytes()
	return bytes.Equal(other, byts), NO_GOOF
}

// Returns a Goof to send to the client, and a Goof for the server.
func CheckPassHash(passhash string, box *Box) (*Goof, *Goof) {
	env := EnvOrDefault(box)
	goof2send := env.Goofs().MakeGoof("BAD_ARGUMENTS",
		"Problem with passhash")
	// Pass hash (double hash)
	phashbyts, err := hex.DecodeString(passhash)
	if IsError(err) {
		return goof2send, env.Goofs().WrapGoof("BAD_ARGUMENTS", err,
			"Problem parsing passhash %q", passhash)
	}
	if len(phashbyts) != int(COMMON_KEY_LENGTH) {
		return goof2send, env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Passhash hex string %q should be %d characters "+
				"long, got %d characters",
			passhash, 2*COMMON_KEY_LENGTH, len(passhash))
	}
	return NO_GOOF, NO_GOOF
}

// ENCRYPTION SCHEME ===========================================================

type EncryptionScheme interface {
	Name() string
	KeyLength() int
	Level() int
	IsStream() bool
	Copy() EncryptionScheme
	Encrypt([]byte) ([]byte, *Goof)
	Decrypt([]byte) ([]byte, *Goof)
	BytesEncrypted() int
	BytesDecrypted() int
	ResetCipher() // for stream ciphers
	SetRandomKeys() ([]byte, *Goof)
	KeysToString() (string, *Goof)
	KeysFromString(string) *Goof
	Equible
	Stateful
}

// ENCRYPTION SCHEME FACTORY ===================================================

func NewEncryptionScheme(typ string) (EncryptionScheme, *Goof) {
	if !BinaryAlgoEncryption.Exists(typ) {
		return NoEncryption(), Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"%q is not a recognised binary encryption algorithm",
			typ)
	}
	switch typ {
	case "ENCRYPTION_SECRETBOX":
		return NewSecret(), NO_GOOF
	case "ENCRYPTION_AES_256":
		return NewAES_256(), NO_GOOF
	case "ENCRYPTION_RSA_OAEP":
		return NewRSA_OAEP(), NO_GOOF
	case "ENCRYPTION_NONE":
		return NoEncryption(), NO_GOOF
	default:
		return NoEncryption(), Env.Goofs().MakeGoof("NOT_IMPLEMENTED",
			"New %q EncryptionScheme must be instantiated manually",
			typ)
	}
}

// IDENTITY ENCRYPTION SCHEME ==================================================

// Expressly for the purpose of avoiding nil initialisation for an
// EncryptionScheme.
type IdentityEncryptionScheme struct {
	in  int
	out int
	*State
}

var _ EncryptionScheme = NewIdentityEncryptionScheme()

func NewIdentityEncryptionScheme() *IdentityEncryptionScheme {
	ies := &IdentityEncryptionScheme{
		State: NewState(),
	}
	ies.NotEmpty()
	ies.NowComplete()
	return ies
}

func NoEncryption() *IdentityEncryptionScheme {
	return NewIdentityEncryptionScheme()
}

func (ies *IdentityEncryptionScheme) Name() string   { return "ENCRYPTION_NONE" }
func (ies *IdentityEncryptionScheme) KeyLength() int { return 0 }
func (ies *IdentityEncryptionScheme) Level() int     { return 0 }
func (ies *IdentityEncryptionScheme) IsStream() bool { return false }

func (ies *IdentityEncryptionScheme) BytesEncrypted() int { return ies.in }
func (ies *IdentityEncryptionScheme) BytesDecrypted() int { return ies.out }

func (ies *IdentityEncryptionScheme) Copy() EncryptionScheme {
	ies2 := NewIdentityEncryptionScheme()
	ies2.State = ies.State.Copy()
	return ies2
}

func (ies *IdentityEncryptionScheme) Encrypt(src []byte) ([]byte, *Goof) {
	dst := make([]byte, len(src))
	copy(dst, src)
	ies.in += len(src)
	return dst, NO_GOOF
}

func (ies *IdentityEncryptionScheme) Decrypt(src []byte) ([]byte, *Goof) {
	dst := make([]byte, len(src))
	copy(dst, src)
	ies.out += len(src)
	return dst, NO_GOOF
}

func (ies *IdentityEncryptionScheme) ResetCipher() {}
func (ies *IdentityEncryptionScheme) SetRandomKeys() ([]byte, *Goof) {
	return []byte{}, NO_GOOF
}

func (ies *IdentityEncryptionScheme) KeysToString() (string, *Goof) {
	return "", NO_GOOF
}

func (ies *IdentityEncryptionScheme) KeysFromString(txt string) *Goof {
	return NO_GOOF
}

func (ies *IdentityEncryptionScheme) Equals(other interface{}) bool {
	switch other.(type) {
	case IdentityEncryptionScheme, *IdentityEncryptionScheme:
		return true
	default:
		return false
	}
}

// SECRET BOX ==================================================================

type Secret struct {
	// Meta
	key   [SECRETBOX_KEY_LENGTH]byte
	nonce [NONCE_LENGTH]byte // to encrypt
	// Box containing encrypted secret
	box []byte // the secretbox
	in  int
	out int
	*State
}

//var _ Binaryable = NewSecret()
var _ EncryptionScheme = NewSecret()

func NewSecret() *Secret {
	sec := &Secret{
		key:   [SECRETBOX_KEY_LENGTH]byte{},
		nonce: [NONCE_LENGTH]byte{},
		box:   []byte{},
		State: NewState(),
	}
	copy(sec.nonce[:], RandomBytes(NONCE_LENGTH))
	sec.NotEmpty()
	return sec
}

func MakeSecret(key []byte) (*Secret, *Goof) {
	sec := NewSecret()
	goof := CheckKeyLength("ENCRYPTION_SECRETBOX", len(key))
	if IsError(goof) {
		return sec, goof
	}
	copy(sec.key[:], key)
	sec.NowComplete()
	return sec, NO_GOOF
}

func BuildSecret(passphrase string, box *Box) (*Secret, *PassHash, *Goof) {
	sec := NewSecret()
	phash, goof := BuildDefaultPassHash(passphrase, SECRETBOX_KEY_LENGTH, box)
	if IsError(goof) {
		return sec, phash, Env.Goofs().WrapGoof("CRYPTOGRAPHY", goof,
			"While generating passhash for %T", sec)
	}
	copy(sec.key[:], phash.Key())
	sec.NowComplete()
	return sec, phash, NO_GOOF
}

// Getters
func (sec *Secret) Nonce() [NONCE_LENGTH]byte       { return sec.nonce }
func (sec *Secret) Key() [SECRETBOX_KEY_LENGTH]byte { return sec.key }
func (sec *Secret) Box() []byte                     { return sec.box }

func (sec *Secret) Name() string   { return "ENCRYPTION_SECRETBOX" }
func (sec *Secret) KeyLength() int { return int(SECRETBOX_KEY_LENGTH) }
func (sec *Secret) Level() int     { return 0 }
func (sec *Secret) IsStream() bool { return false }

func (sec *Secret) BytesEncrypted() int { return sec.in }
func (sec *Secret) BytesDecrypted() int { return sec.out }

func (sec *Secret) Copy() EncryptionScheme {
	sec2 := NewSecret()
	copy(sec2.key[:], sec.key[:])
	copy(sec2.nonce[:], sec.nonce[:])
	sec2.box = make([]byte, len(sec.box))
	copy(sec2.box, sec.box)
	sec2.State = sec.State.Copy()
	return sec2
}

func (sec *Secret) Encrypt(secret []byte) ([]byte, *Goof) {
	sec.box = secretbox.Seal([]byte{}, secret, &sec.nonce, &sec.key)
	sec.in = len(secret)
	return sec.box, NO_GOOF
}

func (sec *Secret) Decrypt(box []byte) ([]byte, *Goof) {
	sec.box = box
	sec.out = 0
	opened, ok := secretbox.Open([]byte{}, box, &sec.nonce, &sec.key)
	if !ok {
		return opened, Env.Goofs().MakeGoof("DECRYPTING",
			"There was an unspecified problem opening the secret box, "+
				"probably an incorrect passphrase")
	}
	sec.out = len(box)
	return opened, NO_GOOF
}

func (sec *Secret) ResetCipher() {}

func (sec *Secret) SetRandomKeys() ([]byte, *Goof) {
	copy(sec.key[:], RandomBytes(sec.KeyLength()))
	sec.NowComplete()
	return sec.key[:], NO_GOOF
}

func (sec *Secret) KeysToString() (string, *Goof) {
	return hex.EncodeToString(sec.nonce[:]) + "," +
		hex.EncodeToString(sec.key[:]), NO_GOOF
}

func (sec *Secret) KeysFromString(txt string) *Goof {
	parts := strings.Split(txt, ",")
	if len(parts) != 2 {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Require 2 parameters in text encoding of %q "+
				"key parameters %q, got %d",
			sec.Name(), txt, len(parts))
	}

	byts, err := hex.DecodeString(parts[0])
	if IsError(err) {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding hex string %q representing the %q nonce",
			parts[0], sec.Name())
	}
	if len(byts) != NONCE_LENGTH {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding hex string %q representing the %q nonce "+
				"expected byte length %d, got %d",
			parts[0], sec.Name(), NONCE_LENGTH, len(byts))
	}
	copy(sec.nonce[:], byts)

	byts, err = hex.DecodeString(parts[1])
	if IsError(err) {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding hex string %q representing the %q key",
			parts[1], sec.Name())
	}
	if len(byts) != int(SECRETBOX_KEY_LENGTH) {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding hex string %q representing the %q key "+
				"expected byte length %d, got %d",
			parts[1], sec.Name(), SECRETBOX_KEY_LENGTH, len(byts))
	}
	copy(sec.key[:], byts)
	sec.NowComplete()
	return NO_GOOF
}

func (sec *Secret) Equals(other interface{}) bool {
	sec2 := NewSecret()
	switch v := other.(type) {
	case Secret:
		sec2 = &v
	case *Secret:
		sec2 = v
	default:
		return false
	}
	for i, byt := range sec.nonce[:] {
		if sec2.nonce[i] != byt {
			return false
		}
	}
	for i, byt := range sec.key[:] {
		if sec2.key[i] != byt {
			return false
		}
	}
	return true
}

// IDENTITY CIPHER BLOCK =======================================================

// Expressly for the purpose of avoiding nil initialisation for a cipher.Block.
type IdentityCipherBlock struct{}

func NewIdentityCipherBlock() cipher.Block {
	return &IdentityCipherBlock{}
}

func (icb *IdentityCipherBlock) BlockSize() int {
	return 1
}

func (icb *IdentityCipherBlock) Encrypt(dst, src []byte) {
	copy(dst, src)
	return
}

func (icb *IdentityCipherBlock) Decrypt(dst, src []byte) {
	copy(dst, src)
	return
}

// IDENTITY CIPHER STREAM ======================================================

// Expressly for the purpose of avoiding nil initialisation for a cipher.Stream.
type IdentityCipherStream struct{}

func NewIdentityCipherStream() cipher.Stream {
	return &IdentityCipherStream{}
}

func (ics *IdentityCipherStream) XORKeyStream(dst, src []byte) {
	copy(dst, src)
	return
}

// AES =========================================================================

type AES_256 struct {
	key       []byte
	block     cipher.Block // interface
	iv        []byte       // initialisation vector
	encrypter cipher.Stream
	decrypter cipher.Stream
	in        int // for debugging streams
	out       int
	*State
}

var _ EncryptionScheme = NewAES_256()

func NewAES_256() *AES_256 {
	return &AES_256{
		key:       []byte{},
		block:     NewIdentityCipherBlock(),
		iv:        []byte{},
		encrypter: NewIdentityCipherStream(),
		decrypter: NewIdentityCipherStream(),
		State:     NewState(),
	}
}

func MakeAES_256(key []byte) (*AES_256, *Goof) {
	aes_256 := NewAES_256()
	aes_256.key = key
	goof := CheckKeyLength("ENCRYPTION_AES_256", len(key))
	if IsError(goof) {
		return aes_256, goof
	}
	var err error
	aes_256.block, err = aes.NewCipher(aes_256.key)
	if IsError(err) {
		return aes_256,
			Env.Goofs().WrapGoof("GENERATING_BLOCK_CIPHER", err,
				"While generating block cipher for AES-256")
	}
	aes_256.NotEmpty()
	cipher, err := hex.DecodeString(H2GO_CIPHERTEXT)
	if IsError(err) {
		return aes_256, Env.Goofs().WrapGoof("DECODING", err,
			"While hex decoding %q to a byte slice", H2GO_CIPHERTEXT)
	}
	aes_256.iv = cipher[:aes.BlockSize]
	aes_256.BuildCipherStreams()
	aes_256.NowComplete()
	return aes_256, NO_GOOF
}

func BuildAES_256(key []byte, iv []byte) (*AES_256, *Goof) {
	aes_256 := NewAES_256()
	aes_256.key = key
	goof := CheckKeyLength("ENCRYPTION_AES_256", len(key))
	if IsError(goof) {
		return aes_256, goof
	}
	if len(iv) != aes.BlockSize {
		return aes_256, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"AES-256 init vector is length %d, require %d",
			len(iv), aes.BlockSize)
	}
	aes_256.iv = iv
	aes_256.NotEmpty()
	var err error
	aes_256.block, err = aes.NewCipher(key)
	if IsError(err) {
		return aes_256,
			Env.Goofs().WrapGoof("GENERATING_BLOCK_CIPHER", err,
				"While generating block cipher for AES-256")
	}
	aes_256.BuildCipherStreams()
	aes_256.NowComplete()
	return aes_256, NO_GOOF
}

func ConstructAES_256(passphrase string, box *Box) (*AES_256, *PassHash, *Goof) {
	phash, goof := BuildDefaultPassHash(passphrase, AES_256_KEY_LENGTH, box)
	if IsError(goof) {
		aes_256 := NewAES_256()
		return aes_256, phash, Env.Goofs().WrapGoof("CRYPTOGRAPHY", goof,
			"While generating passhash for %T", aes_256)
	}
	aes256, goof := MakeAES_256(phash.Key())
	return aes256, phash, goof
}

func (aes_256 *AES_256) BuildCipherStreams() *AES_256 {
	aes_256.encrypter = cipher.NewCFBEncrypter(aes_256.block, aes_256.iv)
	aes_256.decrypter = cipher.NewCFBDecrypter(aes_256.block, aes_256.iv)
	aes_256.in = 0
	aes_256.out = 0
	return aes_256
}

func (aes_256 *AES_256) Copy() EncryptionScheme {
	newaes_256 := NewAES_256()
	newaes_256.block = aes_256.block
	newaes_256.iv = make([]byte, len(aes_256.iv))
	copy(newaes_256.iv, aes_256.iv)
	newaes_256.BuildCipherStreams()
	newaes_256.State = aes_256.State.Copy()
	return newaes_256
}

// Getters
func (aes_256 *AES_256) Block() cipher.Block      { return aes_256.block }
func (aes_256 *AES_256) InitVector() []byte       { return aes_256.iv }
func (aes_256 *AES_256) Encrypter() cipher.Stream { return aes_256.encrypter }
func (aes_256 *AES_256) Decrypter() cipher.Stream { return aes_256.decrypter }

func (aes_256 *AES_256) Name() string   { return "ENCRYPTION_AES_256" }
func (aes_256 *AES_256) KeyLength() int { return int(AES_256_KEY_LENGTH) }
func (aes_256 *AES_256) Level() int     { return 0 }
func (aes_256 *AES_256) IsStream() bool { return true }

func (aes_256 *AES_256) BytesEncrypted() int { return aes_256.in }
func (aes_256 *AES_256) BytesDecrypted() int { return aes_256.out }

func (aes_256 *AES_256) Encrypt(secret []byte) ([]byte, *Goof) {
	encrypted := make([]byte, len(secret))
	aes_256.Encrypter().XORKeyStream(encrypted, secret)
	aes_256.in += len(secret)
	return encrypted, NO_GOOF
}

func (aes_256 *AES_256) Decrypt(encrypted []byte) ([]byte, *Goof) {
	secret := make([]byte, len(encrypted))
	aes_256.Decrypter().XORKeyStream(secret, encrypted)
	aes_256.out += len(encrypted)
	return secret, NO_GOOF
}

func (aes_256 *AES_256) ResetCipher() {
	aes_256.BuildCipherStreams()
	return
}

func (aes_256 *AES_256) SetRandomKeys() ([]byte, *Goof) {
	key := RandomBytes(aes_256.KeyLength())
	newaes_256, goof := MakeAES_256(key)
	if IsError(goof) {
		return key, goof
	}
	*aes_256 = *newaes_256
	return key, NO_GOOF
}

func (aes_256 *AES_256) KeysToString() (string, *Goof) {
	return hex.EncodeToString(aes_256.key), NO_GOOF
}

func (aes_256 *AES_256) KeysFromString(txt string) *Goof {
	byts, err := hex.DecodeString(txt)
	if IsError(err) {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding hex string %q representing the %q key",
			txt, aes_256.Name())
	}
	if len(byts) != int(AES_256_KEY_LENGTH) {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding hex string %q representing the %q nonce "+
				"expected byte length %d, got %d",
			txt, aes_256.Name(), AES_256_KEY_LENGTH, len(byts))
	}
	newaes_256, goof := MakeAES_256(byts)
	if IsError(goof) {
		return goof
	}
	*aes_256 = *newaes_256
	return NO_GOOF
}

func (aes_256 *AES_256) Equals(other interface{}) bool {
	aes_256_2 := NewAES_256()
	switch v := other.(type) {
	case AES_256:
		aes_256_2 = &v
	case *AES_256:
		aes_256_2 = v
	default:
		return false
	}
	for i, byt := range aes_256_2.key {
		if aes_256.key[i] != byt {
			return false
		}
	}
	return true
}

// RSA =========================================================================

type RSA_OAEP struct {
	privkey *rsa.PrivateKey
	in      int // for debugging streams
	out     int
	*State
}

var _ EncryptionScheme = NewRSA_OAEP()

func NewRSA_OAEP() *RSA_OAEP {
	return &RSA_OAEP{
		privkey: &rsa.PrivateKey{
			PublicKey: rsa.PublicKey{
				N: big.NewInt(0),
				E: 0,
			},
			D:      big.NewInt(0),
			Primes: []*big.Int{big.NewInt(1), big.NewInt(1)},
			Precomputed: rsa.PrecomputedValues{
				Dp:   big.NewInt(0),
				Dq:   big.NewInt(0),
				Qinv: big.NewInt(0),
				CRTValues: []rsa.CRTValue{
					rsa.CRTValue{
						Exp:   big.NewInt(0),
						Coeff: big.NewInt(0),
						R:     big.NewInt(0),
					},
				},
			},
		},
		State: NewState(),
	}
}

func (rsa_oaep *RSA_OAEP) Copy() EncryptionScheme {
	newrsa_oaep := NewRSA_OAEP()
	newrsa_oaep.privkey.PublicKey.N = rsa_oaep.privkey.PublicKey.N
	newrsa_oaep.privkey.PublicKey.E = rsa_oaep.privkey.PublicKey.E
	newrsa_oaep.privkey.D.Set(rsa_oaep.privkey.D)
	newrsa_oaep.privkey.Primes = []*big.Int{}
	for _, bint := range rsa_oaep.privkey.Primes {
		newrsa_oaep.privkey.Primes = append(newrsa_oaep.privkey.Primes, bint)
	}
	newrsa_oaep.privkey.Precomputed.Dp.Set(rsa_oaep.privkey.Precomputed.Dp)
	newrsa_oaep.privkey.Precomputed.Dq.Set(rsa_oaep.privkey.Precomputed.Dq)
	newrsa_oaep.privkey.Precomputed.Qinv.Set(rsa_oaep.privkey.Precomputed.Qinv)
	newrsa_oaep.privkey.Precomputed.CRTValues = []rsa.CRTValue{}
	for _, crtv := range rsa_oaep.privkey.Precomputed.CRTValues {
		newcrtv := rsa.CRTValue{
			Exp:   big.NewInt(0).Set(crtv.Exp),
			Coeff: big.NewInt(0).Set(crtv.Coeff),
			R:     big.NewInt(0).Set(crtv.R),
		}
		newrsa_oaep.privkey.Precomputed.CRTValues = append(
			newrsa_oaep.privkey.Precomputed.CRTValues,
			newcrtv)
	}
	newrsa_oaep.State = rsa_oaep.State.Copy()
	return newrsa_oaep
}

func (rsa_oaep *RSA_OAEP) PrivateKey() *rsa.PrivateKey { return rsa_oaep.privkey }
func (rsa_oaep *RSA_OAEP) PublicKey() *rsa.PublicKey   { return &rsa_oaep.privkey.PublicKey }

func (rsa_oaep *RSA_OAEP) Name() string   { return "ENCRYPTION_RSA_OAEP" }
func (rsa_oaep *RSA_OAEP) KeyLength() int { return int(RSA_OAEP_KEY_LENGTH) } // bytes
func (rsa_oaep *RSA_OAEP) Level() int     { return 0 }
func (rsa_oaep *RSA_OAEP) IsStream() bool { return false }

func (rsa_oaep *RSA_OAEP) BytesEncrypted() int { return rsa_oaep.in }
func (rsa_oaep *RSA_OAEP) BytesDecrypted() int { return rsa_oaep.out }

func (rsa_oaep *RSA_OAEP) MaxMessageLength() int {
	return rsa_oaep.maxMessageLength(rsa_oaep.KeyLength(), 256/8)
}

func (rsa_oaep *RSA_OAEP) maxMessageLength(keybyts, hashbyts int) int {
	return keybyts - 2*hashbyts - 2
}

func (rsa_oaep *RSA_OAEP) Encrypt(secret []byte) ([]byte, *Goof) {
	if len(secret) > RSA_OAEP_MAX_MSG_LENGTH {
		return []byte{}, Env.Goofs().MakeGoof("LIMIT_EXCEEDED",
			"Maximum number of bytes to encrypt for %q is %d, got %d",
			rsa_oaep.Name(), RSA_OAEP_MAX_MSG_LENGTH, len(secret))
	}
	encrypted, err := rsa.EncryptOAEP(
		sha256.New(),
		TrueRandomSource(),
		rsa_oaep.PublicKey(),
		secret,
		[]byte{})
	if IsError(err) {
		return encrypted, Env.Goofs().WrapGoof("ENCRYPTING", err,
			"While encrypting %d bytes using %q",
			len(secret), rsa_oaep.Name())
	}
	rsa_oaep.in += len(secret)
	return encrypted, NO_GOOF
}

func (rsa_oaep *RSA_OAEP) Decrypt(encrypted []byte) ([]byte, *Goof) {
	secret, err := rsa.DecryptOAEP(
		sha256.New(),
		TrueRandomSource(),
		rsa_oaep.PrivateKey(),
		encrypted,
		[]byte{})
	if IsError(err) {
		return secret, Env.Goofs().WrapGoof("DECRYPTING", err,
			"While decrypting %d bytes using %q",
			len(encrypted), rsa_oaep.Name())
	}
	rsa_oaep.out += len(encrypted)
	return secret, NO_GOOF
}

func (rsa_oaep *RSA_OAEP) ResetCipher() {
	return
}

func (rsa_oaep *RSA_OAEP) SetRandomKeys() ([]byte, *Goof) {
	privkey, err := rsa.GenerateKey(TrueRandomSource(), rsa_oaep.KeyLength()*8)
	if IsError(err) {
		return []byte{}, Env.Goofs().WrapGoof("CRYPTOGRAPHY", err,
			"While generating random key for %q",
			rsa_oaep.Name())
	}
	rsa_oaep.privkey = privkey
	return []byte{}, NO_GOOF
}

func (rsa_oaep *RSA_OAEP) KeysToString() (string, *Goof) {
	result := strconv.Itoa(
		3 + 6 + len(rsa_oaep.privkey.Primes) +
			3*len(rsa_oaep.privkey.Precomputed.CRTValues))
	result += "," + rsa_oaep.PublicKey().N.String()
	result += "," + strconv.Itoa(rsa_oaep.PublicKey().E)
	result += "," + rsa_oaep.privkey.D.String()
	result += "," + strconv.Itoa(len(rsa_oaep.privkey.Primes))
	for _, bint := range rsa_oaep.privkey.Primes {
		result += "," + bint.String()
	}
	result += "," + rsa_oaep.privkey.Precomputed.Dp.String()
	result += "," + rsa_oaep.privkey.Precomputed.Dq.String()
	result += "," + rsa_oaep.privkey.Precomputed.Qinv.String()
	result += "," + strconv.Itoa(len(rsa_oaep.privkey.Precomputed.CRTValues))
	for _, crtv := range rsa_oaep.privkey.Precomputed.CRTValues {
		result += "," + crtv.Exp.String()
		result += "," + crtv.Coeff.String()
		result += "," + crtv.R.String()
	}
	return result, NO_GOOF
}

func (rsa_oaep *RSA_OAEP) KeysFromString(txt string) *Goof {
	parts := strings.Split(txt, ",")
	if len(parts) == 0 {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"No key parameters detected for %q in %q",
			rsa_oaep.Name(), txt)
	}
	c := 0
	n, err := strconv.Atoi(parts[c])
	if IsError(err) {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding %T %q representing the total number of "+
				"%q key parameters in %q",
			rsa_oaep.privkey, parts[c], rsa_oaep.Name(), txt)
	}
	if len(parts) != n {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Expected %d parameters in text encoding of %q "+
				"keys %q, got %d",
			n, rsa_oaep.Name(), txt, len(parts))
	}

	c++
	_, ok := rsa_oaep.PublicKey().N.SetString(parts[c], 10)
	if !ok {
		return Env.Goofs().MakeGoof("PARSING_TEXT",
			"While decoding %T %q representing the %q public key N",
			rsa_oaep.PublicKey(), parts[c], rsa_oaep.Name())
	}
	c++
	rsa_oaep.PublicKey().E, err = strconv.Atoi(parts[c])
	if IsError(err) {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding %T %q representing the %q public key E",
			rsa_oaep.PublicKey(), parts[c], rsa_oaep.Name())
	}
	c++
	_, ok = rsa_oaep.privkey.D.SetString(parts[c], 10)
	if !ok {
		return Env.Goofs().MakeGoof("PARSING_TEXT",
			"While decoding %T %q representing the %q private key D",
			rsa_oaep.privkey, parts[c], rsa_oaep.Name())
	}
	c++
	n, err = strconv.Atoi(parts[c])
	if IsError(err) {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding %T %q representing the %q "+
				"number of private key primes",
			rsa_oaep.privkey, parts[c], rsa_oaep.Name())
	}
	if n < 2 {
		return Env.Goofs().WrapGoof("INVALID_DATA", err,
			"While decoding %T %q representing the %q "+
				"number of private key primes, require at least 2, got %d",
			rsa_oaep.privkey, parts[c], rsa_oaep.Name(), n)
	}
	rsa_oaep.privkey.Primes = []*big.Int{}
	for i := 0; i < n; i++ {
		c++
		bint, ok := big.NewInt(0).SetString(parts[c], 10)
		if !ok {
			return Env.Goofs().MakeGoof("PARSING_TEXT",
				"While decoding %T %q representing the %dth prime in the %q "+
					"private key",
				rsa_oaep.privkey, parts[c], i, rsa_oaep.Name())
		}
		rsa_oaep.privkey.Primes = append(rsa_oaep.privkey.Primes, bint)
	}
	c++
	_, ok = rsa_oaep.privkey.Precomputed.Dp.SetString(parts[c], 10)
	if !ok {
		return Env.Goofs().MakeGoof("PARSING_TEXT",
			"While decoding %T %q representing the %q private key Dp",
			rsa_oaep.privkey, parts[c], rsa_oaep.Name())
	}
	c++
	_, ok = rsa_oaep.privkey.Precomputed.Dq.SetString(parts[c], 10)
	if !ok {
		return Env.Goofs().MakeGoof("PARSING_TEXT",
			"While decoding %T %q representing the %q private key Dq",
			rsa_oaep.privkey, parts[c], rsa_oaep.Name())
	}
	c++
	_, ok = rsa_oaep.privkey.Precomputed.Qinv.SetString(parts[c], 10)
	if !ok {
		return Env.Goofs().MakeGoof("PARSING_TEXT",
			"While decoding %T %q representing the %q private key Qinv",
			rsa_oaep.privkey, parts[c], rsa_oaep.Name())
	}
	c++
	n, err = strconv.Atoi(parts[c])
	if IsError(err) {
		return Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding %T %q representing the %q "+
				"number of private key CRT values",
			rsa_oaep.privkey, parts[c], rsa_oaep.Name())
	}
	if n < 0 {
		return Env.Goofs().WrapGoof("INVALID_DATA", err,
			"While decoding %T %q representing the %q "+
				"number of private key CRT values must be positive, got %d",
			rsa_oaep.privkey, parts[c], rsa_oaep.Name(), n)
	}
	c++
	rsa_oaep.privkey.Precomputed.CRTValues = []rsa.CRTValue{}
	for i := 0; i < n; i++ {
		crtv := rsa.CRTValue{}
		crtv.Exp, ok = big.NewInt(0).SetString(parts[c], 10)
		if !ok {
			return Env.Goofs().MakeGoof("PARSING_TEXT",
				"While decoding %T %q representing the %dth CRT Exp value "+
					"in the %q private key",
				rsa_oaep.privkey, parts[c], i, rsa_oaep.Name())
		}
		c++
		crtv.Coeff, ok = big.NewInt(0).SetString(parts[c], 10)
		if !ok {
			return Env.Goofs().MakeGoof("PARSING_TEXT",
				"While decoding %T %q representing the %dth CRT Coeff value "+
					"in the %q private key",
				rsa_oaep.privkey, parts[c], i, rsa_oaep.Name())
		}
		c++
		crtv.R, ok = big.NewInt(0).SetString(parts[c], 10)
		if !ok {
			return Env.Goofs().MakeGoof("PARSING_TEXT",
				"While decoding %T %q representing the %dth CRT R value "+
					"in the %q private key",
				rsa_oaep.privkey, parts[c], i, rsa_oaep.Name())
		}
		rsa_oaep.privkey.Precomputed.CRTValues =
			append(rsa_oaep.privkey.Precomputed.CRTValues, crtv)
	}
	rsa_oaep.NowComplete()
	return NO_GOOF
}

func (rsa_oaep *RSA_OAEP) Equals(other interface{}) bool {
	rsa_oaep2 := NewRSA_OAEP()
	switch v := other.(type) {
	case RSA_OAEP:
		rsa_oaep2 = &v
	case *RSA_OAEP:
		rsa_oaep2 = v
	default:
		return false
	}
	if rsa_oaep.PublicKey().N.Cmp(rsa_oaep2.PublicKey().N) != 0 {
		return false
	}
	if rsa_oaep.PublicKey().E != rsa_oaep2.PublicKey().E {
		return false
	}
	if rsa_oaep.privkey.D.Cmp(rsa_oaep2.privkey.D) != 0 {
		return false
	}
	for i, bint := range rsa_oaep.privkey.Primes {
		if rsa_oaep2.privkey.Primes[i].Cmp(bint) != 0 {
			return false
		}
	}
	if rsa_oaep.privkey.Precomputed.Dp.Cmp(
		rsa_oaep2.privkey.Precomputed.Dp) != 0 {
		return false
	}
	if rsa_oaep.privkey.Precomputed.Dq.Cmp(
		rsa_oaep2.privkey.Precomputed.Dq) != 0 {
		return false
	}
	if rsa_oaep.privkey.Precomputed.Qinv.Cmp(
		rsa_oaep2.privkey.Precomputed.Qinv) != 0 {
		return false
	}
	for i, crtv := range rsa_oaep.privkey.Precomputed.CRTValues {
		if rsa_oaep2.privkey.Precomputed.CRTValues[i].Exp.Cmp(crtv.Exp) != 0 {
			return false
		}
		if rsa_oaep2.privkey.Precomputed.CRTValues[i].Coeff.Cmp(crtv.Coeff) != 0 {
			return false
		}
		if rsa_oaep2.privkey.Precomputed.CRTValues[i].R.Cmp(crtv.R) != 0 {
			return false
		}
	}
	return true
}

// SUPPORT =====================================================================

// Hiding user text input requires a linux system using gopass.
func GetStdinString(prompt string, hide bool, testAddress string) (string, *Goof) {
	if len(testAddress) > 0 {
		return Env.WaitForMailString(testAddress)
	} else {
		if len(prompt) == 0 && hide {
			if hide {
				prompt = "Enter passphrase> "
			} else {
				prompt = "> "
			}
		}
		strval := ""
		var err error
		if hide {
			strval, err = gopass.GetPass(prompt)
		} else {
			r := bufio.NewReader(os.Stdin)
			fmt.Printf(prompt)
			strval, err = r.ReadString('\n')
			strval = strings.TrimSuffix(strval, "\n")
		}
		if IsError(err) {
			return strval, Env.Goofs().WrapGoof("PARSING_TEXT", err,
				"While processing console input")
		}
		return strval, NO_GOOF
	}
}

// Random numbers.

func TrueRandomSource() *os.File {
	frnd, err := os.OpenFile("/dev/urandom", os.O_RDONLY, 0)
	if IsError(err) {
		Env.Log().IsError(Env.Goofs().WrapGoof("OPENING_FILE", err,
			"While trying to obtain true random data"))
	}
	return frnd
}

// Generate a slice of random hex strings of random length within the given
// range of lengths.
// Credit to Russ Cox https://groups.google.com/forum/#!topic/golang-nuts/d0nF_k4dSx4
// for the idea of using /dev/urandom.
// TODO check cross compatibility of /dev/urandom
func GenerateRandomHexStrings(n, minsize, maxsize uint64) []string {
	frnd := TrueRandomSource()
	defer frnd.Close()

	maxuint := float64(^uint64(0))
	rng := float64(maxsize - minsize)
	if rng < 0 {
		Env.Log().IsError(GoofMaxSmallerThanMin(int(minsize), int(maxsize)))
	}
	adjlen := uint64(0)
	rawlen := uint64(0)
	result := make([]string, n)
	for i := 0; i < int(n); i++ {
		binary.Read(frnd, binary.BigEndian, &rawlen)
		adjlen = uint64(float64(rawlen)*rng/maxuint) + minsize
		rndval := make([]byte, int(adjlen)/2)
		frnd.Read(rndval)
		result[i] = hex.EncodeToString(rndval)
	}
	return result
}

func RandomHexString(length uint64) string {
	return GenerateRandomHexStrings(1, length, length)[0]
}

func RandomBytes(n int) []byte {
	frnd := TrueRandomSource()
	defer frnd.Close()
	byts := make([]byte, n)
	binary.Read(frnd, binary.BigEndian, &byts)
	return byts
}

func RandomInteger(n int) uint64 {
	frnd := TrueRandomSource()
	defer frnd.Close()
	byts := make([]byte, n)
	binary.Read(frnd, binary.BigEndian, &byts)
	return binary.BigEndian.Uint64(byts)
}

func GenerateHexPass(minsize, maxsize uint64) string {
	return GenerateRandomHexStrings(1, minsize, maxsize)[0]
}

// Zappbase aggressively wants to remain ignorant of plain text passphrases.
func PurgePassphrase(u url.URL) url.URL {
	if u.User != nil {
		u.User = url.UserPassword(u.User.Username(), "")
	}
	return u
}

func CheckKeyLength(encType string, keylength int) *Goof {
	i64, goof := EncryptionKeyLength(encType)
	if IsError(goof) {
		return goof
	}
	if int64(keylength) != i64 {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Require %q encryption key length %d bytes, got length %d",
			encType, i64, keylength)
	}
	return NO_GOOF
}

func EncryptionKeyLength(encType string) (int64, *Goof) {
	obj, found := BinaryAlgoEncryption.GetInfo(encType, "key_length")
	if !found {
		return 0, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Could not find %q for %q in %T",
			"key_length", encType, BinaryAlgoEncryption)
	}
	i64, _ := obj.(int64)
	return i64, NO_GOOF
}

// CHECKSUM ====================================================================

func Checksum(byts []byte) (CHKUINT, *Goof) {
	if len(byts) == 0 {
		return 0, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Cannot calculate checksum of empty byte slice")
	}
	return AsCHKUINT(uint(crc32.ChecksumIEEE(byts)))
}
