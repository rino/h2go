H2GO
====

H2GO is the water I need to make my apps grow.  It contains a bunch of smaller miscellaneous components, and also describes the overall collection of dependent packages named "..2go" that are big enough or functionally important enough to be separated.  Generally, something I've learned using Go and other languages is to modularise right from the start.  If done well, it's the best way to manage complexity.

As I write apps, a functional requirement will emerge and I'll try to genericise it and include it in h2go if it seems like it has potential to grow.

Features
--------

types.go
allows an app to build on a set of base types including native types defined in a two way TypeMap.  The base global var is Types, defined in h2go.go.  

bytes.go
provides a Buffer that wraps a bytes.Buffer, including a TypeMap and a the byte order.  It also defines a BinaryEncodable interface.

codes.go
I have used simple constants for enumerating error and wire messages, and this hardwiring is no doubt computationally faster but lacks flexibility and automated reversibility.  Codes is similar to a TypeMap, and a base set of Goof codes is defined in h2go.go.

compare.go
provides a Comparable interface for equality and sorting purposes.

enum.go
provides reversible string-number maps.

exit.go
provides some functions to manage executable shutdown.

field.go
defines a way to wrap an interface{} value with its type.  A Field is meant to be as light as possible and does not know what its Type() means, but a FieldMap and FieldList must include a pointer to a TypeMap. A FieldMap uses a Field for the key and \*Field for the values, which requires some extra type switching for transparent handling.

files.go
file related functions.

goof.go
provides a customised error, a Goof, and a set of convenience formatters. 

h2go.go
acts as the central file for defining globals and controls used in the package.

int.go
provides methods for the number subtypes defined in h2go.go.  These suibtypes allow easier refactoring if you want to change the encoding of some data from say UINT16 to UINT32.

intbox.go
originally a separate package, this defines an interface for boxing  and transforming integers in as fluent a way as possible.  Its used by time2go to make the underlying representation of an integer more generic.

lang\_english.go
provides an instances of a Language description.

language.go
describes a Language interface, for the description of elements of a human language, prompted by internationalisation of time2go.

linklist.go
provides a doubly linked list with InsertSort and Collect methods, using the Comparable interface.

log.go
provides a simple general purpose logger which simply sends messages to a chosen set of output streams.  Most of the time, apps will use the singleton global h2go.Log.

math.go
currently empty.

portlock.go
allows use of a TCP port as a generic locking flag.

security.go
provides wrappers for encryption tools.

struct.go
uses reflection to determine if an object is a struct, and represents a struct as a tree data structure StructTree.

tree.go
provides a simple tree data structure using Comparable.

types.go
defines a TypeMap and provides associated methods, including GetValType which type switches in order to map the base h2go.Types TypeMap to native types. 

version.go
represents a version using three numbers - major, minor and revision.  The h2go version is set via h2go.H2GO\_VERSION and can be used to ensure compatibility. 

worditer.go
provides a simple iterator along words in a string slice, used for parsing.

Types
-----

h2go provides a base set of types in Types.  To add a user defined type as a new h2go type, follow these steps:

1. ensure the user defined type is a Typester() (i.e. it includes a TypeId() method),
2. create a binary encoder function generator that itself returns a binary encoder function of the form specified by TypeInfo.  Similarly, create a binary decoder function generator.  The simplest way to go about this is to ensure the user defined type is BinaryEncodable, then simply rename copies of bigIntBinaryEncoder and bigIntBinaryDecoder, replacing the type assertion with your user defined type and the GobEncode/Decode with your BinaryEncodable methods,
3. create a map[TYPE]TypeInfo containing your custom TypeInfo instance.  There are only 256 possible types, so the TYPE index must be outside the reserved range from 0 to USER\_TYPE\_START.
4. Add your type(s) using TypeMap.Add.

Fields
------

Constructors
------------

Consider a struct named MyStruct.  The nil constructor is NewMyStruct().  This initialises any maps or embedded structures.  The constructor MakeMyStruct(..) accepts parameters for the new MyStruct.  The constructor BuildMyStruct(..) accepts both parameters and data in a more elaborate process.
