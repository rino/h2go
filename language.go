package h2go

import (
	"reflect"
	"strconv"
)

type Language interface {
	Name() string
	Copy() Language
	ISO639() []string
	ShortOrdinals() []string
	LongOrdinals() Uint8Enum
	MeridiemMarkers() []string
	AmMarkers() []string
	PmMarkers() []string
	NoonMarkers() []string
	ClockDividers() []string
	DateDividers() []string
	IsUpperLetter(s string) bool
	IsLowerLetter(s string) bool
	IsLetter(s string) bool
	IsDigit(s string) bool
	IsPrintable(s string) bool
	Days(cal string) Uint8Enum
	Months(cal string) Uint8Enum
}

func GetShortOrdinal(phrase string, lang Language) (uint8, *Goof) {
	suffix, ok := HasSuffix(phrase, lang.ShortOrdinals())
	if ok {
		// Prefix must be numbers
		ui := uint8(0)
		i, err := strconv.Atoi(phrase[0 : len(phrase)-len(suffix)])
		if IsError(err) {
			return 0, Env.Goofs().WrapGoof("PARSING_TEXT", err,
				"While converting phrase text to integer")
		} else {
			oflow := reflect.ValueOf(ui).OverflowUint(uint64(i))
			if oflow {
				return 0, Env.Goofs().MakeGoof("PARSING_TEXT",
					"The phrase %q is recognised as a short ordinal "+
						"but the cardinal %d cannot fit in a uint8",
					phrase, i)
			}
			return uint8(i), NO_GOOF
		}
	}
	return 0, Env.Goofs().MakeGoof("PARSING_TEXT",
		"The phrase %q is not recognised as a short ordinal "+
			"in the language %q",
		phrase, lang.Name())
}

func IsOrdinalSuffix(phrase string, lang Language) bool {
	return StringIsIn(phrase, lang.ShortOrdinals())
}

func IsMeridiemMarker(phrase string, lang Language) bool {
	return StringIsIn(phrase, lang.MeridiemMarkers())
}

func IsAmMarker(phrase string, lang Language) bool {
	return StringIsIn(phrase, lang.AmMarkers())
}

func IsPmMarker(phrase string, lang Language) bool {
	return StringIsIn(phrase, lang.PmMarkers())
}

func IsNoonMarker(phrase string, lang Language) bool {
	return StringIsIn(phrase, lang.NoonMarkers())
}

func IsDateDivider(s string, lang Language) bool {
	return StringIsIn(s, lang.DateDividers())
}

func IsClockDivider(s string, lang Language) bool {
	return StringIsIn(s, lang.ClockDividers())
}

func IsDivider(s string, lang Language) bool {
	if IsDateDivider(s, lang) || IsClockDivider(s, lang) {
		return true
	}
	return false
}

func DividersToString(lang Language) string {
	result := ""
	for _, s := range lang.ClockDividers() {
		result += s
	}
	for _, s := range lang.DateDividers() {
		result += s
	}
	return result
}

func GetOrdinal(phrase string, lang Language) (uint8, *Goof) {
	ord, exists := lang.LongOrdinals().FromString[phrase]
	if exists {
		return ord, NO_GOOF
	} else {
		return GetShortOrdinal(phrase, lang)
	}
}
