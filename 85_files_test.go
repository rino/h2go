package h2go

import (
	"testing"
)

func TestFilePerms(t *testing.T) {
	expected := NewFilePermissions()
	expected.User.CanRead().CanWrite().CanExec()
	expected.Group.CanRead().CanExec()
	expected.Other.CanRead()
	txt := "rwxr-xr--"
	result, goof := NewFilePermissions().DecodeString(txt, EMPTY_BOX)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decoding %q", txt)
	}
	if !expected.Equals(result.GetDecoded().Value()) {
		t.Fatalf("Expected %q, got %q", expected, result.GetDecoded().Value())
	}
	if "-"+txt != expected.ToFileMode().String() {
		t.Fatalf("Expected %q, got %q", "-"+txt, expected.ToFileMode().String())
	}
}
