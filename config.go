/*
	Wraps a workspace and configuration struct to facilitate loading and saving
	configuration information to file.
*/
package h2go

import (
	"os"
)

type Validatable interface {
	Validate() *Goof
}

type EnvConfig struct {
	GOOF_JUMPS      int
	GOOF_DEPTH      int
	TRACE_BACK      int
	TRACE_PATHDEPTH int
	LANGUAGE        string
	CALENDAR        string
}

var _ Validatable = NewEnvConfig()

func NewEnvConfig() *EnvConfig {
	return &EnvConfig{}
}

func (cfg *EnvConfig) Copy() *EnvConfig {
	cfg2 := NewEnvConfig()
	DeepCopyStructPtrs(cfg2, cfg)
	return cfg2
}

// Set the number of previous callers displayed in a Goof.
func (cfg *EnvConfig) Validate() *Goof {
	// This range is arbitrary
	min := 1
	max := 10
	if !IsInRange(int64(cfg.GOOF_JUMPS), min, max) {
		return Env.Goofs().MakeGoof("BAD_CONFIG",
			"Attempt to set the number of goof jumps to %d, must be "+
				"between %d and %d inclusive",
			cfg.GOOF_JUMPS, min, max)
	}
	if !IsInRange(int64(cfg.GOOF_DEPTH), 1, 10) {
		return Env.Goofs().MakeGoof("BAD_CONFIG",
			"Attempt to set the path depth of callers displayed for "+
				"goofs %d, must be between %d and %d inclusive",
			cfg.GOOF_DEPTH, min, max)
	}
	return NO_GOOF
}

type FieldBoxParamsConfig struct {
	TIMESTAMPING         bool
	COMPRESSION_NONE     bool
	COMPRESSION_GZIP     bool
	ENCRYPTION_NONE      bool
	ENCRYPTION_SECRETBOX bool
	ENCRYPTION_AES_256   bool
	ENCRYPTION_RSA_OAEP  bool
}

var _ Validatable = NewFieldBoxParamsConfig()

func NewFieldBoxParamsConfig() *FieldBoxParamsConfig {
	return &FieldBoxParamsConfig{}
}

func (cfg *FieldBoxParamsConfig) Copy() *FieldBoxParamsConfig {
	cfg2 := NewFieldBoxParamsConfig()
	DeepCopyStructPtrs(cfg2, cfg)
	return cfg2
}

func (cfg *FieldBoxParamsConfig) ToSwitches() (*Switches, *Goof) {
	return MakeSwitchesFromStruct(cfg)
}

func (cfg *FieldBoxParamsConfig) Validate() *Goof {
	switches, goof := MakeSwitchesFromStruct(cfg)
	if IsError(goof) {
		return goof
	}
	goof = switches.RequireNumberWithKeyPrefixBeThis(
		1, "COMPRESSION_", true)
	if IsError(goof) {
		return goof
	}
	goof = switches.RequireNumberWithKeyPrefixBeThis(
		1, "ENCRYPTION_", true)
	if IsError(goof) {
		return goof
	}
	return NO_GOOF
}

// CONFIGFILER =================================================================

type ConfigFiler struct {
	*Workspace
	strptrs map[string]interface{}
}

func NewConfigFiler() *ConfigFiler {
	return &ConfigFiler{
		Workspace: NewWorkspace(),
		strptrs:   make(map[string]interface{}),
	}
}

// A new Config requires a validator.
func MakeConfigFiler(allowedRoot string, strptrs map[string]interface{}) (*ConfigFiler, *Goof) {
	cfiler := NewConfigFiler()
	if len(strptrs) == 0 {
		return cfiler, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"The config struct cannot be nil")
	}
	for name, strptr := range strptrs {
		_, _, _, goof := RequireStructPtr(strptr)
		if IsError(goof) {
			return cfiler, goof
		}
		v, isa := strptr.(Validatable)
		if !isa {
			return cfiler, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"The config struct %q must be Validatable",
				name, v)
		}
		_, exists := cfiler.strptrs[name]
		if exists {
			return cfiler, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Duplicate %q struct pointer provided",
				name)
		}
	}
	cfiler.strptrs = strptrs
	ws, _ := Env.Types().MakeWorkspace(allowedRoot)
	cfiler.Workspace = ws
	return cfiler, NO_GOOF
}

// Load configuration workspace file. Returns whether the file could be found.
func (cfiler *ConfigFiler) Load(path string) (bool, []string, []string, *Goof) {
	configFound := []string{}
	configNotFound := []string{}
	fileFound := true
	_, err := os.Stat(path)
	if IsError(err) {
		if os.IsNotExist(err) {
			return false, configFound, configNotFound, NO_GOOF
		} else {
			return fileFound, configFound, configNotFound,
				Env.Goofs().WrapGoof("GETTING_FILE_INFO", err,
					"While loading config file %s", path)
		}
	}
	cfiler.ClearOutput()
	cfiler.Run(path, "config file")
	if cfiler.HasGoofs() {
		return fileFound, configFound, configNotFound,
			Env.Goofs().WrapGoof("BAD_CONFIG", cfiler.LastGoof(),
				"While trying to load configuration file %q", path)
	}
	Env.Log().Basic("Loaded config file %q, mapping and validating...", path)
	for name, strptr := range cfiler.strptrs {
		if !cfiler.FieldMap.KeyExists(name) {
			configNotFound = append(configNotFound, name)
		} else {
			fmap := cfiler.FieldMap.Get(name).AsFieldMap()
			if fmap.IsEmpty() {
				configNotFound = append(configNotFound, name)
			} else {
				_, _, goof := fmap.ToStruct(strptr)
				if IsError(goof) {
					return fileFound, configFound, configNotFound, goof
				}
				//v, _ := strptr.(Validatable)
				//goof = v.Validate()
				//if IsError(goof) {
				//	return fileFound, configFound, configNotFound, goof
				//}
				configFound = append(configFound, name)
			}
		}
	}
	return fileFound, configFound, configNotFound, NO_GOOF
}

func (cfiler *ConfigFiler) Save(path, header string) *Goof {
	file, err := os.Create(path)
	defer file.Close()
	if IsError(err) {
		return Env.Goofs().WrapGoof("CREATING_FILE_OR_DIR", err,
			"While saving config file %s", path)
	}
	return cfiler.Write(file, "", header)
}
