/*
	Provides orderable tree data structure.
*/
package h2go

import ()

// TREELINK ====================================================================
type TreeLink struct {
	tree     *Tree
	value    Comparable
	parent   *TreeLink
	children *LinkList
	*State
}

var _ Comparable = NewTreeLink()
var _ Stateful = NewTreeLink() // embedded State

func NewTreeLink() *TreeLink {
	return &TreeLink{
		State: NewState(),
	}
}

func (tlink *TreeLink) Value() Comparable   { return tlink.value }
func (tlink *TreeLink) Tree() *Tree         { return tlink.tree }
func (tlink *TreeLink) Parent() *TreeLink   { return tlink.parent }
func (tlink *TreeLink) Children() *LinkList { return tlink.children }

func (tlink *TreeLink) SetValue(value Comparable) *TreeLink {
	tlink.value = value
	if value != nil {
		tlink.NotEmpty()
	}
	return tlink
}

func (tlink *TreeLink) Cmp(other Comparable) int {
	tlink2, ok := other.(*TreeLink)
	if !ok {
		Env.Log().IsError(Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"other=%v is a %T, not a *TreeLink", other, other))
		return COMPARE_ERR
	}
	return tlink.value.Cmp(tlink2.value)
}

func (tlink *TreeLink) AddChild(child *TreeLink) bool {
	return tlink.children.Iterator().InsertSort(child)
}

func (tlink *TreeLink) String() string {
	return tlink.value.String()
}

func (tlink *TreeLink) NewTreeLink(value Comparable) *TreeLink {
	return tlink.Tree().MakeTreeLink(value)
}

// TREE ========================================================================
type Tree struct {
	root *TreeLink
}

func NewTree() *Tree {
	return &Tree{
		root: NewTreeLink(),
	}
}

func (tree *Tree) Root() *TreeLink { return tree.root }

func (tree *Tree) MakeTreeLink(value Comparable) *TreeLink {
	tlink := NewTreeLink()
	tlink.tree = tree
	tlink.SetValue(value)
	tlink.children = NewLinkList()
	return tlink
}

func (tree *Tree) IsEmpty() bool {
	return tree.root.IsEmpty()
}

func (tlink *TreeLink) PrettyStringLines(result []string, tab, indent string) []string {
	result = append(result, indent+tlink.String())
	if !tlink.children.IsEmpty() {
		for _, comparable := range tlink.children.Collect() {
			child, ok := comparable.(*TreeLink)
			if ok {
				result = child.PrettyStringLines(result, tab, indent+tab)
			}
		}
	}
	return result
}

// TREE ITERATOR ===============================================================
type TreeIterator struct {
	tree   *Tree
	this   *TreeLink
	atLeaf bool
}

func NewTreeIterator() *TreeIterator {
	return &TreeIterator{
		tree: NewTree(),
		this: NewTreeLink(),
	}
}

func (iter *TreeIterator) Tree() *Tree     { return iter.tree }
func (iter *TreeIterator) This() *TreeLink { return iter.this }
func (iter *TreeIterator) AtLeaf() bool    { return iter.atLeaf }

func (iter *TreeIterator) SetAtLeaf(atLeaf bool) *TreeIterator {
	iter.atLeaf = atLeaf
	return iter
}

func (tree *Tree) Iterator() *TreeIterator {
	iter := NewTreeIterator()
	iter.tree = tree
	iter.this = tree.root
	iter.atLeaf = tree.IsEmpty()
	return iter
}

// Go to the given child link.
func (iter *TreeIterator) GotoChild(child *TreeLink) *TreeIterator {
	iter2 := iter.this.children.Iterator().First()
	var val Comparable
	if !iter.this.children.IsEmpty() {
		for {
			if !iter2.this.IsEmpty() {
				val = iter2.this.value
				if val.Cmp(child) == 0 {
					iter.this = child
					if iter.this.children.IsEmpty() {
						iter.SetAtLeaf(true)
					}
					break
				}
			}
			iter2.Inc()
			if iter2.AtEnd() {
				break
			}
		}
	}
	return iter
}
