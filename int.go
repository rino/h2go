/*
	Immutable boxed native and math/big integers.
	// TODO using binary package methods on ByteOrder might be faster?
*/
package h2go

import (
	"math"
	"reflect"
)

// UINT8

type UINT8 uint8

var _ Binaryable = NewUINT8()

func NewUINT8() UINT8 {
	return UINT8(0)
}

var UINT8_Size int = int(reflect.TypeOf(UINT8(0)).Size())

const UINT8_Max UINT8 = UINT8(math.MaxUint8)

func (ui UINT8) Size() int {
	return UINT8_Size
}
func (ui UINT8) Max() UINT8 {
	return UINT8_Max
}

// Encode UINT8 as binary.
func (ui UINT8) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(ui)
	byts, goof := bfr.Write(ui, box)
	out.AppendEncodedAs("UINT8", byts)
	return out, goof
}

// Decode UINT16 from binary.
func (ui UINT8) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	byts, goof := bfr.Read(&ui, box)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding binary to %T", ui)
	}
	out.AutoSetField(ui)
	out.AppendEncodedAs("UINT8", byts)
	return out, NO_GOOF
}

// UINT16

type UINT16 uint16

var _ Binaryable = NewUINT16()

func NewUINT16() UINT16 {
	return UINT16(0)
}

var UINT16_Size int = int(reflect.TypeOf(UINT16(0)).Size())

const UINT16_Max UINT16 = UINT16(math.MaxUint16)

func (ui UINT16) Size() int {
	return UINT16_Size
}
func (ui UINT16) Max() UINT16 {
	return UINT16_Max
}

// Encode UINT16 as binary.
func (ui UINT16) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(ui)
	byts, goof := bfr.Write(ui, box)
	out.AppendEncodedAs("UINT16", byts)
	return out, goof
}

// Decode UINT16 from binary.
func (ui UINT16) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	byts, goof := bfr.Read(&ui, box)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding binary to %T", ui)
	}
	out.AutoSetField(ui)
	out.AppendEncodedAs("UINT16", byts)
	return out, NO_GOOF
}

// UINT32

type UINT32 uint32

var _ Binaryable = NewUINT32()

func NewUINT32() UINT32 {
	return UINT32(0)
}

var UINT32_Size int = int(reflect.TypeOf(UINT32(0)).Size())

const UINT32_Max UINT32 = UINT32(math.MaxUint32)

func (ui UINT32) Size() int {
	return UINT32_Size
}
func (ui UINT32) Max() UINT32 {
	return UINT32_Max
}

func AsUINT32(num int) UINT32 {
	if !CanMakeUINT32(int64(num)) {
		Env.Log().IsError(
			GoofIntOutsideRange(num, 0, int64(UINT32_Max)))
	}
	return UINT32(num)
}

// Can the given int be cast to h2go.UINT32 without overflow?
func CanMakeUINT32(num int64) bool {
	if num <= int64(UINT32_Max) && num >= 0 {
		return true
	}
	return false
}

// Encode UINT32 as binary.
func (ui UINT32) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(ui)
	byts, goof := bfr.Write(ui, box)
	out.AppendEncodedAs("UINT32", byts)
	return out, goof
}

// Decode UINT32 from binary.
func (ui UINT32) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	byts, goof := bfr.Read(&ui, box)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding binary to %T", ui)
	}
	out.AutoSetField(ui)
	out.AppendEncodedAs("UINT32", byts)
	return out, NO_GOOF
}

// UINT64

type UINT64 uint64

var _ Binaryable = NewUINT64()

func NewUINT64() UINT64 {
	return UINT64(0)
}

var UINT64_Size int = int(reflect.TypeOf(UINT64(0)).Size())

const UINT64_MAX UINT64 = UINT64(math.MaxUint64)
const UINT64_MAXUINT uint64 = math.MaxUint64

func (ui UINT64) Size() int {
	return UINT64_Size
}
func (ui UINT64) Max() UINT64 {
	return UINT64_MAX
}

// Encode UINT64 as binary.
func (ui UINT64) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(ui)
	byts, goof := bfr.Write(ui, box)
	out.AppendEncodedAs("UINT64", byts)
	return out, goof
}

// Decode UINT64 from binary.
func (ui UINT64) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	byts, goof := bfr.Read(&ui, box)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding binary to %T", ui)
	}
	out.AutoSetField(ui)
	out.AppendEncodedAs("UINT64", byts)
	return out, NO_GOOF
}

// UINT

type UINT uint

var _ Binaryable = NewUINT()

func NewUINT() UINT {
	return UINT(0)
}

func MakeUINT(i int) UINT {
	return UINT(uint(i))
}

func (ui UINT) AsInt64() (int64, *Goof) {
	ui64 := MakeUint64(uint64(ui))
	i64, oflow := ui64.AsGoInt64()
	if oflow {
		return i64, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
			"Cannot convert UINT %d to int64 (max %d), overflow occurs",
			ui, math.MaxInt64)
	}
	return i64, NO_GOOF

}

// Encode UINT as binary.  Only use the bytes we need.
// Starts with one byte indicating how many bytes are to follow,
// either equaling 1, 2, 4 or 8. The first and only byte is zero
// for a zero value.
func (ui UINT) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(ui)
	byts := []byte{}
	goof := NO_GOOF
	if ui == 0 {
		byts, goof = bfr.Write(uint8(0), box)
		out.AppendEncodedAs("UINT size", byts)
		byts = []byte{}
	} else if ui <= UINT(math.MaxUint8) {
		byts, goof = bfr.Write(uint8(1), box)
		if IsError(goof) {
			return out, goof
		}
		out.AppendEncodedAs("UINT size", byts)
		byts, goof = bfr.Write(UINT8(ui), box)
	} else if ui <= UINT(math.MaxUint16) {
		byts, goof = bfr.Write(uint8(2), box)
		if IsError(goof) {
			return out, goof
		}
		out.AppendEncodedAs("UINT size", byts)
		byts, goof = bfr.Write(UINT16(ui), box)
	} else if ui <= UINT(math.MaxUint32) {
		byts, goof = bfr.Write(uint8(4), box)
		if IsError(goof) {
			return out, goof
		}
		out.AppendEncodedAs("UINT size", byts)
		byts, goof = bfr.Write(UINT32(ui), box)
	} else {
		byts, goof = bfr.Write(uint8(8), box)
		if IsError(goof) {
			return out, goof
		}
		out.AppendEncodedAs("UINT size", byts)
		byts, goof = bfr.Write(UINT64(ui), box)
	}
	out.AppendEncodedAs("UINT", byts)
	return out, goof
}

// Decode UINT from binary.
func (ui UINT) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	typ := uint8(0)
	byts, goof := bfr.Read(&typ, box)
	if IsError(goof) {
		return out, Env.Goofs().WrapGoof("DECODING", goof,
			"While decoding binary to %T", ui)
	}
	out.AppendEncodedAs("UINT size", byts)
	byts = []byte{}
	switch typ {
	case 0:
		ui = UINT(0)
	case 1:
		val := uint8(0) // bypass UINT8 for performance
		byts, goof = bfr.Read(&val, box)
		ui = UINT(val)
	case 2:
		val := uint16(0) // bypass UINT16 for performance
		byts, goof = bfr.Read(&val, box)
		ui = UINT(val)
	case 4:
		val := uint32(0) // bypass UINT32 for performance
		byts, goof = bfr.Read(&val, box)
		ui = UINT(val)
	case 8:
		val := uint64(0) // bypass UINT64 for performance
		byts, goof = bfr.Read(&val, box)
		ui = UINT(val)
	}
	out.AutoSetField(ui) // currently UINT64
	out.AppendEncodedAs("UINT", byts)
	return out, goof
}

// UINTS

type UINTS []UINT

var _ Binaryable = NewUINTS()

func NewUINTS() UINTS {
	return UINTS([]UINT{})
}

// Encode UINTS as binary.  Only use the bytes we need.
func (uis UINTS) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out, goof := UINT(len(uis)).EncodeBinary(bfr, box)
	out.(*BinaryRepresentation).RelabelLastBinaryEncoding(0, "UINTS size")
	if IsError(goof) {
		return out, goof
	}
	var out2 Representation
	for _, ui := range uis {
		out2, goof = ui.EncodeBinary(bfr, box)
		out.AppendEncoded(out2.ChooseEncoding(0))
		if IsError(goof) {
			return out, goof
		}
	}
	return out, goof
}

// Decode UINTS from binary.
func (uis UINTS) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out2, goof := NewUINT().DecodeBinary(bfr, box)
	out2.(*BinaryRepresentation).RelabelLastBinaryEncoding(0, "UINTS size")
	out.AppendEncoded(out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}
	ui, _ := out2.GetDecoded().Value().(UINT)
	i64, goof := ui.AsInt64()
	if IsError(goof) {
		return out, goof
	}
	size := int(i64)
	for i := 0; i < size; i++ {
		out2, goof := NewUINT().DecodeBinary(bfr, box)
		out.AppendEncoded(out2.ChooseEncoding(0))
		if IsError(goof) {
			return out, goof
		}
		ui, _ = out2.GetDecoded().Value().(UINT)
		uis = append(uis, ui)
	}
	out.AutoSetField(uis)
	return out, goof
}

// CHKUINT - used for hash checksums

type CHKUINT UINT32 // Must be >= uint32 in size

const CHKUINT_Max CHKUINT = CHKUINT(UINT32_Max)

var CHKUINT_Size int = int(reflect.TypeOf(CHKUINT(0)).Size())

func (ui CHKUINT) Size() int {
	return CHKUINT_Size
}
func (ui CHKUINT) Max() CHKUINT {
	return CHKUINT_Max
}

func AsCHKUINT(a uint) (CHKUINT, *Goof) {
	if a < 0 {
		return 0, Env.Goofs().MakeGoof("INT_OVERFLOW",
			"While converting %d to CHKUINT (must be > 0)",
			a)
	}
	if a > uint(CHKUINT_Max) {
		return 0, Env.Goofs().MakeGoof("INT_OVERFLOW",
			"While converting %d to CHKUINT with max %d",
			a, CHKUINT_Max)
	}
	return CHKUINT(a), NO_GOOF
}

func IsInRange(v int64, min, max int) bool {
	if v >= int64(min) && v <= int64(max) {
		return true
	}
	return false
}
