/*
	Customised errors containing caller data.
*/
package h2go

import (
	"errors"
	"fmt"
	"io"
)

type Goof struct {
	caller  []*GoCaller
	Code    CODE
	codestr string
	Msg     string // Error message
	EOF     bool   // end of file
	Id      string
	wrapped *Goof
	*State
}

var _ Binaryable = NewGoof()
var _ Stateful = NewGoof() // embedded State

func NewGoof() *Goof {
	return &Goof{
		caller: []*GoCaller{NewGoCaller()},
		wrapped: &Goof{
			caller: []*GoCaller{NewGoCaller()},
			Code:   NO_GOOF_CODE,
			Msg:    "NO_GOOF",
			State:  NewState(),
		},
		State: NewState(),
	}
}

func NoGoof() *Goof {
	goof := NewGoof()
	goof.Code = NO_GOOF_CODE
	goof.Msg = "NO_GOOF"
	return goof
}

func MakeGoof(jumps0 int) *Goof {
	calls := []*GoCaller{}
	caller := NewGoCaller()
	for i := jumps0 + int(Env.Config().GOOF_JUMPS) - 1; i >= 3; i-- {
		caller = CaptureCaller(i)
		if !caller.IsEmpty() {
			calls = append(calls, CaptureCaller(i))
		}
	}
	goof := NewGoof()
	goof.caller = calls
	goof.NotEmpty()
	return goof
}

func (goof *Goof) Wrapped() *Goof { return goof.wrapped }

func (goof *Goof) MakeId() *Goof {
	goof.Id = RandomHexString(GOOF_ID_LENGTH)
	return goof
}

func (goof *Goof) SetIdOfLength(length uint64) *Goof {
	goof.Id = RandomHexString(length)
	return goof
}

func (goof *Goof) Is(name string) bool {
	code, _ := Env.Goofs().Number(name)
	return goof.Code == CODE(code)
}

// Satisfy error interface. Note that Error() takes precedence over String()
// with %v.
func (goof *Goof) Error() string {
	return goof.String()
}

func (goof *Goof) String() string {
	if goof.Code == NO_GOOF_CODE {
		return goof.Msg
	}
	goof2 := goof
	result := ""
	c := 0
	eof := ""
	for {
		if goof2.EOF {
			eof = " (EOF)"
		}
		if c == 0 {
			c++
		} else {
			result += "; "
		}
		result += goof2.CallerString() + ": " + goof2.Msg + eof
		if goof2.wrapped.Code == NO_GOOF_CODE {
			break
		}
		goof2 = goof2.wrapped
		eof = ""
	}
	return result
}

func (goof *Goof) CallerString() string {
	result := ""
	if len(goof.caller) > 0 {
		for i, call := range goof.caller {
			if i != 0 {
				result += ">"
			}
			result += TruncatePathToSlashLevel(
				int(Env.Config().GOOF_DEPTH),
				call.String())
		}
		result += " "
	}
	return result
}

// Returns callers newest first.
func (goof *Goof) CallerLines() []string {
	result := []string{}
	goof2 := goof
	N := 1
	// Count number of linked errors
	for {
		if goof2.wrapped.IsEmpty() {
			break
		} else {
			goof2 = goof2.wrapped
			N++
		}
	}
	goof2 = goof
	result = append(result,
		fmt.Sprintf("Displaying traces (newest first) for %d error(s)",
			N))
	nt := 0
	// Loop through errors
	for n := 0; n < N; n++ {
		result = append(result,
			fmt.Sprintf("ERROR %d: %s", n+1, goof2.Msg))
		nt = len(goof2.caller)
		if nt > 0 {
			call := NewGoCaller()
			for i := nt - 1; i >= 0; i-- {
				call = goof2.caller[i]
				result = append(result, " "+
					TruncatePathToSlashLevel(
						int(Env.Config().GOOF_DEPTH),
						call.String()))
			}
		}
		goof2 = goof2.wrapped
	}
	return result
}

func (goof *Goof) ToError() error {
	return errors.New(goof.Msg)
}

func (goof *Goof) SetEOF(eof bool) *Goof {
	goof.EOF = eof
	return goof
}

// Implement BinaryEncodable interface.
func (goof *Goof) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(goof)
	byts, goof2 := bfr.Write(goof.Code, box)
	out.AppendEncodedAs("goof type code", byts)
	if IsError(goof2) {
		return out, goof2
	}
	byts, goof2 = bfr.WriteBool(goof.EOF, box)
	out.AppendEncodedAs("goof eof (bool)", byts)
	if IsError(goof2) {
		return out, goof2
	}
	strType, goof2 := Env.Types().ProtocolByName("STRING")
	if IsError(goof2) {
		return out, goof2
	}
	out2, goof2 := strType.BinaryTx(bfr, box.Set(BOX_ITEM_STRING, goof.Msg))
	out.AppendEncoded(out2.ChooseEncoding(0))
	return out, goof2
}

func (goof *Goof) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(goof)
	code, byts, goof2 := bfr.ReadCode(box)
	out.AppendEncodedAs("goof type code", byts)
	if IsError(goof2) {
		return out, goof2
	}
	goof.Code = CODE(code)
	goof.EOF, byts, goof2 = bfr.ReadBool(box)
	out.AppendEncodedAs("goof eof (bool)", byts)
	if IsError(goof2) {
		return out, goof2
	}
	strType, goof2 := Env.Types().ProtocolByName("STRING")
	if IsError(goof2) {
		return out, goof2
	}
	out2, goof2 := strType.BinaryRx(bfr, box)
	goof.Msg = out2.GetDecoded().AsString()
	out.AppendEncoded(out2.ChooseEncoding(0))
	return out, goof2
}

func (goof *Goof) AppendGoof(goof2 *Goof) *Goof {
	goof.SetEOF(goof2.EOF)
	goof.wrapped = goof2
	return goof
}

// Mismatches.

func GoofIntOutsideRange(num int, min, max int64) *Goof {
	return Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
		"The number %d does not fit within "+
			"[min, max] = [%d, %d]",
		num, min, max)
}

func GoofUintOutsideRange(num uint, min, max uint64) *Goof {
	return Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
		"The number %d does not fit within "+
			"[min, max] = [%d, %d]",
		num, min, max)
}

func GoofFloatOutsideRange(num, min, max float64) *Goof {
	return Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
		"The number %f does not fit within "+
			"[min, max] = [%f, %f]",
		num, min, max)
}

func GoofInt64Mismatch(num64 int64, origin, byA string, num int) *Goof {
	if len(origin) > 0 {
		origin += " "
	}
	return Env.Goofs().MakeGoof("INT_MISMATCH",
		"The number %d %scannot be properly "+
			"represented by a %s, result is %d",
		num64, origin, byA, num)
}

func GoofMaxSmallerThanMin(min, max int) *Goof {
	return Env.Goofs().MakeGoof("MAX_SMALLER_THAN_MIN",
		"Max %d is smaller than required min %d", max, min)
}

func GoofInvalidKey(msg string, a ...interface{}) *Goof {
	return Env.Goofs().MakeGoof("INVALID_KEY", msg, a...)
}

func GoofMissingField(label interface{}) *Goof {
	return Env.Goofs().MakeGoof("MISSING_FIELD",
		"FieldMap has no field \"%v\"", label)
}

func GoofBadFieldType(label interface{}, reqType string, field *Field) *Goof {
	return Env.Goofs().MakeGoof("BAD_FIELD_TYPE",
		"FieldMap has field \"%v\" but the value is of type %T "+
			"(%v) when it should be a %s",
		label, field.Value(), field.Type(), reqType)
}

func GoofBadFieldListIndex(i, max int) *Goof {
	return Env.Goofs().MakeGoof("BAD_FIELD_LIST_INDEX",
		"Attempt to index element %d of FieldList of length %d",
		i, max)
}

func GoofInvalidNil(i interface{}) *Goof {
	return Env.Goofs().MakeGoof("INVALID_NIL",
		"The variable of type %T is not allowed to be nil", i)
}

func GoofBinaryCompatibility(ver *Version) *Goof {
	return Env.Goofs().MakeGoof("DECODING",
		"Binary encoding from version %v not compatible with "+
			"current version %v",
		ver, Env.Version)
}

/*
func GoofUnexpectedControl(expected CODE, expectedName string, ctrl CODE, ctrlName string) *Goof {
	return Env.Goofs().MakeGoof("UNEXPECTED_CONTROL_CODE",
		"Expecting control code (%d %s) but received (%d %s)",
		expected, expectedName,
		ctrl, ctrlName)
}
*/

// ENUM METHODS ================================================================

func (enum *Enum) MakeGoof(name, msg string, a ...interface{}) *Goof {
	goof := MakeGoof(3).MakeId()
	return enum.FormatGoofMessage(goof, name, msg, a...)
}

func (enum *Enum) FormatGoofMessage(goof *Goof, name, msg string, a ...interface{}) *Goof {
	goof.codestr = name
	code, found := enum.Number(name)
	goof.Code = CODE(code)
	prefix := "%s: "
	if !found {
		prefix = "***ENUM %q UNKNOWN***: "
	}
	prefix = fmt.Sprintf(prefix, name)
	goof.Msg = fmt.Sprintf(prefix+msg, a...)
	return goof
}

func (enum *Enum) WrapGoof(name string, err error, msg string, a ...interface{}) *Goof {
	return MakeGoof(4).MakeId().WrapGoof(name, enum, err, msg, a...)
}

func (goof *Goof) WrapGoof(name string, enum *Enum, err error, msg string, a ...interface{}) *Goof {
	eof := false
	goof2, isa := err.(*Goof)
	if isa {
		eof = goof2.EOF
	} else {
		if err == io.EOF {
			eof = true
		}
		goof2 = NewGoof()
		goof2.Msg = "WRAPPED: " + err.Error()
		goof2.NotEmpty()
		code, _ := enum.Number("WRAPPED")
		goof2.Code = CODE(code)
	}
	goof.SetEOF(eof)
	goof.wrapped = goof2
	return enum.FormatGoofMessage(goof, name, msg, a...)
}
