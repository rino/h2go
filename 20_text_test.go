package h2go

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
	"testing"
)

// TESTS ======================================================================

// Test parsing differing brackets.
func TestParsingDifferingBrackets(t *testing.T) {
	Env.Log().MajorSection("Test parsing of differing brackets")
	inputs := []string{
		"  <hello> < know>  ",
		"  <hello> < know>  <goodbye>",
		" <<<<<hello>>>   ", // extra left brackets shouldn't work, they must close
		"<<hello>>>   ",     // but excessive right brackets should be ok
		" <hello  > goodbye",
		" {a:{b},c:{d}}[e] ",
	}
	expected := [][]string{
		[]string{"hello", " know"},
		[]string{"hello", " know", "goodbye"},
		[]string{},
		[]string{"<hello>"},
		[]string{"hello  "},
		[]string{"a:{b},c:{d}"},
	}
	lims := []int{
		-1,
		-1,
		-1,
		-1,
		1,
		1,
	}
	rems := []string{
		"  ",
		"",
		"", // TODO not sure about this, revisit
		">   ",
		" goodbye",
		"[e] ",
	}
	lbrac := []string{
		"<",
		"<",
		"<",
		"<",
		"<",
		"{",
	}
	rbrac := []string{
		">",
		">",
		">",
		">",
		">",
		"}",
	}

	for i, input := range inputs {
		words, endpoints, rem :=
			ParseDifferingBrackets(input, lbrac[i], rbrac[i], lims[i], true)
		Env.Log().Basic(
			"input %q words = %v endpoints = %v", input, words, endpoints)
		if len(words) != len(expected[i]) {
			t.Fatalf("For input %q, expected %v word(s) but got %v",
				input, len(expected[i]), len(words))
		}
		if rem != rems[i] {
			t.Fatalf("For input %q, expected remainder %q but got %q",
				input, rems[i], rem)
		}
		for j, word := range words {
			//Env.Log().Basic("%v %q", j, word)
			if expected[i][j] != word {
				t.Fatalf("For input %q, expected word %v to be %q but got %q",
					input, j, expected[i][j], word)
			}
		}
	}
}

// Test parsing matching brackets.
func TestParsingSameBrackets(t *testing.T) {
	Env.Log().MajorSection("Test parsing of the same brackets")
	inputs := []string{
		"  (|x|) ",
		"  (|x|) |y|",
		"  (|x|) |y|",
	}
	expectedStrings := [][]string{
		[]string{"x"},
		[]string{"x", "y"},
		[]string{"x"},
	}
	expectedEnds := [][][]int{
		[][]int{[]int{3, 5}},
		[][]int{[]int{3, 5}, []int{8, 10}},
		[][]int{[]int{3, 5}},
	}
	lims := []int{
		-1,
		-1,
		1,
	}
	expectedRems := []string{
		") ",
		"",
		") |y|",
	}

	for i, input := range inputs {
		words, endpoints, rem :=
			ParseSameBrackets(input, "|", lims[i])
		Env.Log().Basic(
			"input %q words = %v endpoints = %v", input, words, endpoints)
		if len(words) != len(expectedStrings[i]) {
			t.Fatalf("For input %q, expected %v word(s) but got %v",
				input, len(expectedStrings[i]), len(words))
		}
		if rem != expectedRems[i] {
			t.Fatalf("For input %q, expected remainder %q but got %q",
				input, expectedRems[i], rem)
		}
		for j, word := range words {
			Env.Log().Basic("%v %q", j, word)
			if expectedStrings[i][j] != word {
				t.Fatalf("For input %q, expected word %v to be %q but got %q",
					input, j, expectedStrings[i][j], word)
			}
		}
		Env.Log().Basic("remainder = %q", rem)
		if expectedRems[i] != rem {
			t.Fatalf("For input %q, expected remainder to be %q but got %q",
				input, expectedRems[i], rem)
		}
		for j, points := range endpoints {
			Env.Log().Basic("%v %v", j, points)
			for k := 0; k < 2; k++ {
				if expectedEnds[i][j][k] != points[k] {
					t.Fatalf(
						"For input %q, expected endpoint %v for string %v "+
							"to be %d but got %d",
						input, k, j, expectedEnds[i][j][k], points[k])
				}
			}
		}
	}
}

// Test parsing prefixed labels.
func TestParsingPrefixedLabels(t *testing.T) {
	Env.Log().MajorSection("Test parsing of prefixed labels (e.g. $label)")
	inputs := []string{
		"($param.field1) ",
		"($$param.field1) ",
		"$a.field1+$b:parent-$c[3]",
		"$a.field1+$b:parent-$c[3]",
		"{$(a:b):value1,field2:$c[3],field3:4}",
		`<"mydoc" {field5:[$id1, $node2:id],field6:val2}>`,
		`<"mydoc" {field5:[$id1, $(node2:id)],field6:val2}>`,
	}
	expectedStrings := [][]string{
		[]string{"param.field1"},
		[]string{},
		[]string{"a.field1", "b:parent", "c[3]"},
		[]string{"a.field1", "b:parent"},
		[]string{"a:b", "c[3]"},
		[]string{"id1", "node2:id"},
		[]string{"id1", "node2:id"},
	}
	expectedEnds := [][][]int{
		[][]int{[]int{1, 13}},
		[][]int{[]int{}},
		[][]int{[]int{0, 8}, []int{10, 18}, []int{20, 24}},
		[][]int{[]int{0, 8}, []int{10, 18}},
		[][]int{[]int{1, 6}, []int{22, 26}},
		[][]int{[]int{18, 21}, []int{24, 32}},
		[][]int{[]int{18, 21}, []int{24, 34}},
	}
	lims := []int{
		-1,
		-1,
		-1,
		2,
		-1,
		-1,
		-1,
	}
	expectedRems := []string{
		") ",
		"",
		"",
		"-$c[3]",
		",field3:4}",
		"],field6:val2}>",
		"],field6:val2}>",
	}

	for i, input := range inputs {
		labels, endpoints, rem :=
			ParsePrefixedLabels(input, "$", Env.Language(), lims[i])
		Env.Log().Basic(
			"input %q labels = %q endpoints = %v rem = %v",
			input, labels, endpoints, rem)
		if len(labels) != len(expectedStrings[i]) {
			t.Fatalf("For input %q, expected %v label(s) but got %v",
				input, len(expectedStrings[i]), len(labels))
		}
		if rem != expectedRems[i] {
			t.Fatalf("For input %q, expected remainder %q but got %q",
				input, expectedRems[i], rem)
		}
		for j, label := range labels {
			Env.Log().Basic("%v %q", j, label)
			if expectedStrings[i][j] != label {
				t.Fatalf("For input %q, expected label %v to be %q but got %q",
					input, j, expectedStrings[i][j], label)
			}
		}
		Env.Log().Basic("remainder = %q", rem)
		if expectedRems[i] != rem {
			t.Fatalf("For input %q, expected remainder to be %q but got %q",
				input, expectedRems[i], rem)
		}
		for j, points := range endpoints {
			Env.Log().Basic("%v %v", j, points)
			for k := 0; k < 2; k++ {
				if expectedEnds[i][j][k] != points[k] {
					t.Fatalf(
						"For input %q, expected endpoint %v for string %v "+
							"to be %d but got %d",
						input, k, j, expectedEnds[i][j][k], points[k])
				}
			}
		}
	}
}

// Test removing double quoted text.
func TestRemovingDoubleQuoted(t *testing.T) {
	Env.Log().MajorSection("Test removal of text in double quotes")
	inputs := []string{
		`"here is some text", only this bit should remain`,
		`  "hello   "" goodbye " jan feb "mar april`,
		`"dear me"`,
		`  "1" "2" "3" "4" "5" `,
	}
	expected := []string{
		`, only this bit should remain`,
		`   jan feb `,
		``,
		`    "3" "4" "5" `,
	}
	lims := []int{
		-1,
		-1,
		-1,
		2,
	}
	for i, input := range inputs {
		txt := RemoveDoubleQuoted(input, lims[i])
		if txt != expected[i] {
			t.Fatalf("In removing double quoted text from %q, "+
				"expected %q, but got %q", input, expected[i], txt)
		}
	}
}

// Test removing comments from zbconsole script input.
func TestRemovingComments(t *testing.T) {
	Env.Log().MajorSection("Test removing comments")
	inputs := []string{
		"here is some text, // only this \nbit should remain",
		"text, // only this \nbit //should remain",
		"// only this \nbit //",
		"/ only this \nbit //",
		"/ only this \nbit /",
	}
	expected := []string{
		"here is some text, bit should remain",
		"text, bit ",
		"bit ",
		"/ only this \nbit ",
		"/ only this \nbit /",
	}
	for i, input := range inputs {
		txt := RemoveComments(input)
		if txt != expected[i] {
			t.Fatalf("In removing comments from %q, "+
				"expected %q, but got %q", input, expected[i], txt)
		}
	}
}

// Test splitting outside double quotes.
func TestSplitting(t *testing.T) {
	Env.Log().MajorSection("Test splitting text")
	inputs := []string{
		"this is a test",                                             // 0
		"this is a test;",                                            // 1
		"this is; a test;",                                           // 2
		"this is; a test; big deal",                                  // 3
		`    n1 = <"node1"  {     "field1": 1,    "field2": 2,  }>;`, // 4
		`n1:id`, // 5
		`x = 3`, // 6
		"path/to/zbase as   user with alias myzbase",      // 7
		`path/to/zbase as "user name" with alias myzbase`, // 8
		`path/to/zbase as user with alias "myzbase`,       // 9
		`a b`, // 10
	}
	splitters := []string{
		";", // 0
		";", // 1
		";", // 2
		";", // 3
		";", // 4
		":", // 5
		"=", // 6
		" ", // 7
		" ", // 8
		" ", // 9
		" ", // 10
	}
	lims := []int{
		-1, // 0
		-1, // 1
		-1, // 2
		2,  // 3
		-1, // 4
		-1, // 5
		-1, // 6
		-1, // 7
		-1, // 8
		-1, // 9
		-1, // 10
	}
	expectedNumberParts := []int{
		1, // 0
		1, // 1
		2, // 2
		2, // 3
		1, // 4
		2, // 5
		2, // 6
		7, // 7
		6, // 8
		6, // 9
		2, // 10
	}
	expectedRem := []string{
		"",          // 0
		"",          // 1
		"",          // 2
		" big deal", // 3
		"",          // 4
		"",          // 5
		"",          // 6
		"",          // 7
		"",          // 8
		"",          // 9
		"",          // 10
	}
	for i, input := range inputs {
		parts, _, rem, _ := SplitOutsideDoubleQuotes(input, splitters[i], lims[i])
		errprefix := fmt.Sprintf(
			"When splitting item %d, %#q, with %q, parts = %#q, rem = %#q",
			i, input, splitters[i], parts, rem)
		if len(parts) != expectedNumberParts[i] {
			t.Fatalf(
				"%s, number of parts should be %d, got %d",
				errprefix, expectedNumberParts[i], len(parts))
		}
		if rem != expectedRem[i] {
			t.Fatalf(
				"%s, remainder should be %#q, got %#q",
				errprefix, expectedRem[i], rem)
		}
	}
}

// Test argument magnet.
func TestExtractKeywordArgs(t *testing.T) {
	Env.Log().MajorSection("Test extracting keyword arguments")
	inputs := [][]string{
		[]string{"path/to/zbase", "as", "user", "with", "alias", "myzbase"},
		[]string{"path/to/zbase", "with", "alias", "myzbase"},
	}
	keywords := [][]string{
		[]string{"as"},
		[]string{"as"},
	}
	nreq := []int{
		1,
		1,
	}
	expectedArgs := [][]string{
		[]string{"user"},
		[]string{},
	}
	expectedRem := [][]string{
		[]string{"path/to/zbase", "with", "alias", "myzbase"},
		[]string{"path/to/zbase", "with", "alias", "myzbase"},
	}
	for i, input := range inputs {
		args, rem, goof := ExtractKeywordArgs(keywords[i], input, nreq[i])
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem extracting keyword args from %#q", input)
		}
		errprefix := fmt.Sprintf(
			"When extracting keyword arguments from %#q, returned "+
				"arguments = %#q, remaining arguments = %#q", input, args, rem)
		if len(args) != len(expectedArgs[i]) {
			t.Fatalf(
				"%s, number of returned args should be %d, got %d",
				errprefix, len(expectedArgs[i]), len(args))
		}
		if len(rem) != len(expectedRem[i]) {
			t.Fatalf(
				"%s, number of remaining words should be %d, got %d",
				errprefix, len(expectedRem[i]), len(rem))
		}
		for j, expected := range expectedArgs[i] {
			if expected != args[j] {
				t.Fatalf(
					"%s, returned argument %d should be %#q, got %#q",
					errprefix, j, expected, args[j])
			}
		}
		for j, expected := range expectedRem[i] {
			if expected != rem[j] {
				t.Fatalf(
					"%s, remaining word %d should be %#q, got %#q",
					errprefix, j, expected, rem[j])
			}
		}
	}
}

func TestParsing(t *testing.T) {
	Env.Log().MajorSection("Test parsing of values, fields and field maps/lists")
	ws, goof := Env.Types().NewVirtualWorkspace()
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating new blank workspace")
	}
	// Singletons and tuples
	//in := `"node:hello"`
	//vf, goof := ProcessText(in, ws)
	//checkValue(t, goof, in, vf, reflect.String, Env.Types().Number["STRING"])
	in := `123`
	vf := ws.ProcessText(in, NewField(), false)
	checkValue(t, ws.LastGoof(), in, vf, reflect.Int64, Env.Types().Number["int64"])
	in = `123.45`
	vf = ws.ProcessText(in, NewField(), false)
	checkValue(t, ws.LastGoof(), in, vf, reflect.Float64, Env.Types().Number["float64"])
	in = `(12  , UINT8 )`
	vf = ws.ProcessText(in, NewField(), false)
	checkValue(t, ws.LastGoof(), in, vf, reflect.Uint8, Env.Types().Number["UINT8"])

	Env.Log().MinorSection("Simplest FieldMap")
	in = `{"field1":"val1","field2":"val2"}`
	fmap := Env.Types().MintFieldMap()
	fmap.AutoSet("field1", "val1")
	fmap.AutoSet("field2", "val2")
	CompareDecodedString(t, in, fmap, "FIELDMAP")

	Env.Log().MinorSection("FieldMap with nested typed value")
	in = `{field3:(123,uint16),field4:val2}`
	fmap = Env.Types().MintFieldMap()
	fmap.AutoSet("field3", uint16(123))
	fmap.AutoSet("field4", "val2")
	CompareDecodedString(t, in, fmap, "FIELDMAP")

	Env.Log().MinorSection("Simplest FieldList")
	in = `[val1,val2,"val3 hello"]`
	flist := Env.Types().MintFieldList()
	flist.AutoAdd("val1")
	flist.AutoAdd("val2")
	flist.AutoAdd("val3 hello")
	CompareDecodedString(t, in, flist, "FIELDLIST")

	Env.Log().MinorSection("FieldMap with nested FieldList and a blank value")
	in = `{field5:([val1, val2, "val3 hello"],FIELDLIST), field6:val2}`
	fmap = Env.Types().MintFieldMap()
	fmap.AutoSet("field5", flist)
	fmap.AutoSet("field6", "val2")
	CompareDecodedString(t, in, fmap, "FIELDMAP")

	lines := fmap.MultilineString("", "")
	Env.Log().Dump(lines, "FieldMap Multiline")

	Env.Log().MinorSection("FieldList with nested FieldMap and a blank field")
	in = `["val1","val2", "", {field5:3,field6:val2}]`
	fmap = Env.Types().MintFieldMap()
	fmap.AutoSet("field5", 3)
	fmap.AutoSet("field6", "val2")
	flist = Env.Types().MintFieldList()
	flist.AutoAdd("val1")
	flist.AutoAdd("val2")
	flist.AutoAdd("")
	flist.AutoAdd(fmap)
	CompareDecodedString(t, in, flist, "FIELDLIST")
}

func TestDirectStringDecode(t *testing.T) {
	Env.Log().MajorSection("Test direct string decoding of a simple FieldMap")
	in := `{"field1":"val1","field2":"val2"}`
	fmap2 := Env.Types().MintFieldMap()
	fmap2.AutoSet("field1", "val1")
	fmap2.AutoSet("field2", "val2")
	fmap := Env.Types().MintFieldMap()
	ws, goof := Env.Types().NewVirtualWorkspace()
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating new blank workspace")
	}
	_, goof = fmap.DecodeString(in,
		NewBox().Set(BOX_ITEM_WORKSPACE_ACTION, ws.MakeAction(NewField(), false)))
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decoding %T", fmap)
	}
	if !fmap.Equals(fmap2) {
		t.Fatalf("While decoding %T, expected %v but got %v", fmap, fmap2, fmap)
	}
}

func TestWorkspace(t *testing.T) {
	Env.Log().MajorSection("Test a Workspace")
	err := os.Chdir(testHome)
	if Env.Log().IsError(err) {
		t.Fatalf("Problem changing to dir %q", testHome)
	}
	filename1 := "test1.h2go"
	filename2 := "./test/test2.h2go"
	file1, err := os.Create(filename1)
	if Env.Log().IsError(err) {
		t.Fatalf("Problem opening file %q", filename1)
	}
	file2, err := os.Create(filename2)
	if Env.Log().IsError(err) {
		t.Fatalf("Problem opening file %q", filename2)
	}
	filetext1 := `
a = 34;
b = 2;
c = 45;
run test/test2.h2go;
`
	_, err = io.WriteString(file1, filetext1)
	if Env.Log().IsError(err) {
		t.Fatalf("Problem writing file %q", filename1)
	}
	filetext2 := `
d = $a;
e = $b;
f := $c;
cd test;
`
	_, err = io.WriteString(file2, filetext2)
	if Env.Log().IsError(err) {
		t.Fatalf("Problem writing file %q", filename2)
	}

	ws, goof := Env.Types().MakeWorkspace(testHome)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating new blank workspace")
	}
	ws.Run(filename1, "test")
	if ws.HasGoofs() {
		t.Fatalf("Problem processing command: %v", ws.FirstGoof())
	}
	if !ws.Get("a").Equals(ws.Get("d")) {
		t.Fatalf("Expected %v for parameter %q but got %v",
			ws.Get("a"), "d", ws.Get("d"))
	}
	if !ws.Get("b").Equals(ws.Get("e")) {
		t.Fatalf("Expected %v for parameter %q but got %v",
			ws.Get("b"), "e", ws.Get("e"))
	}
	cwd, err := os.Getwd()
	if Env.Log().IsError(err) {
		t.Fatalf("Problem getting current dir")
	}
	if cwd != testDir {
		t.Fatalf("Expected to be in dir %q but am in %q",
			testDir, cwd)
	}
	err = os.Chdir(testHome)
	if Env.Log().IsError(err) {
		t.Fatalf("Problem changing to dir %q", testHome)
	}
	err = os.Remove(filename1)
	if Env.Log().IsError(err) {
		t.Fatalf("Problem deleting file %q", filename1)
	}
	expected := Env.Types().BuildField("$c", "WORKSPACE_FN")
	if !ws.Get("f").Equals(expected) {
		t.Fatalf("Expected %v for parameter %q but got %v",
			expected, "f", ws.Get("f"))
	}
}

func TestWriteWorkspace(t *testing.T) {
	Env.Log().MajorSection("Test pretty printing a Workspace")
	bfr := MakeByteBuffer(BYTEORDER)

	flist := Env.Types().MintFieldList()
	flist.AutoAdd("val1")
	flist.AutoAdd("val2")
	fmap0 := Env.Types().MintFieldMap()
	fmap0.AutoSet("kA", true)
	fmap0.AutoSet("kB", false)
	fmap0.AutoSet("kC", flist)
	fmap1 := Env.Types().MintFieldMap()
	fmap1.AutoSet("key1", UINT8(3))
	fmap1.AutoSet("key2", fmap0)
	fmap2 := Env.Types().MintFieldMap()
	fmap2.AutoSet("VAR1", flist)
	fmap2.AutoSet("VAR2", fmap1)
	fmap2.AutoSet("VAR3", 13)

	expected :=
		`// Test writing of workspace
"VAR1" = [
	"val1",
	"val2",
];
"VAR2" = {
	"key1": (3,UINT8),
	"key2": {
		"kA": TRUE,
		"kB": FALSE,
		"kC": [
			"val1",
			"val2",
		],
	},
};
"VAR3" = (13,int64);`

	ws, goof := Env.Types().NewVirtualWorkspace()
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating new blank workspace")
	}
	ws.FieldMap = fmap2
	goof = ws.Write(bfr.Raw(), "", "// Test writing of workspace")
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem writing workspace")
	}
	byts, _ := bfr.Bytes()
	if string(byts) != expected {
		lines := strings.Split(expected, "\n")
		Env.Log().Dump(lines, "EXPECTED:")
		lines = strings.Split(string(byts), "\n")
		Env.Log().Dump(lines, "RESULT:")
		t.Fatalf("Problem writing workspace, result not as expected")
	}
}

func TestTabCompletion(t *testing.T) {
	Env.Log().MajorSection("Test tab completion for a Workspace")
	Env.Log().Basic("Tab Completion")
	err := os.Chdir(testHome)
	if Env.Log().IsError(err) {
		t.Fatalf("Problem changing to home directory")
	}
	ws, goof := Env.Types().MakeWorkspace(testHome)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating new blank workspace")
	}
	line := `help r`
	alts := ws.TabCompleter(line)
	expected := []string{"help run"}
	Env.Log().Basic("Comparing tab completions for: %q", line)
	ws.CompareTabCompletions(t, alts, expected)
	ws.ClearGoofs()

	line = `cd te`
	alts = ws.TabCompleter(line)
	expected = []string{"cd test"}
	Env.Log().Basic("Comparing tab completions for: %q", line)
	ws.CompareTabCompletions(t, alts, expected)
	ws.ClearGoofs()
}

func TestLowestNotDotBranch(t *testing.T) {
	Env.Log().MajorSection("Test path parsing of lowest not-dot branch")
	paths := []string{
		"a/b/c/.d/.e",
		"",
		".",
		"/.c/.d/.e",
	}
	expecteds := []string{
		"c/.d/.e",
		"",
		"",
		"/.c/.d/.e",
	}
	result := ""
	expected := ""
	for i, path := range paths {
		result = LowestNotDotBranch(path)
		expected = expecteds[i]
		if result != expected {
			t.Fatalf("Expected %q, got %q", expected, result)
		}
	}
}

func TestInsertString(t *testing.T) {
	Env.Log().MajorSection("Test string insertion")
	s0 := "0123456789"
	i0 := 3
	d0 := 2
	s1 := "___"
	result, _ := InsertString(s0, s1, i0, d0, 0)
	expected := "012___56789"
	if result != expected {
		t.Fatalf("After inserting %q into %q at index %d after deleting %d chars, "+
			"expected %q, got %q",
			s1, s0, i0, d0, expected, result)
	}
}

func checkValue(t *testing.T, goof *Goof, in string, v *Field, reqkind reflect.Kind, reqtype CODE) {
	Env.Log().MinorSection("Checking parsing of %q", in)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem parsing %s", in)
	}
	if v == nil {
		t.Fatalf("Problem parsing %s: nil returned", in)
	}
	if reflect.TypeOf(v.Value()).Kind() != reqkind {
		t.Fatalf("Parsed value %v native type should be %v but is %T",
			v, reqkind, v)
	}
	if v.Type() != reqtype {
		t.Fatalf("Parsed value %v type should be %q but is %q",
			v, Env.Types().Name(reqtype), Env.Types().Name(v.Type()))
	}
	return
}
