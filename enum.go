/*
	Enumeration, or bidirectional map.
*/
package h2go

import (
	"errors"
	"math"
	"sort"
	"strconv"
)

// ENUM ITEM ===================================================================

type EnumItem struct {
	name  string
	index uint64
	info  map[string]interface{}
}

var _ Binaryable = NewEnumItem()

func NewEnumItem() *EnumItem {
	return &EnumItem{
		info: make(map[string]interface{}),
	}
}

func MakeEnumItem(name string, index uint64) *EnumItem {
	enum := NewEnumItem()
	enum.name = name
	enum.index = index
	return enum
}

func (item *EnumItem) Name() string   { return item.name }
func (item *EnumItem) Number() uint64 { return item.index }
func (item *EnumItem) GetInfo(key string) (interface{}, bool) {
	obj, exists := item.info[key]
	if exists {
		return obj, true
	}
	return 0, false
}

func (item *EnumItem) SetInfo(key string, obj interface{}) bool {
	_, exists := item.info[key]
	item.info[key] = obj
	return exists
}

func (item *EnumItem) SetInfoMap(info map[string]interface{}) *EnumItem {
	item.info = info
	return item
}

func (item *EnumItem) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	return UINT(item.index).EncodeBinary(bfr, box)
}

func (item *EnumItem) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	ui := NewUINT()
	out, goof := ui.DecodeBinary(bfr, box)
	if IsError(goof) {
		return out, goof
	}
	item.index = uint64(ui)
	return out, NO_GOOF
}

// ENUMERATION =================================================================

// A general purpose, flexible enumerator. We can't use Goofs here because Goof
// is an Enum.
type Enum struct {
	fwd      map[uint64]*EnumItem
	rev      map[string]*EnumItem
	nbytes   uint8 // Number of bytes for encoding of index
	next     uint64
	maxindex uint64
	*State
}

func NewEnum() *Enum {
	return &Enum{
		fwd:    make(map[uint64]*EnumItem),
		rev:    make(map[string]*EnumItem),
		nbytes: 8,
		State:  NewState(),
	}
}

func MakeEnum(nbytes uint8, names []string) (*Enum, error) {
	enum := NewEnum()
	enum.nbytes = nbytes
	var err error
	enum.maxindex, err = enum.maxIndex()
	if IsError(err) {
		return enum, err
	}
	enum.NotEmpty()
	sort.Strings(names)
	item := NewEnumItem()
	for i, name := range names {
		if enum.next > enum.maxindex {
			return enum, errors.New(
				"Could only add " + strconv.Itoa(i) +
					" item(s), index exceeds Enum capacity of " +
					strconv.FormatUint(enum.maxindex, 10))
		}
		item = MakeEnumItem(name, enum.next)
		enum.fwd[enum.next] = item
		enum.rev[name] = item
		enum.next++
	}
	enum.NowComplete()
	return enum, nil
}

func BuildEnum(nbytes uint8, items map[string]map[string]interface{}) (*Enum, error) {
	enum := NewEnum()
	enum.nbytes = nbytes
	var err error
	enum.maxindex, err = enum.maxIndex()
	if IsError(err) {
		return enum, err
	}
	enum.NotEmpty()
	names := []string{}
	for name, _ := range items {
		names = append(names, name)
	}
	sort.Strings(names)
	item := NewEnumItem()
	for i, name := range names {
		if enum.next > enum.maxindex {
			return enum, errors.New(
				"Could only add " + strconv.Itoa(i) +
					" item(s), index exceeds Enum capacity of " +
					strconv.FormatUint(enum.maxindex, 10))
		}
		item = MakeEnumItem(name, enum.next).SetInfoMap(items[name])
		enum.fwd[enum.next] = item
		enum.rev[name] = item
		enum.next++
	}
	enum.NowComplete()
	return enum, nil
}

func (enum *Enum) Len() int { return len(enum.fwd) }

func (enum *Enum) Get(index uint64) (*EnumItem, bool) {
	enum.RLock()
	defer enum.RUnlock()
	eitem, exists := enum.fwd[index]
	return eitem, exists
}

func (enum *Enum) Exists(name string) bool {
	enum.RLock()
	defer enum.RUnlock()
	_, exists := enum.rev[name]
	return exists
}

func (enum *Enum) Name(index uint64) (string, bool) {
	enum.RLock()
	defer enum.RUnlock()
	item, exists := enum.fwd[index]
	if exists {
		return item.Name(), true
	}
	return "!!UNKNOWN!!", false
}

func (enum *Enum) Names() []string {
	enum.RLock()
	defer enum.RUnlock()
	result := []string{}
	for k, _ := range enum.rev {
		result = append(result, k)
	}
	return result
}

func (enum *Enum) SortedNames() []string {
	result := enum.Names()
	sort.Strings(result)
	return result
}

func (enum *Enum) Number(name string) (uint64, bool) {
	enum.RLock()
	defer enum.RUnlock()
	item, exists := enum.rev[name]
	if exists {
		return item.Number(), true
	}
	return 0, false
}

func (enum *Enum) GetInfo(name, key string) (interface{}, bool) {
	enum.RLock()
	defer enum.RUnlock()
	item, exists := enum.rev[name]
	if exists {
		return item.GetInfo(key)
	}
	return 0, false
}

func (enum *Enum) Copy() *Enum {
	enum.RLock()
	defer enum.RUnlock()
	enum2 := NewEnum()
	for k, v := range enum.fwd {
		enum2.fwd[k] = v
	}
	for k, v := range enum.rev {
		enum2.rev[k] = v
	}
	enum2.nbytes = enum.nbytes
	enum2.next = enum.next
	enum2.maxindex = enum.maxindex
	enum2.State = enum.State.Copy()
	return enum2
}

func (enum *Enum) maxIndex() (uint64, error) {
	switch enum.nbytes {
	case 0:
		return 0, errors.New("Byte number cannot be zero")
	case 1:
		return uint64(math.MaxUint8), nil
	case 2:
		return uint64(math.MaxUint16), nil
	case 4:
		return uint64(math.MaxUint32), nil
	case 8:
		return uint64(math.MaxUint64), nil
	default:
		return 0, errors.New("Byte number " +
			strconv.FormatUint(uint64(enum.nbytes), 10) +
			" invalid, use 1, 2, 4 or 8")
	}
}

func (enum *Enum) Add(items []string) error {
	enum.Lock()
	defer enum.Unlock()
	item := NewEnumItem()
	for i, name := range items {
		if enum.next > enum.maxindex {
			return errors.New(
				"Could only add " + strconv.Itoa(i) +
					" item(s), index exceeds Enum capacity of " +
					strconv.FormatUint(enum.maxindex, 10))
		}
		item = MakeEnumItem(name, enum.next)
		enum.fwd[enum.next] = item
		enum.rev[name] = item
		enum.next++
	}
	return nil
}

// Can only be used when Goofs are well defined.
func (enum *Enum) NumberNotFound(index uint64) *Goof {
	return Env.Goofs().MakeGoof("BAD_INDEX",
		"%T has %d members and a capacity of %d, index %d not found",
		enum, enum.Len(), enum.maxindex, index)
}

// Can only be used when Goofs are well defined.
func (enum *Enum) NameNotFound(name string) *Goof {
	return Env.Goofs().MakeGoof("BAD_NAME",
		"%T does not contain %q",
		enum, name)
}

// UINT8 ENUM ITEM =============================================================

// A more specialised enumerator mainly for internal use.
type Uint8Enum struct {
	FromNumber map[uint8][]string
	FromString map[string]uint8
	*State
}

var _ Stateful = NewUint8Enum() // embedded State

func NewUint8Enum() Uint8Enum {
	return Uint8Enum{
		FromNumber: make(map[uint8][]string),
		FromString: make(map[string]uint8),
		State:      NewState(),
	}
}

func (enum Uint8Enum) Copy() Uint8Enum {
	enum2 := NewUint8Enum()
	for k, v := range enum.FromNumber {
		for _, str := range v {
			enum2.FromNumber[k] = append(enum2.FromNumber[k], str)
		}
	}
	for k, v := range enum.FromString {
		enum2.FromString[k] = v
	}
	enum2.State = enum.State.Copy()
	return enum2
}

func MakeUint8Enum(phrases [][]string) Uint8Enum {
	enum := NewUint8Enum()
	if len(phrases) > 0 {
		n := uint8(0)
		for i, phraselist := range phrases {
			n = uint8(i + 1)
			enum.FromNumber[n] = phraselist
			for _, phrase := range phraselist {
				enum.FromString[phrase] = n
			}
		}
		enum.NotEmpty()
		enum.NowComplete()
	}
	return enum
}

func (enum Uint8Enum) Len() uint8 {
	return uint8(len(enum.FromNumber))
}
