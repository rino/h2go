/*
	Encoding/decoding objects and methods.

	Decoders for types defined in Env.Types should always include the decoding
	target in Representation.  Binary encoders/decoders really deal with byte
	streams, whereas string encoders/decoders deal with a chunk of text and
	can look ahead.  The idea here is to treat them as uniformly as possible.
*/
package h2go

import (
	"bytes"
	"compress/gzip"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io"
	"math"
	"math/big"
	"reflect"
	"sort"
	"strconv"
)

// REPRESENTATION ==============================================================
// A Representation at a minimum counts encoded/decoded bytes but can also
// record encodings for debugging purposes.
type Representation interface {
	SetRecording(on bool)
	IsRecording() bool
	IsEncoding(key interface{}) bool
	WhichEncoding() interface{}
	ChooseEncoding(key interface{}) Representation
	ListEncodingKeys() []string
	AppendEncodedTo(key, val interface{})
	AppendEncodedToAs(key interface{}, label string, val interface{})
	AppendEncodedAs(label string, val interface{})
	AppendEncoded(val interface{})
	AppendCopy(fromkey, tokey interface{})
	AppendCopyAllFrom(from Representation, prefix string)
	PrependEncoded(val interface{})
	PrependEncodedAs(label string, val interface{})
	GetDecoded() *Field
	SetField(field *Field)
	AutoSetField(val interface{})
	Len(key interface{}) int
	EncodingToStringSlice(maxlen int) []string
	Equible
	Stateful
}

type LabelledBytes struct {
	label string
	byts  []byte
}

func NewLabelledBytes() *LabelledBytes {
	return &LabelledBytes{
		label: "",
		byts:  []byte{},
	}
}

func MakeLabelledBytes(label string, byts []byte) *LabelledBytes {
	lbyts := NewLabelledBytes()
	lbyts.label = label
	lbyts.byts = byts
	return lbyts
}

func (lbyts *LabelledBytes) StampLabel(prefix string) *LabelledBytes {
	if len(prefix) == 0 {
		return lbyts
	}
	if len(lbyts.label) == 0 {
		lbyts.label = prefix
	} else {
		lbyts.label = prefix + ", " + lbyts.label
	}
	return lbyts
}

// BINARY REPRESENTATION -------------------------------------------------------
// Captures the encoding of a Field, usually as a []byte.
// The Field is not really needed for encoding/decoding methods where we already
// have access to the source object (the method receiver).
type BinaryRepresentation struct {
	record  bool // Record Mode on? Captures not just encoding size, but encoding
	decoded *Field
	encoded map[interface{}]([]*LabelledBytes)
	length  map[interface{}]int
	currkey interface{}
	*State
}

var _ Representation = NewBinaryRepresentation()

// Constructors
func NewBinaryRepresentation() *BinaryRepresentation {
	return &BinaryRepresentation{
		record:  false,
		decoded: NewField(),
		encoded: map[interface{}]([]*LabelledBytes){0: []*LabelledBytes{}},
		length:  map[interface{}]int{0: 0},
		currkey: int(0),
		State:   NewState(),
	}
}

func MakeBinaryRepresentation(record bool) *BinaryRepresentation {
	rep := NewBinaryRepresentation()
	rep.record = record
	rep.NotEmpty()
	return rep
}

func BuildBinaryRepresentation(field *Field, label string, byts []byte, record bool) *BinaryRepresentation {
	rep := MakeBinaryRepresentation(record)
	rep.decoded = field
	rep.length = map[interface{}]int{0: len(byts)}
	rep.currkey = int(0)
	if record {
		rep.encoded = map[interface{}]([]*LabelledBytes){
			0: []*LabelledBytes{MakeLabelledBytes(label, byts)},
		}
	} else {
		rep.encoded = map[interface{}]([]*LabelledBytes){0: []*LabelledBytes{}}
	}
	rep.NowComplete()
	return rep
}

func (rep *BinaryRepresentation) SetRecording(on bool) {
	rep.record = on
	return
}
func (rep *BinaryRepresentation) IsRecording() bool { return rep.record }

func (rep *BinaryRepresentation) IsEncoding(key interface{}) bool {
	_, present := rep.encoded[key]
	return present
}

func (rep *BinaryRepresentation) WhichEncoding() interface{} { return rep.currkey }

// If the key does not exist, make the map entry.
func (rep *BinaryRepresentation) ChooseEncoding(key interface{}) Representation {
	_, present := rep.encoded[key]
	if !present {
		rep.encoded[key] = []*LabelledBytes{}
	}
	rep.currkey = key
	return rep
}

func (rep *BinaryRepresentation) ListEncodingKeys() []string {
	result := []string{}
	for k, _ := range rep.encoded {
		result = append(result, fmt.Sprintf("%v (%T)", k, k))
	}
	sort.Strings(result)
	return result
}

// Accepts a single key or a key slice.
func (rep *BinaryRepresentation) AppendEncodedTo(key, val interface{}) {
	rep.AppendEncodedToAs(key, "", val)
	return
}

// Accepts a single key or a key slice.
// Label ignored if val is another BinaryRepresentation.
func (rep *BinaryRepresentation) AppendEncodedToAs(key interface{}, label string, val interface{}) {
	switch k0 := key.(type) {
	case []interface{}:
		isbyts := false
		for i, k1 := range k0 {
			_, isbyts = val.([]byte)
			if i == 0 || isbyts {
				rep.appendEncodedToAs(k1, label, val)
			} else {
				// Stamp label only once
				rep.appendEncodedToAs(k1, "", val)
			}
		}
	default:
		rep.appendEncodedToAs(key, label, val)
	}
	return
}

// Label ignored if val is another BinaryRepresentation.
func (rep *BinaryRepresentation) AppendEncodedAs(label string, val interface{}) {
	rep.appendEncodedToAs(rep.currkey, label, val)
	return
}

// Appends to the current level.
func (rep *BinaryRepresentation) AppendEncoded(val interface{}) {
	rep.appendEncodedToAs(rep.currkey, "", val)
	return
}

// The key is assumed to exist.  Takes either another BinaryRepresentation,
// using its current encoding, or a byte slice, as a val argument.
func (rep *BinaryRepresentation) appendEncodedToAs(key interface{}, label string, val interface{}) {
	_, present := rep.encoded[key]
	if !present {
		rep.encoded[key] = []*LabelledBytes{}
	}
	switch other := val.(type) {
	case *BinaryRepresentation:
		for _, lbyts := range other.encoded[other.currkey] {
			if rep.record {
				rep.encoded[key] = append(rep.encoded[key], lbyts.StampLabel(label))
			}
			rep.length[key] += len(lbyts.byts)
		}
	case []byte:
		if rep.record {
			rep.encoded[key] =
				append(rep.encoded[key], MakeLabelledBytes(label, other))
		}
		rep.length[key] += len(other)
	}
	return
}

// Append a copy of the encoding from one key to another.
func (rep *BinaryRepresentation) AppendCopy(fromkey, tokey interface{}) {
	if rep.IsEncoding(fromkey) && fromkey != tokey {
		if enc, ok := rep.encoded[fromkey]; ok {
			_, ok2 := rep.encoded[tokey]
			if !ok2 {
				rep.encoded[tokey] = []*LabelledBytes{}
			}
			for _, lbyts := range enc {
				if rep.record {
					rep.encoded[tokey] = append(rep.encoded[tokey], lbyts)
				}
				rep.length[tokey] += len(lbyts.byts)
			}
		}
	}
	return
}

// Append a copy of all encodings in the given Representation to the current
// Representation, using a new string key with the given prefix.  Does not
// check if recording, that's the caller's responsibility.
func (rep *BinaryRepresentation) AppendCopyAllFrom(from Representation, prefix string) {
	rep2, isa := from.(*BinaryRepresentation)
	if !isa {
		return
	}
	for k2, enc2 := range rep2.encoded {
		k1 := fmt.Sprintf(prefix+"%v", k2)
		_, present := rep.encoded[k1]
		if !present {
			rep.encoded[k1] = []*LabelledBytes{}
		}
		for _, lbyts2 := range enc2 {
			rep.encoded[k1] = append(rep.encoded[k1], lbyts2)
			rep.length[k1] += len(lbyts2.byts)
		}
	}
	return
}

// Label ignored if val is another BinaryRepresentation.
func (rep *BinaryRepresentation) PrependEncoded(val interface{}) {
	rep.prependEncodedAs("", val)
	return
}

// Label ignored if val is another BinaryRepresentation.
func (rep *BinaryRepresentation) PrependEncodedAs(label string, val interface{}) {
	rep.prependEncodedAs(label, val)
	return
}

// Inserts the encoding to the front of the encoded slice, using the
// current Representation encoding key(s).
func (rep *BinaryRepresentation) prependEncodedAs(label string, val interface{}) {
	switch other := val.(type) {
	case *BinaryRepresentation:
		rep.length[rep.currkey] += other.length[other.currkey]
		if rep.record {
			v0 := other.encoded[other.currkey]
			for _, lbyts := range rep.encoded[rep.currkey] {
				v0 = append(v0, lbyts)
			}
			rep.encoded[rep.currkey] = v0
		}
	case []byte:
		rep.length[rep.currkey] += len(other)
		if rep.record {
			v0 := []*LabelledBytes{MakeLabelledBytes(label, other)}
			for _, lbyts := range rep.encoded[rep.currkey] {
				v0 = append(v0, lbyts)
			}
			rep.encoded[rep.currkey] = v0
		}
	}
	return
}

func (rep *BinaryRepresentation) GetDecoded() *Field {
	return rep.decoded
}

func (rep *BinaryRepresentation) SetField(field *Field) {
	rep.decoded = field
	return
}

func (rep *BinaryRepresentation) AutoSetField(val interface{}) {
	rep.decoded = Env.Types().AutoField(val)
	return
}

func (rep *BinaryRepresentation) Len(key interface{}) int {
	return rep.length[key]
}

// The maximum number of bytes to convert to a string for each
// fragment must be specified.
func (rep *BinaryRepresentation) EncodingToStringSlice(maxlen int) []string {
	result := []string{}
	if len(rep.encoded) > 0 {
		keys := make(map[string]interface{})
		skeys := []string{}
		s := ""
		var k interface{}
		for k, _ = range rep.encoded {
			s = fmt.Sprintf("%v (%T)", k, k)
			keys[s] = k
			skeys = append(skeys, s)
		}
		sort.Strings(skeys)
		for _, s = range skeys {
			k = keys[s]
			result = append(result,
				fmt.Sprintf("Encoding: %s (%d bytes)", s, rep.length[k]))
			for i, lbyts := range rep.encoded[k] {
				header, lines :=
					BytesToHexStringLines(
						lbyts.byts, maxlen, " Fragment %d %q",
						i, lbyts.label)
				result = append(result, header)
				for _, line := range lines {
					result = append(result, "  "+line)
				}
			}
		}
	}
	return result
}

// Only consider encoding for 0 key, disregard decoded Field.
func (rep *BinaryRepresentation) Equals(other interface{}) bool {
	rep2 := NewBinaryRepresentation()
	switch v := other.(type) {
	case BinaryRepresentation:
		rep2 = &v
	case *BinaryRepresentation:
		rep2 = v
	default:
		return false
	}
	if len(rep.encoded[0]) != len(rep2.encoded[0]) {
		return false
	}
	for i, lbyts := range rep.encoded[0] {
		if len(lbyts.byts) != len(rep2.encoded[0][i].byts) {
			return false
		}
		for j, byt := range lbyts.byts {
			if byt != rep2.encoded[0][i].byts[j] {
				return false
			}
		}
	}
	return true
}

// Outside of interface
func (rep *BinaryRepresentation) LastBinaryEncoding(key interface{}) []byte {
	if rep.record {
		if enc, ok := rep.encoded[key]; ok && len(enc) > 0 {
			return enc[len(enc)-1].byts
		}
	}
	return []byte{}
}

func (rep *BinaryRepresentation) RelabelLastBinaryEncoding(key interface{}, label string) {
	if rep.record {
		if enc, ok := rep.encoded[key]; ok && len(enc) > 0 {
			enc[len(enc)-1].label = label
		}
	}
	return
}

func (rep *BinaryRepresentation) JoinEncodedBytes(key interface{}) []byte {
	result := []byte{}
	if rep.record {
		if enc, ok := rep.encoded[key]; ok {
			for _, lbyts := range enc {
				for _, byt := range lbyts.byts {
					result = append(result, byt)
				}
			}
		}
	}
	return result
}

// STRING REPRESENTATION -------------------------------------------------------

type StringRepresentation struct {
	record  bool // Record Mode on? Captures not just encoding size, but encoding
	decoded *Field
	encoded map[interface{}][]string
	length  map[interface{}]int
	currkey interface{}
	*State
}

var _ Representation = NewStringRepresentation()

// Constructors
func NewStringRepresentation() *StringRepresentation {
	return &StringRepresentation{
		record:  false,
		decoded: NewField(),
		encoded: map[interface{}]([]string){0: []string{}},
		length:  map[interface{}]int{0: 0},
		currkey: int(0),
		State:   NewState(),
	}
}

func MakeStringRepresentation(record bool) *StringRepresentation {
	rep := NewStringRepresentation()
	rep.record = record
	rep.NotEmpty()
	return rep
}

func BuildStringRepresentation(field *Field, str string, record bool) *StringRepresentation {
	rep := MakeStringRepresentation(record)
	rep.decoded = field
	rep.length = map[interface{}]int{0: len([]byte(str))}
	rep.currkey = int(0)
	if record {
		rep.encoded = map[interface{}]([]string){0: []string{str}}
	} else {
		rep.encoded = map[interface{}]([]string){0: []string{}}
	}
	rep.NowComplete()
	return rep
}

func (rep *StringRepresentation) SetRecording(on bool) {
	rep.record = on
	return
}
func (rep *StringRepresentation) IsRecording() bool { return rep.record }

func (rep *StringRepresentation) IsEncoding(key interface{}) bool {
	_, present := rep.encoded[key]
	return present
}

func (rep *StringRepresentation) WhichEncoding() interface{} { return rep.currkey }

// If the key does not exist, make the map entry.
func (rep *StringRepresentation) ChooseEncoding(key interface{}) Representation {
	_, present := rep.encoded[key]
	if !present {
		rep.encoded[key] = []string{}
	}
	rep.currkey = key
	return rep
}

func (rep *StringRepresentation) ListEncodingKeys() []string {
	result := []string{}
	for k, _ := range rep.encoded {
		result = append(result, fmt.Sprintf("%v (%T)", k, k))
	}
	sort.Strings(result)
	return result
}

// Accepts a single key or a key slice.
func (rep *StringRepresentation) AppendEncodedTo(key, val interface{}) {
	switch k0 := key.(type) {
	case []interface{}:
		for _, k1 := range k0 {
			rep.appendEncodedTo(k1, val)
		}
	default:
		rep.appendEncodedTo(k0, val)
	}
	return
}

// The label is ignored in this string case.
func (rep *StringRepresentation) AppendEncodedToAs(key interface{}, label string, val interface{}) {
	rep.AppendEncodedTo(key, val)
	return
}

// The label is ignored in this string case.
func (rep *StringRepresentation) AppendEncodedAs(label string, val interface{}) {
	rep.AppendEncoded(val)
	return
}

// Appends using the current key.
func (rep *StringRepresentation) AppendEncoded(val interface{}) {
	rep.appendEncodedTo(rep.currkey, val)
	return
}

// The key is assumed to exist.  Takes either another StringRepresentation,
// using its current encoding, or a string, as a val argument.
func (rep *StringRepresentation) appendEncodedTo(key, val interface{}) {
	_, present := rep.encoded[key]
	if !present {
		rep.encoded[key] = []string{}
	}
	switch other := val.(type) {
	case *StringRepresentation:
		for _, str := range other.encoded[other.currkey] {
			if rep.record {
				rep.encoded[key] = append(rep.encoded[key], str)
			}
			rep.length[key] += len([]byte(str))
		}
	case string:
		if rep.record {
			rep.encoded[key] = append(rep.encoded[key], other)
		}
		rep.length[key] += len([]byte(other))
	}
	return
}

// Append a copy of the encoding from one key to another.
func (rep *StringRepresentation) AppendCopy(fromkey, tokey interface{}) {
	if rep.record && fromkey != tokey {
		if enc, ok := rep.encoded[fromkey]; ok {
			_, ok2 := rep.encoded[tokey]
			if !ok2 {
				rep.encoded[tokey] = []string{}
			}
			for _, str := range enc {
				rep.encoded[tokey] = append(rep.encoded[tokey], str)
			}
		}
	}
	return
}

// Append a copy of all encodings in the given Representation to the current
// Representation, using a new string key with the given prefix.  Does not
// check if recording, that's the caller's responsibility.
func (rep *StringRepresentation) AppendCopyAllFrom(from Representation, prefix string) {
	rep2, isa := from.(*StringRepresentation)
	if !isa {
		return
	}
	for k2, enc2 := range rep2.encoded {
		k1 := fmt.Sprintf(prefix+"%v", k2)
		_, present := rep.encoded[k1]
		if !present {
			rep.encoded[k1] = []string{}
		}
		for _, str := range enc2 {
			rep.encoded[k1] = append(rep.encoded[k1], str)
			rep.length[k1] += len([]byte(str))
		}
	}
	return
}

// Label is ignored here.
func (rep *StringRepresentation) PrependEncodedAs(label string, val interface{}) {
	rep.PrependEncoded(val)
	return
}

// Inserts the encoding to the front of the encoded slice.
func (rep *StringRepresentation) PrependEncoded(val interface{}) {
	switch other := val.(type) {
	case *StringRepresentation:
		rep.length[rep.currkey] += other.length[other.currkey]
		if rep.record {
			v0 := other.encoded[other.currkey]
			for _, str := range rep.encoded[rep.currkey] {
				v0 = append(v0, str)
			}
			rep.encoded[rep.currkey] = v0
		}
	case string:
		rep.length[rep.currkey] += len([]byte(other))
		if rep.record {
			v0 := []string{other}
			for _, str := range rep.encoded[rep.currkey] {
				v0 = append(v0, str)
			}
			rep.encoded[rep.currkey] = v0
		}
	}
	return
}

func (rep *StringRepresentation) GetDecoded() *Field {
	return rep.decoded
}

func (rep *StringRepresentation) SetField(field *Field) {
	rep.decoded = field
	return
}

func (rep *StringRepresentation) AutoSetField(val interface{}) {
	rep.decoded = Env.Types().AutoField(val)
	return
}

func (rep *StringRepresentation) Len(key interface{}) int {
	return rep.length[key]
}

func (rep *StringRepresentation) EncodingToStringSlice(maxlen int) []string {
	result := []string{}
	if len(rep.encoded) > 0 {
		keys := make(map[string]interface{})
		skeys := []string{}
		s := ""
		var k interface{}
		for k, _ = range rep.encoded {
			s = fmt.Sprintf("%v (%T)", k, k)
			keys[s] = k
			skeys = append(skeys, s)
		}
		sort.Strings(skeys)
		for _, s = range skeys {
			k = keys[s]
			result = append(result, " Encoding: "+s)
			for _, str := range rep.encoded[k] {
				result = append(result, "  "+str)
			}
		}
	}
	return result
}

// Only consider encoding for 0 key, disregard decoded Field.
func (rep *StringRepresentation) Equals(other interface{}) bool {
	rep2 := NewStringRepresentation()
	switch v := other.(type) {
	case StringRepresentation:
		rep2 = &v
	case *StringRepresentation:
		rep2 = v
	default:
		return false
	}
	if len(rep.encoded[0]) != len(rep2.encoded[0]) {
		return false
	}
	for i, str := range rep.encoded[0] {
		if str != rep2.encoded[0][i] {
			return false
		}
	}
	return true
}

// Outside of interface
func (rep *StringRepresentation) JoinEncodedString(key interface{}, delimiter string) string {
	result := ""
	if enc, ok := rep.encoded[key]; ok {
		for i, str := range enc {
			if i != 0 {
				result += delimiter
			}
			result += str
		}
	}
	return result
}

// STRING ======================================================================

// String receiver can decode and execute a response.
type StringTask func(txt string, box *Box) (Representation, *Goof)

type Stringable interface {
	EncodeString(box *Box) (Representation, *Goof)
	DecodeString(txt string, box *Box) (Representation, *Goof)
}

type MultilineStringable interface {
	MultilineString(firstprefix, prefix string) []string
}

func HexStringDecode(txt string, b Binaryable, order ByteOrder) (Representation, *Goof) {
	byts, err := hex.DecodeString(txt)
	if IsError(err) {
		return NewStringRepresentation(), Env.Goofs().WrapGoof("DECODING", err,
			"While converting hex string %s to bytes", txt)
	}
	bfr := BuildByteBufferFromBytes(order, byts)
	return b.DecodeBinary(bfr, EMPTY_BOX)
}

// Generate StringTask that does nothing.
func NilStringTask() StringTask {
	return func(txt string, box *Box) (Representation, *Goof) {
		return MakeStringRepresentation(Env.RecordEncoding()), NO_GOOF
	}
}

// Pass through given text
func IdentityStringDecoder() StringTask {
	return func(txt string, box *Box) (Representation, *Goof) {
		out := MakeStringRepresentation(Env.RecordEncoding())
		out.AutoSetField(txt)
		return out, NO_GOOF
	}
}

func BooleanStringDecoder() StringTask {
	return func(txt string, box *Box) (Representation, *Goof) {
		out := MakeStringRepresentation(Env.RecordEncoding())
		v, err := strconv.ParseBool(txt)
		if IsError(err) {
			return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
				"While parsing boolean %q", txt)
		}
		out.AppendEncoded(txt)
		out.AutoSetField(v)
		return out, NO_GOOF
	}
}

func NativeNumberFieldStringDecoder(v interface{}) StringTask {
	switch v.(type) {
	case uint8:
		return func(txt string, box *Box) (Representation, *Goof) {
			ui64, goof := ParseUint(txt, 8, 0, uint64(math.MaxUint8))
			return BuildStringRepresentation(
				MakeField(uint8(ui64), Env.Types().Number["uint8"]),
				txt, Env.RecordEncoding()), goof
		}
	case uint16:
		return func(txt string, box *Box) (Representation, *Goof) {
			ui64, goof := ParseUint(txt, 16, 0, uint64(math.MaxUint16))
			return BuildStringRepresentation(
				MakeField(uint16(ui64), Env.Types().Number["uint16"]),
				txt, Env.RecordEncoding()), goof
		}
	case uint32:
		return func(txt string, box *Box) (Representation, *Goof) {
			ui64, goof := ParseUint(txt, 32, 0, uint64(math.MaxUint32))
			return BuildStringRepresentation(
				MakeField(uint32(ui64), Env.Types().Number["uint32"]),
				txt, Env.RecordEncoding()), goof
		}
	case UINT8:
		return func(txt string, box *Box) (Representation, *Goof) {
			ui64, goof := ParseUint(txt, 8, 0, uint64(math.MaxUint8))
			return BuildStringRepresentation(
				MakeField(UINT8(ui64), Env.Types().Number["UINT8"]),
				txt, Env.RecordEncoding()), goof
		}
	case UINT16:
		return func(txt string, box *Box) (Representation, *Goof) {
			ui64, goof := ParseUint(txt, 16, 0, uint64(math.MaxUint16))
			return BuildStringRepresentation(
				MakeField(UINT16(ui64), Env.Types().Number["UINT16"]),
				txt, Env.RecordEncoding()), goof
		}
	case UINT32:
		return func(txt string, box *Box) (Representation, *Goof) {
			ui64, goof := ParseUint(txt, 32, 0, uint64(math.MaxUint32))
			return BuildStringRepresentation(
				MakeField(UINT32(ui64), Env.Types().Number["UINT32"]),
				txt, Env.RecordEncoding()), goof
		}
	case uint, uint64:
		return func(txt string, box *Box) (Representation, *Goof) {
			ui64, goof := ParseUint(txt, 64, 0, uint64(math.MaxUint64))
			return BuildStringRepresentation(
				MakeField(ui64, Env.Types().Number["uint64"]),
				txt, Env.RecordEncoding()), goof
		}
	case int8:
		return func(txt string, box *Box) (Representation, *Goof) {
			i64, goof := ParseInt(txt, 8, int64(math.MinInt8), int64(math.MaxInt8))
			return BuildStringRepresentation(
				MakeField(int8(i64), Env.Types().Number["int8"]),
				txt, Env.RecordEncoding()), goof
		}
	case int16:
		return func(txt string, box *Box) (Representation, *Goof) {
			i64, goof := ParseInt(txt, 16, int64(math.MinInt16), int64(math.MaxInt16))
			return BuildStringRepresentation(
				MakeField(int16(i64), Env.Types().Number["int16"]),
				txt, Env.RecordEncoding()), goof
		}
	case int32:
		return func(txt string, box *Box) (Representation, *Goof) {
			i64, goof := ParseInt(txt, 32, int64(math.MinInt32), int64(math.MaxInt32))
			return BuildStringRepresentation(
				MakeField(int32(i64), Env.Types().Number["int32"]),
				txt, Env.RecordEncoding()), goof
		}
	case int, int64:
		return func(txt string, box *Box) (Representation, *Goof) {
			i64, goof := ParseInt(txt, 64, int64(math.MinInt64), int64(math.MaxInt64))
			return BuildStringRepresentation(
				MakeField(i64, Env.Types().Number["int64"]),
				txt, Env.RecordEncoding()), goof
		}
	case float32:
		return func(txt string, box *Box) (Representation, *Goof) {
			f64, goof := ParseFloat(txt, 32, float64(-math.MaxFloat32), float64(math.MaxFloat32))
			return BuildStringRepresentation(
				MakeField(float32(f64), Env.Types().Number["float32"]),
				txt, Env.RecordEncoding()), goof
		}
	case float64:
		return func(txt string, box *Box) (Representation, *Goof) {
			f64, goof := ParseFloat(txt, 64, float64(-math.MaxFloat64), float64(math.MaxFloat64))
			return BuildStringRepresentation(
				MakeField(f64, Env.Types().Number["float64"]),
				txt, Env.RecordEncoding()), goof
		}
	default:
		Fatal("Unrecognised type %T when generating string decoder", v)
		return func(txt string, box *Box) (Representation, *Goof) {
			return NewStringRepresentation(), NO_GOOF
		}
	}
}

func ParseUint(txt string, size int, min, max uint64) (uint64, *Goof) {
	ui64, err := strconv.ParseUint(txt, 10, size)
	if IsError(err) {
		return ui64, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding string %q", txt)
	}
	if ui64 < min || ui64 > max {
		return ui64, GoofUintOutsideRange(uint(ui64), min, max)
	}
	return ui64, NO_GOOF
}

func ParseInt(txt string, size int, min, max int64) (int64, *Goof) {
	i64, err := strconv.ParseInt(txt, 10, size)
	if IsError(err) {
		return i64, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding string %q", txt)
	}
	if i64 < min || i64 > max {
		return i64, GoofIntOutsideRange(int(i64), min, max)
	}
	return i64, NO_GOOF
}

func ParseFloat(txt string, size int, min, max float64) (float64, *Goof) {
	f64, err := strconv.ParseFloat(txt, size)
	if IsError(err) {
		return f64, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While decoding string %q", txt)
	}
	if f64 < min || f64 > max {
		return f64, GoofFloatOutsideRange(f64, min, max)
	}
	return f64, NO_GOOF
}

func BigIntFieldStringDecoder() StringTask {
	return func(txt string, box *Box) (Representation, *Goof) {
		out := MakeStringRepresentation(Env.RecordEncoding())
		n := big.NewInt(0)
		_, ok := n.SetString(txt, 10)
		if !ok {
			return out, Env.Goofs().MakeGoof("PARSING_TEXT",
				"While decoding BIGINT from text %s", txt)
		}
		out.AutoSetField(n)
		out.AppendEncoded(txt)
		return out, NO_GOOF
	}
}

func BigRatFieldStringDecoder() StringTask {
	return func(txt string, box *Box) (Representation, *Goof) {
		out := MakeStringRepresentation(Env.RecordEncoding())
		n := big.NewRat(1, 1)
		_, ok := n.SetString(txt)
		if !ok {
			return out, Env.Goofs().MakeGoof("PARSING_TEXT",
				"While decoding BIGRAT from text %s", txt)
		}
		out.AutoSetField(n)
		out.AppendEncoded(txt)
		return out, NO_GOOF
	}
}

func ByteFieldStringDecoder() StringTask {
	return func(txt string, box *Box) (Representation, *Goof) {
		out := MakeStringRepresentation(Env.RecordEncoding())
		byts, err := hex.DecodeString(txt)
		out.AutoSetField(byts)
		out.AppendEncoded(txt)
		return out, Env.Goofs().WrapGoof("DECODING", err,
			"While decoding BYTES from text %s", txt)
	}
}

// Returns the header and string lines.  The maximum number of bytes
// must be specified.
func BytesToHexStringLines(byts []byte, maxlen int, header string, a ...interface{}) (string, []string) {
	if len(byts) > maxlen {
		byts = byts[:maxlen]
		header += fmt.Sprintf(" (%d bytes truncated to %d)", len(byts), maxlen)
	} else {
		header += fmt.Sprintf(" (%d bytes)", len(byts))
	}
	const mincols = 4
	const majcols = 4
	const format = HEXDUMP_FORMAT
	majcolcnt := 0
	chunksize := 0
	chunk := []byte{}
	L := len(byts)
	c := 0
	lines := []string{}
	line := ""
	linestr := ""
	for {
		chunksize = mincols
		if c >= L {
			if len(line) > 0 {
				lines = append(lines, fmt.Sprintf(format, line, linestr))
			}
			break
		}
		if L-c < mincols {
			chunksize = L - c
		}
		chunk = byts[c : c+chunksize]
		linestr += ByteSliceToString(chunk)
		c += chunksize
		if majcolcnt == 0 {
			line += fmt.Sprintf("% x", chunk)
		} else {
			line += fmt.Sprintf("  % x", chunk)
		}
		majcolcnt++
		if majcolcnt > majcols-1 {
			majcolcnt = 0
			lines = append(lines, fmt.Sprintf(format, line, linestr))
			line = ""
			linestr = ""
		}
	}
	return fmt.Sprintf(header, a...), lines
}

func ByteSliceToString(byts []byte) string {
	result := ""
	for _, b := range byts {
		chr := fmt.Sprintf("%s", string(b))
		if len(chr) > 0 && Env.Languages()["English"].IsPrintable(chr) {
			result += chr
		} else {
			result += "."
		}
	}
	return result
}

// BINARY ======================================================================

// Binary receivers and transmitters can encode/decode as well as perform
// actions.  BinaryRx returns an int specifying the theoretical encoded
// binary size of the data, not including any prepended CODE.  In practice,
// an EOF may be encountered before this size is reached.
type BinaryTask func(bfr *Buffer, box *Box) (Representation, *Goof)

type Binaryable interface {
	BinaryEncodable
	BinaryDecodable
}

type BinaryEncodable interface {
	EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof)
}

type BinaryDecodable interface {
	DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof)
}

// Generate BinaryTask that does nothing.
func NilBinaryTask() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		return MakeBinaryRepresentation(Env.RecordEncoding()), NO_GOOF
	}
}

// Return binary size of native types (or derivatives).
func BinarySize(val interface{}) int {
	return binary.Size(val)
}

func CalculateBinarySize(b BinaryEncodable, order ByteOrder) (Representation, *Goof) {
	bfr := MakeByteBuffer(order)
	return b.EncodeBinary(bfr, EMPTY_BOX)
}

type ByteOrder binary.ByteOrder

func BinaryEncode(b BinaryEncodable, order ByteOrder) []byte {
	bfr := MakeByteBuffer(order)
	_, goof := b.EncodeBinary(bfr, EMPTY_BOX)
	if IsError(goof) {
		return []byte{}
	}
	byts, _ := bfr.Bytes()
	return byts
}

func HexStringEncode(b BinaryEncodable, order ByteOrder) string {
	return hex.EncodeToString(BinaryEncode(b, order))
}

// Encodes a string directly (without using Field), and using a UINT for the
// length, which can save a few bytes for practical strings.
func StringBinaryEncoder(bfr *Buffer, txt string) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(txt)
	byts := []byte(txt)
	out2, goof := MakeUINT(len(byts)).EncodeBinary(bfr, EMPTY_BOX)
	out2.(*BinaryRepresentation).RelabelLastBinaryEncoding(
		0, "string length (UINT)")
	out.AppendEncoded(out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}
	byts, goof = bfr.WriteBytes(byts)
	out.AppendEncodedAs("string", byts)
	return out, goof
}

func StringBinaryDecoder(bfr *Buffer) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out2, goof := NewUINT().DecodeBinary(bfr, EMPTY_BOX)
	out2.(*BinaryRepresentation).RelabelLastBinaryEncoding(
		0, "string length (UINT)")
	out.AppendEncoded(out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}
	ui, _ := out2.GetDecoded().Value().(UINT)
	i64, goof := ui.AsInt64()
	if IsError(goof) {
		return out, goof
	}
	byts := make([]byte, int(i64))
	byts2, goof := bfr.ReadBytes(byts)
	out.AppendEncodedAs("string", byts2)
	out.SetField(Env.Types().BuildField(string(byts), "STRING"))
	return out, goof
}

// Generic var length binary encoder.
//  +-------+-----------+------------------+
//  | code  | size      | bytes            |
//  | CODE  | UINT      |                  |
//  +-------+------------------------------+
func (bfr *Buffer) GenericVarLengthFieldBinaryEncoder(code CODE, byts []byte, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	byts2, goof := bfr.Write(code, box)
	out.AppendEncodedAs("generic var length type code", byts2)
	if IsError(goof) {
		return out, goof
	}
	out2, goof := MakeUINT(len(byts)).EncodeBinary(bfr, EMPTY_BOX)
	out2.(*BinaryRepresentation).
		RelabelLastBinaryEncoding(0, "generic var length size (UINT)")
	out.AppendEncoded(out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}
	byts2, goof = bfr.Write(byts, box)
	out.AppendEncodedAs("generic var length bytes", byts2)
	return out, goof
}

// Generic var length binary decoder. Assumes code has been read.
//  +-------+-----------+------------------+
//  | code  | size      | bytes            |
//  | CODE  | UINT      |                  |
//  +-------+------------------------------+
func (bfr *Buffer) GenericVarLengthFieldBinaryDecoder(box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out2, goof := NewUINT().DecodeBinary(bfr, EMPTY_BOX)
	out2.(*BinaryRepresentation).
		RelabelLastBinaryEncoding(0, "generic var length size (UINT)")
	out.AppendEncoded(out2.ChooseEncoding(0))
	if IsError(goof) {
		return out, goof
	}
	ui, _ := out2.GetDecoded().Value().(UINT)
	i64, goof := ui.AsInt64()
	if IsError(goof) {
		return out, goof
	}
	byts := make([]byte, int(i64))
	_, goof = bfr.Read(&byts, box)
	out.AppendEncodedAs("generic var length bytes", byts)
	out.SetField(Env.Types().BuildField(byts, "BYTES"))
	return out, goof
}

func (bfr *Buffer) BinaryDecodeAsType(code CODE) (Representation, *Goof) {
	typ, goof := Env.Types().ProtocolByCode(code)
	if IsError(goof) {
		return NewBinaryRepresentation(), goof
	}
	return typ.BinaryRx(bfr, EMPTY_BOX)
}

// Generic binaryable encoder.
//  +-------+-----------------------+
//  | code  | BinaryEncodable bytes |
//  | CODE  |                       |
//  +-------+-----------------------+
func (bfr *Buffer) GenericFieldBinaryEncodableEncoder(code CODE, box *Box) BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		out := MakeBinaryRepresentation(Env.RecordEncoding())
		byts, goof := bfr.Write(code, box)
		if IsError(goof) {
			return out, goof
		}
		out.AppendEncodedAs("generic BinaryEncodable type code", byts)
		inside, goof := box.Require(BOX_ITEM_BINARY_ENCODABLE)
		if IsError(goof) {
			return out, goof
		}
		bable, _ := inside.(BinaryEncodable)
		out2, goof := bable.EncodeBinary(bfr, EMPTY_BOX)
		out.AutoSetField(bable)
		out.AppendEncoded(out2.ChooseEncoding(0))
		return out, goof
	}
}

// Generate generic binary encoder. Encoding fixed for all time.
//  +-------+---------------+
//  | code  | box.Inside()  |
//  | CODE  |               |
//  +-------+---------------+
func NativeNumberFieldBinaryEncoder(code CODE) BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		out := MakeBinaryRepresentation(Env.RecordEncoding())
		inside, goof := box.Require(BOX_ITEM_ENCODE)
		if IsError(goof) {
			return out, goof
		}
		out.AutoSetField(inside)
		byts, goof := bfr.Write(code, box)
		if IsError(goof) {
			return out, goof
		}
		out.AppendEncodedAs("native number type code", byts)
		byts, goof = bfr.Write(inside, box)
		out.AppendEncodedAs(fmt.Sprintf("number (%T)", inside), byts)
		return out, goof
	}
}

// Generate generic binary decoder for in-built Go codes.
// Assumes code has been read.
func NativeNumberFieldBinaryDecoder(v interface{}) BinaryTask {
	n := GetUnderlyingNativeNumberInstance(v)
	if n == nil {
		Fatal(
			"When generating a native number binary decoder, "+
				"the provided value %v (type %T), the reflect.Kind %v "+
				"is not recognised as a native number type",
			v, v, reflect.TypeOf(v).Kind())
	}
	switch n.(type) {
	case int8:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := int8(0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["int8"]),
				"int8", byts, Env.RecordEncoding()), goof
		}
	case int16:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := int16(0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["int16"]),
				"int16", byts, Env.RecordEncoding()), goof
		}
	case int32:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := int32(0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["int32"]),
				"int32", byts, Env.RecordEncoding()), goof
		}
	case int, int64:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := int64(0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["int64"]),
				"int64", byts, Env.RecordEncoding()), goof
		}
	case uint8:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := uint8(0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["uint8"]),
				"uint8", byts, Env.RecordEncoding()), goof
		}
	case uint16:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := uint16(0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["uint16"]),
				"uint16", byts, Env.RecordEncoding()), goof
		}
	case uint32:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := uint32(0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["uint32"]),
				"uint32", byts, Env.RecordEncoding()), goof
		}
	case UINT8:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			return NewUINT8().DecodeBinary(bfr, box)
		}
	case UINT16:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			return NewUINT16().DecodeBinary(bfr, box)
		}
	case UINT32:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			return NewUINT32().DecodeBinary(bfr, box)
		}
	case uint, uint64:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := uint64(0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["uint64"]),
				"uint64", byts, Env.RecordEncoding()), goof
		}
	case float32:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := float32(0.0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["float32"]),
				"float32", byts, Env.RecordEncoding()), goof
		}
	case float64:
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			d := float64(0.0)
			byts, goof := bfr.Read(&d, box)
			return BuildBinaryRepresentation(
				MakeField(d, Env.Types().Number["float64"]),
				"float64", byts, Env.RecordEncoding()), goof
		}
	default:
		Fatal("Unrecognised code %T when binary decoding %v", v, v)
		return func(bfr *Buffer, box *Box) (Representation, *Goof) {
			return NewBinaryRepresentation(), NO_GOOF
		}
	}
}

// Generate nil binary encoder.  Encoding fixed for all time.
func CodeOnlyBinaryEncoder(code CODE) BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		byts, goof := bfr.Write(code, box)
		out := MakeBinaryRepresentation(Env.RecordEncoding())
		out.AppendEncoded(byts)
		return out, goof
	}
}

// Generate nil binary decoder. Assumes code has been read, so there is nothing
// more to be done.
func ConstBinaryDecoder(val interface{}) BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		out := MakeBinaryRepresentation(Env.RecordEncoding())
		out.AutoSetField(val)
		return out, NO_GOOF
	}
}

// Generate big.Int or big.Rat binary encoder.
//  +-------+----------+------------------+
//  | code  | size     | gob              |
//  | CODE  | UINT     |                  |
//  +-------+-----------------------------+
func BigNumFieldBinaryEncoder(code CODE) BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		out := MakeBinaryRepresentation(Env.RecordEncoding())
		byts := []byte{}
		var err error
		typstr := ""
		inside, goof := box.Require(BOX_ITEM_ENCODE)
		if IsError(goof) {
			return out, goof
		}
		switch v := inside.(type) {
		case *big.Int:
			typstr = "BIGINT"
			byts, err = v.GobEncode()
		case *big.Rat:
			typstr = "BIGRAT"
			byts, err = v.GobEncode()
		default:
			return out, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Argument %v is a %T but should be a *big.Int or *big.Rat",
				inside, inside)
		}
		if IsError(err) {
			return out, Env.Goofs().WrapGoof("ENCODING", err,
				"While encoding %v (%T)", inside, inside)
		}
		out2, goof := bfr.GenericVarLengthFieldBinaryEncoder(code, byts, box)
		out2.SetField(Env.Types().BuildField(inside, typstr))
		return out2, goof
	}
}

// Generate big.Int binary decoder. Assumes code has been read.
func BigIntFieldBinaryDecoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		n := big.NewInt(0)
		out := MakeBinaryRepresentation(Env.RecordEncoding())
		out.AutoSetField(n)
		out2, goof := bfr.GenericVarLengthFieldBinaryDecoder(box)
		out.AppendEncoded(out2.ChooseEncoding(0))
		if IsError(goof) {
			return out, goof
		}
		byts := out2.(*BinaryRepresentation).LastBinaryEncoding(0)
		err := n.GobDecode(byts) // out points to updated n
		if IsError(err) {
			return out, Env.Goofs().WrapGoof("DECODING", err,
				"While decoding a big.Int")
		}
		return out, NO_GOOF
	}
}

// Generate big.Rat binary decoder. Assumes code has been read.
func BigRatFieldBinaryDecoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		n := big.NewRat(1, 1)
		out := MakeBinaryRepresentation(Env.RecordEncoding())
		out.AutoSetField(n)
		out2, goof := bfr.GenericVarLengthFieldBinaryDecoder(box)
		out.AppendEncoded(out2.ChooseEncoding(0))
		if IsError(goof) {
			return out, goof
		}
		byts := out2.(*BinaryRepresentation).LastBinaryEncoding(0)
		err := n.GobDecode(byts) // out points to updated n
		if IsError(err) {
			return out, Env.Goofs().WrapGoof("DECODING", err,
				"While decoding a big.Rat")
		}
		return out, NO_GOOF
	}
}

// Generate byte slice binary encoder.
func ByteSliceFieldBinaryEncoder(code CODE) BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		inside, goof := box.RequireOfKnownType(
			BOX_ITEM_ENCODE,
			BOX_ITEM_BYTE_SLICE)
		if IsError(goof) {
			return NewBinaryRepresentation(), goof
		}
		v, _ := inside.([]byte)
		out, goof := bfr.GenericVarLengthFieldBinaryEncoder(code, v, box)
		out.SetField(Env.Types().BuildField(v, "BYTES"))
		return out, goof
	}
}

// Generate byte slice binary decoder. Assumes code has been read.
func ByteSliceFieldBinaryDecoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		out, goof := bfr.GenericVarLengthFieldBinaryDecoder(box)
		out.SetField(Env.Types().BuildField(
			out.(*BinaryRepresentation).LastBinaryEncoding(0),
			"BYTES"))
		return out, goof
	}
}

// Generate string binary encoder.
func StringFieldBinaryEncoder(code CODE) BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		inside, goof := box.RequireOfKnownType(
			BOX_ITEM_ENCODE,
			BOX_ITEM_STRING)
		if IsError(goof) {
			return NewBinaryRepresentation(), goof
		}
		v, _ := inside.(string)
		out, goof := bfr.GenericVarLengthFieldBinaryEncoder(code, []byte(v), box)
		out.SetField(Env.Types().BuildField(v, "STRING"))
		return out, goof
	}
}

// Generate string binary decoder. Assumes code has been read.
func StringFieldBinaryDecoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		out, goof := bfr.GenericVarLengthFieldBinaryDecoder(box)
		out.SetField(Env.Types().BuildField(string(
			out.(*BinaryRepresentation).LastBinaryEncoding(0)),
			"STRING"))
		return out, goof
	}
}

// COMPRESSION SCHEME ==========================================================

type CompressionScheme interface {
	Name() string
	Level() UINT8
	Copy() CompressionScheme
	Compress([]byte) ([]byte, *Goof)
	Decompress([]byte) ([]byte, *Goof)
	SetLevel(UINT8)
	Stateful
}

// COMPRESSION SCHEME FACTORY ==================================================

func NewCompressionScheme(typ string) (CompressionScheme, *Goof) {
	if !BinaryAlgoCompression.Exists(typ) {
		return NoCompression(), Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"%q is not a recognised binary compression algorithm",
			typ)
	}
	switch typ {
	case "COMPRESSION_GZIP":
		return NewGzipCompressor(), NO_GOOF
	case "COMPRESSION_NONE":
		return NoCompression(), NO_GOOF
	default:
		return NoCompression(), Env.Goofs().MakeGoof("NOT_IMPLEMENTED",
			"New %q CompressionScheme must be instantiated manually",
			typ)
	}
}

// IDENTITY COMPRESSION SCHEME =================================================

type IdentityCompressionScheme struct {
	*State
}

func NewIdentityCompressionScheme() *IdentityCompressionScheme {
	ics := &IdentityCompressionScheme{
		State: NewState(),
	}
	ics.NotEmpty()
	ics.NowComplete()
	return ics
}

func NoCompression() *IdentityCompressionScheme {
	return NewIdentityCompressionScheme()
}

func (ics *IdentityCompressionScheme) Name() string      { return "COMPRESSION_NONE" }
func (ics *IdentityCompressionScheme) Level() UINT8      { return 0 }
func (ics *IdentityCompressionScheme) Threshold() UINT16 { return 0 }

func (ics *IdentityCompressionScheme) Copy() CompressionScheme {
	ics2 := NewIdentityCompressionScheme()
	ics2.State = ics.State.Copy()
	return ics2
}

func (ics *IdentityCompressionScheme) SetLevel(level UINT8) { return }

func (ics *IdentityCompressionScheme) Compress(byts []byte) ([]byte, *Goof) {
	return byts, NO_GOOF
}

func (ics *IdentityCompressionScheme) Decompress(byts []byte) ([]byte, *Goof) {
	return byts, NO_GOOF
}

// GZIP ========================================================================

type GzipCompressor struct {
	level UINT8
	*State
}

func NewGzipCompressor() *GzipCompressor {
	return &GzipCompressor{
		State: NewState(),
	}
}

func MakeGzipCompressor(level UINT8) *GzipCompressor {
	comp := NewGzipCompressor()
	comp.level = level
	comp.NotEmpty()
	comp.NowComplete()
	return comp
}

func (comp *GzipCompressor) Name() string { return "COMPRESSION_GZIP" }
func (comp *GzipCompressor) Level() UINT8 { return comp.level }

func (comp *GzipCompressor) Copy() CompressionScheme {
	comp2 := NewGzipCompressor()
	comp2.level = comp.level
	comp2.State = comp.State.Copy()
	return comp2
}

func (comp *GzipCompressor) SetLevel(level UINT8) {
	comp.level = level
	comp.NowComplete()
	return
}

func (comp *GzipCompressor) Compress(byts []byte) ([]byte, *Goof) {
	bfr := bytes.NewBuffer([]byte{})
	writer := gzip.NewWriter(bfr)
	n, err := writer.Write(byts)
	if IsError(err) {
		return []byte{}, Env.Goofs().WrapGoof("COMPRESSING", err,
			"While compressing %d bytes, ended after %d bytes",
			len(byts), n)
	}
	err = writer.Close()
	if IsError(err) {
		return []byte{}, Env.Goofs().WrapGoof("COMPRESSING", err,
			"While closing %T for %T", writer, comp)
	}
	return bfr.Bytes(), NO_GOOF
}

func (comp *GzipCompressor) Decompress(byts []byte) ([]byte, *Goof) {
	bfr := bytes.NewBuffer(byts)
	reader, err := gzip.NewReader(bfr)
	if IsError(err) {
		return []byte{}, Env.Goofs().WrapGoof("UNCOMPRESSING", err,
			"While creating %T for %T", reader, comp)
	}
	out := new(bytes.Buffer)
	n, err := io.Copy(out, reader)
	if IsError(err) {
		return []byte{}, Env.Goofs().WrapGoof("UNCOMPRESSING", err,
			"While uncompressing %d bytes, ended after %d bytes",
			len(byts), n)
	}
	err = reader.Close()
	if IsError(err) {
		return []byte{}, Env.Goofs().WrapGoof("UNCOMPRESSING", err,
			"While closing %T for %T", reader, comp)
	}
	return out.Bytes(), NO_GOOF
}

// Allows future, version-based changes.
func MakeEncodeSwitches(ver *Version) *Switches {
	switches, _ := MakeSwitchesFromStruct(NewFieldBoxParamsConfig())
	return switches
}
