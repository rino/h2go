/*
	Language instance = English
*/
package h2go

import ()

type English struct {
	name     string
	iso639   []string
	ordinals Uint8Enum
	days     map[string]Uint8Enum
	months   map[string]Uint8Enum
	*State
}

var _ Language = NewEnglish()
var _ Stateful = NewEnglish() // Embedded State

func NewEnglish() *English {
	return &English{
		iso639:   []string{},
		ordinals: NewUint8Enum(),
		days:     make(map[string]Uint8Enum),
		months:   make(map[string]Uint8Enum),
		State:    NewState(),
	}
}

func MakeEnglish() Language {
	lang := NewEnglish()
	lang.name = "English"
	lang.NotEmpty()
	lang.iso639 = []string{"en", "eng", "eng"}
	lang.ordinals = lang.makeOrdinals()
	lang.days = lang.makeDays()
	lang.months = lang.makeMonths()
	lang.NowComplete()
	return lang
}

func (lang *English) Copy() Language {
	lang2 := NewEnglish()
	lang2.name = lang.name
	for _, str := range lang.iso639 {
		lang2.iso639 = append(lang2.iso639, str)
	}
	lang2.ordinals = lang.ordinals.Copy()
	for k, v := range lang.days {
		lang2.days[k] = v.Copy()
	}
	for k, v := range lang.months {
		lang2.months[k] = v.Copy()
	}
	lang2.State = lang.State.Copy()
	return lang2
}

func (lang *English) makeOrdinals() Uint8Enum {
	return MakeUint8Enum([][]string{
		[]string{"first"},
		[]string{"second"},
		[]string{"third"},
		[]string{"fourth"},
		[]string{"fifth"},
		[]string{"sixth"},
		[]string{"seventh"},
		[]string{"eighth"},
		[]string{"ninth"},
		[]string{"tenth"},
		[]string{"eleventh"},
		[]string{"twelth"},
		[]string{"thirteenth"},
		[]string{"fourteenth"},
		[]string{"fifteenth"},
		[]string{"sixteenth"},
		[]string{"seventeenth"},
		[]string{"eighteenth"},
		[]string{"nineteenth"},
		[]string{"twentieth"},
		[]string{"twentyfirst"},
		[]string{"twentysecond"},
		[]string{"twentythird"},
		[]string{"twentyfourth"},
		[]string{"twentyfifth"},
		[]string{"twentysixth"},
		[]string{"twentyseventh"},
		[]string{"twentyeighth"},
		[]string{"twentyninth"},
		[]string{"thirtieth"},
		[]string{"thirtyfirst"},
	})
}

func (lang *English) makeDays() map[string]Uint8Enum {
	return map[string]Uint8Enum{
		"Gregorian": MakeUint8Enum([][]string{
			[]string{"monday", "mon"},
			[]string{"tuesday", "tue", "tues"},
			[]string{"wednesday", "wed"},
			[]string{"thursday", "thurs", "thrs", "thr"},
			[]string{"friday", "fri"},
			[]string{"saturday", "sat"},
			[]string{"sunday", "sun"},
		})}
}

func (lang *English) makeMonths() map[string]Uint8Enum {
	return map[string]Uint8Enum{
		"Gregorian": MakeUint8Enum([][]string{
			[]string{"january", "jan"},
			[]string{"february", "feb"},
			[]string{"march", "mar"},
			[]string{"april", "apr"},
			[]string{"may"},
			[]string{"june", "jun"},
			[]string{"july", "jul"},
			[]string{"august", "aug"},
			[]string{"september", "sept", "sep"},
			[]string{"october", "oct"},
			[]string{"november", "nov"},
			[]string{"december", "dec"},
		})}
}

// Language interface
func (lang *English) Name() string     { return lang.name }
func (lang *English) ISO639() []string { return lang.iso639 }
func (lang *English) ShortOrdinals() []string {
	return []string{"st", "nd", "rd", "th"}
}
func (lang *English) LongOrdinals() Uint8Enum {
	return lang.ordinals
}
func (lang *English) MeridiemMarkers() []string {
	result := []string{}
	marker := ""
	for _, marker = range lang.AmMarkers() {
		result = append(result, marker)
	}
	for _, marker = range lang.PmMarkers() {
		result = append(result, marker)
	}
	for _, marker = range lang.NoonMarkers() {
		result = append(result, marker)
	}
	return result
}
func (lang *English) AmMarkers() []string {
	return []string{"am"}
}
func (lang *English) PmMarkers() []string {
	return []string{"pm"}
}
func (lang *English) NoonMarkers() []string {
	return []string{"noon"}
}
func (lang *English) ClockDividers() []string {
	return []string{":", "."}
}
func (lang *English) DateDividers() []string {
	return []string{"-", "/"}
}
func (lang *English) Days(cal string) Uint8Enum {
	enum, ok := lang.days[cal]
	if ok {
		return enum
	}
	return NewUint8Enum()
}
func (lang *English) Months(cal string) Uint8Enum {
	enum, ok := lang.months[cal]
	if ok {
		return enum
	}
	return NewUint8Enum()
}

// ASCII tests
func (lang *English) IsUpperLetter(s string) bool {
	return s[0] > 64 && s[0] < 91
}
func (lang *English) IsLowerLetter(s string) bool {
	return s[0] > 96 && s[0] < 123
}
func (lang *English) IsLetter(s string) bool {
	return lang.IsUpperLetter(s) || lang.IsLowerLetter(s)
}
func (lang *English) IsDigit(s string) bool {
	return s[0] > 47 && s[0] < 58
}
func (lang *English) IsPrintable(s string) bool {
	return lang.IsLetter(s) || lang.IsDigit(s)
}
