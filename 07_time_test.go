package h2go

import (
	"math/big"
	"testing"
)

func init() {
	lang = Env.Languages()["English"]
	if lang == nil {
		Fatal("Problem acquiring language")
	}
	tcal = Env.Calendars()["Gregorian"]
	if tcal == nil {
		Fatal("Problem acquiring calendar")
	}
	Env.Log().Advise("Language is %q", lang.Name())
	Env.Log().Advise("Calendar is %q", tcal.Name())
	return
}

var lang Language
var tcal Calendar // test calendar

// TESTS ======================================================================

// Create a calendar.
func TestNewCalendar(t *testing.T) {
	Env.Log().MajorSection("Some basic checks on the Gregorian calendar with English")
	name, exists1 := tcal.MonthPhrase(3, lang)
	if !exists1 {
		t.Fatalf("Could not get month number for %q", "march")
	}
	if name != "march" {
		t.Fatalf("Month should be %q but was %q", "march", name)
	}
	num, exists2 := tcal.MonthNumber("march", lang)
	if !exists2 {
		t.Fatalf("Could not get name for month %v", 3)
	}
	if num != 3 {
		t.Fatalf("Month should be %d but was %d", 3, num)
	}
}

// Test Date.
func TestDate(t *testing.T) {
	Env.Log().MajorSection("Create a Date and transform to/from a Julian day number")
	mth, goof := MakeMonth(MakeInt64(2013), 3, tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Month")
	}
	date1, goof := MakeDate(mth, 17)
	expected := "2013-03-17"
	if date1.String() != expected {
		t.Fatalf("Date should be %q but was %q", expected, date1)
	}
	date2, goof := MakeDate(mth, 40)
	if !IsError(goof) {
		t.Fatalf("Failed to return error for invalid date %v", date2)
	}
	date3, goof := date1.Plus(0, 0, 3)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem with Date.Plus")
	}
	expected = "2013-03-20"
	if date3.String() != expected {
		t.Fatalf("Date should be %q but was %q", expected, date3)
	}
	// Date -> Julian day number
	Env.Log().MinorSection("Date to Julian day number")
	tjd, goof := date1.ToNum(new(JulianDay))
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Julian day number")
	}
	ijd1, _ := tjd.Value().AsGoInt64()
	jde := int64(2456369)
	if ijd1 != jde {
		diff := jde - ijd1
		t.Fatalf("Julian day number should be %v but was %v, diff %v",
			jde, ijd1, diff)
	}
	// Julian day number -> Date
	Env.Log().MinorSection("Julian day number to Date")
	jdn, _ := tjd.(*JulianDay)
	date4, goof := jdn.ToDate(tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem with converting Julian day number to date")
	}
	if !date4.Equals(date1) {
		t.Fatalf("Date should be %v but was %v", date1, date4)
	}

	date5 := date1.Copy()
	if !date5.Equals(date1) {
		t.Fatalf("Dates %v and %v should be equal", date5, date1)
	}
	if date5.IsBefore(date1) {
		t.Fatalf("Date %v is not before %v", date5, date1)
	}

	date6, goof := date5.Inc(1)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem with Date.Inc")
	}
	if date6.Equals(date5) {
		t.Fatalf("Dates %v and %v should not be equal", date6, date5)
	}
	if !date5.IsBefore(date6) {
		t.Fatalf("Date %v should be before %v", date5, date6)
	}
	if date5.IsAfter(date6) {
		t.Fatalf("Date %v should be before %v", date5, date6)
	}
}

// Test CalClock and LocalTime.
func TestCalClockAndLocalTime(t *testing.T) {
	Env.Log().MajorSection("Change a LocalTime location, involving a possible time zone shift")
	perth, goof := LoadLocation("Australia/Perth")
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating location")
	}
	sydney, goof := LoadLocation("Australia/Sydney")
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating location")
	}
	cc1, goof :=
		BuildCalClock(2013, 3, 17, 13, 30, 30, ZeroFrac(), tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating date")
	}
	lt1 := MakeLocalTime(cc1, perth)
	Env.Log().Advise("Time in Perth: %v", lt1)
	tlt2, goof := lt1.At(sydney)
	Env.Log().Advise("Time in Sydney: %v", tlt2)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem changing localtime from %v to %v", perth, sydney)
	}
	lt2, _ := tlt2.(*LocalTime)
	Env.Log().Advise("CalClock in Sydney: %v", lt2.Time())
}

// Test UTC CalClock <-> some time numbers.
func TestTimeNumbers(t *testing.T) {
	Env.Log().MajorSection("Transform a CalClock (UTC) to/from some time numbers")
	cc1, goof :=
		BuildCalClock(2013, 3, 17, 13, 30, 30, ZeroFrac(), tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem creating date")
	}
	// CalClock -> Unix time
	Env.Log().MinorSection("CalClock to unix time")
	tut, goof := cc1.ToNum(new(UnixTime))
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem converting calclock to unix time")
	}
	Env.Log().Advise("Unix time %v", tut)
	expected := int64(1363527030)
	i64, _ := tut.Value().AsGoInt64()
	diff := i64 - expected
	if diff != 0 {
		hrs := float64(diff) / 3600
		days := hrs / 24
		t.Fatalf(
			"Unix time should be %v but was %v a diff of %v[s] "+
				"(%.2f[hrs], %.2f[days])",
			expected, i64, diff, hrs, days)
	}
	// Unix time -> CalClock
	Env.Log().MinorSection("Unix time to CalClock")
	cc2, goof := tut.ToCalClock(tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem with converting Unix time to calclock")
	}
	equals, goof := cc2.Equals(cc1)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem testing if %v equals %v", cc2, cc1)
	}
	if !equals {
		t.Fatalf("CalClock should be %v but was %v", cc1, cc2)
	}
	// CalClock -> Nano time
	Env.Log().MinorSection("CalClock to nano time")
	cc1, goof =
		BuildCalClock(2013, 3, 17, 13, 30, 30, MakeNanoFrac(12345), tcal)
	tnt, goof := cc1.ToNum(new(NanoTime))
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem converting calclock to nano time")
	}
	Env.Log().Advise("Nano time %v", tnt)
	expected = int64(1363527030000012345)
	i64, _ = tnt.Value().AsGoInt64()
	diff = i64 - expected
	if diff != 0 {
		hrs := float64(diff) / (3600 * 1000000000)
		days := hrs / 24
		t.Fatalf(
			"Nano time should be %v but was %v a diff of %v[s] "+
				"(%.2f[hrs], %.2f[days])",
			expected, i64, diff, hrs, days)
	}
	// Nano time -> CalClock
	Env.Log().MinorSection("Nano time to CalClock")
	cc2, goof = tnt.ToCalClock(tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem with converting nano time to calclock")
	}
	equals, goof = cc2.Equals(cc1)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem testing if %v equals %v", cc2, cc1)
	}
	if !equals {
		t.Fatalf("CalClock should be %v but was %v", cc1, cc2)
	}
	// CalClock -> Java time
	Env.Log().MinorSection("CalClock to java time")
	cc1, goof =
		BuildCalClock(2013, 3, 17, 13, 30, 30, MakeNanoFrac(123000000), tcal)
	tjt, goof := cc1.ToNum(new(JavaTime))
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem converting calclock to java time")
	}
	Env.Log().Advise("Java time %v", tjt)
	expected = int64(1363527030123)
	i64, _ = tjt.Value().AsGoInt64()
	diff = i64 - expected
	if diff != 0 {
		hrs := float64(diff) / (3600 * 1000)
		days := hrs / 24
		t.Fatalf(
			"Java time should be %v but was %v a diff of %v[s] "+
				"(%.2f[hrs], %.2f[days])",
			expected, i64, diff, hrs, days)
	}
	// Java time -> CalClock
	Env.Log().MinorSection("Java time to CalClock")
	cc2, goof = tjt.ToCalClock(tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem with converting java time to calclock")
	}
	equals, goof = cc2.Equals(cc1)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem testing if %v equals %v", cc2, cc1)
	}
	if !equals {
		t.Fatalf("CalClock should be %v but was %v", cc1, cc2)
	}
}

func TestNow(t *testing.T) {
	Env.Log().MajorSection("Create and copy locations, change time zones")
	perth, goof := LoadLocation("Australia/Perth")
	if Env.Log().IsError(goof) {
		t.Fatal("Problem creating location")
	}
	sydney, goof := LoadLocation("Australia/Sydney")
	if Env.Log().IsError(goof) {
		t.Fatal("Problem creating location")
	}
	now_utc, goof := Now(tcal, UTC)
	if Env.Log().IsError(goof) {
		t.Fatal("Problem getting current time for location %v", UTC)
	}
	Env.Log().Advise("Now at UTC = %v", now_utc)
	now_perth, goof := Now(tcal, perth)
	if Env.Log().IsError(goof) {
		t.Fatal("Problem getting current time for location %v", perth)
	}
	Env.Log().Advise("Now at Perth = %v", now_perth)
	now_perth_copy := now_perth.Copy()
	now_sydney, goof := Now(tcal, sydney)
	if Env.Log().IsError(goof) {
		t.Fatal("Problem getting current time for location %v", sydney)
	}
	Env.Log().Advise("Now at Sydney = %v", now_sydney)
	utc_at_perth, goof := now_perth.At(UTC)
	if Env.Log().IsError(goof) {
		t.Fatal("Problem converting time from location %v to %v", perth, UTC)
	}
	Env.Log().Advise("UTC at Perth = %v", utc_at_perth)
	perth_at_utc, goof := now_utc.At(perth)
	if Env.Log().IsError(goof) {
		t.Fatal("Problem converting time from location %v to %v", UTC, perth)
	}
	Env.Log().Advise("Perth at UTC = %v", perth_at_utc)
	syd_at_perth, goof := now_sydney.At(perth)
	if Env.Log().IsError(goof) {
		t.Fatal("Problem converting time from location %v to %v", sydney, perth)
	}
	Env.Log().Advise("Sydney at Perth = %v", syd_at_perth)
	equals, goof := now_perth.Equals(now_perth_copy)
	if Env.Log().IsError(goof) {
		t.Fatal("Problem comparing equality between %v and %v",
			now_perth, now_perth_copy)
	}
	if !equals {
		t.Fatal("Times %v and %v should be equal", now_perth, now_perth_copy)
	}
}

func TestOrdinals(t *testing.T) {
	word := "4th"
	Env.Log().MajorSection("Parse ordinal %q", word)
	expected := uint8(4)
	actual, goof := GetOrdinal("4th", lang)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem getting ordinal")
	}
	if actual != expected {
		t.Fatalf(
			"Ordinal %q should yield %d, got %d of type %T",
			word, expected, actual, actual)
	}
}

func TestMonthFind1(t *testing.T) {
	what := "the 3rd Sunday of the month"
	Env.Log().MajorSection("Find %q", what)
	mth, goof := MakeMonth(MakeInt64(2013), 3, tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Month")
	}
	expected, _ := MakeDate(mth, 17)
	actual, goof := mth.Find(what, lang, NewIntervals())
	Env.Log().IsError(goof)
	if !actual.Equals(expected) {
		t.Fatalf("Date should be %v but was %v", expected, actual)
	}
}

func TestMonthFind2(t *testing.T) {
	what := "second business day after the first Monday"
	Env.Log().MajorSection("Find %q", what)
	mth, goof := MakeMonth(MakeInt64(2013), 3, tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Month")
	}
	expected, _ := MakeDate(mth, 6)
	actual, goof := mth.Find(what, lang, NewIntervals())
	Env.Log().IsError(goof)
	if !actual.Equals(expected) {
		t.Fatalf("Date should be %v but was %v", expected, actual)
	}
}

func TestMonthFind3(t *testing.T) {
	what := "the 2nd weekday prior to the 25th"
	Env.Log().MajorSection("Find %q", what)
	mth, goof := MakeMonth(MakeInt64(2013), 3, tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Month")
	}
	expected, _ := MakeDate(mth, 21)
	actual, goof := mth.Find(what, lang, NewIntervals())
	Env.Log().IsError(goof)
	if !actual.Equals(expected) {
		t.Fatalf("Date should be %v but was %v", expected, actual)
	}
}

func TestMonthFind4(t *testing.T) {
	what := "the 2nd weekday prior to the end of the month"
	Env.Log().MajorSection("Find %q", what)
	mth, goof := MakeMonth(MakeInt64(2013), 3, tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Month")
	}
	expected, _ := MakeDate(mth, 28)
	actual, goof := mth.Find(what, lang, NewIntervals())
	Env.Log().IsError(goof)
	if !actual.Equals(expected) {
		t.Fatalf("Date should be %v but was %v", expected, actual)
	}
	mth, goof = MakeMonth(MakeYear(2010), 3, tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Month")
	}
	expected, _ = MakeDate(mth, 29)
	actual, goof = mth.Find(what, lang, NewIntervals())
	Env.Log().IsError(goof)
	if !actual.Equals(expected) {
		t.Fatalf("Date should be %v but was %v", expected, actual)
	}
}

func TestMonthFind5(t *testing.T) {
	what := "the first weekday after the 22nd"
	Env.Log().MajorSection("Find %q", what)
	mth, goof := MakeMonth(MakeYear(2013), 3, tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Month")
	}
	expected, _ := MakeDate(mth, 25)
	actual, goof := mth.Find(what, lang, NewIntervals())
	Env.Log().IsError(goof)
	if !actual.Equals(expected) {
		t.Fatalf("Date should be %v but was %v", expected, actual)
	}
}

// Test Duration.
func TestDuration(t *testing.T) {
	Env.Log().MajorSection("Create a duration from two times")
	cc1, goof := BuildCalClock(1972, 2, 28, 23, 30, 0, ZeroFrac(), tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem building CalClock")
	}
	dy := 0
	dm := 0
	dd := 5
	dh := 3
	dmin := 46
	ds := 12
	df := MakeMilliFrac(217)
	cc2, goof := cc1.Plus(dy, dm, dd, dh, dmin, ds, df)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem adding to CalClock")
	}
	intv, goof := BuildInterval(cc1, cc2)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Interval")
	}
	result := intv.ToDuration(tcal)
	dsec := dd*86400 + dh*3600 + dmin*60 + ds
	rsec := ZeroFrac().SetInt64(int64(dsec))
	expected := NewDuration().SetValue(ZeroFrac().Add(rsec, df))
	if !result.Equals(expected) {
		t.Fatalf("Expected %v but got %v", expected, result)
	}
	// Days
	result = Days(900)
	expected = MakeDuration(900 * 86400)
	if !result.Equals(expected) {
		t.Fatalf("Expected %v but got %v", expected, result)
	}
}

// Test sorting of Intervals.
func TestIntervalsSort(t *testing.T) {
	Env.Log().MajorSection("Sort intervals")
	intvs, goof := makeIntervals(tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making intervals")
	}
	intervals, goof := MakeIntervals(intvs[2], intvs[3], intvs[1])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	intervals = intervals.Add(intvs[4], intvs[0])
	expected, goof := BuildIntervals(intvs[0:5])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}

	Env.Log().Advise("Raw, mixed: %v", intervals)
	displayIntervals(intervals, tcal)
	Env.Log().Advise("Sorted    : %v", intervals.Sort())
	displayIntervals(intervals, tcal)
	if !intervals.Equals(expected) {
		t.Fatalf("Expected %v but got %v", expected, intervals)
	}
}

func TestIntervalsMerge(t *testing.T) {
	Env.Log().MajorSection("Merge multiple intervals")
	intvs, goof := makeIntervals(tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making intervals")
	}
	intervals, goof := MakeIntervals(intvs[2], intvs[3], intvs[1])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	intervals = intervals.Add(intvs[4], intvs[0])
	expected, goof := MakeIntervals(intvs[5], intvs[6])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	Env.Log().Advise("Raw, mixed: %v", intervals)
	displayIntervals(intervals, tcal)
	intervals = intervals.Merge()
	Env.Log().Advise("Merged    : %v", intervals)
	displayIntervals(intervals, tcal)
	if !intervals.Equals(expected) {
		t.Fatalf("Expected %v but got %v", expected, intervals)
	}
}

func TestIntersectionOfTwoIntervals(t *testing.T) {
	Env.Log().MajorSection("Intersect two intervals")
	intvs, goof := makeIntervals2(tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making intervals")
	}
	intervals, goof := MakeIntervals(intvs[0], intvs[1])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	expected, goof := MakeIntervals(intvs[5])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	Env.Log().Advise("Raw, mixed: %v", intervals)
	displayIntervals(intervals, tcal)
	intv2, goof := intvs[0].IntersectWith(intvs[1])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem performing intersection")
	}
	result, goof := MakeIntervals(intv2)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	Env.Log().Advise("Intersect : %v", result)
	displayIntervals(result, tcal)
	if !result.Equals(expected) {
		Env.Log().Advise("Expected: %v", expected)
		Env.Log().Advise("Got     : %v", result)
		t.Fatalf("Expected %v but got %v", expected, result)
	}
}

func TestIntervalsIntersectionWithInterval(t *testing.T) {
	Env.Log().MajorSection("Intersect, merge multiple intervals with another intervals")
	intvs, goof := makeIntervals(tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making intervals")
	}
	intervals, goof := MakeIntervals(intvs[2], intvs[3], intvs[1])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	intervals = intervals.Add(intvs[4], intvs[0])
	expected, goof := MakeIntervals(intvs[8], intvs[9])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	Env.Log().Advise("Raw, mixed: %v", intervals)
	displayIntervals(intervals, tcal)
	Env.Log().Advise("Bracket:")
	bracket, _ := MakeIntervals(intvs[7])
	displayIntervals(bracket, tcal)
	intervals = intervals.IntersectWithInterval(intvs[7])
	intervals = intervals.Merge()
	Env.Log().Advise("Intersect : %v", intervals)
	displayIntervals(intervals, tcal)
	if !intervals.Equals(expected) {
		Env.Log().Advise("Expected: %v", expected)
		Env.Log().Advise("Got     : %v", intervals)
		t.Fatalf("Expected %v but got %v", expected, intervals)
	}
}

func TestIntervalsIntersectionWithIntervals(t *testing.T) {
	Env.Log().MajorSection("Intersect, merge multiple intervals with another set of intervals")
	intvs, goof := makeIntervals2(tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making intervals")
	}
	intervals1, goof := MakeIntervals(intvs[0], intvs[2], intvs[4])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	intervals2, goof := MakeIntervals(intvs[1], intvs[3])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	expected, goof := MakeIntervals(intvs[5], intvs[6], intvs[7])
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Intervals")
	}
	Env.Log().Advise("Intervals1:")
	displayIntervals(intervals1, tcal)
	Env.Log().Advise("Intervals2:")
	displayIntervals(intervals2, tcal)
	intervals := intervals1.IntersectWithIntervals(intervals2)
	Env.Log().Advise("Expected:")
	displayIntervals(expected, tcal)
	Env.Log().Advise("Intersection:")
	displayIntervals(intervals, tcal)
	if !intervals.Equals(expected) {
		t.Fatalf("Expected %v but got %v", expected, intervals)
	}
}

func TestCalendarRuleByYears(t *testing.T) {
	Env.Log().MajorSection("Test calendar rule by years")
	dates, goof := makeDates("ByYearsCount", tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Dates")
	}
	rule := MakeCalendarRule(tcal).ByYears(dates[0], 1).WithCountLimit(3)

	iter := rule.MintIterator()
	result := NewDates()
	for i := 0; i < 3; i++ {
		next, goof := iter.Pop()
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem generating next date using rule %v", rule)
		}
		result = append(result, next)
	}

	expected := Dates(dates[0:3])
	if !result.Equals(expected) {
		t.Fatalf("Expected %v but got %v", expected, result)
	}
}

func TestCalendarRuleByExplicitMonths(t *testing.T) {
	Env.Log().MajorSection("Test calendar rule by explicit months count")
	dates, goof := makeDates("ByExplicitMonthsCount", tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Dates")
	}
	dinc, _ := MakeDayIncFromString("third Thursday", lang, tcal)
	monthSet := MakeMonthSet(tcal, lang)
	months := []string{"march", "may", "august"}
	for _, month := range months {
		monthSet.Add(month, lang)
	}
	startMonth, goof := MakeMonth(MakeYear(2013), 3, tcal)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making Month")
	}
	rule := MakeCalendarRule(tcal)
	rule = rule.ByExplicitMonths(startMonth, monthSet, dinc)
	rule = rule.WithCountLimit(3)

	iter := rule.MintIterator()
	next := NewDate()
	result := NewDates()
	for i := 0; i < 3; i++ {
		next, goof = iter.Pop()
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem generating next date using rule %v", rule)
		}
		result = append(result, next)
	}

	expected := Dates(dates[0:3])
	if !result.Equals(expected) {
		t.Fatalf("Expected %v but got %v", expected, result)
	}

	// Use a duration that excludes the last result
	rule = rule.WithDuration(Days(90))
	iter.Reset(NewDate())
	result = NewDates()
	endDate, goof := iter.Peek().PlusDuration(rule.Duration())
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making end date")
	}
	for {
		if iter.Peek().IsAfter(endDate) {
			break
		}
		next, goof = iter.Pop()
		if Env.Log().IsError(goof) {
			t.Fatalf("Problem generating next date using rule %v", rule)
		}
		result = append(result, next)
	}

	expected = Dates(dates[0:2])
	if !result.Equals(expected) {
		t.Fatalf("Expected %v but got %v", expected, result)
	}
}

func TestTimeParsing(t *testing.T) {
	Env.Log().MajorSection("Parse date and time strings")
	inputs := []string{
		"SUN 12:00",
		"Monday 9:00:0.00345 pm",
		"2014-Feb-24 09:00:00",
	}

	sun, _ := tcal.DayNumber("sun", lang)
	mon, _ := tcal.DayNumber("mon", lang)
	expected := []*TimeFieldHolder{}
	holder := NewTimeFieldHolder()
	holder.SetDayOfWeek(sun)
	holder.SetHour(12)
	holder.SetMinute(0)
	expected = append(expected, holder)
	holder = NewTimeFieldHolder()
	holder.SetDayOfWeek(mon)
	holder.SetHour(21)
	holder.SetMinute(0)
	holder.SetSecond(0)
	holder.SetFracSec(big.NewRat(345, 100000))
	expected = append(expected, holder)
	holder = NewTimeFieldHolder()
	holder.SetYear(2014)
	holder.SetMonth(2)
	holder.SetDay(24)
	holder.SetHour(9)
	holder.SetMinute(0)
	holder.SetSecond(0)
	expected = append(expected, holder)

	if len(inputs) != len(expected) {
		t.Fatalf("Number of inputs and expected do not match")
	}
	for i, input := range inputs {
		fields, goof := Parse(input, lang, tcal)
		if Env.Log().IsError(goof) {
			t.Fatalf("Error parsing %q", input)
		}
		if !fields.Equals(expected[i]) {
			t.Fatalf(
				"When parsing %q, expected %s but got %s",
				input, expected[i].LocalisedString(tcal, lang),
				fields.LocalisedString(tcal, lang))
		}
	}
}

func TestTimeStamp(t *testing.T) {
	Env.Log().MajorSection("Test time stamp")

	Env.Log().MinorSection("Make and parse time stamp with some info")
	now, goof := NowUTC(Env.Calendar())
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making %T", now)
	}
	ts0, _, goof := BuildTimeStamp(now, "This is a test, over.")
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making time stamp")
	}
	Env.Log().Basic("Time stamp = %q", ts0)
	cc, info, _, goof := ParseTimeStamp(ts0)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem parsing time stamp")
	}
	ts1, _, goof := BuildTimeStamp(cc, info)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making time stamp")
	}
	if ts0 != ts1 {
		t.Fatalf("Expected %q, got %q", ts0, ts1)
	}

	Env.Log().MinorSection("Make and parse the simplest time stamp")
	ts2, bi0, goof := NewTimeStamp()
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making time stamp")
	}
	_, _, bi1, goof := ParseTimeStamp(ts2)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem parsing time stamp")
	}
	if bi0.Cmp(bi1) != 0 {
		t.Fatalf("Expected %q, got %q", bi0, bi1)
	}
}

// SUPPORT FUNCTIONS ==========================================================

// Days of month
//  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 ...
// -=--=--=--=--=--=--=--=--=--=--=--=--=--=--=--=-->
// Overlapping intervals:
//  +---- 0 ----+
//        +---- 1 ----+
//              +--- 2 --+
//                             +---- 3 ----+
//                                   +-- 4 ---+
// Expected merge result:
//  +--------- 5 --------+     +------ 6 -----+
// Intersecting bracket:
//     +------ 7 -----------------+
// Expected intersection:
//     +--- 8 -----------+
//                             +-9+
func makeIntervals(cal Calendar) ([]*Interval, *Goof) {
	intvs := []*Interval{}
	yr := 2012
	zero := ZeroFrac()
	perth, _ := LoadLocation("Australia/Perth")
	//              0  1  2  3  4  5  6   7   8   9  10  11
	days := []uint8{1, 5, 3, 7, 5, 8, 10, 14, 12, 15, 2, 11}
	zones := []*Location{
		UTC,   // 0
		UTC,   // 1
		perth, // 2
		perth, // 3
		UTC,   // 4
		UTC,   // 5
		UTC,   // 6
		UTC,   // 7
		UTC,   // 8
		UTC,   // 9
		UTC,   // 10
		UTC,   // 11
	}
	times := make([]*CalClock, len(days))
	for i, day := range days {
		cc, goof := BuildCalClock(yr, 1, day, 10, 30, 0, zero, cal)
		if Env.Log().IsError(goof) {
			return intvs, goof
		}
		lt, goof := MakeLocalTime(cc, zones[i]).At(UTC)
		if Env.Log().IsError(goof) {
			return intvs, goof
		}
		cc, goof = lt.ToCalClock(cal)
		if Env.Log().IsError(goof) {
			return intvs, goof
		}
		times[i] = cc
	}
	intvs = make([]*Interval, 10)
	intvs[0] = MakeIntervalUsingCalClocks(times[0], times[1])
	intvs[1] = MakeIntervalUsingCalClocks(times[2], times[3])
	intvs[2] = MakeIntervalUsingCalClocks(times[4], times[5])
	intvs[3] = MakeIntervalUsingCalClocks(times[6], times[7])
	intvs[4] = MakeIntervalUsingCalClocks(times[8], times[9])
	// expected from merge
	intvs[5] = MakeIntervalUsingCalClocks(times[0], times[5])
	intvs[6] = MakeIntervalUsingCalClocks(times[6], times[9])
	// intersecting bracket
	intvs[7] = MakeIntervalUsingCalClocks(times[10], times[11])
	// expected from intersection
	intvs[8] = MakeIntervalUsingCalClocks(times[10], times[5])
	intvs[9] = MakeIntervalUsingCalClocks(times[6], times[11])
	return intvs, NO_GOOF
}

// Days of month
//  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 ...
// -=--=--=--=--=--=--=--=--=--=--=--=--=--=--=--=-->
// Overlapping intervals:
//  +---- 0 ----+
//        +---- 1 ----+
//                 +--- 2 --+
//                             +---- 3 ----+
//                                   +-- 4 ---+
// Expected intersection:
//        +- 5 -+  +6-+              +- 7 -+
func makeIntervals2(cal Calendar) ([]*Interval, *Goof) {
	intvs := []*Interval{}
	yr := 2012
	zero := ZeroFrac()
	perth, _ := LoadLocation("Australia/Perth")
	//              0  1  2  3  4  5  6   7   8   9
	days := []uint8{1, 5, 3, 7, 6, 9, 10, 14, 12, 15}
	zones := []*Location{
		UTC,   // 0
		UTC,   // 1
		perth, // 2
		perth, // 3
		UTC,   // 4
		UTC,   // 5
		UTC,   // 6
		UTC,   // 7
		UTC,   // 8
		UTC,   // 9
	}
	times := make([]*CalClock, len(days))
	for i, day := range days {
		cc, goof := BuildCalClock(yr, 1, day, 10, 30, 0, zero, cal)
		if Env.Log().IsError(goof) {
			return intvs, goof
		}
		lt, goof := MakeLocalTime(cc, zones[i]).At(UTC)
		if Env.Log().IsError(goof) {
			return intvs, goof
		}
		cc, goof = lt.ToCalClock(cal)
		if Env.Log().IsError(goof) {
			return intvs, goof
		}
		times[i] = cc
	}
	intvs = make([]*Interval, 10)
	intvs[0] = MakeIntervalUsingCalClocks(times[0], times[1])
	intvs[1] = MakeIntervalUsingCalClocks(times[2], times[3])
	intvs[2] = MakeIntervalUsingCalClocks(times[4], times[5])
	intvs[3] = MakeIntervalUsingCalClocks(times[6], times[7])
	intvs[4] = MakeIntervalUsingCalClocks(times[8], times[9])
	// expected from intersection
	intvs[5] = MakeIntervalUsingCalClocks(times[2], times[1])
	intvs[6] = MakeIntervalUsingCalClocks(times[4], times[3])
	intvs[7] = MakeIntervalUsingCalClocks(times[8], times[7])
	return intvs, NO_GOOF
}

func displayIntervals(intvs Intervals, cal Calendar) {
	Env.Log().Advise("Days of month:")
	Env.Log().Advise("1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20")
	Env.Log().Advise("+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+")
	for _, intv := range intvs {
		cc1, _ := intv.Start().ToCalClock(cal)
		cc2, _ := intv.End().ToCalClock(cal)
		str := ""
		for i := uint8(1); i <= cc2.Day(); i++ {
			if i < cc1.Day() {
				str += "   "
				continue
			}
			if i == cc1.Day() {
				str += "+--"
				continue
			}
			if i < cc2.Day() {
				str += "---"
				continue
			}
			if i == cc2.Day() {
				str += "--+"
				continue
			}
		}
		Env.Log().Advise("%s", str)
	}
}

func makeDates(choice string, cal Calendar) ([]*Date, *Goof) {
	dates := []*Date{}
	y := []int{}
	m := []uint8{}
	d := []uint8{}
	switch choice {
	case "ByYearsCount":
		y = []int{2010, 2011, 2012}
		m = []uint8{3, 3, 3}
		d = []uint8{6, 6, 6}
	case "ByExplicitMonthsCount":
		y = []int{2013, 2013, 2013}
		m = []uint8{3, 5, 8}
		d = []uint8{21, 16, 15}
	}
	for i, _ := range y {
		date, goof := BuildDate(y[i], m[i], d[i], cal)
		if IsError(goof) {
			return dates, goof
		}
		dates = append(dates, date)
	}
	return dates, NO_GOOF
}
