/*
	A REPL for a Workspace.
*/
package h2go

import (
	"fmt"
	"github.com/peterh/liner"
	"io"
	"net/url"
	"os"
	"strings"
)

type Console struct {
	*Workspace
	here        string
	historyPath string
	cfiler      *ConfigFiler
	log         *Logger
	logpath     string
	liner       *liner.State
	out         io.Writer
	origin      url.URL
	shutdown    func() *Goof
}

func (cs *Console) Here() string           { return cs.here }
func (cs *Console) HistoryPath() string    { return cs.historyPath }
func (cs *Console) Config() *ConfigFiler   { return cs.cfiler }
func (cs *Console) Log() *Logger           { return cs.log }
func (cs *Console) LogPath() string        { return cs.logpath }
func (cs *Console) Liner() *liner.State    { return cs.liner }
func (cs *Console) Writer() io.Writer      { return cs.out }
func (cs *Console) Origin() url.URL        { return cs.origin }
func (cs *Console) Shutdown() func() *Goof { return cs.shutdown }

func NewConsole() *Console {
	return &Console{
		Workspace: NewWorkspace(),
		cfiler:    NewConfigFiler(),
		log:       NewLogger(),
		liner:     liner.NewLiner(),
		out:       os.Stdout,
		shutdown:  func() *Goof { return NO_GOOF },
	}
}

func (ws *Workspace) MintConsole(root string, out io.Writer) *Console {
	cs := NewConsole()
	cs.Workspace = ws
	cs.root = root
	cs.out = out
	return cs
}

func (cs *Console) Init(hereDir, historyPath, logPath, configPath string, splashLines []string, cfiler *ConfigFiler, shutdown func() *Goof, box *Box) *Goof {
	// Direct global logging to app log
	if !Env.IsTest() {
		cs.logpath = logPath
		Env.Log().Basic("Appending all debug logging to file %s", cs.LogPath())
		cs.log = MakeFileLogger(cs.LogPath())
		Env.log = cs.log // switch global screen logging to file logging
	} else {
		cs.log = Env.Log()
	}
	if shutdown == nil {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Must provide a non-nil shutdown function")
	}
	cs.shutdown = shutdown
	info, err := os.Stat(hereDir)
	if IsError(err) {
		return Env.Goofs().WrapGoof("GETTING_DIR_INFO", err,
			"While initialising a %T", cs)
	}
	if !info.IsDir() {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Must provide the current directory when initialising a %T, "+
				"%q is a file", cs, hereDir)
	}
	cs.here = hereDir
	Env.Log().Basic("Current directory is %s", cs.Here)

	cs.cfiler = cfiler
	_, _, _, goof := cs.cfiler.Load(configPath)
	if IsError(goof) {
		return goof
	}

	cs.liner = liner.NewLiner()
	cs.liner.SetCompleter(cs.TabCompleter)

	// Splash text
	if len(splashLines) > 0 {
		cs.Log().DumpQuietly(splashLines)
	}

	// Grab IP address of this machine
	local, goof := GetLocalAddress()
	if IsError(goof) {
		return goof
	}
	u, err := url.Parse("http://" + local)
	cs.origin = *u
	if IsError(err) {
		return cs.WrapGoof("PARSING_URL", err,
			"While getting local url")
	}
	cs.Log().Basic("Local IP address is %v", cs.origin.Host)

	// History
	cs.historyPath = historyPath
	goof = Touch(cs.historyPath)
	if IsError(goof) {
		return cs.WrapGoof("WRITING_FILE", goof,
			"Problem touching history file %q", cs.historyPath)
	}
	if info, err := os.Stat(cs.historyPath); !IsError(err) && info.Size() > 0 {
		hfile, err := os.Open(cs.historyPath)
		if IsError(err) {
			return cs.WrapGoof("OPENING_FILE", err,
				"Problem opening history file %q", cs.historyPath)
		}
		_, err = cs.liner.ReadHistory(hfile)
		if IsError(err) {
			return cs.WrapGoof("READING_FILE", err,
				"Problem reading history file %q", cs.historyPath)
		}
	}
	return NO_GOOF
}

func (cs *Console) RunFile(filename string) {
	arg := strings.Trim(filename, " \"'")
	line := "run " + arg
	cs.liner.AppendHistory(line)
	cs.NewProcess(line, "invocation")
	cs.SaveHistory()
	os.Exit(0)
}

func (cs *Console) Exit() {
	fmt.Printf("%s%sbye\n", cs.Prefixes["OK"], cs.Prefixes["PROMPT"])
	cs.liner.Close()
	os.Exit(0)
}

// Main interaction loop.
func (cs *Console) Start() {
	defer cs.liner.Close()
	// Read interactive console commands
	cs.liner.SetCtrlCAborts(false)
	line := ""
	var err error

	goof := NO_GOOF
	var out io.Writer
	var outRedirect io.Writer
	var file *os.File
console_loop:
	for {
		line, err = cs.liner.Prompt(cs.PREFIX_OK)
		if IsError(err) {
			if err == io.EOF {
				goof = cs.shutdown()
				if !IsError(goof) {
					fmt.Println("bye")
				} else {
					fmt.Printf("\n%sShutdown: %v\n", cs.PREFIX_ERR, goof)
				}
				break
			}
			fmt.Printf("%s%v\n", cs.PREFIX_ERR, err)
			continue console_loop
		}
		if len(line) == 0 {
			continue console_loop
		}
		line = TrimText(line)
		cs.liner.AppendHistory(line)
		outRedirect = cs.NewProcess(line, "command line entry")
		out = cs.out
		file = nil
		if outRedirect != nil {
			file, _ = outRedirect.(*os.File)
			out = outRedirect
		}
		_, goof = cs.WriteOutput(out)
		if IsError(goof) {
			fmt.Println(cs.PREFIX_ERR + goof.Msg)
		}
		_, goof = cs.WriteGoof(out)
		if IsError(goof) {
			fmt.Println(cs.PREFIX_ERR + goof.Msg)
		}
		if file != nil {
			file.Close()
		}
	}
	cs.SaveHistory()
}

func (cs *Console) SaveHistory() {
	hfile, err := os.Create(cs.historyPath)
	defer hfile.Close()
	if IsError(err) {
		cs.Problem("OPENING_FILE",
			"Problem opening history file %q: %v",
			cs.HistoryPath, err)
	}
	_, err = cs.liner.WriteHistory(hfile)
	if IsError(err) {
		cs.Problem("WRITING_FILE",
			"Problem saving history file %q: %v",
			cs.HistoryPath, err)
	}
	return
}

// CRYPTO ---------------------------------------------------------------------

func (cs *Console) AskForUser(prompt string) (string, error) {
	if Env.IsTest() {
		if len(TEST_MAILBOX_USER) > 0 {
			return Env.WaitForMailString(TEST_MAILBOX_USER)
		}
		return "", Env.Goofs().MakeGoof("PARAMETER_UNDEFINED",
			"Set TEST_MAILBOX_USER to allow automated test "+
				"user prompt completion")
	} else {
		return cs.liner.Prompt(prompt)
	}
}

func (cs *Console) AskForPass(prompt string) (string, error) {
	if Env.IsTest() {
		if len(TEST_MAILBOX_PASS) > 0 {
			return Env.WaitForMailString(TEST_MAILBOX_PASS)
		}
		return "", Env.Goofs().MakeGoof("PARAMETER_UNDEFINED",
			"Set TEST_MAILBOX_PASS to allow automated test "+
				"passphrase prompt completion")
	} else {
		return cs.liner.PasswordPrompt(prompt)
	}
}

func (cs *Console) makeSecret() *Secret {
	secret := NewSecret()
	// Get the pass phrase
	pass, err := cs.AskForPass("Enter passphrase> ")
	if cs.IsError(err) {
		cs.Problem("USER_INPUT",
			"Problem with obtaining passphrase: %v", err)
		return secret
	}
	pass2, err := cs.AskForPass("Repeat passphrase> ")
	if cs.IsError(err) {
		cs.Problem("USER_INPUT",
			"Problem with obtaining repeat of passphrase: %v", err)
		return secret
	}
	// Repeat entry
	if pass != pass2 {
		cs.Problem("BAD_ARGUMENTS",
			"The passphrases differ, aborting")
		return secret
	}

	// Ask for secret
	msg, err := cs.AskForPass("Enter secret> ")
	if cs.IsError(err) {
		cs.Problem("USER_INPUT",
			"Problem with obtaining secret: %v", err)
		return secret
	}
	if len(msg) == 0 {
		cs.Problem("BAD_ARGUMENTS",
			"Empty secret, aborting")
		return secret
	}
	msg = fmt.Sprintf("%s%s", PREFIX_SECRET, msg) // prepend validation text
	secret, _, goof := BuildSecret(pass, EmptyBox())
	if cs.IsError(goof) {
		cs.Problem("ENCRYPTING",
			"Problem creating secret: %v", goof)
		return secret
	}
	_, goof = secret.Encrypt([]byte(msg))
	if cs.IsError(goof) {
		cs.Problem("ENCRYPTING",
			"Problem encrypting secret: %v", goof)
		return secret
	}
	return secret
}

func (cs *Console) CmdNotImplemented(cmd string) *Goof {
	return cs.MakeGoof("BAD_ARGUMENTS",
		"Command %q not yet implemented", cmd)
}
