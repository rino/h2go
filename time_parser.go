/*
 Provides a parser to enable conversion of natural language descriptions to
 Time objects.

 Translated from the original java CalClock implementation.

 Examples of input:
   2011-01-03
   2011-01-03 14:03:00
   14:03:00.234567, 3/1/2011
   3rd January 2011
   3rd January 2011 02.03 pm
   2.03 pm Jan 3, 2011
   3/1/2011
   Jan 3, 2011

 This parser is designed to be as low level as possible for
 speed.  It uses two passes, the first (lexer) identifies tokens,
 while the second (parser) interprets the pattern of tokens.
 Example processing patterns.

 Character Key:
  d digit
  l letter
  s space
  v divider

 Token Key:
  N number
  W word
  G gap
  V divider

 Input: 2011-01-03
 Char:  ddddvddvdd
 Token:     N  N N
 Parse:     |  | |
     year --+  | +--
    month*--+  | +--
      day --+  |
               +--
  * if year or date, but not both, have been defined
 Note:  '-' is a recognised date divider that helps
        interpretation by the parser.

 Input  14:03:00.234567, 3/1/2011
 Char:  ddvddvddvddddddvsdvdvdddd
 Token:   N  N  N      N
 Note:

 Input  14:03:00.234567, 3/1/2011
 Input  3rd January 2011
 Input  3rd January 2011 02.03 pm
 Input  2.03 pm Jan 3, 2011
 Input  3/1/2011
 Input  Jan 3, 2011

 NOTE First thing to check with debug is that we are breaking out of loops
 once valid values have been parsed.
*/
package h2go

import (
	"fmt"
	"math/big"
	"strconv"
	"strings"
)

// TimeFieldHolder, a generic container for time fields.

type TimeFieldHolderStates struct {
	year      *State
	month     *State
	day       *State
	hour      *State
	minute    *State
	second    *State
	fracSec   *State
	dayOfWeek *State
}

func NewTimeFieldHolderStates() *TimeFieldHolderStates {
	return &TimeFieldHolderStates{
		year:      NewState(),
		month:     NewState(),
		day:       NewState(),
		hour:      NewState(),
		minute:    NewState(),
		second:    NewState(),
		fracSec:   NewState(),
		dayOfWeek: NewState(),
	}
}

func (fstates *TimeFieldHolderStates) Year() *State      { return fstates.year }
func (fstates *TimeFieldHolderStates) Month() *State     { return fstates.month }
func (fstates *TimeFieldHolderStates) Day() *State       { return fstates.day }
func (fstates *TimeFieldHolderStates) Hour() *State      { return fstates.hour }
func (fstates *TimeFieldHolderStates) Minute() *State    { return fstates.minute }
func (fstates *TimeFieldHolderStates) Second() *State    { return fstates.second }
func (fstates *TimeFieldHolderStates) FracSec() *State   { return fstates.fracSec }
func (fstates *TimeFieldHolderStates) DayOfWeek() *State { return fstates.dayOfWeek }

type TimeFieldHolder struct {
	year      int
	month     uint8
	day       int
	hour      int
	minute    int
	second    int
	fracSec   *big.Rat
	dayOfWeek uint8
	*Location
	states *TimeFieldHolderStates
	*State
}

func NewTimeFieldHolder() *TimeFieldHolder {
	return &TimeFieldHolder{
		fracSec:  ZeroFrac(),
		Location: NewLocation(),
		states:   NewTimeFieldHolderStates(),
		State:    NewState(),
	}
}

func (fields *TimeFieldHolder) Year() int                      { return fields.year }
func (fields *TimeFieldHolder) Month() uint8                   { return fields.month }
func (fields *TimeFieldHolder) Day() int                       { return fields.day }
func (fields *TimeFieldHolder) Hour() int                      { return fields.hour }
func (fields *TimeFieldHolder) Minute() int                    { return fields.minute }
func (fields *TimeFieldHolder) Second() int                    { return fields.second }
func (fields *TimeFieldHolder) FracSec() *big.Rat              { return fields.fracSec }
func (fields *TimeFieldHolder) DayOfWeek() uint8               { return fields.dayOfWeek }
func (fields *TimeFieldHolder) States() *TimeFieldHolderStates { return fields.states }

func (fields *TimeFieldHolder) SetYear(val int) *TimeFieldHolder {
	fields.year = val
	fields.states.year.NotEmpty()
	return fields
}
func (fields *TimeFieldHolder) SetMonth(val uint8) *TimeFieldHolder {
	fields.month = val
	fields.states.month.NotEmpty()
	return fields
}
func (fields *TimeFieldHolder) SetDay(val int) *TimeFieldHolder {
	fields.day = val
	fields.states.day.NotEmpty()
	return fields
}
func (fields *TimeFieldHolder) SetHour(val int) *TimeFieldHolder {
	fields.hour = val
	fields.states.hour.NotEmpty()
	return fields
}
func (fields *TimeFieldHolder) SetMinute(val int) *TimeFieldHolder {
	fields.minute = val
	fields.states.minute.NotEmpty()
	return fields
}
func (fields *TimeFieldHolder) SetSecond(val int) *TimeFieldHolder {
	fields.second = val
	fields.states.second.NotEmpty()
	return fields
}
func (fields *TimeFieldHolder) SetFracSec(val *big.Rat) *TimeFieldHolder {
	fields.fracSec = val
	if val != nil {
		fields.states.fracSec.NotEmpty()
	}
	return fields
}
func (fields *TimeFieldHolder) SetDayOfWeek(val uint8) *TimeFieldHolder {
	fields.dayOfWeek = val
	fields.states.dayOfWeek.NotEmpty()
	return fields
}

func (fields *TimeFieldHolder) Equals(other *TimeFieldHolder) bool {
	return fields.Year() == other.Year() &&
		fields.Month() == other.Month() &&
		fields.Day() == other.Day() &&
		fields.Hour() == other.Hour() &&
		fields.Minute() == other.Minute() &&
		fields.Second() == other.Second() &&
		fields.DayOfWeek() == other.DayOfWeek() &&
		fields.FracSec().Cmp(other.FracSec()) == 0 &&
		fields.Location.Name() == fields.Location.Name()
}

func (fields *TimeFieldHolder) LocalisedString(cal Calendar, lang Language) string {
	day, _ := cal.DayPhrase(fields.DayOfWeek(), lang)
	return fmt.Sprintf(
		"(Y:%d M:%d D:%d H:%d Min:%d S:%d F:%v "+
			"DOW:%q Loc:%v)",
		fields.Year(),
		fields.Month(),
		fields.Day(),
		fields.Hour(),
		fields.Minute(),
		fields.Second(),
		fields.FracSec(),
		day,
		fields.Location)
}

// No error checking is included.  Result is UTC.
func (fields *TimeFieldHolder) ToCalClock(cal Calendar) (*CalClock, *Goof) {
	return BuildCalClock(
		fields.Year(),
		fields.Month(),
		uint8(fields.Day()),
		uint8(fields.Hour()),
		uint8(fields.Minute()),
		uint8(fields.Second()),
		fields.FracSec(),
		cal)
}

// Interprets time as if it was local.
func (fields *TimeFieldHolder) ToLocalTime(cal Calendar) (Time, *Goof) {
	if fields.Location.IsEmpty() {
		return NewLocalTime(), Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"%T Location is empty", fields)
	}
	cc, goof := fields.ToCalClock(cal)
	if IsError(goof) {
		return NewLocalTime(), goof
	}
	return MakeLocalTime(cc, fields.Location), NO_GOOF
}

// Interprets time as if it was local, for given location.
func (fields *TimeFieldHolder) ToLocalTimeAt(loc *Location, cal Calendar) (Time, *Goof) {
	cc, goof := fields.ToCalClock(cal)
	if IsError(goof) {
		return NewLocalTime(), goof
	}
	return MakeLocalTime(cc, loc), NO_GOOF
}

// Rounds to nearest second but returns error if other clock fields undefined.
func ParseToLocalTimeAt(input string, loc *Location, lang Language, cal Calendar) (Time, *Goof) {
	lt := NewLocalTime()
	fields, goof := Parse(input, lang, cal)
	if IsError(goof) {
		return lt, goof
	}
	if fields.States().Second().IsEmpty() {
		return lt, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Second undefined")
	}
	if fields.States().Minute().IsEmpty() {
		return lt, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Minute undefined")
	}
	if fields.States().Hour().IsEmpty() {
		return lt, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Hour undefined")
	}
	return fields.ToLocalTimeAt(loc, cal)
}

func ParseToDate(input string, lang Language, cal Calendar) (*Date, *Goof) {
	fields, goof := Parse(input, lang, cal)
	if IsError(goof) {
		return NewDate(), goof
	}
	return BuildDate(
		fields.Year(),
		fields.Month(),
		uint8(fields.Day()),
		cal)
}

func ParseToClock(input string, lang Language, cal Calendar) (*Clock, *Goof) {
	fields, goof := Parse(input, lang, cal)
	if IsError(goof) {
		return NewClock(), goof
	}
	return BuildClock(
		uint8(fields.Hour()),
		uint8(fields.Minute()),
		uint8(fields.Second()),
		fields.FracSec())
}

// Token and constants used by the parser.

type Token struct {
	Value     string
	Type      int // ParserTokenType
	CharStart int
}

func tokenSpecToString(input string, tokens []Token, j int, qualifier string) string {
	return fmt.Sprintf(
		"The %s token %q starting at character %d of input %q",
		qualifier, tokens[j].Value, tokens[j].CharStart, input)
}

// Parse.

/*
// Backup or for comparison, with UTC.
func Parse2(template, source string, cal Calendar) (*CalClock, error) {
	t, err := time.Parse(template, source)
	if err != nil {return nil, err}
	return MakeGoTime(&t).ToCalClock(cal)
}
*/

func Parse(input string, lang Language, cal Calendar) (*TimeFieldHolder, *Goof) {

	fields := NewTimeFieldHolder()

	var err error

	// Lexer
	curr := uint8(0)
	currCharType := TIME_LEXER_CHAR_TYPE_NONE
	prevCharType := TIME_LEXER_CHAR_TYPE_NONE
	startNewToken := false
	sameAsLast := false
	atStartOfInput := false
	atEndOfInput := false
	onePastEndOfInput := false
	captureToken := false
	currTokenStart := 0
	prevTokenStart := 0

	// Parser
	tokens := []Token{}
	currTokenType := TIME_PARSER_TOKEN_TYPE_NONE
	currToken := ""

	input = strings.TrimSpace(input)
	n := len(input)
	Env.Log().Fine(
		"Enter Parser, lexing %d chars of input = %q",
		n, input)

	// Lexer pass
	// Valid date/time strings use ASCII only, not UTF-8
	for i := 0; i <= n; i++ {
		atStartOfInput = (i == 0)
		atEndOfInput = (i == n-1)
		onePastEndOfInput = (i == n)

		if !atStartOfInput {
			prevCharType = currCharType
		}

		if !onePastEndOfInput {
			curr = input[i]

			if lang.IsLetter(string(curr)) {
				currCharType = TIME_LEXER_CHAR_TYPE_LETTER
			} else if lang.IsDigit(string(curr)) {
				currCharType = TIME_LEXER_CHAR_TYPE_DIGIT
			} else if IsDivider(string(curr), lang) {
				currCharType = TIME_LEXER_CHAR_TYPE_DIVIDER
			} else {
				currCharType = TIME_LEXER_CHAR_TYPE_SPACE
			}

			Env.Log().Fine(
				"Char %d = %q Type = %d",
				i, curr, currCharType)

			sameAsLast = (currCharType == prevCharType)

			if atStartOfInput {
				Env.Log().Fine("This is the first character")
			}

			if atEndOfInput {
				Env.Log().Fine("This is the last character")
			}

			// Start reading token only if character types repeat
			// Add special case of start of input
			startNewToken = !sameAsLast || atStartOfInput

			// Process if character type changes
			// Add special case of end of input
			captureToken = !sameAsLast && !atStartOfInput
		} else {
			captureToken = true
			Env.Log().Fine("Capture final token")
		}

		if startNewToken || onePastEndOfInput {
			prevTokenStart = currTokenStart
			currTokenStart = i
			Env.Log().Fine("Start new token at position %d", i)
		}

		if captureToken {
			endOfToken := i
			Env.Log().Fine(
				"Start at %d, End at %d",
				prevTokenStart, endOfToken-1)

			currToken = strings.ToLower(input[prevTokenStart:endOfToken])

			Env.Log().Fine("Capture token %q", currToken)

			switch prevCharType {
			case TIME_LEXER_CHAR_TYPE_LETTER:
				currTokenType = TIME_PARSER_TOKEN_TYPE_WORD
			case TIME_LEXER_CHAR_TYPE_DIGIT:
				currTokenType = TIME_PARSER_TOKEN_TYPE_NUMBER
			case TIME_LEXER_CHAR_TYPE_SPACE:
				currTokenType = TIME_PARSER_TOKEN_TYPE_GAP
			case TIME_LEXER_CHAR_TYPE_DIVIDER:
				currTokenType = TIME_PARSER_TOKEN_TYPE_DIVIDER
			}

			tokens = append(tokens, Token{currToken, currTokenType, i})
		}
	}

	Env.Log().Fine("Input = %q", input)
	Env.Log().Fine("Lexer output: ==================================")

	n = len(tokens)

	for i := 0; i < n; i++ {
		Env.Log().Fine(
			"Token %d = %q Type = %d",
			i, tokens[i].Value, tokens[i].Type)
	}

	Env.Log().Fine("================================================")

	atFirstToken := false
	atLastToken := false
	prevToken := ""
	prevTokenType := TIME_PARSER_TOKEN_TYPE_NONE
	nextToken := ""
	nextTokenType := TIME_PARSER_TOKEN_TYPE_NONE
	value := -1

	Env.Log().Fine("Parsing %d tokens...", n)

	// Parser pass
	for j := 0; j < n; j++ {
		currToken = tokens[j].Value
		currTokenType = tokens[j].Type

		atFirstToken = (j == 0)
		atLastToken = (j == n-1)

		Env.Log().Fine(
			"Token %d = %q Type = %d",
			j, currToken, currTokenType)

		Env.Log().Fine("  %s", fields.LocalisedString(cal, lang))

		// Skip gaps
		if currTokenType == TIME_PARSER_TOKEN_TYPE_GAP {
			Env.Log().Fine("Skipping gap")
			continue
		}

		if atFirstToken {
			Env.Log().Fine("This is the first token")
			if atLastToken {
				return fields, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
					"The input string %q "+
						"has insufficient tokens to construct "+
						"a time representation",
					input)
			}
		} else {
			// Not atFirstToken
			prevToken = tokens[j-1].Value
			prevTokenType = tokens[j-1].Type

			if !atLastToken {
				nextToken = tokens[j+1].Value
				nextTokenType = tokens[j+1].Type
			} else {
				Env.Log().Fine("This is the last token")
			}
		}

		switch currTokenType {
		case TIME_PARSER_TOKEN_TYPE_DIVIDER:
			if atFirstToken {
				// Cannot start with a divider
				return fields, Env.Goofs().MakeGoof("UNRECOGNISED_SYMBOLS",
					"%s cannot be a member of the recognised "+
						"divider set %q",
					tokenSpecToString(input, tokens, j, "first"),
					DividersToString(lang))
			}

			if atLastToken {
				// Cannot end with a divider
				return fields, Env.Goofs().MakeGoof("UNRECOGNISED_SYMBOLS",
					"%s cannot be a member of the recognised "+
						"divider set %q",
					tokenSpecToString(input, tokens, j, "last"),
					DividersToString(lang))
			}

			// There must be a number or a word to the left of a divider
			if prevTokenType != TIME_PARSER_TOKEN_TYPE_NUMBER &&
				prevTokenType != TIME_PARSER_TOKEN_TYPE_WORD {
				return fields, Env.Goofs().MakeGoof("UNRECOGNISED_SYMBOLS",
					"%s cannot be a member of the recognised "+
						"divider set %q",
					tokenSpecToString(input, tokens, j, "\b"),
					DividersToString(lang))
				// \b compensates for the double space around qualifier
			}
		case TIME_PARSER_TOKEN_TYPE_WORD:
			if atFirstToken {
				// If we start with a word, it must be a day of the
				// week, or month of the year

				if !fields.processDayOfWeekMonthOfYear(currToken, lang, cal) {
					return fields, Env.Goofs().MakeGoof("UNRECOGNISED_SYMBOLS",
						"%s is not a recognised day of the "+
							"week or month of the year",
						tokenSpecToString(input, tokens, j, "\b"))
				}
			} else {
				// It can be one of
				//   * an am/pm/noon indicator
				//   * an ordinal suffix (1)
				//   * a day of the week
				//   * a month of the year
				//
				//   (1) in which case, ignore since it is dealt with
				//       elsewhere

				if fields.processDayOfWeekMonthOfYear(currToken, lang, cal) {
					break
				}

				Env.Log().Fine("Not a day of week or month of year")

				if IsOrdinalSuffix(currToken, lang) {
					// The previous token must be a number and it should
					// be the day of the month
					if prevTokenType == TIME_PARSER_TOKEN_TYPE_NUMBER {
						if fields.States().Day().IsEmpty() {
							// validate
							value, err = strconv.Atoi(prevToken)
							if IsError(err) {
								return fields, Env.Goofs().MakeGoof("SYNTAX",
									"%s is recognised as a valid ordinal "+
										"suffix but is not preceded by a "+
										"number",
									tokenSpecToString(input, tokens, j, "\b"))

							}
							if cal.IsPossibleDay(uint8(value)) {
								fields.SetDay(value)
								Env.Log().Fine("Previous token recognised as a day")
							}
						} else {
							if fields.Day() == value {
								Env.Log().Fine("Previous day token recognised as valid")
								break
							}
							return fields, Env.Goofs().MakeGoof("DATA_MISMATCH",
								"%s is recognised as a valid ordinal "+
									"suffix but the day of the month has "+
									"already been defined",
								tokenSpecToString(input, tokens, j, "\b"))
						}
					} else {
						return fields, Env.Goofs().MakeGoof("SYNTAX",
							"%s is recognised as a valid ordinal "+
								"suffix but is not preceded by a number",
							tokenSpecToString(input, tokens, j, "\b"))
					}
					break
				}

				goof := fields.processNoonMarker(
					currToken,
					tokenSpecToString(input, tokens, j, "\b"),
					lang,
					cal)
				if IsError(goof) {
					return fields, goof
				}

			} // !atFirstToken
		case TIME_PARSER_TOKEN_TYPE_NUMBER:
			value, err = strconv.Atoi(currToken)
			if IsError(err) {
				return fields, Env.Goofs().WrapGoof("UNRECOGNISED_SYMBOLS", err,
					"While converting token %q to number", currToken)
			}

			if atFirstToken {
				// If we start with a number, this must be followed by:
				//   * a divider token which determines whether it is
				//     part of a date or clock
				//   * or an ordinal suffix or gap indicating a day

				// Can look ahead because single token input filtered
				nextToken = tokens[j+1].Value
				nextTokenType = tokens[j+1].Type

				interpretAsDay := false

				switch nextTokenType {
				case TIME_PARSER_TOKEN_TYPE_DIVIDER:
					if IsDateDivider(string(nextToken), lang) {
						// Can only be a year or a day
						// We can swap these around later if they don't
						// make sense
						Env.Log().Fine("Next token is a date divider")
						if cal.IsPossibleDay(uint8(value)) {
							fields.SetDay(value)
							Env.Log().Fine("Possible day recognised")
						} else {
							fields.SetYear(value)
							Env.Log().Fine("Cannot be a day, treat as a year")
						}
					} else { // must be a recognised Clock divider
						Env.Log().Fine("Next token is a clock divider")
						// It can only be an hour token
						if IsPossibleHour(value) {
							fields.SetHour(value)
							Env.Log().Fine("Possible hour recognised")
						} else {
							return fields, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
								"%s is recognised as an hour but is invalid",
								tokenSpecToString(input, tokens, j, "first"))
						}
					}
				case TIME_PARSER_TOKEN_TYPE_WORD:
					// This must be an ordinal suffix
					if IsOrdinalSuffix(nextToken, lang) {
						interpretAsDay = true
					} else {
						return fields, Env.Goofs().MakeGoof("UNRECOGNISED_SYMBOLS",
							"%s is followed by a word %q that is not a "+
								"recognised ordinal suffix",
							tokenSpecToString(input, tokens, j, "first"),
							nextToken)
					}
				case TIME_PARSER_TOKEN_TYPE_GAP:
					interpretAsDay = true
				}

				if interpretAsDay {
					if cal.IsPossibleDay(uint8(value)) {
						fields.SetDay(value)
						Env.Log().Fine("Interpreted as possible day")
					} else {
						return fields, Env.Goofs().MakeGoof("UNRECOGNISED_SYMBOLS",
							"%s is recognised as a month but is invalid",
							tokenSpecToString(input, tokens, j, "\b"))
					}
				}
			} else { // not atFirstToken
				// If there is a date divider on either side, the number is
				// a month
				if !atLastToken &&
					IsDateDivider(string(prevToken), lang) &&
					IsDateDivider(string(nextToken), lang) {
					Env.Log().Fine(
						"This token is surrounded by date dividers, " +
							"therefore it must be a month")
					if cal.IsPossibleMonth(uint8(value)) {
						fields.SetMonth(uint8(value))
						Env.Log().Fine("Possible month recognised")
					} else {
						return fields, Env.Goofs().MakeGoof("UNRECOGNISED_SYMBOLS",
							"%s is recognised as a month but is invalid",
							tokenSpecToString(input, tokens, j, "\b"))
					}
				}

				// If there is:
				//  * a date divider on the left and a gap or end on
				//    the right,
				//  * a gap on the left and a date divider or end or gap
				//    on the right
				// the number is either a year or a day

				dateDivLeft := IsDateDivider(string(prevToken), lang)

				dateDivRight :=
					!atLastToken && IsDateDivider(string(nextToken), lang)

				gapRight := !atLastToken && (nextTokenType == TIME_PARSER_TOKEN_TYPE_GAP)

				gapLeft := (prevTokenType == TIME_PARSER_TOKEN_TYPE_GAP)

				indeterminateDay := false

				if (dateDivLeft && (gapRight || atLastToken)) ||
					(gapLeft && (dateDivRight || gapRight || atLastToken)) {
					Env.Log().Fine("This token can be either a year or day")
					if cal.IsPossibleDay(uint8(value)) {
						Env.Log().Fine("Possible day recognised")
						if fields.States().Day().IsEmpty() {
							Env.Log().Fine("Day not yet defined, set valid day")
							fields.SetDay(value)
							break
						} else {
							indeterminateDay = true
						}
					} else {
						// No? Ok, it must be a year
						// But year may already be defined
						Env.Log().Fine("Not a valid day, but could be a year")
						if fields.States().Year().IsEmpty() {
							// If so, can the already-defined year be
							// recognised as a day?
							if cal.IsPossibleDay(uint8(fields.Year())) {
								Env.Log().Fine(
									"Previously defined year is a " +
										"valid day, swapping")
								// Ok, great, do the swap
								fields.SetDay(fields.Year())
								fields.SetYear(value)
							} else {
								// So the already-defined year can only
								// be a year, can the current token be
								// a year also?
								if cal.IsPossibleYear(value) {
									return fields, Env.Goofs().MakeGoof("DATA_MISMATCH",
										"%s is a possible year but a year %d has "+
											"already been defined",
										tokenSpecToString(input, tokens, j, "\b"),
										fields.Year())
								} else {
									// It can't be a day or a year, botheration
									return fields, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
										"%s is neither a valid year or day number",
										tokenSpecToString(input, tokens, j, "\b"))
								}
							}
						} else {
							Env.Log().Fine("Year not yet defined, setting now")
							fields.SetYear(value)
							break
						}
					}
				}

				if indeterminateDay {
					// Then we are indeterminate, we
					// have two valid days
					return fields, Env.Goofs().MakeGoof("DATA_MISMATCH",
						"%s is a possible day but a day %d "+
							"has already been defined",
						tokenSpecToString(input, tokens, j, "\b"),
						fields.Day())
				}

				// CLOCK ##############################################

				// If '.' is on the left and hour, minute and second
				// have been defined, this must be a second fraction

				dotLeft := (prevToken == ".")

				allButFracSecDefined :=
					!fields.States().Hour().IsEmpty() &&
						!fields.States().Minute().IsEmpty() &&
						!fields.States().Second().IsEmpty() &&
						fields.States().FracSec().IsEmpty()

				if dotLeft && allButFracSecDefined {
					// The precision is arbitrary
					frac, ok := ZeroFrac().SetString("0." + currToken)
					if ok {
						fields.SetFracSec(frac)
						Env.Log().Fine("Valid nanosecond recognised")
					} else {
						return fields, Env.Goofs().MakeGoof("UNRECOGNISED_SYMBOLS",
							"%s is recognised as a possible fraction "+
								"of a second but 0.%s could not be parsed",
							tokenSpecToString(input, tokens, j, "\b"),
							currToken)
					}
				}

				clockDivLeft := IsClockDivider(string(prevToken), lang)

				// If there is a clock divider on the left and both hour
				// and minute have been defined, this must be a second

				bothHourMinDefined :=
					!fields.States().Hour().IsEmpty() &&
						!fields.States().Minute().IsEmpty() &&
						fields.States().Second().IsEmpty() &&
						fields.States().FracSec().IsEmpty()

				if clockDivLeft && bothHourMinDefined {
					if IsPossibleSecond(value) {
						fields.SetSecond(value)
						Env.Log().Fine("Valid second recognised")
					} else {
						return fields, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
							"%s is recognised as a second but is invalid",
							tokenSpecToString(input, tokens, j, "\b"))
					}
				}

				// If there is a clock divider on the left and only hour
				// has been defined, this must be a minute
				onlyHourDefined :=
					!fields.States().Hour().IsEmpty() &&
						fields.States().Minute().IsEmpty() &&
						fields.States().Second().IsEmpty() &&
						fields.States().FracSec().IsEmpty()

				if clockDivLeft && onlyHourDefined {
					if IsPossibleMinute(value) {
						fields.SetMinute(value)
						Env.Log().Fine("Valid minute recognised")
					} else {
						return fields, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
							"%s is recognised as a minute but is invalid",
							tokenSpecToString(input, tokens, j, "\b"))
					}
				}

				// If there is a gap or clock divider on the left and
				// no clock fields have been defined, this must be an hour
				clockDivGapLeft :=
					clockDivLeft || (prevTokenType == TIME_PARSER_TOKEN_TYPE_GAP)

				noClockFieldsDefined :=
					fields.States().Hour().IsEmpty() &&
						fields.States().Minute().IsEmpty() &&
						fields.States().Second().IsEmpty() &&
						fields.States().FracSec().IsEmpty()

				if clockDivGapLeft && noClockFieldsDefined {
					if IsPossibleHour(value) {
						fields.SetHour(value)
						Env.Log().Fine("Valid hour recognised")
					} else {
						return fields, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
							"%s is recognised as an hour but is invalid",
							tokenSpecToString(input, tokens, j, "\b"))
					}
				}
			}
		} // switch
	} // j loop

	Env.Log().Fine("%s", fields.LocalisedString(cal, lang))
	Env.Log().Fine("Leaving parser, final checks...")

	return fields, NO_GOOF
}

func IsPossibleSecond(value int) bool {
	if value >= 0 && value <= 60 {
		return true
	}
	return false
}

func IsPossibleMinute(value int) bool {
	return IsPossibleSecond(value)
}

func IsPossibleHour(value int) bool {
	if value >= 0 && value <= 24 {
		return true
	}
	return false
}

func (fields *TimeFieldHolder) processDayOfWeekMonthOfYear(token string, lang Language, cal Calendar) bool {
	dow, ok := cal.DayNumber(token, lang)

	if ok {
		fields.SetDayOfWeek(dow)
		Env.Log().Fine("Valid day of week recognised")
	} else {
		moy, ok2 := cal.MonthNumber(token, lang)

		if ok2 {
			fields.SetMonth(moy)
			Env.Log().Fine("Valid month of year recognised")
		} else {
			return false
		}
	}

	return true
}

func (fields *TimeFieldHolder) processNoonMarker(token, errprefix string, lang Language, cal Calendar) *Goof {
	if IsMeridiemMarker(token, lang) {
		if fields.Hour() != 0 {
			if fields.Hour() > 12 {
				Env.Log().Fine("Noon marker %q is redundant, ignoring", token)
			} else {
				if IsNoonMarker(token, lang) {
					if fields.Hour() == 12 {
						// Set remaining time fields to zero
						if fields.States().Minute().IsEmpty() {
							fields.SetMinute(0)
							Env.Log().Fine("Minute set to zero")
						} else {
							if fields.Minute() != 0 {
								return Env.Goofs().MakeGoof("DATA_MISMATCH",
									"%s is present, but a non-zero minute "+
										"%d has previously been defined",
									errprefix, fields.Minute())
							}
						}
						if fields.States().Second().IsEmpty() {
							fields.SetSecond(0)
							Env.Log().Fine("Second set to zero")
						} else {
							if fields.Second() != 0 {
								return Env.Goofs().MakeGoof("DATA_MISMATCH",
									"%s is present, but a non-zero second "+
										"%d has previously been defined",
									errprefix, fields.Second())
							}
						}
						if fields.States().FracSec().IsEmpty() {
							fields.SetFracSec(ZeroFrac())
							Env.Log().Fine("Fractional second set to zero")
						} else {
							if fields.FracSec().Cmp(ZeroFrac()) != 0 {
								return Env.Goofs().MakeGoof("DATA_MISMATCH",
									"%s is present, but a non-zero second "+
										"fraction %v has previously been defined",
									errprefix, fields.FracSec())
							}
						}
					} else {
						return Env.Goofs().MakeGoof("DATA_MISMATCH",
							"%s requires that the hour previously "+
								"set to %d should have been set to 12",
							errprefix, fields.Hour())
					}
				} else if IsPmMarker(token, lang) {
					if fields.Hour() > 12 {
						Env.Log().Fine(
							"After midday marker %q is redundant, ignoring",
							token)
					} else {
						fields.SetHour(fields.Hour() + 12)
					}
				} else { // if IsAmMarker(token, lang)
					if fields.Hour() < 12 {
						Env.Log().Fine(
							"Before midday marker %q is redundant, ignoring",
							token)
					} else {
						return Env.Goofs().MakeGoof("DATA_MISMATCH",
							"%s requires that the hour previously "+
								"set to %d should have been set to less than 12",
							errprefix, fields.Hour())
					}
				}
			}
		} else {
			return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"%s is present, but an hour has no yet been defined",
				errprefix)
		}
	} else {
		return Env.Goofs().MakeGoof("UNRECOGNISED_SYMBOLS",
			"%s unrecognised midday marker %q",
			errprefix, token)
	}
	return NO_GOOF
}
