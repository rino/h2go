/*
 */
package h2go

import (
	"bytes"
	"fmt"
	"runtime"
	"strconv"
)

// ENVIRONMENT =================================================================
// Note that an Environment can only have one Logger.

type Environment struct {
	root           string // root of accessible/allowable filesystem
	ver            *Version
	log            *Logger
	types          *TypeMap
	goofs          *Enum
	langs          map[string]Language
	cals           map[string]Calendar
	config         *EnvConfig
	mail           map[string]chan interface{}
	test           bool
	recordEncoding bool
	threads        []string // thread trail leading to this Environment
	*State
}

var _ Stateful = NewEnvironment() // embedded State

func NewEnvironment() *Environment {
	return &Environment{
		root:           "/",
		ver:            NewVersion(),
		types:          NewTypeMap(),
		goofs:          NewEnum(),
		langs:          make(map[string]Language),
		cals:           make(map[string]Calendar),
		mail:           make(map[string]chan interface{}),
		test:           false,
		recordEncoding: false,
		threads:        []string{threadstamp()},
		State:          NewState(),
	}
}

// Does not include Logger, which is doubly linked, use SetLogger after
// copying.
func (env *Environment) CopyPartial() *Environment {
	env2 := NewEnvironment()
	env2.root = env.root
	env2.ver = env.ver.Copy()
	env2.types = env.types.Copy()
	env2.goofs = env.goofs.Copy()
	for k, v := range env.langs {
		env2.langs[k] = v.Copy()
	}
	for k, v := range env.cals {
		env2.cals[k] = v.Copy()
	}
	env2.config = env.config
	for k, _ := range env.mail {
		env2.mail[k] = make(chan interface{})
	}
	env2.test = env.test
	env2.recordEncoding = env.recordEncoding
	env2.threads = []string{}
	for _, thread := range env.threads {
		env2.threads = append(env2.threads, thread)
	}
	env2.State = env.State.Copy()
	return env2
}

// Avoid a self-referential infinite loop.
func (env *Environment) Copy() *Environment {
	env2 := env.CopyPartial()
	log2 := env.Log().CopyPartial()
	env2.SetLogger(log2)
	log2.SetEnv(env2)
	return env2
}

func EnvOrDefault(box *Box) *Environment {
	if box.HasEnv() {
		return box.Env()
	}
	return Env // the package specific global
}

func (env *Environment) Root() string                      { return env.root }
func (env *Environment) Version() *Version                 { return env.ver }
func (env *Environment) Log() *Logger                      { return env.log }
func (env *Environment) Types() *TypeMap                   { return env.types }
func (env *Environment) Goofs() *Enum                      { return env.goofs }
func (env *Environment) Languages() map[string]Language    { return env.langs }
func (env *Environment) Calendars() map[string]Calendar    { return env.cals }
func (env *Environment) Config() *EnvConfig                { return env.config }
func (env *Environment) Mail() map[string]chan interface{} { return env.mail }
func (env *Environment) IsTest() bool                      { return env.test }
func (env *Environment) RecordEncoding() bool              { return env.recordEncoding }
func (env *Environment) Threads() []string                 { return env.threads }

func (env *Environment) Language() Language {
	return env.Languages()[env.Config().LANGUAGE]
}

func (env *Environment) Calendar() Calendar {
	return env.Calendars()[env.Config().CALENDAR]
}

func (env *Environment) SetConfig(cfg *EnvConfig) *Environment {
	env.config = cfg
	return env
}

func (env *Environment) SetVersion(ver *Version) *Environment {
	env.ver = ver
	return env
}

// Note that the channel must be created before calling this method.
func (env *Environment) WaitForMailString(address string) (string, *Goof) {
	ch, exists := env.mail[address]
	if !exists {
		return "", env.Goofs().MakeGoof("NO_SUCH_CHANNEL",
			"Waiting for test to send string input via "+
				"Env.Mail[%q]", address)
	} else {
		val := <-ch
		strval, ok := val.(string)
		if !ok {
			return "", env.Goofs().MakeGoof("BAD_TYPE",
				"Expecting string in Env.Mail[%q] got %T",
				address, val)
		} else {
			return strval, NO_GOOF
		}
	}
}

func (env *Environment) PostMail(address string, mail interface{}) {
	env.mail[address] = make(chan interface{}, 1)
	env.mail[address] <- mail
	return
}

func (env *Environment) SetTestingOn() *Environment {
	env.test = true
	env.recordEncoding = true
	return env
}

func (env *Environment) SetLogger(logger *Logger) *Environment {
	env.log = logger
	return env
}

func (env *Environment) StampThread(post string) string {
	stamp := threadstamp() + post
	env.threads = append(env.threads, stamp)
	return stamp
}

func (env *Environment) ThreadStamps() string {
	result := ""
	for i, thread := range env.threads {
		if i > 0 {
			result += "|"
		}
		result += thread
	}
	return result
}

func (env *Environment) CopyAndStamp(post string) *Environment {
	env2 := env.Copy()
	env2.StampThread(post)
	return env2
}

// Get the current goroutine id.  Use for debugging only.
// Credit: http://blog.sgmansfield.com/2015/12/goroutine-ids/
func threadstamp() string {
	b := make([]byte, 64)
	b = b[:runtime.Stack(b, false)]
	b = b[10:]
	b = b[:bytes.IndexByte(b, ' ')]
	n, _ := strconv.ParseUint(string(b), 10, 64)
	f := "%016x"
	if n <= 65536 { // FFFF
		f = "%04x"
	} else if n <= 4294967295 { // FFFF FFFF
		f = "%08x"
	} else if n <= 281474976710655 { // FFFF FFFF FFFF
		f = "%012x"
	}
	return fmt.Sprintf("go"+f, n)
}
