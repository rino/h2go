/*
	Version designation and representation.
*/
package h2go

import (
	"strconv"
	"strings"
)

// VERSION =====================================================================

type Version struct {
	maj UINT8
	min UINT8
	rev UINT8
	*State
}

var _ Binaryable = NewVersion()
var _ Stringable = NewVersion()
var _ Stateful = NewVersion() // embedded State

// Constructors
func NewVersion() *Version {
	return &Version{
		State: NewState(),
	}
}

func MakeVersion(maj, min, rev uint) (*Version, *Goof) {
	v := NewVersion()
	if maj > 31 {
		return v, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Major version must be a member of [0..31]")
	}
	if min > 31 {
		return v, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Minor version must be a member of [0..31]")
	}
	if rev > 31 {
		return v, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"Revision version must be a member of [0..31]")
	}
	v.maj = UINT8(maj)
	v.NotEmpty()
	v.min = UINT8(min)
	v.rev = UINT8(rev)
	v.NowComplete()
	return v, NO_GOOF
}

func (v *Version) Copy() *Version {
	v2 := NewVersion()
	v2.maj = v.maj
	v2.min = v.min
	v2.rev = v.rev
	v2.State = v2.State.Copy()
	return v2
}

func (v *Version) Major() int    { return int(v.maj) }
func (v *Version) Minor() int    { return int(v.min) }
func (v *Version) Revision() int { return int(v.rev) }
func (v *Version) IsEmpty() bool {
	return v.maj == 0 && v.min == 0 && v.rev == 0
}
func (v *Version) AsKey() int {
	return VersionKey(uint(v.maj), uint(v.min), uint(v.rev))
}

func (v *Version) String() string {
	return strconv.FormatInt(int64(v.maj), 10) + "." +
		strconv.FormatInt(int64(v.min), 10) + "." +
		strconv.FormatInt(int64(v.rev), 10)
}

// Binary encoding of the h2go version, fixed for all time.
func (v *Version) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(v)
	ui16 := UINT16(v.maj)<<10 | UINT16(v.min)<<5 | UINT16(v.rev)
	byts, goof := bfr.Write(ui16, box)
	out.AppendEncodedAs("version (UINT16)", byts)
	return out, goof
}

func (v *Version) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(v)
	ui16 := UINT16(0)
	byts, goof := bfr.Read(&ui16, box)
	v.maj = UINT8(ui16 >> 10)
	v.min = UINT8((ui16 & 33760) >> 5)
	v.rev = UINT8(ui16 & 31)
	out.AppendEncodedAs("version (UINT16)", byts)
	return out, goof
}

func (v *Version) EncodeString(box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AppendEncoded(v.String())
	return out, NO_GOOF
}

func (v *Version) DecodeString(txt string, box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AutoSetField(v)
	parts := strings.Split(txt, ".")
	if len(parts) != 3 {
		return out, Env.Goofs().MakeGoof("PARSING_TEXT",
			"While parsing a %T %q, expected %d parts, got %d",
			v, txt, 3, len(parts))
	}

	i, err := strconv.Atoi(TrimText(parts[0]))
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While converting %q in %q to an integer",
			parts[0], txt)
	}
	if i < 0 || i > 255 {
		return out, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
			"Major version number must be between 0 and 255, got %d", i)
	}
	v.maj = UINT8(i)
	out.AppendEncodedAs("ver.maj", v.maj)

	i, err = strconv.Atoi(TrimText(parts[1]))
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While converting %q in %q to an integer",
			parts[1], txt)
	}
	if i < 0 || i > 255 {
		return out, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
			"Minor version number must be between 0 and 255, got %d", i)
	}
	v.min = UINT8(i)
	out.AppendEncodedAs("ver.min", v.min)

	i, err = strconv.Atoi(TrimText(parts[2]))
	if IsError(err) {
		return out, Env.Goofs().WrapGoof("PARSING_TEXT", err,
			"While converting %q in %q to an integer",
			parts[2], txt)
	}
	if i < 0 || i > 255 {
		return out, Env.Goofs().MakeGoof("OUTSIDE_ALLOWED_RANGE",
			"Revision version number must be between 0 and 255, got %d", i)
	}
	v.rev = UINT8(i)
	out.AppendEncodedAs("ver.rev", v.rev)
	return out, NO_GOOF
}

func (v *Version) Equals(other *Version) bool {
	return *v == *other
}

func (v *Version) LessThan(other *Version) bool {
	if v.maj >= other.maj {
		return false
	} else {
		if v.min >= other.min {
			return false
		} else {
			if v.rev >= other.rev {
				return false
			}
		}
	}
	return true
}

// Modify this function to handle special cases.
func (v *Version) BinaryEncodingIsCompatible(other *Version) bool {
	return true
	/*
		if v.Equals(other) || v.LessThan(other) {
			return true
		}
		return false
	*/
}

// Generate a unique key for VERSION_PARAMS without creating a *Version.
func VersionKey(maj, min, rev uint) int {
	return int((maj + 1) * (min + 1) * (rev + 1))
}

// VERSION PARAMS ==============================================================

// TODO ditch this
type VersionParams struct {
	Compressor       string
	CompressionLevel int
}

func NewVersionParams() VersionParams {
	return VersionParams{}
}
