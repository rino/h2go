package h2go

import ()

type Gregorian struct {
	name string
	//monthDicts	map[uint8]*Dictionary
	monthEnum       map[Language]Uint8Enum
	baseDaysInMonth map[uint8]uint8
	//dayDicts	map[uint8]*Dictionary
	dayEnum      map[Language]Uint8Enum
	daysInWeek   uint8
	monthsInYear uint8
	*State
}

var _ Calendar = new(Gregorian)

func NewGregorian() *Gregorian {
	return &Gregorian{
		name:            "Gregorian",
		monthEnum:       make(map[Language]Uint8Enum),
		baseDaysInMonth: make(map[uint8]uint8),
		dayEnum:         make(map[Language]Uint8Enum),
		State:           NewState(),
	}
}

func MakeGregorian(langs map[string]Language) (*Gregorian, *Goof) {
	cal := NewGregorian()
	goof := cal.makeMaps(langs)
	cal.NotEmpty()
	if !IsError(goof) {
		cal.NowComplete()
	}
	return cal, goof
}

func (cal *Gregorian) Copy() Calendar {
	cal2 := NewGregorian()
	for k, v := range cal.monthEnum {
		cal2.monthEnum[k] = v.Copy()
	}
	for k, v := range cal.baseDaysInMonth {
		cal2.baseDaysInMonth[k] = v
	}
	for k, v := range cal.dayEnum {
		cal2.dayEnum[k] = v.Copy()
	}
	cal2.daysInWeek = cal.daysInWeek
	cal2.monthsInYear = cal.monthsInYear
	cal2.State = cal.State.Copy()
	return cal2
}

func (cal *Gregorian) makeMaps(langs map[string]Language) *Goof {
	daysInWeek := uint8(0)
	monthsInYear := uint8(0)
	first := true
	firstLang := ""
	for _, lang := range langs {
		cal.dayEnum[lang] = lang.Days(cal.Name())
		if cal.dayEnum[lang].IsEmpty() {
			ExitOnError(Env.Goofs().MakeGoof("MISSING_VALUE",
				"Language %q does not provide a day enumeration for the "+
					"%q calendar", lang.Name(), cal.Name()))
		}
		cal.monthEnum[lang] = lang.Months(cal.Name())
		if cal.monthEnum[lang].IsEmpty() {
			ExitOnError(Env.Goofs().MakeGoof("MISSING_VALUE",
				"Language %q does not provide a month enumeration for the "+
					"%q calendar", lang.Name(), cal.Name()))
		}
		if first {
			daysInWeek = cal.dayEnum[lang].Len()
			monthsInYear = cal.monthEnum[lang].Len()
			firstLang = lang.Name()
		} else {
			if daysInWeek != cal.dayEnum[lang].Len() {
				return Env.Goofs().MakeGoof("CONFLICTING_SETTINGS",
					"Could not init calendar, language %q has %d "+
						"days in week but %q has %d",
					lang.Name(), cal.dayEnum[lang].Len(),
					firstLang, daysInWeek)
			}
			if monthsInYear != cal.monthEnum[lang].Len() {
				return Env.Goofs().MakeGoof("CONFLICTING_SETTINGS",
					"Could not init calendar, language %q has %d "+
						"months in year but %q has %d",
					lang.Name(), cal.monthEnum[lang].Len(),
					firstLang, monthsInYear)
			}
		}
		first = false
	}

	cal.daysInWeek = daysInWeek
	cal.monthsInYear = monthsInYear

	daysInMonth := map[uint8]uint8{
		1:  31, // jan
		2:  28, // feb
		3:  31, // mar
		4:  30, // apr
		5:  31, // may
		6:  30, // jun
		7:  31, // jul
		8:  31, // aug
		9:  30, // sep
		10: 31, // oct
		11: 30, // nov
		12: 31, // dec
	}
	cal.baseDaysInMonth = daysInMonth

	return NO_GOOF
}

// Calendar interface.

func (cal *Gregorian) Name() string        { return cal.name }
func (cal *Gregorian) String() string      { return cal.Name() }
func (cal *Gregorian) NumberMonths() uint8 { return uint8(12) }

func (cal *Gregorian) MonthPhrase(m uint8, lang Language) (string, bool) {
	s, exists := cal.monthEnum[lang].FromNumber[m]
	if exists {
		return s[0], true
	}
	return "", false
}

func (cal *Gregorian) MonthNumber(phrase string, lang Language) (uint8, bool) {
	ui, b := cal.monthEnum[lang].FromString[phrase]
	return ui, b
}

func (cal *Gregorian) MonthModulo(m uint8) uint8 {
	rem := m % cal.NumberMonths()
	if rem == 0 {
		rem = cal.NumberMonths()
	}
	if rem < 0 {
		rem += cal.NumberMonths()
	}
	return rem
}

func (cal *Gregorian) DayPhrase(d uint8, lang Language) (string, bool) {
	s, exists := cal.dayEnum[lang].FromNumber[d]
	if exists {
		return s[0], true
	}
	return "", false
}

func (cal *Gregorian) DaysInMonth(mth *Month) uint8 {
	m := mth.Value()
	y := mth.Year()
	d := cal.baseDaysInMonth[m]
	if m == 2 && cal.IsLeapYear(y) {
		return d + 1
	}
	return d
}

func (cal *Gregorian) DayNumber(phrase string, lang Language) (uint8, bool) {
	ui, b := cal.dayEnum[lang].FromString[phrase]
	return ui, b
}

func (cal *Gregorian) DaysInWeek() uint8 { return cal.daysInWeek }

func (cal *Gregorian) IsValidDay(mth *Month, d uint8) bool {
	if d <= cal.DaysInMonth(mth) {
		return true
	}
	return false
}

func (cal *Gregorian) IsPossibleDay(d uint8) bool {
	if d < 0 || d > 31 {
		return false
	}
	return true
}

func (cal *Gregorian) IsPossibleMonth(m uint8) bool {
	if m < 0 || m > 12 {
		return false
	}
	return true
}

func (cal *Gregorian) IsPossibleYear(y int) bool {
	if y < 0 {
		return false
	}
	return true
}

func (cal *Gregorian) IsValidDate(date *Date) bool {
	if date.Day() <= cal.DaysInMonth(date.Month()) {
		return true
	}
	return false
}

//// Convert date to a Julian day number.
//// Credit: http://en.wikipedia.org/wiki/Julian_day
//func (cal *Gregorian) DateToNum2(date *Date) (TimeNum, *Goof) {
//	m := int(date.Month().Value())
//	a := (14 - m)/12
//	y := date.Year().To(new(Int64)).PlusInt(4800).MinusInt(a)
//	x1 := ((153*m + 2)/5) + int(date.Day()) - 32045
//	x2 := y.MultByInt(365).
//		Plus(y.DivByInt(4)).
//		Minus(y.DivByInt(100)).
//		Plus(y.DivByInt(400))
//	return MakeJulianDay(x2.PlusInt(x1)), NO_GOOF
//}

// Convert date to a Julian day number.
// Credit: http://pmyers.pcug.org.au/General/JulianDates.htm
// Result is at midday.
func (cal *Gregorian) DateToNum(date *Date, tn TimeNum) (TimeNum, *Goof) {
	switch to := tn.(type) {
	case *JulianDay:
		y := date.Year().To(new(Int64))
		m := date.Month().Value()
		if m > 2 {
			m = m - 3
		} else {
			m = m + 9
			y = y.MinusInt(1)
		}

		c := y.DivByInt(100)
		ya := y.Rem(100)

		x1 := c.MultByInt(146097).DivByInt(4)
		x2 := ya.MultByInt(1461).DivByInt(4)
		i := int(date.Day()) + 1721119 + (153*int(m)+2)/5
		return MakeJulianDay(x1.Plus(x2).PlusInt(i)), NO_GOOF
	default:
		return NewUnixTime(), Env.Goofs().MakeGoof("NOT_IMPLEMENTED",
			"Conversion from Date to %T not yet recognised for %q calendar",
			to, cal)
	}
}

// Convert Julian day number to date.
// Credit: http://pmyers.pcug.org.au/General/JulianDates.htm#J2G
func (cal *Gregorian) DateFromNum(tn TimeNum) (*Date, *Goof) {

	// IntResults (contain carried overflow state)
	val := tn.Value().MinusInt(1721119)
	x1 := val.MultByInt(4).MinusInt(1)
	y1 := x1.DivByInt(146097)
	val = x1.Rem(146097)
	d1 := val.DivByInt(4)
	x2 := d1.MultByInt(4).PlusInt(3)
	val = x2.DivByInt(1461)
	d1 = x2.Rem(1461)
	d1 = d1.PlusInt(4).DivByInt(4)
	x3 := d1.MultByInt(5).MinusInt(3)
	m1 := x3.DivByInt(153)
	d1 = x3.Rem(153)
	d1 = d1.PlusInt(5).DivByInt(5)
	y := y1.MultByInt(100).Plus(val)

	mui8 := m1.To(&Uint8{})
	m, _ := (mui8.AsNative()).(uint8)
	//moflow := mui8.Overflow()

	dui8 := d1.To(&Uint8{})
	d, _ := (dui8.AsNative()).(uint8)
	//doflow := dui8.Overflow()

	if m < 10 {
		m = m + 3
	} else {
		m = m - 9
		y = y.PlusInt(1)
	}

	month, goof := MakeMonth(y, m, cal)
	if IsError(goof) {
		return NewDate(), goof
	}
	return MakeDate(month, d)
}

// Convert calclock to the specified TimeNumber (UTC).  Currently
// handles UnixTime, JavaTime and NanoTime, all using big.Int as
// the underlying representation, so that there is no finite
// limitation.
// TODO handle leap seconds!?
func (cal *Gregorian) CalClockToNum(cc *CalClock, tn TimeNum) (TimeNum, *Goof) {
	switch to := tn.(type) {
	case *UnixTime:
		// Seconds in date
		jdn, goof := cal.DateToNum(cc.Date, new(JulianDay)) // midday
		if IsError(goof) {
			return tn, goof
		}

		ut := jdn.Value().To(new(BigInt)) // ** Defines underlying storage

		const jdn_ut_epoch int = 2440588                // julian day for unix time epoch
		ut = ut.MinusInt(jdn_ut_epoch).MultByInt(86400) //.PlusInt(43200)
		// Seconds in clock
		hrs := int(cc.Hour()) // - 12
		daysecs := hrs*3600 + int(cc.Minute())*60 + int(cc.Second())
		return MakeUnixTime(ut.PlusInt(daysecs)), NO_GOOF
	case *JavaTime:
		utn, goof := cal.CalClockToNum(cc, new(UnixTime))
		if IsError(goof) {
			return utn, goof
		}
		jtn, goof := utn.ToNum(new(JavaTime))
		if IsError(goof) {
			return jtn, goof
		}
		jt, goof := jtn.PlusInt(int(cc.FracAsNano()) / int(NanosPerMilli))
		if IsError(goof) {
			return jtn, goof
		}
		jtn, _ = jt.(*JavaTime)
		return jtn, NO_GOOF
	case *NanoTime:
		utn, goof := cal.CalClockToNum(cc, new(UnixTime))
		if IsError(goof) {
			return utn, goof
		}
		ntn, goof := utn.ToNum(new(NanoTime))
		if IsError(goof) {
			return ntn, goof
		}
		nt, goof := ntn.PlusInt(int(cc.FracAsNano()))
		if IsError(goof) {
			return ntn, goof
		}
		ntn, _ = nt.(*NanoTime)
		return ntn, NO_GOOF
	default:
		return NewUnixTime(), Env.Goofs().MakeGoof("NOT_IMPLEMENTED",
			"Conversion from CalClock to %T not yet recognised for %q calendar",
			to, cal)
	}
}

// Convert Time number to calclock (UTC).
func (cal *Gregorian) CalClockFromNum(tn TimeNum) (*CalClock, *Goof) {
	switch to := tn.(type) {
	case *UnixTime:
		dayfrac, _ := tn.Value().Rem(86400).AsGoInt64()
		days, _ := tn.Value().DivByInt(86400).PlusInt(2440588).AsGoInt64()
		date, goof := cal.DateFromNum(MakeJulianDay(MakeInt64(days)))
		if IsError(goof) {
			return NewCalClock(), goof
		}
		h := uint8(dayfrac / 3600)
		rem := dayfrac % 3600
		m := uint8(rem / 60)
		s := uint8(rem % 60)
		clock, goof := BuildClock(h, m, s, ZeroFrac())
		if IsError(goof) {
			return NewCalClock(), goof
		}
		return MakeCalClock(date, clock), NO_GOOF
	case *JavaTime:
		utn, goof := to.ToNum(new(UnixTime))
		if IsError(goof) {
			return NewCalClock(), goof
		}
		cc, goof := cal.CalClockFromNum(utn)
		if IsError(goof) {
			return cc, goof
		}
		boxedMillis := to.Value().Rem(MillisPerSec)
		millis, _ := boxedMillis.AsGoInt64()
		return cc.Plus(0, 0, 0, 0, 0, 0, MakeMilliFrac(int(millis)))
	case *NanoTime:
		utn, goof := to.ToNum(new(UnixTime))
		if IsError(goof) {
			return NewCalClock(), goof
		}
		cc, goof := cal.CalClockFromNum(utn)
		if IsError(goof) {
			return cc, goof
		}
		boxedNanos := to.Value().Rem(NanosPerSec)
		nanos, _ := boxedNanos.AsGoInt64()
		return cc.Plus(0, 0, 0, 0, 0, 0, MakeNanoFrac(int(nanos)))
	default:
		return NewCalClock(), Env.Goofs().MakeGoof("NOT_IMPLEMENTED",
			"Conversion from %T to CalClock not yet recognised for %q calendar",
			to, cal)
	}
}

// Algorithm for determining the day of the week.
//
// Zeller's congruence @endlink was devised by Christian Zeller
// circa 1882.
// http://en.wikipedia.org/wiki/Zeller%27s_congruence
func (cal *Gregorian) DayOfWeek(date *Date) uint8 {
	y := date.Year()
	m := (int)(date.Month().Value())
	q := (int)(date.Day())
	if m == 1 || m == 2 {
		m = m + 12
		y = y.MinusInt(1)
	}
	K := y.Rem(100)
	J := y.DivByInt(100)

	K1 := K.Plus(K.DivByInt(4))
	J1 := J.MultByInt(5).Plus(J.DivByInt(4))
	ti, _ := K1.Plus(J1).PlusInt(q + ((13 * (m + 1)) / 5)).Rem(7).Explode()
	h, _ := ti.AsGoInt64()

	return uint8(((h + 5) % 7) + 1)
}

func (cal *Gregorian) IsWeekend(date *Date) bool {
	d := cal.DayOfWeek(date)
	return d == 6 || d == 7
}

// Other.

func (cal *Gregorian) IsLeapYear(y IntBox) bool {
	if y.Rem(400).Sign() == 0 {
		return true
	} else if y.Rem(100).Sign() == 0 {
		return false
	} else if y.Rem(4).Sign() == 0 {
		return true
	}
	return false
}
