/*
	Generic data structures.
*/
package h2go

import (
	"sync"
)

// ENTITY STATE ================================================================

// Originally intended for user information.
type EntityState struct {
	Key string
	*FieldMap
	sync.RWMutex
}

func NewEntityState() *EntityState {
	return &EntityState{
		FieldMap: Env.Types().MintFieldMap(),
	}
}

func MakeEntityState(key string) *EntityState {
	state := NewEntityState()
	state.Key = key
	return state
}

// Getters
//func (state *EntityState) Key() string { return state.key }
func (state *EntityState) Log(activity string) *FieldList {
	return state.Get(
		MakeStringField(PREFIX_LOG + activity)).AsFieldList()
}

func (state *EntityState) GetString(k string) string {
	return state.Get(k).AsString()
}

func (state *EntityState) GetBoolean(k string) bool {
	return state.Get(k).AsBoolean()
}

func (state *EntityState) IsBlocked() bool {
	return state.GetBoolean(STATE_BLOCKED)
}

func (state *EntityState) IsLoggedIn() bool {
	return state.GetBoolean(STATE_LOGGED_IN)
}

func (state *EntityState) IsQualified() bool {
	return state.GetBoolean(STATE_QUALIFIED)
}

// Setters
func (state *EntityState) Set(k, v interface{}) *EntityState {
	state.AutoSet(k, v)
	return state
}

func (state *EntityState) Touch() {
	state.Lock()
	defer state.Unlock()
	state.touch()
	return
}

func (state *EntityState) touch() {
	timestamp, _, goof := NewTimeStamp()
	Env.Log().IsError(goof)
	state.Set(STATE_LAST_ACTIVITY, timestamp)
	return
}

func (state *EntityState) StampTime(activity string) *Goof {
	return state.StampTimeWithInfo(activity, "")
}

func (state *EntityState) StampTimeWithInfo(activity, info string) *Goof {
	log := state.Get(
		MakeStringField(PREFIX_LOG + activity)).AsFieldList()
	ts, _, goof := MakeTimeStamp(info)
	if IsError(goof) {
		return goof
	}
	log.Add(MakeStringField(ts))
	if log.Len() > MAX_LENGTH_ACTIVITY_LOG {
		log.Delete(0)
	}
	state.Touch()
	return NO_GOOF
}

func (state *EntityState) SetBlocked(b bool) {
	state.Set(STATE_BLOCKED, b)
	return
}

func (state *EntityState) SetLoggedIn(b bool) {
	state.Set(STATE_LOGGED_IN, b)
	return
}

func (state *EntityState) SetQualified(b bool) {
	state.Set(STATE_QUALIFIED, b)
	return
}

func (state *EntityState) ToStringSlice() []string {
	result := []string{
		"Key: " + state.Key,
	}
	for _, line := range state.MultilineString("", "") {
		result = append(result, line)
	}
	return result
}

// ENTITY DATABASE =============================================================

type EntityDatabase interface {
	Get(key string) (*EntityState, bool)
	GetString(key1, key2 string) string
	GetBoolean(key1, key2 string) bool
	Put(key string, es *EntityState)
	Delete(key string) bool
	Export() []*EntityState
	ToStringSlice() []string
}

// ENTITY ACTIVITY =============================================================

type EntityActivity struct {
	states map[string]*EntityState
	sync.RWMutex
}

var _ EntityDatabase = NewEntityActivity()

func NewEntityActivity() *EntityActivity {
	return &EntityActivity{
		states: make(map[string]*EntityState),
	}
}

// Getters
func (ea *EntityActivity) Get(key string) (*EntityState, bool) {
	ea.RLock()
	defer ea.RUnlock()
	state, exists := ea.states[key]
	if !exists {
		state = NewEntityState()
	}
	return state, exists
}

func (ea *EntityActivity) GetString(key1, key2 string) string {
	state, exists := ea.Get(key1)
	if exists {
		state.RLock()
		defer state.RUnlock()
		return state.GetString(key2)
	} else {
		return ""
	}
}

func (ea *EntityActivity) GetBoolean(key1, key2 string) bool {
	state, exists := ea.Get(key1)
	if exists {
		state.RLock()
		defer state.RUnlock()
		return state.GetBoolean(key2)
	} else {
		return false
	}
}

// Setters
func (ea *EntityActivity) Put(key string, state *EntityState) {
	ea.Lock()
	defer ea.Unlock()
	ea.states[key] = state
	return
}

func (ea *EntityActivity) Delete(key string) bool {
	ea.Lock()
	defer ea.Unlock()
	_, exists := ea.states[key]
	delete(ea.states, key)
	return exists
}

func (ea *EntityActivity) Export() []*EntityState {
	result := []*EntityState{}
	for key, es := range ea.states {
		if len(es.Key) == 0 {
			es.Key = key
		}
		result = append(result, es)
	}
	return result
}

func (ea *EntityActivity) ToStringSlice() []string {
	result := []string{}
	for _, es := range ea.states {
		result = append(result, es.ToStringSlice()...)
	}
	return result
}
