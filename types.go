/*
	Something of a customisable code layer on top of the native Go codes.

	TypeMap.binaryEncoder/Decoder differ from the BinaryEncodable interface
	methods for a user defined code, e.g.

	    TypeMap.EncodeBinaryr(TypeMap.Number["FIELDMAP"])(bfr) "code encoder"
	    versus
	    FieldMap.EncodeBinary(bfr) "method encoder"

	in that the former encodes the code to the front and wraps variable length
	data in a version container.  TypeMap.DecodeBinaryr then assumes the code
	has been removed.  Typically the code encoder will be generated from a
	method encoder, which should therefore not prepend the code.
*/
package h2go

import (
	"math/big"
	"reflect"
)

// TYPEMAP =====================================================================

type TypeMap struct {
	*ProtocolMap
}

func NewTypeMap() *TypeMap {
	return &TypeMap{
		ProtocolMap: NewProtocolMap(),
	}
}

func MakeTypeMap(ver *Version) *TypeMap {
	tm := NewTypeMap()
	tm.ProtocolMap = MakeProtocolMap(ver)
	return tm
}

func (types *TypeMap) Copy() *TypeMap {
	types2 := NewTypeMap()
	types2.ProtocolMap = types.ProtocolMap.Copy()
	return types2
}

func (types *TypeMap) GetType(val interface{}) CODE {
	switch v := val.(type) {
	case nil:
		return types.Number["NIL"]
	case Codified: // When adding a Type, ensure it is Codified
		return v.Code()
	// Base types
	case bool:
		if v {
			return types.Number["TRUE"]
		} else {
			return types.Number["FALSE"]
		}
	case int8:
		return types.Number["int8"]
	case int16:
		return types.Number["int16"]
	case int32:
		return types.Number["int32"]
	case int, int64:
		return types.Number["int64"]
	case uint8:
		return types.Number["uint8"]
	case uint16:
		return types.Number["uint16"]
	case uint32:
		return types.Number["uint32"]
	case UINT8:
		return types.Number["UINT8"]
	case UINT16:
		return types.Number["UINT16"]
	case UINT32:
		return types.Number["UINT32"]
	case uint, uint64:
		return types.Number["uint64"]
	case float32:
		return types.Number["float32"]
	case float64:
		return types.Number["float64"]
	case *big.Int:
		return types.Number["BIGINT"]
	case *big.Rat:
		return types.Number["BIGRAT"]
	case []byte:
		return types.Number["BYTES"]
	case string:
		return types.Number["STRING"]
	default:
		return types.Number["UNKNOWN_TYPE"]
	}
}

func (types *TypeMap) IsBoolean(code CODE) bool {
	return code == types.Number["TRUE"] || code == types.Number["FALSE"]
}

func ContainsOnlyAlphaNumerics(in string) bool {
	// TODO I think this might be for ascii only?
	lowercase := false
	uppercase := false
	numeral := false
	for _, b := range []byte(in) {
		lowercase = b >= 97 && b <= 122
		uppercase = b >= 65 && b <= 90
		numeral = b >= 48 && b <= 57
		if !lowercase && !uppercase && !numeral {
			return false
		}
	}
	return true
}

// Recursively find the enclosed object that is not a TypedValue (currently
// a Field or FieldBox, and determine if it is hashable for use as a map key.
func ObjectIsAllowableKey(object interface{}) bool {
	typed, hasType := object.(TypedValue)
	if hasType {
		_, hasTypedVal := typed.Value().(TypedValue)
		if hasTypedVal {
			return ObjectIsAllowableKey(typed.Value())
		} else {
			return TypeIsAllowableKey(typed.Type())
		}
	}
	return false
}

// Note that reflect.Type.Comparable() only arrived in Go 1.4.2.
func TypeIsAllowableKey(code CODE) bool {
	typ, goof := Env.Types().ProtocolByCode(code)
	if IsError(goof) {
		return false
	}
	if reflect.TypeOf(typ.Nil).Comparable() {
		return true
	}
	return false
}
