/*
	A Workspace provides a FieldMap for manipulating data in downstream
	applications.

	TODO Workspace is not concurrent-safe,
	e.g. NumGoofs() is used a lot to for goof detection
	maybe it should be explicitely single-user
*/
package h2go

import (
	"bytes"
	"fmt"
	"io"
	"math"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
)

// INPUT =======================================================================

type Input struct {
	Text   string
	Args   []string
	Source string
}

func NewInput() *Input {
	return &Input{
		Args: []string{},
	}
}

func (input *Input) HasNArgs(n int) *Goof {
	if len(input.Args) != n {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"For %q you must specify %d arguments", input.Text, n)
	}
	return NO_GOOF
}

func (input *Input) HasAtLeastNArgs(n int) *Goof {
	if len(input.Args) < n {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"For %q you must specify at least %d arguments", input.Text, n)
	}
	return NO_GOOF
}

func (input *Input) HasNoneOrOneArg() *Goof {
	if len(input.Args) > 1 {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"For %q you must specify no more than a single argument", input.Text)
	}
	return NO_GOOF
}

func (input *Input) HasOneArg() *Goof {
	if len(input.Args) != 1 {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"For %q you must specify a single argument", input.Text)
	}
	return NO_GOOF
}

func (input *Input) HasOneOrMoreArgs() *Goof {
	if len(input.Args) == 0 {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"For %q you must specify at least one argument", input.Text)
	}
	return NO_GOOF
}

func (input *Input) HasTwoArgs() *Goof {
	if len(input.Args) != 2 {
		return Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"For %q you must specify two arguments", input.Text)
	}
	return NO_GOOF
}

// COMMAND =====================================================================

type Command interface {
	Execute(input *Input)
	TabCompleter(line string, args []string) []string
	// Remainder is help documentation only
	Args() string
	ShortDesc() string
	LongDesc() string
	Examples() string
}

type ExecutableCommand struct {
	Label    string
	Wspace   *Workspace
	Extras   interface{}
	Template map[string]map[string][]string
}

func NewExecutableCommand() *ExecutableCommand {
	return &ExecutableCommand{
		Wspace:   NewWorkspace(),
		Extras:   false,
		Template: make(map[string]map[string][]string),
	}
}

func (ws *Workspace) MakeExecutableCommand(label string) *ExecutableCommand {
	ec := NewExecutableCommand()
	ec.Label = label
	ec.Wspace = ws
	return ec
}

func (ws *Workspace) MakeExecutableCommandWithExtras(label string, extras interface{}) *ExecutableCommand {
	ec := NewExecutableCommand()
	ec.Label = label
	ec.Wspace = ws
	ec.Extras = extras
	return ec
}

func (cmd *ExecutableCommand) SetTemplate(template map[string]map[string][]string) *ExecutableCommand {
	cmd.Template = template
	return cmd
}

func (ws *Workspace) BaseGroupedCommands(extras interface{}) map[string](map[string]Command) {
	return map[string](map[string]Command){
		"Introspective": map[string]Command{
			"help":  CmdHelp{ws.MakeExecutableCommandWithExtras("help", extras)},
			"whos":  CmdWhos{ws.MakeExecutableCommandWithExtras("whos", extras)},
			"clear": CmdClear{ws.MakeExecutableCommandWithExtras("clear", extras)},
			"echo":  CmdEcho{ws.MakeExecutableCommandWithExtras("echo", extras)},
		},
		"System": map[string]Command{
			"run": CmdRun{ws.MakeExecutableCommandWithExtras("run", extras), 0},
			"cd":  CmdCd{ws.MakeExecutableCommandWithExtras("cd", extras)},
			"ls":  CmdLs{ws.MakeExecutableCommandWithExtras("ls", extras)},
			"pwd": CmdPwd{ws.MakeExecutableCommandWithExtras("pwd", extras)},
		},
	}
}

// HELP ========================================================================

func (cmd *ExecutableCommand) BuildHelpArgsUsingArgTemplate() string {
	if len(cmd.Template) == 0 {
		return ""
	}
	keys := []string{}
	bracketted := false
	numUnbracketted := 0
	for k, _ := range cmd.Template {
		keys = append(keys, k)
		if !HasPrefixSuffix(k, "[", "]") {
			numUnbracketted++
		}
	}
	sort.Strings(keys)
	result := ""
	post0 := ""
	preName := "[pre]"
	postName := "[post]"
	first := true
	for _, k := range keys {
		bracketted = HasPrefixSuffix(k, "[", "]")
		if numUnbracketted > 1 && !bracketted {
			if first {
				first = false
			} else {
				result += " "
			}
			result += "{"
		}
		if k == preName {
			if len(cmd.Template[k]) > 0 {
				kws, exists := cmd.Template[k][preName]
				if exists && len(kws) > 0 {
					result = kws[0]
					continue
				}
			}
		}
		if k == postName {
			if len(cmd.Template[k]) > 0 {
				kws, exists := cmd.Template[k][postName]
				if exists && len(kws) > 0 {
					post0 = kws[0]
					continue
				}
			}
		}
		result += "[" + k + "]"
		post1 := ""
		for typ, v := range cmd.Template[k] {
			if typ == preName {
				if len(v) > 0 {
					result += v[0]
					continue
				}
			}
			if typ == postName {
				if len(v) > 0 {
					post1 = v[0]
					continue
				}
			}
			if typ == "[keywords]" {
				result += " "
				for i, kw := range v {
					result += "<" + kw + ">"
					if i < len(v)-1 {
						result += "|"
					}
				}
			}
			if len(post1) > 0 {
				result += post1
			}
		}
		if len(post0) > 0 {
			result += post0
		}
		if numUnbracketted > 1 && !bracketted {
			result += "}"
		}
	}
	return result
}

// WORKSPACE ===================================================================

type Workspace struct {
	types *TypeMap
	*FieldMap
	Commands map[string]Command
	root     string   // root path, can only rwx files in this tree
	lang     Language // interface
	// Output
	output     [][]string // each output line has a (1) prefix and (2) result
	goofs      []*Goof
	Prefixes   map[string]string // easily overridden
	PREFIX_OK  string
	PREFIX_ERR string
	HELP_KEY   string
	HELP_WIDTH int
	// Attached functions
	fieldProcessor func(string, string, *WorkspaceAction) *Field
	rhsProcessor   func(string) *Field
	// For documentation
	HelpTable       [][]string
	GroupedCommands map[string](map[string]Command)
	CommandGroup    map[string]string
	currSource      string
	virtual         bool
	state           *State
}

var _ FieldCollection = NewWorkspace() // embedded FieldMap

// Constructors
func NewWorkspace() *Workspace {
	return &Workspace{
		types:           NewTypeMap(),
		FieldMap:        NewFieldMap(),
		Commands:        make(map[string]Command),
		output:          [][]string{},
		goofs:           []*Goof{},
		Prefixes:        make(map[string]string),
		HelpTable:       [][]string{},
		GroupedCommands: make(map[string](map[string]Command)),
		CommandGroup:    make(map[string]string),
		state:           NewState(), // can't embed due to ambiguity with FieldMap
	}
}

// Does not build base commands, must be done separately.
func (types *TypeMap) MakeWorkspace(root string) (*Workspace, *Goof) {
	ws := NewWorkspace()
	if len(root) != 0 {
		info, err := os.Stat(root)
		if IsError(err) {
			return ws, Env.Goofs().WrapGoof("GETTING_DIR_INFO", err,
				"While verifying the root of a new Workspace")
		}
		if !info.IsDir() {
			return ws, Env.Goofs().MakeGoof("BAD_ARGUMENTS",
				"Root given for a new Workspace is not a directory")
		}
	}
	ws.types = types
	ws.FieldMap = types.MintFieldMap()
	ws.State().NotEmpty()
	ws.root = root
	ws.GroupedCommands = ws.BaseGroupedCommands(false)
	ws.RefreshCommands()
	ws.HELP_KEY = WORKSPACE_HELP_KEY
	ws.HELP_WIDTH = WORKSPACE_HELP_WIDTH
	ws.Prefixes = WORKSPACE_PREFIXES
	ws.SetLanguage(Env.Languages()["English"])
	ws.PREFIX_OK = ws.Prefixes["OK"] + ws.Prefixes["PROMPT"]
	ws.PREFIX_ERR = ws.Prefixes["ERROR"] + ws.Prefixes["PROMPT"]
	ws.lang = Env.Languages()["English"] // Default
	ws.State().NowComplete()
	return ws, NO_GOOF
}

func (types *TypeMap) MakeWorkspaceWithExtras(root string, extras interface{}) (*Workspace, *Goof) {
	ws, goof := types.MakeWorkspace(root)
	if IsError(goof) {
		return ws, goof
	}
	ws.GroupedCommands = ws.BaseGroupedCommands(extras)
	ws.RefreshCommands()
	return ws, NO_GOOF
}

func (types *TypeMap) BuildWorkspace(root string, fmap *FieldMap, extras interface{}, gcmds map[string](map[string]Command)) (*Workspace, *Goof) {
	ws, goof := types.MakeWorkspaceWithExtras(root, extras)
	if IsError(goof) {
		return ws, goof
	}
	ws.AttachGroupedCommands(gcmds)
	if !fmap.IsEmpty() {
		ws.FieldMap = fmap
	}
	return ws, NO_GOOF
}

func (ws *Workspace) CopyWith(fmap *FieldMap) *Workspace {
	newws := NewWorkspace()
	newws.types = ws.types
	newws.FieldMap = fmap
	newws.Commands = ws.Commands
	newws.root = ws.root
	newws.output = ws.output
	newws.goofs = ws.goofs
	newws.Prefixes = ws.Prefixes
	newws.HELP_KEY = ws.HELP_KEY
	newws.HELP_WIDTH = ws.HELP_WIDTH
	newws.PREFIX_OK = ws.PREFIX_OK
	newws.PREFIX_ERR = ws.PREFIX_ERR
	newws.fieldProcessor = ws.fieldProcessor
	newws.rhsProcessor = ws.rhsProcessor
	newws.HelpTable = ws.HelpTable
	newws.GroupedCommands = ws.GroupedCommands
	newws.CommandGroup = ws.CommandGroup
	newws.lang = ws.lang
	newws.state = ws.State().Copy()
	return newws
}

func (types *TypeMap) NewVirtualWorkspace() (*Workspace, *Goof) {
	ws, goof := types.MakeWorkspace("")
	ws.virtual = true
	return ws, goof
}

func (ws *Workspace) Types() *TypeMap    { return ws.types }
func (ws *Workspace) Root() string       { return ws.root }
func (ws *Workspace) Output() [][]string { return ws.output }
func (ws *Workspace) Goofs() []*Goof     { return ws.goofs }
func (ws *Workspace) NumGoofs() int      { return len(ws.goofs) }
func (ws *Workspace) State() *State      { return ws.state }

func (ws *Workspace) RunScriptLevel() int {
	ifacecmd := ws.GroupedCommands["System"]["run"]
	runcmd, _ := ifacecmd.(CmdRun)
	return runcmd.RunScriptLevel
}

func (ws *Workspace) HasOutput() bool  { return len(ws.Output()) > 0 }
func (ws *Workspace) HasGoofs() bool   { return len(ws.Goofs()) > 0 }
func (ws *Workspace) OutputLines() int { return len(ws.Output()) }

func (ws *Workspace) Prefix(i int) string {
	if i >= 0 && i < len(ws.output) && len(ws.output[i]) > 0 {
		return ws.output[i][0]
	}
	return ""
}

func (ws *Workspace) Result(i int) string {
	if i >= 0 && i < len(ws.output) && len(ws.output[i]) > 0 {
		return ws.output[i][1]
	}
	return ""
}

func (ws *Workspace) FirstGoof() *Goof {
	if ws.NumGoofs() > 0 {
		return ws.Goofs()[0]
	}
	return NO_GOOF
}

func (ws *Workspace) LastGoof() *Goof {
	if ws.NumGoofs() > 0 {
		return ws.Goofs()[len(ws.Goofs())-1:][0]
	}
	return NO_GOOF
}

func (ws *Workspace) SetLanguage(lang Language) *Workspace {
	ws.lang = lang
	return ws
}

func (ws *Workspace) AttachFieldProcessor(fp func(txt, splitter string, wsa *WorkspaceAction) *Field) {
	ws.fieldProcessor = fp
}

func (ws *Workspace) AttachRhsProcessor(rhsp func(rhs string) *Field) {
	ws.rhsProcessor = rhsp
}

func (ws *Workspace) AttachGroupedCommands(gcmds map[string](map[string]Command)) *Workspace {
	exists := false
	for group, cmds := range gcmds {
		_, exists = ws.GroupedCommands[group]
		if exists {
			for name, cmd := range cmds {
				// May override existing cmd, that's ok
				ws.GroupedCommands[group][name] = cmd
				ws.CommandGroup[name] = group
			}
		} else {
			ws.GroupedCommands[group] = cmds
		}
	}
	ws.RefreshCommands()
	return ws
}

func (ws *Workspace) ClearData() {
	ws.FieldMap = ws.Types().MintFieldMap()
	return
}

func (ws *Workspace) RefreshCommands() *Workspace {
	for group, cmds := range ws.GroupedCommands {
		for name, cmd := range cmds {
			ws.Commands[name] = cmd
			ws.CommandGroup[name] = group
		}
	}
	ws.HelpTable = ws.BuildHelpTable()
	return ws
}

func (ws *Workspace) BuildHelpTable() [][]string {
	rows := [][]string{}
	for group, cmds := range ws.GroupedCommands {
		rows = append(rows, []string{group, ""})
		keys := []string{}
		for k := range cmds {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		for _, k := range keys {
			rows = append(rows, []string{" " + k, cmds[k].ShortDesc()})
		}
	}
	return rows
}

func (ws *Workspace) String() string {
	bfr := bytes.NewBuffer([]byte{})
	goof := ws.Write(bfr, "", "")
	result := bfr.String()
	if IsError(goof) {
		result += "\n" + goof.Msg
	}
	return result
}

// Dump human-readable workspace to the given io.Writer.
func (ws *Workspace) Write(w io.Writer, prefix, header string) *Goof {
	lines := ws.ToStringSlice(prefix)
	if len(lines) > 0 {
		_, err := io.WriteString(w, header+"\n")
		if IsError(err) {
			return ws.WrapGoof("ENCODING", err,
				"While writing header to given %T", w)
		}
		suffix := ""
		for i, line := range lines {
			if i != len(lines)-1 {
				suffix = "\n"
			} else {
				suffix = ""
			}
			_, err = io.WriteString(w, line+suffix)
			if IsError(err) {
				return ws.WrapGoof("ENCODING", err,
					"While writing line %q to given %T", line, w)
			}
		}
	}
	return NO_GOOF
}

func (ws *Workspace) ToStringSlice(prefix string) []string {
	result := []string{}
	iter := ws.TopLevelSortedKeyLinkList()
	if !iter.List().IsEmpty() {
		errstr := ""
		i := 0
		line := ""
		for {
			if iter.atEnd {
				break
			}
			if !iter.This().IsEmpty() {
				ks, _ := (iter.This().Value()).(KeyString)
				k := ks.Key
				v := ws.Get(k)
				errstr = ""
				if !ws.Types().proto[k.Type()].IsComparable() {
					errstr = fmt.Sprintf("//"+ERROR_IN_STRING, "INVALID_KEY")
				}
				firstline := prefix + errstr + k.StringUsing(ws.Types()) + " = "
				switch underlying := v.Value().(type) {
				case MultilineStringable:
					lines := underlying.MultilineString(firstline, prefix+errstr)
					for i, line = range lines {
						if i == len(lines)-1 {
							line += ";"
						}
						result = append(result, line)
					}
				default:
					line = firstline + v.StringUsing(ws.Types()) + ";"
					result = append(result, line)
				}
			}
			iter.Inc()
		}
	}

	return result
}

// WORKSPACE ACTION ============================================================

type WorkspaceAction struct {
	*Workspace
	SetField *Field
	DelField bool
	*State
}

var _ FieldCollection = NewWorkspaceAction() // embedded Workspace
var _ Stateful = NewWorkspaceAction()        // embedded State, override Workspace

func NewWorkspaceAction() *WorkspaceAction {
	return &WorkspaceAction{
		Workspace: NewWorkspace(),
		SetField:  NewField(),
		State:     NewState(),
	}
}

func (ws *Workspace) MakeAction(setField *Field, delField bool) *WorkspaceAction {
	wsa := NewWorkspaceAction()
	wsa.Workspace = ws
	wsa.SetField = setField
	wsa.DelField = delField
	wsa.NotEmpty()
	wsa.NowComplete()
	return wsa
}

func (ws *Workspace) KeywordNotRecognised(word string) *Goof {
	return ws.MakeGoof("BAD_ARGUMENTS",
		"Keyword(s) %q not recognised", word)
}

// OUTPUT ======================================================================

func (ws *Workspace) ClearOutput() {
	ws.output = [][]string{}
	return
}

func (ws *Workspace) ClearGoofs() {
	ws.goofs = []*Goof{}
	return
}

func (ws *Workspace) ClearLastGoof() {
	if len(ws.goofs) > 0 {
		ws.goofs = ws.goofs[0 : len(ws.goofs)-1]
	}
	return
}

func (ws *Workspace) MakeGoof(name, msg string, a ...interface{}) *Goof {
	goof := MakeGoof(3).MakeId()
	Env.Goofs().FormatGoofMessage(goof, name, msg+" in "+ws.currSource, a...)
	ws.goofs = append(ws.goofs, goof)
	return goof
}

func (ws *Workspace) WrapGoof(name string, err error, msg string, a ...interface{}) *Goof {
	if !IsError(err) {
		return NO_GOOF
	}
	goof := MakeGoof(4).MakeId()
	// TODO fix this mess
	//Env.Goofs().FormatWrappedGoof(goof, name, err, msg+" in "+ws.currSource, a...)
	ws.goofs = append(ws.goofs, goof)
	return goof
}

func (ws *Workspace) Out(output string, a ...interface{}) string {
	result := fmt.Sprintf(output, a...)
	ws.output = append(ws.output, []string{ws.PREFIX_OK, result})
	return ws.PREFIX_OK + result
}

func (ws *Workspace) Literal(output string, a ...interface{}) string {
	result := fmt.Sprintf(output, a...)
	ws.output = append(ws.output, []string{"", result})
	return result
}

func (ws *Workspace) Advise(output string, a ...interface{}) string {
	prefix := ws.Prefixes["ADVISE"] + ws.Prefixes["PROMPT"]
	result := fmt.Sprintf(output, a...)
	ws.output = append(ws.output, []string{prefix, result})
	return prefix + result
}

func (ws *Workspace) Warn(output string, a ...interface{}) string {
	prefix := ws.Prefixes["WARN"] + ws.Prefixes["PROMPT"]
	result := fmt.Sprintf(output, a...)
	ws.output = append(ws.output, []string{prefix, result})
	return prefix + result
}

func (ws *Workspace) Check(output string, a ...interface{}) string {
	prefix := ws.Prefixes["CHECK"] + ws.Prefixes["PROMPT"]
	result := fmt.Sprintf(output, a...)
	ws.output = append(ws.output, []string{prefix, result})
	return prefix + result
}

func (ws *Workspace) Dump(format string, lines []string) {
	prefix := ws.Prefixes["DUMP"] + ws.Prefixes["PROMPT"]
	for _, line := range lines {
		ws.output = append(ws.output, []string{prefix, fmt.Sprintf(format, line)})
	}
	return
}

func (ws *Workspace) LiteralLines(lines []string) {
	for _, line := range lines {
		ws.Literal(line)
	}
	return
}

func (ws *Workspace) LiteralSplit(output, splitter string) {
	lines := strings.Split(output, splitter)
	for _, line := range lines {
		ws.Literal(line)
	}
	return
}

// Makes a Goof and sends a line to output.
func (ws *Workspace) Problem(name, msg string, a ...interface{}) string {
	goof := MakeGoof(3).MakeId()
	Env.Goofs().FormatGoofMessage(goof, name, msg+" in "+ws.currSource, a...)
	ws.goofs = append(ws.goofs, goof)
	//ws.output = append(ws.output, []string{ws.PREFIX_ERR, goof.Msg})
	return ws.PREFIX_ERR + goof.Msg
}

/*
// Wraps a Goof and sends a line to output.
func (ws *Workspace) ErrorOut(name string, err error, msg string, a ...interface{}) string {
	if !IsError(err) {
		return ""
	}
	goof := MakeGoof(5).MakeId()
	// TODO fix this mess
	Env.Goofs().FormatWrappedGoof(goof, name, err, msg+" in "+ws.currSource, a...)
	ws.goofs = append(ws.goofs, goof)
	//ws.output = append(ws.output, []string{ws.PREFIX_ERR, err.Error()})
	return ws.PREFIX_ERR + err.Error()
}

func (ws *Workspace) Error(err error) bool {
	if IsError(err) {
		ws.ErrorOut("WORKSPACE", err, "")
		return true
	}
	return false
}
*/

func (ws *Workspace) IsError(err error) bool {
	goof, isa := err.(*Goof)
	if isa {
		if goof.IsEmpty() || goof.Code == NO_GOOF_CODE {
			return false
		} else {
			ws.goofs = append(ws.goofs, goof)
			return true
		}
	}
	v := reflect.ValueOf(err)
	if err != nil && v.IsValid() {
		if !v.IsNil() {
			ws.goofs = append(ws.goofs,
				MakeGoof(5).MakeId().WrapGoof("WORKSPACE",
					Env.Goofs(), err, ""))
			return true
		}
	}
	return false
}

// For use by external app, to view output.
func (ws *Workspace) OutputToLines() []string {
	result := []string{}
	line := ""
	for _, parts := range ws.Output() {
		line = ""
		for _, part := range parts {
			line += part
		}
		result = append(result, line)
	}
	return result
}

// For use by external app, to view output.
func (ws *Workspace) WriteOutput(writer io.Writer) (int, *Goof) {
	return ws.DumpToWriter(writer, ws.Output())
}

// For use by external app, to view errors.
func (ws *Workspace) WriteGoofs(writer io.Writer) (int, *Goof) {
	prefixedLines := [][]string{}
	for _, goof := range ws.Goofs() {
		prefixedLines = append(prefixedLines, []string{ws.PREFIX_ERR, goof.Msg})
	}
	return ws.DumpToWriter(writer, prefixedLines)
}

// For use by external app, to view errors.
func (ws *Workspace) WriteGoof(writer io.Writer) (int, *Goof) {
	if len(ws.goofs) > 0 {
		line := [][]string{[]string{ws.PREFIX_ERR, ws.Goofs()[0].Msg}}
		return ws.DumpToWriter(writer, line)
	}
	return 0, NO_GOOF
}

func (ws *Workspace) DumpToWriter(writer io.Writer, prefixedLines [][]string) (int, *Goof) {
	totalBytes := 0
	n := 0
	var err error
	line := ""
	for _, lineParts := range prefixedLines {
		line = ""
		for _, part := range lineParts {
			line += part
		}
		n, err = io.WriteString(writer, line+"\n")
		totalBytes += n
		if IsError(err) {
			return totalBytes, Env.Goofs().WrapGoof("IO_WRITE", err,
				"While writing Workspace output to given write")
		}
	}
	return totalBytes, NO_GOOF
}

// TEXT PROCESSING =============================================================

func (ws *Workspace) processField(txt, splitter string, setField *Field, delField bool) *Field {
	if ws.fieldProcessor == nil {
		return ws.ProcessField(txt, splitter, setField, delField)
	} else {
		return ws.fieldProcessor(txt, splitter, ws.MakeAction(setField, delField))
	}
}

func (ws *Workspace) processRhs(txt string) *Field {
	if ws.rhsProcessor == nil {
		return ws.DefaultRhsProcessor(txt)
	} else {
		return ws.rhsProcessor(txt)
	}
}

func (ws *Workspace) SubstituteThenProcessText(txt string, setField *Field, delField bool) *Field {
	f := NewField()
	ngoofs := ws.NumGoofs()
	txt = ws.SubstitutePrefixedStrings(txt, `$`, -1)
	if ws.NumGoofs() > ngoofs {
		return f
	}
	return ws.ProcessText(txt, setField, delField)
}

func (ws *Workspace) NewProcess(txt, source string) io.Writer {
	ws.ClearOutput()
	ws.ClearGoofs()
	return ws.Process(txt, source)
}

// Process a single instruction, the front door into Workspace.
func (ws *Workspace) Process(txt, source string) io.Writer {
	txt = Normalise(txt)
	parts := strings.SplitN(txt, " ", 2)
	cmdLabel := strings.ToLower(TrimText(parts[0]))
	rem := ""
	if len(parts) > 1 {
		rem = parts[1]
	}

	cmd, exists := ws.Commands[cmdLabel]
	input := NewInput()
	input.Text = txt
	input.Source = source
	ws.currSource = source

	// One word command not recognised?, then try first two words...
	if !exists {
		parts = strings.SplitN(txt, " ", 3)
		if len(parts) > 1 {
			cmdLabel += " " + strings.ToLower(TrimText(parts[1]))
			cmd, exists = ws.Commands[cmdLabel]
			if len(parts) > 2 {
				rem = parts[2]
			}
		}
	}

	var outRedirect io.Writer
	if !exists {
		ws.evaluate(txt)
	} else {
		args := []string{}
		if len(rem) > 0 {
			args, _, _, _ = SplitOutsideDoubleQuotes(rem, " ", -1)
		}
		input.Args = args
		// Handle something like "> path" or ">> path" redirection of output
		if len(args) > 1 {
			path := args[len(args)-1]
			if path == ">" || path == ">>" {
				ws.MakeGoof("BAD_ARGUMENTS",
					"Missing redirection target path")
			} else {
				redirector := args[len(args)-2]
				if strings.HasPrefix(redirector, ">") {
					input.Args = input.Args[:len(input.Args)-2]
					_, path, _, _ := ws.IsSafePath(path)
					path = ws.SubstitutePrefixedStrings(path, `$`, -1)
					var err error
					if redirector == ">>" {
						outRedirect, err = os.OpenFile(
							path,
							os.O_RDWR|os.O_APPEND,
							0777)
					} else if redirector == ">" {
						outRedirect, err = os.Create(path)
					}
					ws.IsError(err)
				}
			}
		}
		cmd.Execute(input)
	}
	return outRedirect
}

func (ws *Workspace) Run(path, source string) {
	cmd := CmdRun{ws.MakeExecutableCommand("run"), 0}
	input := NewInput()
	input.Args = []string{path}
	input.Source = source
	cmd.Execute(input)
	return
}

// Determines if the given path is within the safe file tree defined by
// Workspace.Root().  If so, return includes a normalised, absolute path.
func (ws *Workspace) IsSafePath(path string) (bool, string, string, *Goof) {
	if len(ws.Root()) == 0 {
		return false, "", "", ws.MakeGoof("BAD_ARGUMENTS",
			"This is a virtual workspace, "+
				"filesystem is not accessible via path %q",
			path)
	}
	goof := NO_GOOF
	cwd, err := os.Getwd()
	if IsError(err) {
		goof = ws.WrapGoof("GETTING_CURRENT_DIRECTORY", err,
			"While interpolating the given path %q", path)
	}
	if strings.HasPrefix(path, "~") {
		path = strings.Replace(path, "~", ws.Root(), 1)
	} else if !filepath.IsAbs(path) {
		path = filepath.Join(cwd, path)
	}
	if strings.HasPrefix(path, ws.Root()) {
		return true, path, cwd, goof
	}
	return false, path, cwd, ws.MakeGoof("AUTHORISATION",
		"Path %q is not in authorised Workspace root %q",
		path, ws.Root())
}

// TODO
func (ws *Workspace) validateRhs(txt string) bool {
	return true
}

// Immediately evaluate using "=", delay (lazy) evaluation with ":="
func (ws *Workspace) evaluate(txt string) {
	// Assignment for user-defined object
	split := []string{}
	if strings.Contains(txt, ":=") {
		split, _, _, _ = SplitOutsideDoubleQuotes(txt, ":=", -1)
		if len(split) != 2 {
			ws.MakeGoof("PARSING_TEXT",
				"Could not split left and right sides of \":=\" in %q", txt)
			return
		}
		lhs := TrimText(split[0])
		rhs := TrimText(split[1])
		ngoofs := ws.NumGoofs()
		valid := ws.validateRhs(rhs)
		if ws.NumGoofs() > ngoofs {
			return
		}
		if !valid {
			ws.MakeGoof("PARSING_TEXT",
				"Right hand side is not a valid expression")
			return
		}
		ws.Set(lhs, Env.Types().BuildField(rhs, "WORKSPACE_FN"))
	} else if strings.Contains(txt, "=") {
		split, _, _, _ = SplitOutsideDoubleQuotes(txt, "=", -1)
		if len(split) != 2 {
			ws.MakeGoof("PARSING_TEXT",
				"Could not split left and right sides of \"=\" in %q", txt)
			return
		}

		lhs := strings.Trim(TrimText(split[0]), `"`)
		rhs := TrimText(split[1])

		// RHS
		ngoofs := ws.NumGoofs()
		vf := ws.processRhs(rhs)
		if ws.NumGoofs() > ngoofs {
			return
		}

		// LHS
		// examples of what we could get:
		// x = ...
		// x."field1" = ...
		// x. field1 = ...
		// x[0] = ...
		// x.mapfield1.mapfield2 = ...  NOT SUPPORTED
		// x.mapfield1[1].mapfield2[3] = ... NOT SUPPORTED
		// del x.field1 // deletes field in map
		//return SubstitutePrefixedStrings(arg, `$`, ws, -1)

		ws.SubstituteThenProcessText(lhs, vf, false)
		if ws.NumGoofs() > ngoofs {
			return
		}
	} else {
		msg := ""
		if ws.RunScriptLevel() > 0 {
			msg = ", perhaps a missing \";\"?"
		}
		ws.Problem("BAD_COMMAND", "Unrecognised input %q"+msg, txt)
	}
	return
}

func (ws *Workspace) DefaultRhsProcessor(rhs string) *Field {
	f := NewField()
	if strings.HasPrefix(rhs, "new") {
		// e.g. x = new(map)
		// x = new(list)
		rhs = strings.TrimPrefix(rhs, "new")
		rhs = strings.Trim(rhs, " ()'")
		switch rhs {
		case "map":
			f = Env.Types().AutoField(Env.Types().MintFieldMap())
		case "list":
			f = Env.Types().AutoField(Env.Types().MintFieldList())
		default:
			ws.MakeGoof("PARSING_TEXT",
				"Type %q unrecognised", rhs)
		}
	} else {
		f = ws.SubstituteThenProcessText(rhs, NewField(), false)
	}
	return f
}

// Entry point to allow one-time processing of txt before hand off to
// recursive processor.
func (ws *Workspace) ProcessText(txt string, setField *Field, delField bool) *Field {
	txt = Normalise(txt)
	return ws.processText(txt, setField, delField)
}

// Parsing of Json-like text to zappbase object, without dependence on quotes.
// Double quotes are protected literals, single quotes have no special meaning.
func (ws *Workspace) processText(txt string, setField *Field, delField bool) *Field {
	f0 := NewField()
	txt = TrimText(txt)
	if len(txt) == 0 {
		return f0
	}
	bstrs, isa := Env.Types().StringBrackets.Get(string(txt[0]))
	if isa {
		// Handling of Fields, FieldMaps, FieldLists etc.
		out, goof := bstrs.Decoder(txt,
			NewBox().Set(BOX_ITEM_WORKSPACE_ACTION,
				ws.MakeAction(setField, delField)))
		ws.IsError(goof)
		return out.GetDecoded()
	} else if txt[0] == '"' { // DOUBLE QUOTE LITERAL
		if txt[len(txt)-1] == '"' {
			txt = TrimText(txt[1 : len(txt)-1])
			if len(txt) == 0 {
				return f0
			}
			return MakeField(txt, Env.Types().Number["STRING"])
		} else {
			ws.MakeGoof("SYNTAX",
				"Missing closing \" while processing text %q", txt)
			return f0
		}
	} else { // SINGLETON, treat txt as bool, int64, float64 or string
		// Try int
		i64, goof := ParseInt(txt, 64, int64(math.MinInt64), int64(math.MaxInt64))
		if !IsError(goof) {
			return MakeField(i64, Env.Types().Number["int64"])
		}
		// Try bool
		b, goof := ParseBool(txt)
		if !IsError(goof) {
			return MakeBoolField(b)
		}
		// Try float
		f64, goof := ParseFloat(txt, 64, float64(-math.MaxFloat64), float64(math.MaxFloat64))
		if !IsError(goof) {
			return MakeField(f64, Env.Types().Number["float64"])
		}

		// x.label for *FieldMap and Node types
		txtndq := RemoveDoubleQuoted(txt, -1) //txtndq = txt not dbl quoted
		if strings.Count(txtndq, ".") == 1 {
			f := ws.processField(txt, ".", setField, delField)
			return f
		}
		// x:hidden for Node type
		if strings.Count(txtndq, ":") == 1 {
			// Hidden fields not processed in h2go.ProcessField
			f := ws.processField(txt, ":", setField, delField)
			return f
		}
		// x[y] for *FieldList type
		if strings.Count(txtndq, "[") == 1 {
			return ws.ProcessListIndex(txt, setField, delField)
		}

		// Try a reference to the workspace map
		if delField {
			wsf := ws.Get(txt)
			if !wsf.IsEmpty() {
				ws.Delete(txt)
				return wsf
			}
			return f0
		}

		if setField.IsEmpty() {
			// Getter
			// ... = x
			wsf := ws.Get(txt)
			if wsf.IsEmpty() {
				// Its just a boring old string
				return MakeField(txt, Env.Types().Number["STRING"])
			}
			return wsf
		}
		// Setter
		// x = ...
		ws.Set(Env.Types().AutoKey(txt), setField)
		return f0
	}
}

// Allows strings such as "x.label" to be processed, where x is a key in the
// given *FieldMap (and must itself be a *FieldMap), and label is a field label
// in x.  The splitter is usually a dot but is arbitrary here.  The function is
// a getter unless an empty setField is provided, in which case it becomes a
// setter, if possible.  Nested field references are not supported.
func (ws *Workspace) ProcessField(txt, splitter string, setField *Field, delField bool) *Field {
	errprefix := fmt.Sprintf("Cannot process %q, ", txt)
	f0 := NewField()
	sides, _, _, _ := SplitOutsideDoubleQuotes(txt, splitter, -1)
	if len(sides) != 2 {
		ws.MakeGoof("PARSING_TEXT",
			errprefix+"cannot parse nested fields")
		return f0
	}
	//    x  .  MyField = ...
	//    ^  ^     ^       ^
	//    |  |     |       |
	//   lhs |    rhs   setField
	//  (wsf)|    (f)
	//    splitter
	lhs := strings.TrimSpace(sides[0])
	if len(lhs) == 0 {
		ws.MakeGoof("BAD_ARGUMENTS",
			errprefix+"the left hand side of the %q is missing", splitter)
		return f0
	}
	wsf := ws.Get(lhs)
	if wsf.IsEmpty() {
		ws.MakeGoof("KEY_NOT_FOUND",
			"Key \"%v\" (%#v) not found", lhs, lhs)
		return f0
	}
	rhs := strings.TrimSpace(sides[1])
	// Just a getter
	ngoofs := ws.NumGoofs()
	f := ws.ProcessText(rhs, NewField(), false)
	if ws.NumGoofs() > ngoofs {
		return f0
	}
	if f.IsEmpty() {
		ws.MakeGoof("BAD_ARGUMENTS",
			errprefix+"field %q was interpreted as empty", rhs)
		return f0
	}

	if splitter == ":" {
		ws.Warn(
			errprefix + "splitting with : not currently recognised")
	}
	switch u := (wsf.Value()).(type) {
	case *FieldMap:
		if delField {
			u.Delete(*f)
			return f
		}
		if setField.IsEmpty() {
			// Get
			return u.Get(*f)
		} else {
			// Set
			u.Set(*f, setField)
			return MakeField(u, Env.Types().Number["FIELDMAP"])
		}
	default:
		ws.MakeGoof("BAD_TYPE",
			errprefix+"object type %T cannot be processed", u)
		return f0
	}
}

func (ws *Workspace) DecodeTypedString(code CODE, txt string) (Representation, *Goof) {
	typ, goof := Env.Types().ProtocolByCode(code)
	if IsError(goof) {
		return NewStringRepresentation(), goof
	}
	if typ.StringRxIsEmpty() {
		return NewStringRepresentation(), ws.MakeGoof("BAD_TYPE",
			"Cannot currently decode %q for type %q",
			txt, typ.Name)
	}
	return typ.StringRx(txt,
		NewBox().Set(BOX_ITEM_WORKSPACE_ACTION,
			ws.MakeAction(NewField(), false)))
}

func (ws *Workspace) ProcessListIndex(txt string, setField *Field, delField bool) *Field {
	f0 := NewField()
	errprefix := fmt.Sprintf("Cannot process %q, ", txt)
	sides, _, _, _ := SplitOutsideDoubleQuotes(txt, "[", -1)

	if len(sides) != 1 {
		ws.MakeGoof("SYNTAX",
			errprefix+"cannot parse multidimensional arrays")
		return f0
	}

	lhs := strings.TrimSpace(sides[0])
	if len(lhs) == 0 {
		ws.MakeGoof("BAD_ARGUMENTS",
			errprefix+"the left hand side of the [ is missing")
		return f0
	}
	wsf := ws.Get(lhs)
	if wsf.IsEmpty() {
		ws.MakeGoof("KEY_NOT_FOUND",
			"Key \"%v\" (%#v) not found", lhs, lhs)
		return f0
	}
	flist := wsf.AsFieldList()
	if !flist.IsEmpty() {
		ws.MakeGoof("BAD_TYPE",
			errprefix+"object %s is not a *FieldList", lhs)
		return f0
	}

	indstr := strings.TrimSpace(sides[1])
	if strings.HasSuffix(indstr, "]") {
		indstr = strings.Trim(indstr, " ]\"'")
		i64, goof := ParseInt(indstr, 64, int64(math.MinInt64), int64(math.MaxInt64))
		if IsError(goof) {
			ws.WrapGoof("BAD_ARGUMENTS", goof,
				errprefix+"cannot parse numerical index")
			return f0
		}
		i := int(i64)
		if delField {
			fold := flist.Get(i)
			goof = flist.Delete(i)
			if ws.IsError(goof) {
				return f0
			}
			ws.Set(Env.Types().AutoKey(lhs),
				MakeField(flist, Env.Types().Number["FIELDLIST"]))
			return fold
		}
		if flist.Len() >= i {
			flist.ToSlice()[i] = setField
		} else {
			// Fill all items between flist.Len() and i with empty Fields
			for j := flist.Len(); j <= i-1; j++ {
				flist = flist.Add(f0)
			}
			flist = flist.Add(setField)
			// Unlike *FieldMap, need to refresh a *FieldList object
			ws.Set(Env.Types().AutoKey(lhs),
				MakeField(flist, Env.Types().Number["FIELDLIST"]))
		}
	}
	ws.MakeGoof("SYNTAX", errprefix+"missing ]")
	return f0
}

// SUBSTITUTION ================================================================

// Using the given *FieldMap, replaces all occurrences of string labels,
// delimited by the given bracket characters (which must differ), with the
// string representation of the field to which they map.  Can protect double
// quotes.
// For example, with left and right brackets "\" and "/", and a map containing
// "x" -> Field{32, UINT32}, the input "\x/" would be result in "32".
func (ws *Workspace) SubstituteBracketedStrings(txt, left, right string, limit int, protect bool) string {
	if left == right {
		ws.MakeGoof("SYNTAX",
			"Left %q and right %q bracket characters must differ",
			left, right)
		return ""
	}
	txt = strings.TrimSpace(txt)
	labels, endpoints, _ :=
		ParseDifferingBrackets(txt, left, right, limit, protect)
	return ws.SubstituteStrings(txt, labels, endpoints)
}

func (ws *Workspace) SubstitutePrefixedStrings(txt, prefix string, limit int) string {
	txt = strings.TrimSpace(txt)
	labels, endpoints, _ := ParsePrefixedLabels(txt, prefix, ws.lang, limit)
	return ws.SubstituteStrings(txt, labels, endpoints)
}

func (ws *Workspace) SubstituteStrings(txt string, labels []string, endpoints [][]int) string {
	wsf := NewField()
	replacement := ""
	preend := 0
	poststart := 0
	shift := 0
	for i, label := range labels {
		label = TrimText(label)
		ngoofs := ws.NumGoofs()
		wsf = ws.ProcessSubstitution(label)
		if ws.NumGoofs() > ngoofs {
			return txt
		}
		if !wsf.IsEmpty() {
			// The length of txt changes, and we must shift the pre-calculated
			// endpoints of substitutions in an accumulating fashion
			replacement = wsf.String()
			preend = endpoints[i][0] + shift
			poststart = endpoints[i][1] + 1 + shift
			txt = txt[:preend] + replacement + txt[poststart:]
			shift += len(replacement) - len(label) - 2 // 2 accts for brackets
		} else {
			ws.MakeGoof("VALUE_NOT_FOUND",
				"Value %#q not found", label)
			return txt
		}
	}
	return txt
}

// With the label extracted, retrieve the associated Workspace field.
func (ws *Workspace) ProcessSubstitution(label string) *Field {
	f0 := NewField()
	wsf := NewField()
	ngoofs := 0
	if strings.Count(label, ".") == 1 {
		ngoofs = ws.NumGoofs()
		wsf = ws.processField(label, ".", f0, false)
		if ws.NumGoofs() > ngoofs {
			return wsf
		}
	} else if strings.Count(label, ":") == 1 {
		// Hidden fields not processed in h2go.ProcessField
		ngoofs = ws.NumGoofs()
		wsf = ws.processField(label, ":", f0, false)
		if ws.NumGoofs() > ngoofs {
			return wsf
		}
	} else if strings.Count(label, "[") == 1 {
		ngoofs = ws.NumGoofs()
		wsf = ws.ProcessListIndex(label, f0, false)
		if ws.NumGoofs() > ngoofs {
			return wsf
		}
	} else {
		wsf = ws.Get(label)
	}
	return wsf
}

// TAB COMPLETION ==============================================================

// Unlike linux bash shell, this tab completer only works on the end of the
// line.
func (ws *Workspace) TabCompleter(line string) []string {
	result := []string{}
	// Do this loop first to pick up commands containing space characters
	for name, _ := range ws.Commands {
		if strings.HasPrefix(name, line) {
			// Partial match of first word
			result = append(result, name)
		}
	}
	if len(result) == 0 {
		for name, cmd := range ws.Commands {
			if strings.HasPrefix(line, name) {
				// Full match with possible arguments
				rem := TrimText(line[len(name):])
				args, _, _, _ := SplitOutsideDoubleQuotes(rem, " ", -1)
				return cmd.TabCompleter(line, args)
			}
		}
	}
	return result
}

func (cmd *ExecutableCommand) TabCompleterPaths(path0, abspath, cwd string, filter func(fpath string, info os.FileInfo, level int) bool) []string {
	result := []string{}
	info, err := os.Stat(abspath)
	if IsError(err) && !os.IsNotExist(err) {
		cmd.Wspace.WrapGoof("TAB_COMPLETION", err,
			"While getting info about given absolute path %q", abspath)
		return result
	}
	rootdir := abspath
	isdir := false
	if info != nil {
		isdir = info.IsDir()
	}
	if info == nil || !isdir {
		rootdir = filepath.Dir(abspath)
	}
	paths, goof := FindPaths(rootdir, filter, 1)
	if IsError(goof) {
		cmd.Wspace.WrapGoof("TAB_COMPLETION", goof,
			"During console %q tab completion", cmd.Label)
	}
	respath := ""
	for _, path := range paths {
		if !filepath.IsAbs(path0) && isdir {
			respath = filepath.Join(path0, filepath.Base(path))
		} else {
			respath = filepath.Base(path)
		}
		result = append(result, respath)
	}
	if isdir && len(path0) > 0 {
		result = append(result, path0)
	}
	return result
}

func (cmd *ExecutableCommand) DirCompleter(args []string) []string {
	result := []string{}
	path := ""
	if len(args) > 0 {
		path = args[len(args)-1]
	}
	_, abspath, cwd, goof := cmd.Wspace.IsSafePath(path)
	if IsError(goof) {
		return result
	}
	result = cmd.TabCompleterPaths(path, abspath, cwd,
		func(fpath string, info os.FileInfo, level int) bool {
			return info.IsDir() &&
				level > 0 &&
				strings.HasPrefix(fpath, abspath)
		})
	prefix := cmd.RebuildLine(args, 1)
	for i, _ := range result {
		result[i] = prefix + " " + result[i]
	}
	return result
}

func (cmd *ExecutableCommand) FileCompleter(args []string) []string {
	result := []string{}
	path := ""
	if len(args) > 0 {
		path = args[len(args)-1]
	}
	_, abspath, cwd, goof := cmd.Wspace.IsSafePath(path)
	if IsError(goof) {
		return result
	}
	result = cmd.TabCompleterPaths(path, abspath, cwd,
		func(fpath string, info os.FileInfo, level int) bool {
			return !info.IsDir() &&
				level > 0 &&
				strings.HasPrefix(fpath, abspath)
		})
	prefix := cmd.RebuildLine(args, 1)
	for i, _ := range result {
		result[i] = prefix + " " + result[i]
	}
	return result
}

// Completes tabbed console input which allows a "as <user>" and "with alias
// <alias>" component
func (cmd *ExecutableCommand) ConnectTabCompleter(args []string) []string {
	result := []string{}
	last := len(args) - 1
	i := last
	for {
		if i == last && strings.HasPrefix("alias", args[i]) {
			if i-1 >= 0 && args[i-1] == "with" {
				result = []string{cmd.RebuildLine(args, 1) + " alias"}
			}
		} else if i == last && strings.HasPrefix("with", args[i]) {
			if i-1 >= 0 && args[i-1] != "as" && args[i-1] != "alias" {
				if args[i] == "with" {
					result = []string{cmd.RebuildLine(args, 0) + " alias"}
				} else {
					result = []string{cmd.RebuildLine(args, 1) + " with"}
				}
			}
		} else if i == last && strings.HasPrefix("as", args[i]) {
			if i-1 >= 0 && args[i-1] != "with" && args[i-1] != "alias" {
				result = []string{cmd.RebuildLine(args, 1) + " as"}
			}
		} else if i-1 >= 0 && args[i-1] != "with" && args[i-1] != "alias" {
			result = []string{cmd.RebuildLine(args, 0) + " with"}
		} else if i == 0 {
			break
		}
		i--
	}
	return result
}

func (cmd *ExecutableCommand) RebuildLine(args []string, fromEnd int) string {
	line := cmd.Label
	if fromEnd <= len(args)-1 {
		for j := 0; j <= len(args)-1-fromEnd; j++ {
			line += " " + args[j]
		}
	}
	return line
}

func (cmd *ExecutableCommand) RedirectCompleter(args []string) []string {
	result := []string{}
	if len(args) > 0 {
		path := args[len(args)-1]
		if path == ">" {
			result = []string{cmd.RebuildLine(args, 1) + " " + ">>"}
		} else if path == ">>" {
			args = append(args, "")
			result = cmd.FileCompleter(args)
		} else if len(args) > 1 {
			redirector := args[len(args)-2]
			if strings.HasPrefix(redirector, ">") {
				result = cmd.FileCompleter(args)
			}
		}
	}
	return result
}

// Argument Template:
// A a1 a2
// B b1 b2 b3
// C c1
// D
// Scenarios:
// cmd A a2 B => cmd A a2 B [b1 b2 b3]
// cmd A a2 B b => cmd A a2 B [b1 b2 b3]
// cmd A a2 => cmd A a2 [B C D]
// cmd D B => cmd D B [b1 b2 b3]
// cmd C c1 A a2 B => cmd C c1 A a2 B [b1 b2 b3]
func (cmd *ExecutableCommand) ArgTemplateTabCompleter(line string, args, result []string) []string {
	if len(args) == 0 {
		return result
	}
	// Catalogue existing arg keywords
	existingKeywords := []string{}
	newLine := cmd.Label
	L0 := len(args) - 1
	// We assume primary (top level) keywords are all distinct from
	// secondary keywords.
	for i, arg := range args {
		for k1, _ := range cmd.Template {
			if arg == k1 {
				existingKeywords = append(existingKeywords, arg)
			}
		}
		if i != L0 {
			newLine += " " + arg
		}
	}
	primaryKeywordMatch := false
Primary:
	for k1, _ := range cmd.Template {
		if k1 == args[L0] {
			// Full match
			newLine += " " + args[L0]
			primaryKeywordMatch = true
			for k1_2, _ := range cmd.Template {
				if k1_2 != k1 && !StringIsIn(k1_2, existingKeywords) {
					result = append(result, newLine+" "+k1_2)
				}
			}
			break Primary
		} else if strings.HasPrefix(k1, args[L0]) {
			// Partial match
			if !StringIsIn(k1, existingKeywords) {
				result = append(result, newLine+" "+k1)
				primaryKeywordMatch = true
			}
		}
	}
	if !primaryKeywordMatch && len(args) > 1 {
		L1 := L0 - 1 // second last
		for k1, opts := range cmd.Template {
			if len(opts) > 0 {
				if _, hasKeywords := opts["[keywords]"]; hasKeywords {
					for typ, k2s := range opts {
						if typ == "[keywords]" {
							for _, k2 := range k2s {
								if k2 == args[L0] {
									// Full match
									if args[L1] == k1 {
										for k1_2, _ := range cmd.Template {
											if k1_2 != k1 && !StringIsIn(k1_2, existingKeywords) {
												result = append(result, newLine+" "+k1_2)
												break
											}
										}
									}
								} else if strings.HasPrefix(k2, args[L0]) {
									// Partial match
									if args[L1] == k1 {
										result = append(result, newLine+" "+k2)
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return result
}
