/*
	Containers for byte encodable typed values.
*/
package h2go

import (
	"fmt"
	"math/big"
	"reflect"
	"strings"
)

type TypedValue interface {
	Value() interface{}
	Type() CODE
}

type KeyVal struct {
	Key   Field
	Value *Field
}

func NewKeyVal() *KeyVal {
	return &KeyVal{
		Key:   *NewField(),
		Value: NewField(),
	}
}

func (types *TypeMap) MakeKeyVal(k, v interface{}) *KeyVal {
	kv := NewKeyVal()
	kv.Key = *types.AutoField(k)
	kv.Value = types.AutoField(v)
	return kv
}

// FIELD =======================================================================

// Cannot embed State pointer because Field is also used as a map key
type Field struct {
	val interface{}
	typ CODE
}

var _ Binaryable = NewField()
var _ Comparable = NewField()
var _ Equible = NewField()
var _ Stringable = NewField()
var _ TypedValue = NewField()
var _ Codified = NewField()

func (f Field) Value() interface{} { return f.val }
func (f Field) Type() CODE         { return f.typ }
func (f *Field) AsKey() Field      { return *f }

// Constructors
func NewField() *Field {
	return &Field{
		val: nil,
		typ: NIL_TYPE_CODE,
	}
}

func MakeField(val interface{}, typ CODE) *Field {
	switch v := val.(type) {
	case int:
		val = int64(v)
	case uint:
		val = uint64(v)
	}
	f := NewField()
	f.val = val
	f.typ = typ
	return f
}

func (f Field) Copy() *Field {
	f2 := NewField()
	f2.val = f.val
	f2.typ = f.typ
	return f2
}

// Only make a new *Field, if val is not already a *Field.
func WrapField(val interface{}, typ CODE) *Field {
	switch v := val.(type) {
	case *Field:
		return v
	case Field:
		return &v
	default:
		return MakeField(val, typ)
	}
}

func (types *TypeMap) MintField(code CODE) (*Field, *Goof) {
	nilobj, goof := types.NilByCode(code)
	if IsError(goof) {
		return NewField(), goof
	}
	return MakeField(nilobj, code), NO_GOOF
}

// Automatically detect Go primitive or Codified type and create a *Field.
func (types *TypeMap) AutoField(val interface{}) *Field {
	// Don't permit field wrapping
	fp, ok1 := val.(*Field)
	if ok1 {
		return fp
	}
	f, ok2 := val.(Field)
	if ok2 {
		return &f
	}
	return MakeField(val, types.GetType(val))
}

func (types *TypeMap) AutoKey(val interface{}) Field {
	return *types.AutoField(val)
}

func (types *TypeMap) BuildField(val interface{}, typname string) *Field {
	return MakeField(val, types.Number[typname])
}

func (types *TypeMap) BuildKeyVal(key interface{}, keytyp string, val interface{}, valtyp string) (Field, *Field) {
	return *types.BuildField(key, keytyp), types.BuildField(val, valtyp)
}

func (types *TypeMap) AutoKeyVal(key, val interface{}) (Field, *Field) {
	return *types.AutoField(key), types.AutoField(val)
}

func MakeStringField(s string) *Field {
	return MakeField(s, Env.Types().Number["STRING"])
}

func MakeBoolField(b bool) *Field {
	if b {
		return MakeField(true, Env.Types().Number["TRUE"])
	}
	return MakeField(false, Env.Types().Number["FALSE"])
}

func (f Field) Code() CODE {
	return FIELD_TYPE_CODE
}

func (f Field) IsEmpty() bool {
	return f.val == nil && f.typ == NIL_TYPE_CODE
}

func (f Field) Equals(other interface{}) bool {
	f2 := NewField()
	switch v := other.(type) {
	case Field:
		f2 = &v
	case *Field:
		f2 = v
	default:
		f2 = MakeField(other, Env.Types().GetType(other))
	}
	if f.Type() != f2.Type() {
		return false
	}
	/*
		if f.Value() == nil && f2.Value() == nil {
			return true
		} else if (f.Value() == nil && f2.Value() != nil) ||
			(f.Value() != nil && f2.Value() == nil) {
			return false
		}
	*/
	v1 := reflect.Indirect(reflect.ValueOf(f.Value()))
	v2 := reflect.Indirect(reflect.ValueOf(f2.Value()))
	return reflect.DeepEqual(v1.Interface(), v2.Interface())
}

// Satisfy h2go.Comparable interface
func (f Field) Cmp(other Comparable) int {
	f2, ok := other.(*Field)
	result := 0
	if !ok || f2.Type() != f.Type() {
		Env.Log().IsError(Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"When comparing %T %v to other object %v of type %T",
			f, f, other, other))
		result = COMPARE_ERR
	} else {
		result = Compare(f.Value(), f2.Value())
	}
	return result
}

func (f Field) String() string {
	return f.StringUsing(Env.Types())
}

type FieldCollection interface {
	Binaryable
	Equible
	MultilineStringable
	Stateful
	Stringable
}

// KEYSTRING ===================================================================
type KeyString struct {
	Key Field
	Str string
}

var _ Comparable = NewKeyString()

func NewKeyString() KeyString {
	return KeyString{
		Key: *NewField(),
	}
}

func MakeKeyString(k Field) KeyString {
	ks := NewKeyString()
	ks.Key = k
	ks.Str = k.String()
	return ks
}

func (ks KeyString) Cmp(other Comparable) int {
	ks2, ok := other.(KeyString)
	result := 0
	if !ok {
		Env.Log().IsError(Env.Goofs().MakeGoof("BAD_ARGUMENTS",
			"When comparing %T %v to other object %v of type %T",
			ks, ks, other, other))
		result = COMPARE_ERR
	} else {
		result = CmpString(ks.Str, ks2.Str)
	}
	return result
}
func (ks KeyString) String() string {
	return ks.Str
}

/*
func (f Field) IsNil() bool {
	return f.Type().IsNil()
}
*/

func (f Field) IsUnknown() bool {
	return f.Type().IsUnknown()
}

func (f *Field) EncodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	typ, goof := Env.Types().ProtocolByCode(f.typ)
	if IsError(goof) {
		return NewBinaryRepresentation(), goof
	}
	return typ.BinaryTx(bfr, box.Set(BOX_ITEM_ENCODE, f.Value()))
}

// Field binary decoder. Assumes code has been read.
// When binary.ReadBinary decodes a derived type such as
//
// type MYINT int64
//
// it will return the underlying type. We need to cast it back
// up to the original type.
func (f *Field) DecodeBinary(bfr *Buffer, box *Box) (Representation, *Goof) {
	typ, goof := Env.Types().ProtocolByCode(f.typ)
	if IsError(goof) {
		return NewBinaryRepresentation(), goof
	}
	out, goof := typ.BinaryRx(bfr, box)
	if IsError(goof) {
		return out, goof
	}
	actual_typ := reflect.TypeOf(out.GetDecoded().Value())
	nilobj, goof := Env.Types().NilByCode(f.typ)
	if IsError(goof) {
		return out, goof
	}
	reqd_typ := reflect.TypeOf(nilobj)
	if actual_typ != reqd_typ {
		f2, isa := nilobj.(Field)
		if isa {
			reqd_typ = reflect.TypeOf(f2.val)
			if actual_typ != reqd_typ {
				out.SetField(MakeField(
					reflect.ValueOf(f.val).Convert(reqd_typ).Interface(),
					f.typ))
			}
		}
	}
	f.val = out.GetDecoded().val
	return out, NO_GOOF
}

// Generate Field binary encoder.
func FieldBinaryEncoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		inside, goof := box.Require(BOX_ITEM_FIELD)
		if IsError(goof) {
			return MakeBinaryRepresentation(Env.RecordEncoding()),
				goof
		}
		return inside.(*Field).EncodeBinary(bfr, box)
	}
}

// Generate Field binary decoder.
func FieldBinaryDecoder() BinaryTask {
	return func(bfr *Buffer, box *Box) (Representation, *Goof) {
		return Env.Types().BuildField(NewField(), "FIELD").DecodeBinary(bfr, box)
	}
}

func (f Field) StringUsing(types *TypeMap) string {
	typ, goof := types.ProtocolByCode(f.Type())
	if IsError(goof) {
		return "(" + goof.Msg + ")"
	}

	if f.Type() == UNKNOWN_TYPE_CODE {
		return fmt.Sprintf("(UNKNOWN TYPE %T)", f.Value())
	} else if f.Type() == NIL_TYPE_CODE {
		return "(NIL TYPE)"
	}

	fmapCode, fmapExists := types.Number["FIELDMAP"]
	flistCode, flistExists := types.Number["FIELDLIST"]

	if IsString(typ.Nil) {
		if typ.Name == "STRING" {
			return fmt.Sprintf("%q", f.Value())
		}
		return fmt.Sprintf("(%q,%s)", f.Value(), typ.Name)
	} else if typ.IsBoolean() {
		return fmt.Sprintf("%s", typ.Name)
	} else if (fmapExists && f.Type() == fmapCode) || (flistExists && f.Type() == flistCode) {
		return fmt.Sprintf("%s", f.Value())
	} else {
		return fmt.Sprintf("(%v,%s)", f.Value(), typ.Name)
	}
}

func (f Field) TypedString(types *TypeMap) string {
	return fmt.Sprintf("(%v,%s)", f.Value(), types.Name(f.Type()))
}

func (f Field) EncodeString(box *Box) (Representation, *Goof) {
	out := MakeStringRepresentation(Env.RecordEncoding())
	out.AppendEncoded(f.StringUsing(Env.Types()))
	return out, NO_GOOF
}

func (f *Field) DecodeString(txt string, box *Box) (Representation, *Goof) {
	out := MakeBinaryRepresentation(Env.RecordEncoding())
	out.AutoSetField(f)
	out.AppendEncoded(txt)
	inside, goof := box.Require(BOX_ITEM_WORKSPACE_ACTION)
	if IsError(goof) {
		return out, goof
	}
	wsa := inside.(*WorkspaceAction)
	if string(txt[len(txt)-1]) == ")" {
		txt = TrimText(txt[1 : len(txt)-1])
		if len(txt) == 0 {
			return out, NO_GOOF
		}
		lastat := strings.LastIndex(txt, `,`)
		// Multiple commas possible (e.g. list, map)
		if lastat == -1 {
			return out, Env.Goofs().MakeGoof("SYNTAX",
				"Missing value before comma while processing text %s", txt)
		}
		if lastat == len(txt)-1 {
			return out, Env.Goofs().MakeGoof("SYNTAX",
				"Missing type after comma while processing text %s", txt)
		}
		vstr := TrimText(txt[:lastat])
		tstr := TrimText(txt[lastat+1:])
		typ, ok := Env.Types().Number[tstr]
		if !ok {
			return out, Env.Goofs().MakeGoof("BAD_TYPE",
				"Type %q not recognised while processing text", tstr, txt)
		}
		out2, goof := wsa.DecodeTypedString(typ, vstr)
		if out.IsRecording() {
			out.ChooseEncoding("value")
			out.AppendEncoded(vstr)
			out.ChooseEncoding("type")
			out.AppendEncoded(tstr)
		}
		if IsError(goof) {
			return out, Env.Goofs().WrapGoof("PARSING_TEXT", goof,
				"While converting %q to object of type %q",
				vstr, Env.Types().Name(typ))
		}
		f = out2.GetDecoded()
		out.SetField(f)
		if wsa.SetField.IsEmpty() {
			// Getter
			return out, NO_GOOF
		}
		// Setter
		// (x, zbtype) = ...
		if TypeIsAllowableKey(typ) {
			wsa.Set(out.GetDecoded().AsKey(), wsa.SetField)
			return out, NO_GOOF
		}
		return out, Env.Goofs().MakeGoof("INVALID_KEY",
			"%d is not currently a valid key type", typ)
	} else {
		return out, Env.Goofs().MakeGoof("SYNTAX",
			"Missing closing ) while processing text %s", txt)
	}
}

// Generate Field string decoder.
func FieldStringDecoder() StringTask {
	return func(txt string, box *Box) (Representation, *Goof) {
		return Env.Types().BuildField(NewField(), "FIELD").DecodeString(txt, box)
	}
}

// Type asserters
func (f Field) AsBytes() []byte {
	result, _ := (f.Value()).([]byte)
	if f.IsEmpty() || result == nil {
		return []byte{}
	}
	return result
}

func (f Field) AsBoolean() bool {
	if f.IsEmpty() {
		return false
	}
	if f.Type() == Env.Types().Number["TRUE"] {
		return true
	}
	return false
}

func (f Field) AsUint8() uint8 {
	if f.IsEmpty() {
		return 0
	}
	result, _ := (f.Value()).(uint8)
	return result
}

func (f Field) AsUint16() uint16 {
	if f.IsEmpty() {
		return 0
	}
	result, _ := (f.Value()).(uint16)
	return result
}

func (f Field) AsUint32() uint32 {
	if f.IsEmpty() {
		return 0
	}
	result, _ := (f.Value()).(uint32)
	return result
}

func (f Field) AsUint64() uint64 {
	if f.IsEmpty() {
		return 0
	}
	result, _ := (f.Value()).(uint64)
	return result
}

func (f Field) AsUint() uint {
	if f.IsEmpty() {
		return 0
	}
	switch uv := (f.Value()).(type) {
	case uint8:
		return uint(uv)
	case uint16:
		return uint(uv)
	case uint32:
		return uint(uv)
	case uint64:
		return uint(uv)
	default:
		return 0
	}
}

func (f Field) AsInt8() int8 {
	if f.IsEmpty() {
		return 0
	}
	result, _ := (f.Value()).(int8)
	return result
}

func (f Field) AsInt16() int16 {
	if f.IsEmpty() {
		return 0
	}
	result, _ := (f.Value()).(int16)
	return result
}

func (f Field) AsInt32() int32 {
	if f.IsEmpty() {
		return 0
	}
	result, _ := (f.Value()).(int32)
	return result
}

func (f Field) AsInt64() int64 {
	if f.IsEmpty() {
		return 0
	}
	result, _ := (f.Value()).(int64)
	return result
}

func (f Field) AsInt() int {
	if f.IsEmpty() {
		return 0
	}
	switch uv := (f.Value()).(type) {
	case int8:
		return int(uv)
	case int16:
		return int(uv)
	case int32:
		return int(uv)
	case int64:
		return int(uv)
	default:
		return 0
	}
}

func (f Field) AsBigInt() *big.Int {
	result, _ := (f.Value()).(*big.Int)
	if f.IsEmpty() || result == nil {
		return (&big.Int{}).SetInt64(0)
	}
	return result
}

func (f Field) AsBigRat() *big.Rat {
	result, _ := (f.Value()).(*big.Rat)
	if f.IsEmpty() || result == nil {
		return (&big.Rat{}).SetFloat64(0.0)
	}
	return result
}

func (f Field) AsFloat32() float32 {
	if f.IsEmpty() {
		return 0.0
	}
	result, _ := (f.Value()).(float32)
	return result
}

func (f Field) AsFloat64() float64 {
	if f.IsEmpty() {
		return 0.0
	}
	result, _ := (f.Value()).(float64)
	return result
}

func (f Field) AsString() string {
	if f.IsEmpty() {
		return ""
	}
	result, _ := (f.Value()).(string)
	return result
}

func (f Field) AsFieldMap() *FieldMap {
	result, _ := (f.Value()).(*FieldMap)
	if f.IsEmpty() || result == nil {
		return NewFieldMap()
	}
	return result
}

func (f Field) AsFieldList() *FieldList {
	result, _ := (f.Value()).(*FieldList)
	if f.IsEmpty() || result == nil {
		return NewFieldList()
	}
	return result
}
