package h2go

import (
	"testing"
	//"fmt"
)

func TestNewType(t *testing.T) {
	typeName := "TEST_TYPE"
	types := Env.Types().Copy()
	goof := types.Add(
		func(code CODE) Protocol {
			return Protocol{
				Name:     typeName,
				Nil:      TestType{},
				BinaryTx: CodeOnlyBinaryEncoder(128),
				BinaryRx: ConstBinaryDecoder(TestType{}),
			}
		})
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem adding test protocol")
	}
	protocol, goof := types.ProtocolByName(typeName)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem accessing test protocol")
	}
	if protocol.Code.IsUnknown() {
		t.Fatalf("%s was not correctly added to TypeMap, code is %d",
			typeName, protocol.Code)
	}
}

func TestStringBinaryEncoding(t *testing.T) {
	Env.Log().MinorSection("Binary encode and decode a STRING type")
	msg := "This is a test."
	bfr := MakeByteBuffer(BYTEORDER)
	out, goof := StringBinaryEncoder(bfr, msg)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem encoding %q", msg)
	}
	out2, goof := StringBinaryDecoder(bfr)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decoding %q", msg)
	}
	msg2, isa := out2.GetDecoded().Value().(string)
	if !isa || msg2 != msg {
		t.Fatalf("Expected %q, got %v of type %T",
			msg, out2.GetDecoded().Value(), out2.GetDecoded().Value())
	}
	if !out.Equals(out2) {
		Env.Log().Dump(out.EncodingToStringSlice(100), "Encoding")
		Env.Log().Dump(out2.EncodingToStringSlice(100), "Decoding of Encoding")
		t.Fatalf("The encodings do not match")
	}
}

func TestUINTBinaryEncoding(t *testing.T) {
	Env.Log().MinorSection("Binary encode and decode an INT32 type")
	ui := UINT32(3233)
	bfr := MakeByteBuffer(BYTEORDER)
	out, goof := ui.EncodeBinary(bfr, EMPTY_BOX)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem encoding %T %d", ui, ui)
	}
	out2, goof := ui.DecodeBinary(bfr, EMPTY_BOX)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decoding %T %d", ui, ui)
	}
	ui2, isa := out2.GetDecoded().Value().(UINT32)
	if !isa || ui2 != ui {
		t.Fatalf("Expected %d of type %T, got %v of type %T",
			ui, ui, out2.GetDecoded().Value(), out2.GetDecoded().Value())
	}
	if !out.Equals(out2) {
		Env.Log().Dump(out.EncodingToStringSlice(100), "Encoding")
		Env.Log().Dump(out2.EncodingToStringSlice(100), "Decoding of Encoding")
		t.Fatalf("The encodings do not match")
	}
}

func TestSwitchesEncoding(t *testing.T) {
	Env.Log().MajorSection("Encode and decode a %T", NewSwitches())
	box := EmptyBox()
	sw, goof := MakeSwitches(map[string]bool{
		"Z": true,
		"G": false,
		"F": false,
		"H": false,
		"S": false,
		"B": true,
		"K": false,
		"P": false,
		"Q": false,
		"A": true,
	})
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making %T", sw)
	}
	expected := 3
	if sw.NumberEncodedBytes() != expected {
		t.Fatalf("Expected number of encoded bytes to be %d, got %d",
			expected, sw.NumberEncodedBytes())
	}
	bfr := MakeByteBuffer(BYTEORDER)
	_, goof = sw.EncodeBinary(bfr, box)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem encoding %T", sw)
	}
	sw2 := sw.Copy()
	_, goof = sw2.DecodeBinary(bfr, box)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem decoding %T", sw)
	}

	if !sw.Equals(sw2) {
		t.Fatalf("Expected %T %v to equal %v",
			sw, sw2, sw)
	}
}

func TestSwitchesFromStruct(t *testing.T) {
	Env.Log().MajorSection("Make a %T from a struct", NewSwitches())
	type TEST_CONFIG struct {
		FIRST  bool
		SECOND int
		THIRD  bool
		FOURTH bool
	}
	st := TEST_CONFIG{
		FIRST:  true,
		SECOND: 56,
		THIRD:  false,
		FOURTH: true,
	}

	expected, goof := MakeSwitches(map[string]bool{
		"FIRST":  true,
		"THIRD":  false,
		"FOURTH": true,
	})

	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making %T", expected)
	}

	sw, goof := MakeSwitchesFromStruct(&st)
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making %T", sw)
	}

	if !sw.Equals(expected) {
		t.Fatalf("Expected %T %v to equal %v",
			sw, sw, expected)
	}
}

func TestEnumerateSwitches(t *testing.T) {
	sw, goof := MakeSwitches(map[string]bool{
		"A": false,
		"B": false,
		"C": false,
		"D": false,
	})
	if Env.Log().IsError(goof) {
		t.Fatalf("Problem making %T", sw)
	}

	sws := sw.Enumerate()
	expected := 16
	if len(sws) != expected {
		t.Fatalf("Expected %d enumerations, got %d", expected, len(sws))
	}
}

// SUPPORT =====================================================================

type TestType struct{}

func (tt TestType) Code() CODE { return 128 }
